using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using Photon;
using DG.Tweening;
using UnityEngine.SceneManagement;
using ExitGames.Client.Photon;
using UnityEngine;
using System;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    public static NetworkManager instance;
    public RaiseEventOptions raiseEvent = new RaiseEventOptions
    {
        Receivers = ReceiverGroup.All,
        CachingOption = EventCaching.AddToRoomCacheGlobal
    };
     public RaiseEventOptions otherraiseEvent = new RaiseEventOptions
    {
        Receivers = ReceiverGroup.Others,
        CachingOption = EventCaching.DoNotCache
    };
    public RaiseEventOptions raiseEventNoCache = new RaiseEventOptions
    {
        Receivers = ReceiverGroup.All,
        CachingOption = EventCaching.DoNotCache
    };
    public List<RoomInfo> myroomlist = new List<RoomInfo>();
    UI_PVPSelect uI_PVPSelect;
    public int NextPlayerTeam;
    //public bool reconnect;
    
    private void Awake()
    {
        uI_PVPSelect = FindObjectOfType<UI_PVPSelect>();
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
        PhotonNetwork.SendRate = 60;
        PhotonNetwork.SerializationRate = 30;
        
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnEvent(EventData obj)
    {
        if (obj.Code == 37)
        {
            uI_PVPSelect.trueleftroom = true;
            object[] data = new object[]
            {
                (int)PhotonNetwork.LocalPlayer.CustomProperties["Team"],
                1
            };
            PhotonNetwork.RaiseEvent(40, null, NetworkManager.instance.otherraiseEvent, SendOptions.SendReliable);
            PhotonNetwork.RaiseEvent(3, null, NetworkManager.instance.raiseEvent, SendOptions.SendReliable);
            PhotonNetwork.RaiseEvent(7, null, NetworkManager.instance.raiseEvent, SendOptions.SendReliable);
            PhotonNetwork.RaiseEvent(0, data, NetworkManager.instance.raiseEvent, SendOptions.SendReliable);
        }
    }
    private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        uI_PVPSelect = FindObjectOfType<UI_PVPSelect>();
        myroomlist.Clear();
    }

    public void UpdateTeam()
    {
        if (NextPlayerTeam == 1)
        {
            NextPlayerTeam = 2;
        }
        else
            NextPlayerTeam = 1;
    }
    public void Connect() => PhotonNetwork.ConnectUsingSettings();
    public void DisConnect()
    {
        if (PhotonNetwork.IsConnected)
        {
            if (PhotonNetwork.InLobby)
                PhotonNetwork.LeaveLobby();
            else if (PhotonNetwork.InRoom)
            {
                if(uI_PVPSelect != null)
                    uI_PVPSelect.Exit_Ready();

            }
            PhotonNetwork.Disconnect();
            

        }
    }

    public override void OnConnectedToMaster()
    {
        if (PhotonNetwork.NetworkingClient.State != ClientState.JoiningLobby)
        {
            PhotonNetwork.JoinLobby();
        }
    }
    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        switch(returnCode)
        {
            case 2:
        uI_PVPSelect.EnableErrorRoom("방 인원이 꽉 찼습니다.");
                break;
            case 4:
        uI_PVPSelect.EnableErrorRoom("이미 게임이 시작되었습니다.");
                break;
        }
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        int roomCount = roomList.Count;
        for (int i = 0; i < roomCount; i++)
        {
            if (!roomList[i].RemovedFromList)
            {
                if (!myroomlist.Contains(roomList[i]))
                {
                    if(myroomlist.Count > 0 && myroomlist[i].Name != roomList[i].Name || myroomlist.Count == 0)
                    {
                    myroomlist.Add(roomList[i]);
                    uI_PVPSelect.Map.Add(100);
                    if (uI_PVPSelect != null)
                    {
                        uI_PVPSelect.CreateRoomBtn(roomList[i]);
                    }

                    }
                }
                else
                {
                    myroomlist[myroomlist.IndexOf(roomList[i])] = roomList[i];
                }
            }
            else if (myroomlist.IndexOf(roomList[i]) != -1)
            {
                if (uI_PVPSelect != null)
                {
                    uI_PVPSelect.RemoveRoomBtn(i);
                }
                uI_PVPSelect.Map.RemoveAt(myroomlist.IndexOf(roomList[i]));
                myroomlist.RemoveAt(myroomlist.IndexOf(roomList[i]));

            }
        }
        for (int i = 0; i < uI_PVPSelect.RoomList.Count; i++)
        {
            if (uI_PVPSelect != null && uI_PVPSelect.RoomList[i] != null)
            {
                uI_PVPSelect.RoomList[i].Init();
            }
            if (uI_PVPSelect.RoomList.Count >= i+1 && roomList[i+1].Name == roomList[i].Name)
            {
                    uI_PVPSelect.RemoveRoomBtn(i+1);
                uI_PVPSelect.Map.RemoveAt(myroomlist.IndexOf(roomList[i+1]));
                myroomlist.RemoveAt(myroomlist.IndexOf(roomList[i+1]));
            }
        }
        if(uI_PVPSelect.RoomList.Count > roomList.Count)
        {
            uI_PVPSelect.RoomList.RemoveAt(roomList.Count);
        }
    }   
    public override void OnJoinedLobby()
    {
        PhotonNetwork.NickName = SharedObject.g_playerPrefsdata.playerdata.m_strPlayerName;
    }
    private void OnApplicationQuit()
    {
        if (PhotonNetwork.InRoom)
        {
            Photon.Pun.PhotonView pv = SharedObject.g_DataManager.m_MyCharacter.GetComponent<Photon.Pun.PhotonView>();
            if (pv != null && pv.IsMine)
            {
                if (PVPManager.instance != null)
                {
                    PVPManager.instance.WinPlayer((int)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == 1 ? 2 : 1);
                }
            }
            else if(pv==null)
            {
                  if (PhotonNetwork.IsMasterClient)
                {

                    PhotonNetwork.RaiseEvent(37, null, NetworkManager.instance.otherraiseEvent, SendOptions.SendReliable);
                }
            }
        }
        Invoke("calldis", .5f);
    }
    void calldis()
    {
        DisConnect();

    }
    private void OnDisconnectedFromServer()
    {
        if (PhotonNetwork.InLobby)
            PhotonNetwork.LeaveLobby();
        else if (PhotonNetwork.InRoom)
        {
            if (uI_PVPSelect != null)
                    uI_PVPSelect.Exit_Ready();
        }

    }
    public override void OnCreatedRoom()
    {

        PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable { { "CheckPlayer", 0 } });
    }
    public void JoinTeam(int Team)
    {

        ExitGames.Client.Photon.Hashtable playerProps = new ExitGames.Client.Photon.Hashtable { { "Team", Team } };
        PhotonNetwork.SetPlayerCustomProperties(playerProps);

    }
    public override void OnPlayerPropertiesUpdate(Photon.Realtime.Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
            StartCoroutine(SetRoomProperties());
       
    }
    
    IEnumerator SetRoomProperties()
    {
       if(!PhotonNetwork.InRoom)
        {
            yield return new WaitForSeconds(.1f);
            StartCoroutine(SetRoomProperties());
        }
       else
        {
            if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == 1)
            {
                PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable { { "Team0", PhotonNetwork.NickName } });
                PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable { { "Pet0", SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet == null ? "Remove" : ((byte)SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet.playerPetInfo.PetType + SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet.playerPetInfo.Upgrade).ToString() } });
            }
            else if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == 2)
            {
                PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable { { "Team1", PhotonNetwork.NickName } });
                PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable { { "Pet1", SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet == null ? "Remove" : ((byte)SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet.playerPetInfo.PetType + SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet.playerPetInfo.Upgrade).ToString() } });
            }
            if (!MyScene.instance.PVPRoom && uI_PVPSelect != null)
            {
                uI_PVPSelect.InRoomUpdate(!uI_PVPSelect.CheckExit, (int)PhotonNetwork.LocalPlayer.CustomProperties["Team"]);
            }
        }
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
    }
    
    public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        if (!MyScene.instance.PVPRoom && uI_PVPSelect != null && PhotonNetwork.LocalPlayer.CustomProperties.ContainsKey("Team"))
        {
            
            uI_PVPSelect.InRoomUpdate(!uI_PVPSelect.CheckExit, (int)PhotonNetwork.LocalPlayer.CustomProperties["Team"]);
        }
    }
    public override void OnJoinedRoom()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            uI_PVPSelect.MapSelect_.SetActive(true);
            uI_PVPSelect.BtnText.text = "시작하기";
        }
        else
        {

            uI_PVPSelect.BtnText.text = "준비하기";
        }

        ChatTest.instance.ConnectCnannel();
        PhotonNetwork.RaiseEvent(1, null, NetworkManager.instance.raiseEvent, SendOptions.SendReliable);
        JoinTeam(PhotonNetwork.CurrentRoom.PlayerCount);
        //MapSelect[] mapSelects = uI_PVPSelect.MapSelect_.GetComponentsInChildren<MapSelect>();
        //for (int i = 0; i < mapSelects.Length; i++)
        //{
        //   if( mapSelects[i].Map == uI_PVPSelect.currentMap)
        //    {
        //        mapSelects[i].SelectMap();
        //    }
        //}
    }
    public override void OnLeftRoom()
    {

       uI_PVPSelect.trueleftroom = false;
        ChatTest.instance.UnsubscribedChat();

    }
}
