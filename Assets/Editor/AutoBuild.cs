using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class AutoBuild : MonoBehaviour
{
    //static string[] SCENS = FindEnabledEditorScens();
    [MenuItem("Custom/Build Google")]
    static void GoogleBuild()
    {
        string BUILD_TARGET_PATH = "Build/Android/";
        Directory.CreateDirectory(BUILD_TARGET_PATH);
        PlayerSettings.WSA.packageName = "com.pansost.hero";
        
        PlayerSettings.companyName = "PansoST";
        PlayerSettings.productName = "Hero";

        List<string> EditorScens = new List<string>();
        foreach(EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
        {
            if(!scene.enabled)
            {
                continue;
            }
            EditorScens.Add(scene.path);
        }

        PlayerSettings.Android.keystoreName  ="Assets/user.keystore";
        PlayerSettings.Android.keystorePass  ="Popo9741!!";
        PlayerSettings.Android.keyaliasName  ="hero";
        PlayerSettings.Android.keyaliasPass  ="Popo9741!!";

        string strname = BUILD_TARGET_PATH +"_Google" + ".apk";

        BuildPipeline.BuildPlayer(EditorScens.ToArray(), strname, BuildTarget.Android, BuildOptions.None);
    }
}
