using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using Photon;
using Photon.Pun;
using ExitGames.Client.Photon;

[Serializable]
public class PlayerPetInfo
{
    public m_Enum.Pet PetType;
    public string PetName;
    public float Damage;
    public int Upgrade;
    public bool Equip;
    public int AttackDelay;
}
public class PlayerPet : MonoBehaviour
{
    public Animator anim;
    public GameObject Bullet;
    public PlayerPetInfo playerPetInfo;
    [SerializeField]
    List<GameObject> FindEnemies = new List<GameObject>();
    [SerializeField]
    Monster[] Enemies;
    private Boss boss;
    public float FoundDis;
    public AudioSource AttackAudio;
    private void Awake()
    {
        AttackAudio = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        playerPetInfo.Damage = float.Parse(SharedScript.instance.csvdata.Pet[(int)playerPetInfo.PetType+playerPetInfo.Upgrade]["Damage"].ToString());
        playerPetInfo.AttackDelay = int.Parse(SharedScript.instance.csvdata.Pet[(int)playerPetInfo.PetType+playerPetInfo.Upgrade]["AttackDelay"].ToString());
        playerPetInfo.PetName =SharedScript.instance.csvdata.Pet[(int)playerPetInfo.PetType+playerPetInfo.Upgrade]["PetName"].ToString();
        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void Start()
    {
        StartCoroutine(CheckAttack());
        Invoke("SetEnemies", .5f);
        boss = FindObjectOfType<Boss>();
    }
    void SetEnemies()
    {
        Enemies = FindObjectsOfType<Monster>();
    }
    IEnumerator CheckAttack()
    {
        yield return new WaitForSeconds(playerPetInfo.AttackDelay);
        if(FindEnemies.Count != 0)
        Attack();
        for (int i = 0; i < FindEnemies.Count; i++)
        {
            FindEnemies[i].GetComponent<Monster>().StartCoroutine(FindEnemies[i].GetComponent<Monster>(). ApplyDamage(playerPetInfo.Damage));
        }
        StartCoroutine(CheckAttack());
        yield return null;
    }
    public void ResetAttack()
    {
        anim.ResetTrigger("Attack");
    }
    private void Update()
    {
        if (boss != null)
        {
            FindEnemy(boss.gameObject);
        }
        for (int i = 0; i < Enemies.Length; i++)
        {
            if (Enemies[i] != null)
            {
                FindEnemy(Enemies[i].gameObject);
            }
        }
    }
    private void FindEnemy(GameObject obj)
    {
        if (Vector2.Distance(obj.transform.position, transform.position) <= FoundDis)
        {
            if (FindEnemies.Count < 3 && !FindEnemies.Contains(obj.gameObject))
            {
                FindEnemies.Add(obj.gameObject);
            }
        }
        else
        {
            for (int a = 0; a < FindEnemies.Count; a++)
            {
                if (FindEnemies[a] != null)
                {

                    if (FindEnemies.Contains(obj.gameObject))
                    {
                        FindEnemies.Remove(obj.gameObject);
                    }
                }

            }

        }
        for (int i = 0; i < FindEnemies.Count; i++)
        {
            if (FindEnemies[i] == null)
            {
                FindEnemies.RemoveAt(i);
            }
        }
    }
    public void Fire()
    {

    }

    private void OnEvent(EventData obj)
    {
        //if (obj.Code == 101)
        //{
        //    string animname = (string)obj.CustomData;
        //    switch(animname)
        //    {
        //        case "Move":
        //            anim.SetBool("Run", true);
        //            break;
        //        case "Idle":
        //            anim.SetBool("Run", false);
        //            break;
        //        case "Death":
        //            anim.SetTrigger("Death");
        //            break;
        //        case "Hit":
        //            anim.SetTrigger("Hit");
        //            break;
        //        case "Attack":
        //            AttackAudio.Play();
        //            anim.SetTrigger("Attack");
        //            break;
        //        case "Attack2":
        //            AttackAudio.Play();
        //            anim.SetTrigger("Attack2");
        //            break;
        //        case "Attack3":
        //            AttackAudio.Play();
        //            anim.SetTrigger("Attack3");
        //            break;
        //        case "Ability":
        //            AttackAudio.Play();
        //            anim.SetTrigger("Ability");
        //            break;
        //    }
        //}
    }

    public void Move()
    {
        if(PhotonNetwork.InRoom)
        {
        SharedObject.g_DataManager.m_MyCharacter.GetComponent<PhotonView>().RPC("Pun_PetMove",RpcTarget.All,null);

        }
        else
        {
            SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerAnim>().Pun_PetMove();
        }
    }
    public void Idle()
    {
        if (PhotonNetwork.InRoom)
        {
        SharedObject.g_DataManager.m_MyCharacter.GetComponent<PhotonView>().RPC("Pun_PetIdle",RpcTarget.All,null);

        }
        else
        {
            SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerAnim>().Pun_PetIdle();
        }
        
    }
    public void Death()
    {
        if (PhotonNetwork.InRoom)
        {
        SharedObject.g_DataManager.m_MyCharacter.GetComponent<PhotonView>().RPC("Pun_PetDeath",RpcTarget.All,null);

        }
        else
        {
            SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerAnim>().Pun_PetDeath();
        }

    }
    public void Hit()
    {
        if (PhotonNetwork.InRoom)
        {
        SharedObject.g_DataManager.m_MyCharacter.GetComponent<PhotonView>().RPC("Pun_PetHit",RpcTarget.All,null);

        }
        else
        {
            SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerAnim>().Pun_PetHit();
        }

    }
    public void Attack()
    {
        if (PhotonNetwork.InRoom)
        {
        SharedObject.g_DataManager.m_MyCharacter.GetComponent<PhotonView>().RPC("Pun_PetAttack",RpcTarget.All,null);

        }
        else
        {
            SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerAnim>().Pun_PetAttack();
        }

    }
    public void Attack2()
    {

        anim.SetTrigger("Attack 2");
    }
    public void Attack3()
    {

        anim.SetTrigger("Attack 3");
    }
    public void Ability()
    {

        anim.SetTrigger("Ability");
    }
}
