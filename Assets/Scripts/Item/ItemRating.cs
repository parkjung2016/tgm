using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRating : MonoBehaviour
{
    public Color[] Item_BgColor;
    public Color[] Item_OutLinecolor;
    public Color[] Item_PotionBgColor;
    public Color[] Item_PotionOutLinecolor;
    public Color[] Item_LevelOutLinecolor;
    public string[] RatingName;
    public string[] PotionRatingName;
    static public ItemRating instance;
    private void Awake()
    {
        if(instance == null)
        {
        instance = this;
        DontDestroyOnLoad(gameObject);

        }
        else
        {
            Destroy(gameObject);
        }
    }
}
