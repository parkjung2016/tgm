using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class Item : MonoBehaviour,IPointerDownHandler
{
    public ItemInfo itemInfo;
    private void Awake()
    {
        itemInfo = GetComponent<ItemInfo>();
    }
    public void Update()
    {
        if(Input.touchCount > 0)
        {
            if(EventSystem.current.IsPointerOverGameObject(0).Equals(false))
            {
                if (Input.GetTouch(1).phase.Equals(TouchPhase.Began))
                {
                    CallVisibleItemInfo();
                }
            }
        }
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        CallVisibleItemInfo();
    }
    public void CallVisibleItemInfo()
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        SharedScript.instance.inventory.VisibleItemInfo(this);

    }
}
