using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class Item_Smithy : MonoBehaviour, IPointerDownHandler
{

   private SmithyUI smithyUI;
    public ItemInfo itemInfo;
    private void Awake()
    {
        smithyUI = FindObjectOfType<SmithyUI>();
        itemInfo = GetComponent<ItemInfo>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        smithyUI.ShowItemInfo(itemInfo,itemInfo.sprite);
    }
}
