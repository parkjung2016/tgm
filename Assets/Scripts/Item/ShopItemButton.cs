using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class ShopItemButton : MonoBehaviour,IPointerClickHandler
{
    public Shop shop;
    private ItemInfo iteminfo;
    public Text Price;
    public void ValueInit()
    {
        iteminfo = GetComponentInChildren<ItemInfo>();
        ResetInfo();

    }
    public void OnPointerClick(PointerEventData eventData)
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        shop.ShowItemInfo(iteminfo);
    }
    public void ResetInfo()
    {
        if(iteminfo !=null)
        Price.text = iteminfo.itemdata.Prices[0].ToString();
    }

}
