using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;




public class ItemInfo : MonoBehaviour
{
    public Sprite sprite;
    [SerializeField]
    private Image[] imgs;
    public Image weaponImg;
    public Text ItemRatingText;
    public PlayerPrefsData.ItemData itemdata;
    public Material[] LevelOutLineMat;
    public Text UpgradeText;
    public void ResetInfo()
    {
        if (itemdata.Level == 0)
            imgs[1].material = null;
        else if ( itemdata.Level != 5)
            imgs[1].material = LevelOutLineMat[0];
        else
            imgs[1].material = LevelOutLineMat[1];

 
int Price = int.Parse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemdata.m_ITEMTYPE] + itemdata.rating]["Price"].ToString());
        itemdata.Prices[0] = Price;
        itemdata.Prices[1] = (int)(Price * 80 * 0.01f);
        if (itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.POTION)
            itemdata.ratingName = ItemRating.instance.PotionRatingName[itemdata.rating];
        else
            itemdata.ratingName = ItemRating.instance.RatingName[itemdata.rating];
        Color color = ItemRating.instance.Item_OutLinecolor[itemdata.rating];
        itemdata.colorStore2 = new float[4] { color.r, color.g, color.b,1 };
        color = ItemRating.instance.Item_BgColor[itemdata.rating];
        itemdata.colorStore = new float[4] { color.r, color.g, color.b,1};
        sprite = Resources.Load<Sprite>("Image/Item/" + SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemdata.m_ITEMTYPE] + itemdata.rating]["ImagePath"].ToString());
        weaponImg.sprite = sprite;
        if(UpgradeText != null)
        { 
        UpgradeText.gameObject.SetActive(itemdata.Level != 0);
        UpgradeText.text = "+"+itemdata.Level.ToString();
        }
        ItemRatingText.text = itemdata.ratingName;
        itemdata.ReinforcedPrice = int.Parse(SharedScript.instance.csvdata.ItemUpgradeValue[itemdata.Level + SharedScript.instance.csvdata.ItemTypeNum[itemdata.rating]]["Gold"].ToString());
        imgs[0].color =new Color ( itemdata.colorStore[0], itemdata.colorStore[1], itemdata.colorStore[2]); 
            imgs[1].color =new Color ( itemdata.colorStore2[0], itemdata.colorStore2[1], itemdata.colorStore2[2]);
        itemdata.Prices[1] = (int)(itemdata.Prices[0] * 80 * .01f)+(int)(itemdata.ReinforcedPrice*80*.01f);
    }
}
