using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using DG.Tweening;
using UnityEngine.UI;
using Newtonsoft.Json;


public class DialogueManager : MonoBehaviour
{
    [SerializeField]
    private Text nameText;
    [SerializeField]
    private Text dialogueText;
    public GameObject[] InDialogueActionObj;
    public Text BtnText;
    private bool _Auto;
    private Queue<string> sentences =new Queue<string>();
    public int QuestKey;
    public int QuestNum;
    public void StartDialogue(Dialogue dialogue,bool Auto)
    {
        _Auto = Auto;
        nameText.text = dialogue.name;
        sentences.Clear();
        foreach(string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        StartCoroutine(NextSentenceCheck(dialogue.sentences));
        
    }
    IEnumerator NextSentenceCheck(string[] sentences)
    {
        if (_Auto)
            for (int i = 0; i < sentences.Length; i++)
            {
                DisplayNextSentence();
                yield return new WaitForSeconds(1f);
            }
        else
            DisplayNextSentence();

    }
    public void DisplayNextSentence()
    {
        if(sentences.Count == 0)
        {
            int QuestrewardKey = int.Parse(SharedScript.instance.csvdata.Quest[QuestNum]["퀘스트 보상"].ToString());
            switch (QuestrewardKey)
            {
                case 0:
                    break;
                case 1000:
           
                         SharedObject.g_playerPrefsdata.playerdataNoSave.Gold += 1000;
                    CanvasManager.SetGoldText();
                    SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
                    break;
                case 2000:
                    byte index = 4;
                    SharedScript.instance.inventory.CurrentScrollNum = 0;
                    string id = SharedScript.instance.itemids[0] + "_" + index;
                    //PlayerPrefsData.ItemData itemData = new PlayerPrefsData.ItemData();
                    //itemData.rating = 4;
                    //itemData.Values = new float[2];
                    //itemData.Prices = new int[2];
                    //itemData.Data = new string[2] { "공격력", "공속" };
                    //for (int i = 0; i < itemData.Values.Length; i++)
                    //{
                    //    int min;
                    //    int max;
                    //    if (int.TryParse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemData.m_ITEMTYPE] + itemData.rating][i == 0 ? "MinValue" : "MinValue2"].ToString(), out min))
                    //    {
                    //        if (int.TryParse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemData.m_ITEMTYPE] + itemData.rating][i == 0 ? "MaxValue" : "MaxValue2"].ToString(), out max))
                    //            itemData.Values[i] = UnityEngine. Random.Range(min, max + 1);

                    //    }

                    //}
                    //    itemData.Values[1] = float.Parse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemData.m_ITEMTYPE] + itemData.rating]["MinValue2"].ToString());

                    List<string> itemidlist = new List<string>();
                    itemidlist.Add(id);
                    PlayFabServerAPI.GrantItemsToUser(new PlayFab.ServerModels.GrantItemsToUserRequest() { ItemIds = itemidlist, PlayFabId = SharedObject.g_playerPrefsdata.playfabid, CatalogVersion = "Main" }, success =>
                    {
                        GetCatalogItemsRequest requestCatalog = new GetCatalogItemsRequest() { CatalogVersion = "Main" };
                        PlayFabClientAPI.GetCatalogItems(requestCatalog, result =>
                        {
                            List<CatalogItem> items = result.Catalog;
                            foreach (CatalogItem i in items)
                            {
                                if (i.ItemId == id)
                                {
                                    for (int a = 0; a < success.ItemGrantResults.Count; a++)
                                    {
                                        if (success.ItemGrantResults[a].ItemId == i.ItemId)
                                        {
                                            success.ItemGrantResults[a].CustomData = JsonConvert.DeserializeObject<Dictionary<string, string>>(i.CustomData);
                                        }
                                    }
                                    SharedScript.instance.UpdateInventory(SharedScript.instance.inventory.m_WeaponList, index, success.ItemGrantResults.Count - 1,0);
                                    break;
                                }
                            }
                        }, error => { });
                    },error=> { });
                    break;
                case 3000:
                         SharedObject.g_playerPrefsdata.playerdataNoSave.Gold += 10000;
                         SharedObject.g_playerPrefsdata.playerdataNoSave.EarnedGold += 10000;
                    CanvasManager.SetGoldText();
                    SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
                    break;
            }
            QuestManager.instance.questList[QuestKey].questNames[0] = SharedScript.instance.csvdata.Quest[QuestNum]["메인퀘스트내용"].ToString() + "(완료)";
            SharedObject.g_playerPrefsdata.playerdata.questListQUESTDATA[QuestNum].questNames[0] = SharedScript.instance.csvdata.Quest[QuestNum]["메인퀘스트내용"].ToString() + "(완료)";
            QuestUI.instance.RemoveQuest(0, 0);
            QuestManager.instance.GenerateData(QuestKey+1, SharedScript.instance.csvdata.Quest[QuestNum+1]["메인퀘스트내용"].ToString(), SharedScript.instance.csvdata.Quest[QuestNum + 1]["메인퀘스트내용2"].ToString(), SharedScript.instance.csvdata.Quest[QuestNum + 1]["메인퀘스트목표"].ToString().Contains("(0/") ? true : false, QuestNum + 1, SharedScript.instance.csvdata.Quest[QuestNum+1]["NPC번호"].ToString().Split(';'));
            if(!_Auto)
            {
                gameObject.SetActive(false);
            }
            return;
        }
        string sentece =sentences.Dequeue();
        StopCoroutine(TypeSentece(sentece));
        StartCoroutine(TypeSentece(sentece));
    }
    IEnumerator TypeSentece(string sentece)
    {
        dialogueText.text = "";
        foreach (char letter in sentece.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
       

    }
    public void ActiveInDialogueActionObj(GameObject gameObject)
    {
        CanvasGroup canvasGroup = gameObject.GetComponent<CanvasGroup>();
        if(canvasGroup.alpha == 0)
        {
            gameObject.SetActive(true);
        canvasGroup.DOFade(1, .5f);

        }
    }
}

