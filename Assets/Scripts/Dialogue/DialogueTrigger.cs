using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue;
    public DialogueManager dialogueManager;
    public bool Auto;
    private void Awake()
    {
        dialogueManager = FindObjectOfType<DialogueManager>();
    }
    public void TriggerDialogue()
    {
        dialogueManager.StartDialogue(dialogue,Auto);
    }
}
