using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skeleton : Monster
{
   public float DefencePercent;
    [SerializeField]
    private AudioClip[] ShieldAudios;
    void Start()
    {
        start();
    }
    void Update()
    {
        update();
    }
    public void PlayShieldSound()
    {
        audioSource.PlayOneShot(ShieldAudios[Random.Range(0, ShieldAudios.Length)]);
    }
}
