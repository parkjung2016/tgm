using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using DG.Tweening;

public class MonsterBase : Character,IWeaponCol
{
    public AI_Monster aI_Monster = new AI_Monster();
    public Transform castPoint;
    public GameObject Player;
    public Vector2 BoxSize;
    public LayerMask la;
    public float[] Dis = new float[2];
    public GameObject GoldObj;
    public int[] Drops;
    public float Hp;
    public int Damage;
    public float moveSpeed;
    public AudioClip[] AttackAudios;
    public AudioClip[] HitAudios;
    public AudioClip DeathAudios;
    public AudioSource audioSource;
    public Transform CheckGround;
    public float MinSpeed;
    public float MaxSpeed;
    public LayerMask GroundLayerMask;
    public float GroundCheckDis;
    public Transform GroundCheckTrans;
    public  enum MonsterType
    {
        고블린,
        버섯_괴물,
        스켈레톤
    };
    public MonsterType monsterType;
    public void ComboPossible_()
    {

        ComboPossible = true;
    }
    public void Combo()
    {
        if (aI_Monster.m_AI == m_Enum.AI.AI_DIE)
            return;
        switch (ComboStep)
        {
            case 2:
                anim.Play("AttackB");
                break;
            case 3:
                anim.Play("AttackC");
                break;
            case 4:
                anim.Play("AttackD");
                break;
        }
    }
    public void ComboReset()
    {
        Attacking = false;
        ComboPossible = false;
        ComboStep = 0;
    }
    public void WeaponColVisible()
    {
        WeaponCol.SetActive(true);
    }
    public void WeaponColHide()
    {
        WeaponCol.SetActive(false);

    }
    public void PlayAttackSound(AudioClip audioClip)
    {
        if(audioClip == null)
        audioSource.PlayOneShot(AttackAudios[UnityEngine.Random.Range(0, AttackAudios.Length)]);
        else
        audioSource.PlayOneShot(audioClip);
    }
    public void PlayHitSound()
    {
        audioSource.PlayOneShot(HitAudios[UnityEngine.Random.Range(0, HitAudios.Length)]);
    }
    public void PlayDeathSound()
    {
        audioSource.PlayOneShot(DeathAudios);
    }
    public void PlayMoveSound()
    {
        MoveSound.instance.PlayRunSFX(CheckGround, audioSource);
    }

}
public class Monster: MonsterBase
{  

    private void Awake()
    {
        CheckGround = transform.GetChild(2).transform;
        audioSource = GetComponent<AudioSource>();  
       spriteRenderer_ = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        ActorType = m_Enum.ACTORTYPE.MONSTER;
        rig2d = GetComponent<Rigidbody2D>();
        aI_Monster.InitAI(this, 0);
    }
    void CheckStat()
    {
        List<int> PlayerLevelMin = new List<int>();
        List<int> PlayerLevelMax = new List<int>();
        string[] CheckLevel = new string[SharedScript.instance.csvdata.Monster.Count];
        string[] MonsterName = new string[SharedScript.instance.csvdata.Monster.Count];
        int[] Hp_ = new int[SharedScript.instance.csvdata.Monster.Count];
        int[] Power_ = new int[SharedScript.instance.csvdata.Monster.Count];
        float[] MinSpeed_ = new float[SharedScript.instance.csvdata.Monster.Count];
        float[] MaxSpeed_ = new float[SharedScript.instance.csvdata.Monster.Count];
        int[] XPDrops_ = new int[SharedScript.instance.csvdata.Monster.Count];
        int[] GoldDrops_ = new int[SharedScript.instance.csvdata.Monster.Count];
        for(int i =0;i < SharedScript.instance.csvdata.Monster.Count;i++)
        {
            CheckLevel[i] = SharedScript.instance.csvdata.Monster[i]["플레이어 레벨"].ToString();
            MonsterName[i] = SharedScript.instance.csvdata.Monster[i]["몬스터 이름"].ToString();
            Hp_[i] =int.Parse( SharedScript.instance.csvdata.Monster[i]["체력"].ToString());
            Power_[i] =int.Parse( SharedScript.instance.csvdata.Monster[i]["공격력"].ToString());
            MinSpeed_[i] =float.Parse( SharedScript.instance.csvdata.Monster[i]["이동속도"].ToString());
            MaxSpeed_[i] =float.Parse( SharedScript.instance.csvdata.Monster[i]["이동속도2"].ToString());
            XPDrops_[i] =int.Parse( SharedScript.instance.csvdata.Monster[i]["XP드랍"].ToString());
            GoldDrops_[i] =int.Parse( SharedScript.instance.csvdata.Monster[i]["골드드랍"].ToString());
            string[] level = SharedScript.instance.csvdata.Monster[i]["플레이어 레벨"].ToString().Split('~');
            if (!PlayerLevelMin.Contains(int.Parse( level[0])))
            {
                
            PlayerLevelMin.Add(int.Parse( level[0]));
                if (level[1] != "")
                {
                    PlayerLevelMax.Add(int.Parse(level[1]));

                }
                else
                {
                    PlayerLevelMax.Add(0);

                }
            }
        }
        int CurLevel = SharedObject.g_playerPrefsdata.playerdata.m_nStat2[(byte)m_Enum.STAT2.LEVEL];
        for (int i =0; i < PlayerLevelMin.Count;i++)
        {
            if(PlayerLevelMax[i] != 0 && CurLevel >= PlayerLevelMin[i] && CurLevel <= PlayerLevelMax[i] || PlayerLevelMax[i] == 0 && CurLevel >= PlayerLevelMin[i])
            {
                for(int a = 0; a <CheckLevel.Length;a++)
                {
                    if (PlayerLevelMax[i] == 0 &&CheckLevel[a].Contains(PlayerLevelMin[i].ToString()) || PlayerLevelMax[i] != 0 && CheckLevel[a].Contains(PlayerLevelMin[i].ToString())&& CheckLevel[a].Contains(PlayerLevelMax[i].ToString()))
                    {
                        if(MonsterName[a] ==monsterType.ToString())
                        {
                        SetStat(Hp_[a],Power_[a],MinSpeed_[a],MaxSpeed_[a],XPDrops_[a], GoldDrops_[a]);

                        }

                    }

                }
            }
        }
    }
    void SetStat(int Hp_, int Power_, float MinSpeed_, float MaxSpeed_, int XPDrops, int GoldDrops)
    {
        Hp = Hp_;
        Damage = Power_;
        MinSpeed = MinSpeed_;
        MaxSpeed = MaxSpeed_;
        Drops = new int[2]{GoldDrops, XPDrops };
    }
    public virtual void start()
    {
        CheckStat();
        moveSpeed = UnityEngine.Random.Range(MinSpeed, MaxSpeed);
        GoldObj = Resources.Load<GameObject>("Prefabs/Gold");
        Player = GameObject.FindGameObjectWithTag("Player");
        aI_Monster.castPoint = castPoint;
        aI_Monster.spriteRenderer_ = spriteRenderer_;
        aI_Monster.FollowDis = Dis[0];
        aI_Monster.Player_ = Player;
        aI_Monster.la = la;
        aI_Monster.AttackDis = Dis[1];
        aI_Monster.BoxSize = BoxSize;
        aI_Monster.isFacingLeft = Scale / Scale != transform.localScale.x;
        WeaponColHide();
        MoveTrue = true;
        Scale = Mathf.Abs(transform.localScale.x);
            StartCoroutine(CheckDeath());
        anim.SetFloat("MoveSpeed",  moveSpeed*(moveSpeed*.4f));

    }
    IEnumerator CheckDeath()
    {
        if (!gameObject.activeSelf)
        {
            yield return new WaitForSeconds(.2f);
            StartCoroutine(CheckDeath());
        }
        while (aI_Monster.m_AI != m_Enum.AI.AI_DIE)
        {
            aI_Monster.UpdateState();
            if (Hp <= 0)
            {
                StartCoroutine(_Death());
                aI_Monster.m_AI = m_Enum.AI.AI_DIE;
            }
            yield return new WaitForSeconds(.1f);
        }

    }
    public virtual void update()
    {
        switch(aI_Monster.m_AI)
        {
            case m_Enum.AI.AI_SEARCH:
                if (!aI_Monster.isSearching)
                {
                    Invoke("Turn", UnityEngine.Random.Range(2, 4));
                    aI_Monster.isSearching = true;
                }
                break;
            case m_Enum.AI.AI_MOVE:
                CheckCanMove();
                break;
        }
    }
    void CheckCanMove()
    {
        RaycastHit2D hit = Physics2D.Raycast(GroundCheckTrans.position, -GroundCheckTrans.up, GroundCheckDis, GroundLayerMask);
        if(hit.collider == null)
        {
            ((AI_Base)aI_Monster).SetAttack();
        }
    }
    void Turn()
    {
        if (aI_Monster.m_AI != m_Enum.AI.AI_SEARCH)
            return;
        aI_Monster.isFacingLeft = !aI_Monster.isFacingLeft;
        transform.localScale = new Vector2(aI_Monster.isFacingLeft ? -Scale : Scale, Scale);
        Invoke("Turn", UnityEngine.Random.Range(2, 4));
    }
    public void QuestCheck()
    {
        bool[] Check = new bool[2] { false, false };
        int key_ = 0;
        foreach (int key in QuestManager.instance.questList.Keys)
        {
            for (int i = 0; i < 2; i++)
                if (QuestManager.instance.questList[key].type[i] == monsterType.ToString() && !QuestManager.instance.questList[key].questNames[0].Contains("완료"))
                {
                    Check[i] = true;
                    key_ = key;
                }

        }
        for (int i = 0; i < Check.Length; i++)
        {
            if (Check[i])
            {
                for (int Num = 0; Num < SharedObject.g_playerPrefsdata.playerdata.QuestTargetNum.Count; Num++)
                {
                    if (i == 0)
                    {
                        if (QuestManager.instance.questList[key_].questNames[0].Contains(SharedScript.instance.csvdata.Quest[Num]["메인퀘스트내용"].ToString()))
                        {
                            SharedObject.g_playerPrefsdata.playerdata.QuestTargetNum[Num]++;

                        }
                    }
                    else
                    {
                        if (QuestManager.instance.questList[key_].questNames[1].Contains(SharedScript.instance.csvdata.Quest[Num]["메인퀘스트내용2"].ToString()))
                        {
                            SharedObject.g_playerPrefsdata.playerdata.QuestTargetNum2[Num]++;

                        }
                    }
                }
                QuestManager.instance.CheckQuestComplete();
            }
        }

        QuestUI.instance.RefreshQuestValue();
    }
    protected virtual IEnumerator _Death()
    {
        QuestCheck();
        anim.Play("Death");
        Destroy(GetComponent<CapsuleCollider2D>());
        Destroy(GetComponent<Rigidbody2D>());
        if (SharedScript.instance.manager != null)
        { 
        SharedScript.instance.manager.remainingenemies--;
        if (SharedScript.instance.manager.remainingenemies <=0)
            {
                SharedObject.g_playerPrefsdata.playerdata.DungeonOpend[MyScene.instance.DungeonNum-1] = true;
            }
        }
        if(rig2d != null)
       rig2d.velocity = Vector2.zero;
        yield return new WaitForSeconds(0.5f);
        spriteRenderer_.material.DOFloat(0, "_DissolveAmount", 1).OnComplete(() => { Destroy(gameObject); });
        if (BossManager.instance != null && BossManager.instance.Boss_ != null)
        {
                BossManager.instance.Boss_.Skill2LiveEnemy = 0;
            for (int i = 0; i < FindObjectsOfType<MonsterBase>().Length; i++)
                BossManager.instance.Boss_.Skill2LiveEnemy = i;

        }
        else
        {

            for (int i = 0; i < 4; i++)
            {

                GoldMove obj = Instantiate(GoldObj, transform.position + new Vector3(UnityEngine.Random.Range(-2.0f, 2.0f), UnityEngine.Random.Range(0, 2.0f)), Quaternion.identity).GetComponent<GoldMove>();
                obj.GoldValue = Drops[0] / 4;
            }
                SharedObject.g_DataManager.m_MyCharacter.F[(byte)m_Enum.STAT.XP] += Drops[1];
                UIProgressBar.instance.SetXPProgress();
                SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
        }
    }


 
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            collision.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
            rig2d.velocity = Vector2.zero;
            rig2d.angularVelocity = 0;
        }
        if(collision.collider.CompareTag("PlayerSkill"))
        {
            StartCoroutine(ApplyDamage(5));
        }
    }
    public IEnumerator ApplyDamage(float Damage)
    {
        if (aI_Monster.m_AI == m_Enum.AI.AI_DIE)
            yield return null;
        if(!aI_Monster.CanSeePlayer(aI_Monster.FollowDis))
        {
            ((AI_Base)aI_Monster).SetMove();
            aI_Monster.isFacingLeft = !aI_Monster.isFacingLeft;
            transform.localScale = new Vector2(aI_Monster.isFacingLeft ? -Scale : Scale, Scale);

        }
        if(Player.GetComponent<Player>().runningcoroutine != null)
        {
            StopCoroutine(Player.GetComponent<Player>().runningcoroutine);
        }
        anim.SetTrigger("Hit");
        MoveTrue = false;
        Hp -= Damage;
        yield return new WaitForSeconds(0.5f);
        ComboReset();
     aI_Monster.m_AI = m_Enum.AI.AI_SEARCH;
        AttackTrue = false;
        MoveTrue = true;
        Player.GetComponent<Player>().runningcoroutine = Player.GetComponent<Player>().StartCoroutine(Player.GetComponent<Player>().SetIsFightingTrue());
    }
    public void _MoveTrue()
    {
        MoveTrue = true;
    }
}
