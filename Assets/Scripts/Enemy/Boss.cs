using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class Boss :Character, IWeaponCol
{
    public float MaxHp;
    public float Hp;
    [SerializeField]
    private float AttackDis;
    public ushort Damage;
    private GameObject Player;
    public BossHpUI bossHpUI;
    public bool[] SkillOpen = new bool[2];
    [SerializeField]
    private float[] SkillProbability;
    [SerializeField]
    private GameObject FireBall;
    [SerializeField]
    private Transform[] FireBallTrans;
    [SerializeField]
    private int FireballCreateNum = 4;
    public bool PlayingSkill2;
    [SerializeField]
    private GameObject[] EnemyPrefabs;
    [SerializeField]
    private Transform[] EnemyPrefabTrans;
    public int Skill2LiveEnemy;
    public AudioClip[] AttackAudios;
    public AudioClip[] HitAudios;
    public AudioClip DeathAudios;
    public AudioClip AppearAudios;
    public AudioClip ExplosionAudios;
    public AudioSource audioSource;
    public Transform CheckGround;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        bossHpUI = FindObjectOfType<BossHpUI>();
        anim = GetComponent<Animator>();
        GameObject[] objs = GameObject.FindGameObjectsWithTag("BossSkill2EnemyTrans");
        EnemyPrefabTrans = new Transform[objs.Length];
        for(int i =0; i <objs.Length;i++)
        {
            EnemyPrefabTrans[i] = objs[i].transform;
        }
        Player = GameObject.FindGameObjectWithTag("Player");
    }
    public void PlayAttackSound(AudioClip audioClip)
    {
        if (audioClip == null)
            audioSource.PlayOneShot(AttackAudios[UnityEngine.Random.Range(0, AttackAudios.Length)]);
        else
            audioSource.PlayOneShot(audioClip);
    }
    public void PlayHitSound()
    {
        audioSource.PlayOneShot(HitAudios[UnityEngine.Random.Range(0, HitAudios.Length)]);
    }
    public void PlayDeathSound()
    {
        audioSource.PlayOneShot(DeathAudios);
    }
    public void PlayAppearSound()
    {
        audioSource.PlayOneShot(AppearAudios);
    }
    public void PlayExplosionSound()
    {
        audioSource.PlayOneShot(ExplosionAudios);
    }
    public void PlayMoveSound()
    {
        MoveSound.instance.PlayRunSFX(CheckGround, audioSource);
    }

    void Start()
    {
        Hp = 1000;
        MaxHp =Hp;
        AttackTrue = true;
        WeaponColHide();
    }
    void Update()
    {
        if(Death)
            return;
        if (Hp <= MaxHp * 10f * .01f)
        {
            if(!SkillOpen[0] )
            {
                Hp = (MaxHp * 70 * .01f) + 1;
            }
            else
            StartCoroutine(PlayEndTimeLine());
        }
        else
        {
            if (Hp <= (MaxHp * 70 * .01f) + 1)
            {
                if (!SkillOpen[0])
                {
                    SkillOpen[0] = true;
                    StartCoroutine(Skill1());
                }

            }
            else
            {
                    SkillOpen[0] = false;

            }
            if (Hp <= (MaxHp * 30 * .01f) + 1)
            {
                if (!SkillOpen[1])
                {
                    SkillOpen[1] = true;
                    StartCoroutine(Skill2());
                }
            }
            else
            {
                    SkillOpen[1] = false;
            }
        }
        if (AttackTrue)
            if (Vector2.Distance(Player.transform.position, transform.position) <= AttackDis)
            {
                if (!Attacking)
                {
                    anim.Play("AttackA");
                    Attacking = true;
                }

            }
    }
    
    private IEnumerator Skill1()
    {
        if(Random.Range(1,100) <= SkillProbability[0] && !PlayingSkill2)
        {
            for(int i =0;i < FireballCreateNum;)
            {
            GameObject obj = Instantiate(FireBall, FireBallTrans[Random.Range(0, FireBallTrans.Length)].position, Quaternion.identity);
            obj.SetActive(true);
            Vector2 dir = new Vector2(obj.transform.position.x - Player.transform.position.x, (obj.transform.position.y-Player.transform.position.y)-1.5f);
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            Quaternion angleAxis = Quaternion.AngleAxis(angle - 90f, Vector3.forward);
            obj.transform.rotation = angleAxis;
                i++;
        yield return new WaitForSeconds(1f);

            }
        }
        if(SkillOpen[0])
        {
        yield return new WaitForSeconds(4f);
        StartCoroutine(Skill1());

        }    
    }
    private IEnumerator Skill2()
    {
        if (PlayingSkill2)
            yield return null;
        if (Random.Range(1, 100) <= SkillProbability[1])
        {
            for (int i = 0; i < Random.Range(3, 6); i++)
            {
                Instantiate(EnemyPrefabs[Random.Range(0, EnemyPrefabs.Length)], EnemyPrefabTrans[Random.Range(0, EnemyPrefabTrans.Length)].position + new Vector3(Random.Range(-5, 5), 0), Quaternion.identity); ;
                Skill2LiveEnemy++;
            }
            gameObject.SetActive(false);
            Attacking = false;
            AttackTrue = false;
            PlayingSkill2 = true;
        }
        if (SkillOpen[1])
        {
            yield return new WaitForSeconds(8f);
            StartCoroutine(Skill2());
        }
    }
    private IEnumerator PlayEndTimeLine()
    {
        SharedObject.g_DataManager.m_MyCharacter.GetComponent<Rigidbody2D>().AddForce(new Vector2(-20, 20),ForceMode2D.Impulse);
        yield return new WaitForSeconds(.1f);
        Death = true;
        QuestUI.instance.RefreshQuestValue();   
       FadeCanvas .instance.Fade.gameObject.SetActive(true);
        FadeCanvas.instance.Fade.DOFade(1, 1f).OnComplete(() =>
        {
            BossManager.PlayEndTimeLine();
            FadeCanvas.instance.Fade.gameObject.SetActive(false);

        });
        SharedObject.g_playerPrefsdata.playerdata.QuestTargetNum[10] = 1;
    }
    public void ComboReset()
    {
        Attacking = false;
    }
    public void WeaponColVisible()
    {
        WeaponCol.SetActive(true);
    }
    public void WeaponColHide()
    {
        WeaponCol.SetActive(false);

    }
    public IEnumerator ApplyDamage(float Damage)
    {
        if (Death)
            yield return null;
        Hp -= Damage;
        bossHpUI.SetProgressBar(Hp, MaxHp);
        if(!Attacking)
        {
        anim.SetTrigger("Hit");
        yield return new WaitForSeconds(0.5f);
        ComboReset();

        }
    }
}
