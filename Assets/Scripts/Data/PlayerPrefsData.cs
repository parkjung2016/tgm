using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters.Binary;

public class PlayerPrefsData
{
    public string playfabid;
    [Serializable]
    public class ItemData
    {
        public int Level;
        public float[] colorStore = new float[4] { 1f, 1f, 1f, 1f };
        public float[] colorStore2 = new float[4] { 1f, 1f, 1f, 1f };
        public byte rating;
        public string ratingName;
        public bool Equip;
        public string[] Data;
        public float[] Values;
        public int[] Prices;
        public int ReinforcedPrice;
        public string Name;
        public string Description;
        public m_Enum.ITEMTYPE m_ITEMTYPE;
        public string ItemId;
    }
    public class PlayerDataNoSave
    {
        public float[] m_fAdditionalStat = new float[(byte)m_Enum.ADDITIONALSTAT.MAX];
        public int Mana;
        public DateTime? _serverTime;
        public DateTime? ServerTime()
        {
            if (_serverTime == null && SharedObject.g_playerPrefsdata.playerdata.serverTime != "")
            {
                _serverTime = Convert.ToDateTime(SharedObject.g_playerPrefsdata.playerdata.serverTime);
            }
            return _serverTime;
        }
        public bool IsGottenMoneyAutoHunt;

        public List<ItemData> WeaponItem = new List<ItemData>();
        public List<ItemData> HelmetItem = new List<ItemData>();
        public List<ItemData> ArmorItem = new List<ItemData>();
        public List<ItemData> LeggingsItem = new List<ItemData>();
        public List<ItemData> BootsItem = new List<ItemData>();
        public List<ItemData> PotionsItem = new List<ItemData>();
        public int Gold;
        public int EarnedGold;
    }
    [Serializable]
    public class PlayerData
    {
        public bool IsAutoHunt;
        public float[] AddStatValues= new float[(byte)m_Enum.STAT.MAX];
        public string m_strPlayerName;
        public string serverTime;
        public float[] m_fStat = new float[(byte)m_Enum.STAT.MAX];
        public float[] m_fMaxStat = new float[(byte)m_Enum.MAXSTAT.MAX];
        public int[] m_nStat2 = new int[(byte)m_Enum.STAT2.MAX];
        public float SkillPoint;
        public float StatPoint;
        //public List<ItemData> WeaponItem = new List<ItemData>();
        //public List<ItemData> HelmetItem = new List<ItemData>();
        //public List<ItemData> ArmorItem = new List<ItemData>();
        //public List<ItemData> LeggingsItem = new List<ItemData>();
        //public List<ItemData> BootsItem = new List<ItemData>();
        //public List<ItemData> PotionsItem = new List<ItemData>();
        public List<SkillInfo> EquipSkills = new List<SkillInfo>();
        public List<PlayerSkills.SkillType> UnLockSkill = new List<PlayerSkills.SkillType>();
        public List<int> questListID = new List<int>();
        public List<QuestData> questListQUESTDATA = new List<QuestData>();
        public List<int> QuestTargetNum = new List<int>();
        public List<int> QuestTargetNum2 = new List<int>();
        public bool[] DungeonOpend;
        public List<PlayerPetInfo> Pets = new List<PlayerPetInfo>();
        public List<int> StatUpSkillLevel = new List<int>();
        public int GrahicNum = 2;
        public bool BossGateVisible = false;
    }
    public PlayerData playerdata = new PlayerData();
    public PlayerDataNoSave playerdataNoSave = new PlayerDataNoSave();
    public void Savelogininfo(InputField[] inputfield)
    {
        string[] texts = null;
        if(inputfield.Length  == 3)
        {
            texts = new string[] { inputfield[0].text, inputfield[1].text, inputfield[2].text };
        string info = string.Join(",", texts);
        PlayerPrefs.SetString("logininfo", info);
        PlayerPrefs.Save();
        }
        else
        {
            PlayFabClientAPI.GetPlayerProfile(new GetPlayerProfileRequest() {PlayFabId = playfabid,ProfileConstraints = new PlayerProfileViewConstraints() { ShowDisplayName = true } }, (result) =>
            {
                texts = new string[] { inputfield[0].text, result.PlayerProfile.DisplayName, inputfield[1].text };
                string info = string.Join(",", texts);
                PlayerPrefs.SetString("logininfo", info);
                PlayerPrefs.Save();

            }, error => {  });

        }
    }
    public void SaveFile(string _str)
    {
        SaveJsonToPlayfab();
        //if(iscurrentTypePlayFab)
        //{
        //}
        //else
        //{

        //string dataAsJson = JsonUtility.ToJson(playerdata,true);
        //string filePath = _str;
        //File.WriteAllText(filePath, dataAsJson);
        //}
    }
    public void LoadFile(string _str)
    {
        GetUserData();


        //    string path = _str;
        //    string json = File.ReadAllText(path);
        //Debug.Log(JsonUtility.FromJson<PlayerData>(json));

    }
    void SaveJsonToPlayfab()
    {
        Dictionary<string, string> dataDic = new Dictionary<string, string>();
        dataDic.Add("DataContent", JsonUtility.ToJson(playerdata));
        SetUserData(dataDic);
        SetInventoryData();
    }
    public void SetInventoryData()
    {
        PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), result =>
         { 
         for(int i =0; i < result.Inventory.Count;i++)
             {
                 for (int a = 0; a < playerdataNoSave.WeaponItem.Count; a++)
                 {
                     if (result.Inventory[i].ItemInstanceId == playerdataNoSave.WeaponItem[a].ItemId )
                     {
                         //Dictionary<string, string> dic = JsonConvert.DeserializeObject<Dictionary<string, string>>(result.Inventory[i].CustomData["Data"]);
                         //Dictionary<string, string> dic2 = JsonConvert.DeserializeObject<Dictionary<string, string>>(result.Inventory[i].CustomData["Data2"]);
                         //Dictionary<string, string> dic3 = JsonConvert.DeserializeObject<Dictionary<string, string>>(result.Inventory[i].CustomData["Data3"]);
                         //dic.Add("rating", playerdataNoSave.ArmorItem[a].rating.ToString());
                         //dic.Add("m_ITEMTYPE", ((byte)playerdataNoSave.ArmorItem[a].m_ITEMTYPE).ToString());
                         //dic.Add("level", ((byte)playerdataNoSave.ArmorItem[a].Level).ToString());
                         //dic.Add("colorStore", playerdataNoSave.ArmorItem[a].colorStore[0].ToString() + ',' + playerdataNoSave.ArmorItem[a].colorStore[1].ToString() + ',' + playerdataNoSave.ArmorItem[a].colorStore[2].ToString() + ',' + playerdataNoSave.ArmorItem[a].colorStore[3].ToString());
                         //dic.Add("colorStore2", playerdataNoSave.ArmorItem[a].colorStore2[0].ToString() + ',' + playerdataNoSave.ArmorItem[a].colorStore2[1].ToString() + ',' + playerdataNoSave.ArmorItem[a].colorStore2[2].ToString() + ',' + playerdataNoSave.ArmorItem[a].colorStore2[3].ToString());
                         //dic2.Add("Equip", Convert.ToInt32(playerdataNoSave.ArmorItem[a].Equip).ToString());
                         //dic2.Add("Values", playerdataNoSave.ArmorItem[a].Values[0].ToString() + "," + playerdataNoSave.ArmorItem[a].Values[1].ToString());
                         //dic2.Add("Datas", playerdataNoSave.ArmorItem[a].Data[0] + "," + playerdataNoSave.ArmorItem[a].Data[1]);
                         //dic3.Add("Prices", playerdataNoSave.ArmorItem[a].Prices[0].ToString() + "," + playerdataNoSave.ArmorItem[a].Prices[1].ToString());
                         //dic3.Add("ReinforcedPrice", playerdataNoSave.ArmorItem[a].ReinforcedPrice.ToString());
                         //dic3.Add("ratingName", playerdataNoSave.ArmorItem[a].ratingName);
                         //result.Inventory[i].CustomData["Data"] = JsonConvert.SerializeObject(dic);
                         //result.Inventory[i].CustomData["Data3"] = JsonConvert.SerializeObject(dic2);
                         //result.Inventory[i].CustomData["Data2"] = JsonConvert.SerializeObject(dic3);
                         //ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest()
                         //{
                         //    FunctionName = "UpdateUserInventoryItemCustomData",
                         //    GeneratePlayStreamEvent = true,
                         //    FunctionParameter = new
                         //    {

                             //        ItemInstanceId = result.Inventory[i].ItemInstanceId,
                             //        data = result.Inventory[i].CustomData
                             //    }

                             //};
                             //PlayFabClientAPI.ExecuteCloudScript(request, run =>
                             //{
                             //    Debug.Log("����");


                             //}, error => { });
                             SetitemDic(result.Inventory[i].CustomData, playerdataNoSave.WeaponItem, result.Inventory, a, i);
                     }
                 }
                 for (int a = 0; a < playerdataNoSave.HelmetItem.Count; a++)
                 {
                     if (result.Inventory[i].ItemInstanceId == playerdataNoSave.HelmetItem[a].ItemId)
                     {
                         SetitemDic(result.Inventory[i].CustomData, playerdataNoSave.HelmetItem, result.Inventory, a, i);
                     }
                 }
                 for (int a = 0; a < playerdataNoSave.ArmorItem.Count; a++)
                 {
                     if (result.Inventory[i].ItemInstanceId == playerdataNoSave.ArmorItem[a].ItemId)
                     {
                         SetitemDic(result.Inventory[i].CustomData, playerdataNoSave.ArmorItem, result.Inventory, a, i);
                     }
                 }
                 for (int a = 0; a < playerdataNoSave.LeggingsItem.Count; a++)
                 {
                     if (result.Inventory[i].ItemInstanceId == playerdataNoSave.LeggingsItem[a].ItemId)
                     {
                         SetitemDic(result.Inventory[i].CustomData, playerdataNoSave.LeggingsItem, result.Inventory, a, i);
                     }
                 }
                 for (int a = 0; a < playerdataNoSave.BootsItem.Count; a++)
                 {
                     if (result.Inventory[i].ItemInstanceId == playerdataNoSave.BootsItem[a].ItemId)
                     {
                         SetitemDic(result.Inventory[i].CustomData, playerdataNoSave.BootsItem, result.Inventory, a, i);
                     }
                 }
                 for (int a = 0; a < playerdataNoSave.PotionsItem.Count; a++)
                 {
                     if (result.Inventory[i].ItemInstanceId == playerdataNoSave.PotionsItem[a].ItemId)
                     {
                         SetitemDic(result.Inventory[i].CustomData, playerdataNoSave.PotionsItem, result.Inventory, a, i);
                     }
                 }
             }
         }, error => { });
    }
    void SetitemDic(Dictionary<string,string> dictionary,List<ItemData> itemDatas,List<ItemInstance>itemInstances,int index,int index2)
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        Dictionary<string, string> dic2 = new Dictionary<string, string>();
        Dictionary<string, string> dic3 = new Dictionary<string, string>();
        if (dictionary != null && dictionary.ContainsKey("Data"))
        {
            Dictionary<string, string> fakedic = JsonConvert.DeserializeObject<Dictionary<string, string>>(dictionary["Data"]);
            Dictionary<string, string> fakedic2 = JsonConvert.DeserializeObject<Dictionary<string, string>>(dictionary["Data2"]);
            Dictionary<string, string> fakedic3 = JsonConvert.DeserializeObject<Dictionary<string, string>>(dictionary["Data3"]);
            Dictionary<string, string> Data = new Dictionary<string, string>();
            Dictionary<string, string> Data2 = new Dictionary<string, string>();
            Dictionary<string, string> Data3 = new Dictionary<string, string>();
            Data.Add("rating",itemDatas[index].rating.ToString());
            Data.Add("m_ITEMTYPE", ((byte)itemDatas[index].m_ITEMTYPE).ToString());
            Data.Add("level", itemDatas[index].Level.ToString());
            Data2.Add("Equip", (Convert.ToInt32(itemDatas[index].Equip)).ToString());
            Data2.Add("Values", itemDatas[index].Values[0].ToString() + "," + itemDatas[index].Values[1].ToString());
            Data2.Add("Datas", itemDatas[index].Data[0] + "," + itemDatas[index].Data[1]);
            Data3.Add("Prices", itemDatas[index].Prices[0].ToString() + "," + itemDatas[index].Prices[1].ToString());
            Data3.Add("ReinforcedPrice", itemDatas[index].ReinforcedPrice.ToString());
            Data3.Add("ratingName", itemDatas[index].ratingName);
            if(fakedic.Except(Data).Any() || fakedic2.Except(Data2).Any() || fakedic3.Except(Data3).Any())
            {
                fakedic["rating"] = itemDatas[index].rating.ToString();
                fakedic["m_ITEMTYPE"] = ((byte)itemDatas[index].m_ITEMTYPE).ToString();
                fakedic["level"] = ((byte)itemDatas[index].Level).ToString();
                fakedic2["Equip"] = Convert.ToInt32(itemDatas[index].Equip).ToString();
                fakedic2["Values"] = itemDatas[index].Values[0].ToString() + "," + itemDatas[index].Values[1].ToString();
                fakedic2["Datas"] = itemDatas[index].Data[0] + "," + itemDatas[index].Data[1];
                fakedic3["Prices"] = itemDatas[index].Prices[0].ToString() + "," + itemDatas[index].Prices[1].ToString();
                fakedic3["ReinforcedPrice"] = itemDatas[index].ReinforcedPrice.ToString();
                fakedic3["ratingName"] = itemDatas[index].ratingName;
                dic = fakedic;
                dic2 = fakedic2;
                dic3 = fakedic3;
                dictionary["Data"] = JsonConvert.SerializeObject(dic);
                dictionary["Data3"] = JsonConvert.SerializeObject(dic3);
                dictionary["Data2"] = JsonConvert.SerializeObject(dic2);

                ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest()
                {
                    FunctionName = "UpdateUserInventoryItemCustomData",
                    GeneratePlayStreamEvent = true,
                    FunctionParameter = new
                    {

                        ItemInstanceId = itemInstances[index2].ItemInstanceId,
                        data = dictionary
                    }

                };
                PlayFabClientAPI.ExecuteCloudScript(request, run =>
                {


                }, error => {  });
            }
        }
        else
        {

            dictionary = new Dictionary<string, string>();
            Dictionary<string, string> fakedic = new Dictionary<string, string>();
            Dictionary<string, string> fakedic2 = new Dictionary<string, string>();
            Dictionary<string, string> fakedic3 = new Dictionary<string, string>();

            fakedic.Add("rating", itemDatas[index].rating.ToString());
            fakedic.Add("m_ITEMTYPE", ((byte)itemDatas[index].m_ITEMTYPE).ToString());
            fakedic.Add("level", ((byte)itemDatas[index].Level).ToString());
            fakedic2.Add("Equip", Convert.ToInt32(itemDatas[index].Equip).ToString());
            fakedic2.Add("Values", itemDatas[index].Values[0].ToString() + "," + itemDatas[index].Values[1].ToString());
            fakedic2.Add("Datas", itemDatas[index].Data[0] + "," + itemDatas[index].Data[1]);
            fakedic3.Add("Prices", itemDatas[index].Prices[0].ToString() + "," + itemDatas[index].Prices[1].ToString());
            fakedic3.Add("ReinforcedPrice", itemDatas[index].ReinforcedPrice.ToString());
            fakedic3.Add("ratingName", itemDatas[index].ratingName);
            dic = fakedic;
            dic2 = fakedic2;
            dic3 = fakedic3;
            dictionary.Add("Data", JsonConvert.SerializeObject(dic) );
            dictionary.Add("Data3",JsonConvert.SerializeObject(dic3));
            dictionary.Add("Data2", JsonConvert.SerializeObject(dic2));

            ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest()
            {
                FunctionName = "UpdateUserInventoryItemCustomData",
                GeneratePlayStreamEvent = true,
                FunctionParameter = new
                {

                    ItemInstanceId = itemInstances[index2].ItemInstanceId,
                    data = dictionary
                }

            };
            PlayFabClientAPI.ExecuteCloudScript(request, run =>
            {


            }, error => {});
        }
    }
    public void SetUserData(Dictionary<string, string> data)
    {
        var request = new UpdateUserDataRequest() { Data = data, Permission = UserDataPermission.Public };
        try
        {
            //PlayFabClientAPI.google
            PlayFabClientAPI.UpdateUserData(request, (result) =>
            {

            }, error =>
            {
            });
            
        }
        catch (Exception e)
        {
        }
        if(playerdataNoSave.EarnedGold != 0)
        PlayFabClientAPI.AddUserVirtualCurrency(new AddUserVirtualCurrencyRequest() { VirtualCurrency = "GD", Amount =playerdataNoSave.EarnedGold }, result => {  }, error => { });
    }
    public void GetUserData()
    {
        var request = new GetUserDataRequest() { PlayFabId = playfabid };
        PlayFabClientAPI.GetUserData(request, (result) =>
        {
            PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), success =>
             {
                 playerdataNoSave.Gold =success.VirtualCurrency["GD"];
             
             }, error => { });
            if (result.Data.Count == 0)
            {
                for (byte i = 0; i < (byte)m_Enum.STAT.MAX; i++)
                    playerdata.m_fStat[i] = float.Parse(SharedScript.instance.csvdata.PlayerSTAT[i]["fSTAT"].ToString());
                for (byte i = 0; i < (byte)m_Enum.STAT2.MAX; i++)
                    playerdata.m_nStat2[i] = (int)SharedScript.instance.csvdata.PlayerSTAT[i]["nSTAT"];
                for (byte i = 0; i < (byte)m_Enum.MAXSTAT.MAX; i++)
                    playerdata.m_fMaxStat[i] = float.Parse(SharedScript.instance.csvdata.PlayerSTAT[i]["fMAXSTAT"].ToString());
                playerdata.SkillPoint = 0;
                playerdataNoSave.Mana = (int)SharedObject.g_playerPrefsdata.playerdata.m_fMaxStat[(byte)m_Enum.MAXSTAT.MAXMANA];
                playerdata.serverTime = TimeServer.Instance.ServerTime.ToString();
                playerdata.DungeonOpend = new bool[SharedObject.MapNamestr.Length - 1];
                SharedObject.firstconnect = true;
                for (byte i = 0; i < 3; i++)
                {

                    playerdata.EquipSkills.Add(new SkillInfo());
                    playerdata.EquipSkills[i].skilltype = PlayerSkills.SkillType.Max;
                }
                //SaveFile(SharedObject.SavePath);
            }

            foreach (var eachData in result.Data)
            {
                string key = eachData.Key;

                if (eachData.Key.Contains("DataContent"))
                {
                    PlayerData content = JsonUtility.FromJson<PlayerData>(eachData.Value.Value);
                    playerdata = content;
                    PlayFabClientAPI.GetLeaderboard(new GetLeaderboardRequest()
                    {
                        StatisticName = "LevelRank",
                        StartPosition = 0,
                        MaxResultsCount = 10,
                    }, result => {
                        PlayerLeaderboardEntry name = result.Leaderboard.FirstOrDefault(i => i.PlayFabId == playfabid);
                        if (name == null)
                        {
                            PlayFabManager.instance.AddLevelToLeaderBoard(SharedObject.g_playerPrefsdata.playerdata.m_nStat2[(byte)m_Enum.STAT2.LEVEL]);
                        }
                        else
                        {
                            if (name.StatValue != SharedObject.g_playerPrefsdata.playerdata.m_nStat2[(byte)m_Enum.STAT2.LEVEL])
                            {
                                PlayFabManager.instance.AddLevelToLeaderBoard(SharedObject.g_playerPrefsdata.playerdata.m_nStat2[(byte)m_Enum.STAT2.LEVEL]);
                            }
                        }
                    }, error => { });
                    PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), result =>
                     {
                         List<ItemData> weaponitemdata = new List<ItemData>();
                         List<ItemData> helmetitemdata = new List<ItemData>();
                         List<ItemData> armoritemdata = new List<ItemData>();
                         List<ItemData> leggingsitemdata = new List<ItemData>();
                         List<ItemData> bootsitemdata = new List<ItemData>();
                         List<ItemData> potionitemdata = new List<ItemData>();
                             //Debug.Log(result.Inventory[i].CustomData);
                             PlayFabClientAPI.GetCatalogItems(new GetCatalogItemsRequest()
                             {
                                 CatalogVersion = "Main"
                             },
                                 success =>
                              {
                                  for (int i = 0; i < result.Inventory.Count; i++)
                                  {
                                      foreach (CatalogItem catalog in success.Catalog)
                                      {
                                          if (result.Inventory[i].CustomData == null && result.Inventory[i].ItemId == catalog.ItemId/* || result.Inventory[i].CustomData != null && !result.Inventory[i].CustomData.ContainsKey("Data") && result.Inventory[i].ItemId == catalog.ItemId*/)
                                             {
                                              Dictionary<string, string> m = JsonConvert.DeserializeObject<Dictionary<string, string>>(catalog.CustomData);
                                              Dictionary<string, string> _dic = JsonConvert.DeserializeObject<Dictionary<string, string>>(m["Data"]);
                                              ItemData itemdata = new ItemData();
                                              itemdata.rating = byte.Parse(_dic["rating"]);
                                              itemdata.Level = 0;
                                              itemdata.Values = new float[2];
                                              itemdata.Data = new string[2];
                                              itemdata.Prices = new int[2];
                                              if (result.Inventory[i].ItemId.Contains("��"))
                                              {
                                                  itemdata.m_ITEMTYPE = m_Enum.ITEMTYPE.WEAPON;

                                              }
                                              else if (result.Inventory[i].ItemId.Contains("���"))
                                              {
                                                  itemdata.m_ITEMTYPE = m_Enum.ITEMTYPE.HELMET;

                                              }
                                              else if (result.Inventory[i].ItemId.Contains("����"))
                                              {

                                                  itemdata.m_ITEMTYPE = m_Enum.ITEMTYPE.ARMOR;
                                              }
                                              else if (result.Inventory[i].ItemId.Contains("����"))
                                              {

                                                  itemdata.m_ITEMTYPE = m_Enum.ITEMTYPE.LEGGINGS;
                                              }
                                              else if (result.Inventory[i].ItemId.Contains("�Ź�"))
                                              {

                                                  itemdata.m_ITEMTYPE = m_Enum.ITEMTYPE.BOOTS;
                                              }
                                              else
                                              {

                                                  itemdata.m_ITEMTYPE = m_Enum.ITEMTYPE.POTION;
                                              }

                                              itemdata.ReinforcedPrice = int.Parse(SharedScript.instance.csvdata.ItemUpgradeValue[itemdata.Level]["Gold"].ToString());
                                              for (int a = 0; a < itemdata.Values.Length; a++)
                                              {
                                                      //print( ii[a].CustomData.ToString());
                                                      int min;
                                                  int max;
                                                  if (int.TryParse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemdata.m_ITEMTYPE] + itemdata.rating][a == 0 ? "MinValue" : "MinValue" + (a + 1)].ToString(), out min))
                                                  {
                                                      if (int.TryParse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemdata.m_ITEMTYPE] + itemdata.rating][a == 0 ? "MaxValue" : "MaxValue" + (a + 1)].ToString(), out max))
                                                          itemdata.Values[a] = UnityEngine.Random.Range(min, max + 1);
                                                  }
                                                  else
                                                      itemdata.Values[a] = float.Parse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemdata.m_ITEMTYPE] + itemdata.rating][a == 0 ? "MinValue" : "MinValue" + (a + 1)].ToString());
                                              }
                                              if (itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.POTION)
                                              {
                                                  itemdata.Data[0] = ItemRating.instance.PotionRatingName[itemdata.rating];
                                                  itemdata.Data[1] = itemdata.Values[1] != 0 ? SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemdata.m_ITEMTYPE] + itemdata.rating]["property2"].ToString() : "";
                                              }
                                              else
                                              {
                                                  itemdata.Data[0] = SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemdata.m_ITEMTYPE] + itemdata.rating]["property1"].ToString();
                                                  itemdata.Data[1] = SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemdata.m_ITEMTYPE] + itemdata.rating]["property2"].ToString();
                                              }
                                              int Price = int.Parse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemdata.m_ITEMTYPE] + itemdata.rating]["Price"].ToString());
                                              itemdata.Prices[0] = Price;
                                              itemdata.Prices[1] =(int)(Price * 80*0.01f);
                                              itemdata.Name = catalog.DisplayName;
                                              itemdata.ratingName = ItemRating.instance.RatingName[itemdata.rating];
                                              itemdata.Description = SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemdata.m_ITEMTYPE] + itemdata.rating]["Description"].ToString();
                                              if (itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.WEAPON)
                                                  itemdata.Values[1] = float.Parse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemdata.m_ITEMTYPE] + itemdata.rating]["MinValue2"].ToString());

                                              int index = i;
                                              Dictionary<string, string> dic = new Dictionary<string, string>();
                                              Dictionary<string, string> dic2 = new Dictionary<string, string>();
                                              Dictionary<string, string> dic3 = new Dictionary<string, string>();
                                              dic.Add("rating", itemdata.rating.ToString());
                                              dic.Add("m_ITEMTYPE", ((byte)itemdata.m_ITEMTYPE).ToString());
                                              dic.Add("level", ((byte)itemdata.Level).ToString());
                                              dic2.Add("Equip", Convert.ToInt32(itemdata.Equip).ToString());
                                              dic2.Add("Values", itemdata.Values[0].ToString() + "," + itemdata.Values[1].ToString());
                                              dic2.Add("Datas", itemdata.Data[0] + "," + itemdata.Data[1]);
                                              dic3.Add("Prices", itemdata.Prices[0].ToString() + "," + itemdata.Prices[1].ToString());
                                              dic3.Add("ReinforcedPrice", itemdata.ReinforcedPrice.ToString());
                                              dic3.Add("ratingName", itemdata.ratingName);


                                              result.Inventory[index].CustomData = new Dictionary<string, string>();
                                              result.Inventory[index].CustomData.Add("Data", JsonConvert.SerializeObject(dic));
                                              result.Inventory[index].CustomData.Add("Data2", JsonConvert.SerializeObject(dic2));
                                              result.Inventory[index].CustomData.Add("Data3", JsonConvert.SerializeObject(dic3));


                                                  //}
                                                  //result.Inventory[index].CustomData["rating"] = rating.ToString();
                                                  //result.Inventory[index].CustomData["m_ITEMTYPE"] = ((byte)itemdata.m_ITEMTYPE).ToString();
                                                  //result.Inventory[index].CustomData["level"] = ((byte)itemdata.Level).ToString();
                                                  //result.Inventory[index].CustomData["colorStore"] = itemdata.colorStore.ToString();
                                                  //result.Inventory[index].CustomData["colorStore2"] = itemdata.colorStore2.ToString();
                                                  //result.Inventory[index].CustomData["Equip"] = Convert.ToInt32(itemdata.Equip).ToString();
                                                  //result.Inventory[index].CustomData["Values"] = itemdata.Values[0].ToString() + "," + itemdata.Values[1].ToString();
                                                  //result.Inventory[index].CustomData["Prices"] = itemdata.Prices[0].ToString() + "," + itemdata.Prices[1].ToString();
                                                  //result.Inventory[index].CustomData["ReinforcedPrice"] = itemdata.ReinforcedPrice.ToString();
                                                  ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest()
                                              {
                                                  FunctionName = "UpdateUserInventoryItemCustomData",
                                                  GeneratePlayStreamEvent = true,
                                                  FunctionParameter = new
                                                  {

                                                      ItemInstanceId = result.Inventory[index].ItemInstanceId,
                                                      data = result.Inventory[index].CustomData
                                                  }

                                              };
                                              PlayFabClientAPI.ExecuteCloudScript(request, run =>
                                              {


                                              }, error => { });
                                          }
                                      }
                                  }
                                  for (int i = 0; i < result.Inventory.Count; i++)
                                  {
                                      Dictionary<string, string> dic = JsonConvert.DeserializeObject<Dictionary<string, string>>(result.Inventory[i].CustomData["Data"]);
                                      Dictionary<string, string> dic2 = JsonConvert.DeserializeObject<Dictionary<string, string>>(result.Inventory[i].CustomData["Data2"]);
                                      Dictionary<string, string> dic3 = JsonConvert.DeserializeObject<Dictionary<string, string>>(result.Inventory[i].CustomData["Data3"]);
                                      ItemData itemdata = new ItemData();
                                      itemdata.rating = byte.Parse(dic["rating"]);
                                      itemdata.m_ITEMTYPE = (m_Enum.ITEMTYPE)byte.Parse(dic["m_ITEMTYPE"]);
                                      itemdata.Level = byte.Parse(dic["level"]);
                                      itemdata.Values = Array.ConvertAll(dic2["Values"].Split(','), float.Parse);
                                      itemdata.Prices = Array.ConvertAll(dic3["Prices"].Split(','), int.Parse);
                                      itemdata.ReinforcedPrice = int.Parse(dic3["ReinforcedPrice"]);
                                      itemdata.Equip = Convert.ToBoolean(int.Parse(dic2["Equip"]));
                                      itemdata.Name = result.Inventory[i].DisplayName;
                                      itemdata.Data = dic2["Datas"].Split(',');
                                      itemdata.Description = SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemdata.m_ITEMTYPE] + itemdata.rating]["Description"].ToString();
                                      itemdata.ratingName = dic3["ratingName"];
                                      itemdata.ItemId = result.Inventory[i].ItemInstanceId;
                                      if (result.Inventory[i].ItemId.Contains("��"))
                                      {
                                          weaponitemdata.Add(itemdata);

                                      }
                                      else if (result.Inventory[i].ItemId.Contains("���"))
                                      {
                                          helmetitemdata.Add(itemdata);
                                      }
                                      else if (result.Inventory[i].ItemId.Contains("����"))
                                      {
                                          armoritemdata.Add(itemdata);

                                      }
                                      else if (result.Inventory[i].ItemId.Contains("����"))
                                      {
                                          leggingsitemdata.Add(itemdata);

                                      }
                                      else if (result.Inventory[i].ItemId.Contains("�Ź�"))
                                      {
                                          bootsitemdata.Add(itemdata);

                                      }
                                      else
                                      {
                                          potionitemdata.Add(itemdata);

                                      }


                                  }
                                  playerdataNoSave.WeaponItem = weaponitemdata;
                                  playerdataNoSave.LeggingsItem = leggingsitemdata;
                                  playerdataNoSave.BootsItem = bootsitemdata;
                                  playerdataNoSave.HelmetItem = helmetitemdata;
                                  playerdataNoSave.ArmorItem = armoritemdata;
                                  playerdataNoSave.PotionsItem = potionitemdata;
                              }, error => {  });


                     }, error => { });
                }
            }
            PlayFabClientAPI.GetPlayerProfile(new GetPlayerProfileRequest
            {
                PlayFabId = playfabid,
                ProfileConstraints = new PlayerProfileViewConstraints()
                {
                    ShowDisplayName = true,
                    ShowContactEmailAddresses = true,

                }
            }, (GetPlayerProfileResult success) =>
            {
                if (SharedObject.g_playerPrefsdata.playerdata.m_strPlayerName != success.PlayerProfile.DisplayName)
                    playerdata.m_strPlayerName = success.PlayerProfile.DisplayName;
            }, (PlayFabError error) => {  });
        }, error => {});

    }
}

