using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MyScene : MonoBehaviour
{
    public byte SpawnPlayerNum;
    public Transform[] SpawnPlayerPos;
    public static MyScene instance;
    public Transform PlayerPos;
    public Transform PVPPlayerPos;
    public int DungeonNum;
    public GameObject[] SkillCollProgressBarObjs;
    public bool PVPRoom;
    public bool BossGateVisible
    {
        set { SharedObject.g_playerPrefsdata.playerdata.BossGateVisible = value; }
        get { return SharedObject.g_playerPrefsdata.playerdata.BossGateVisible; }
    }
    
    private void Awake()
    {
        MyScene[] myscenes = FindObjectsOfType<MyScene>();

        if (instance == null)
        {
            instance = this;
        DontDestroyOnLoad(gameObject);
        }
        if(myscenes.Length > 1)
        {
            Destroy(gameObject);
        }
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    private void Start()
    {
    }
    void OnSceneLoaded(Scene scene,LoadSceneMode mode)
    {
        SkillCollProgressBarObjs = new GameObject[3];
        for (int i =0; i <3;i++)
        {
            SkillCollProgressBarObjs[i] = GameObject.Find("Skill" + i).transform.GetChild(2).gameObject;

        }
    }
}
