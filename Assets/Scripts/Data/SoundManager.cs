using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine;
using Hellmade.Sound;


public class SoundManager : MonoBehaviour
{
    private AudioSource BGM;
    private GameObject[] SFXs;
    private AudioSource LevelUpSound;
    private AudioSource BtnClickAudio;
    private AudioSource ErrorAudio;
    private AudioSource ChatSendAudio;
    public float[] dis;
    private void Awake()
    {
        if(SharedObject.g_SoundManager == null)
        {
        SharedObject.g_SoundManager = this;

            DontDestroyOnLoad(gameObject);

        }
        SceneManager.sceneLoaded += (scene, Scenemode) =>
        {
            if (ChatSendAudio == null && FindObjectOfType<ChatTest>() != null)
                ChatSendAudio = FindObjectOfType<ChatTest>().GetComponent<AudioSource>();
            if (LevelUpSound == null && GameObject.Find("Xp bar") != null)
                LevelUpSound = GameObject.Find("Xp bar").GetComponent<AudioSource>();
            if (FindObjectOfType<CanvasManager>() != null)
                BtnClickAudio = FindObjectOfType<CanvasManager>().GetComponent<AudioSource>();
            BGM = Camera.main.GetComponent<AudioSource>();
            if (GameObject.Find("ErrorSound") != null)
                ErrorAudio = GameObject.Find("ErrorSound").GetComponent<AudioSource>();
            SFXs = GameObject.FindGameObjectsWithTag("SFX");
        };
    }
    private void Start()
    {
        EazySoundManager.IgnoreDuplicateSounds = false;
    }
    public void CallPlaySound(AudioClip clip, int amount, float Volume,Transform objtransform)
    {
        StartCoroutine(PlaySound(clip, amount, Volume, objtransform));
    }
    private IEnumerator PlaySound(AudioClip clip, int amount,float Volume,Transform objtransform)
    {
        var distanceToPlayer = Vector3.Distance(objtransform.position, SharedObject.g_DataManager.m_MyCharacter.transform.position);
        var soundDelay = distanceToPlayer / 343; //Speed of sound https://en.wikipedia.org/wiki/Speed_of_sound

        //Debug.Log($"Distance to player '{distanceToPlayer}', therefore delayed sound of '{soundDelay}' sec.");

        for (int i = 0; i < amount; i++)
        {
            //Debug.Log($"Play sound: '{clip.name}'");
            int soundId = EazySoundManager.PrepareSound(clip, Random.Range(0.8f, 1.2f), false, objtransform);
            var sound = EazySoundManager.GetSoundAudio(soundId);

            sound.SetVolume(Volume);
            sound.Set3DDistances(dis[0], dis[1]);
            sound.SpatialBlend = 1f;
            sound.Spread = 60f;
            sound.DopplerLevel = 0f;
            sound.Pitch = 1;
            sound.RolloffMode = AudioRolloffMode.Linear;

            //Realistic audio rolloff: https://forum.unity.com/threads/audio-realistic-sound-rolloff-tool.543362/
            var animCurve = new AnimationCurve(
                                                new Keyframe(sound.Min3DDistance, 1f),
                                                new Keyframe(sound.Min3DDistance + (sound.Max3DDistance - sound.Min3DDistance) / 4f, .35f),
                                                new Keyframe(sound.Max3DDistance, 0f));
            animCurve.SmoothTangents(1, .025f);

            //sound.AudioSource.SetCustomCurve(AudioSourceCurveType.CustomRolloff,animCurve);

            StartCoroutine(PlaySound(sound, soundDelay));

            //Attempt to avoid multiple of the same audio being played at the exact same time - as it sounds wierd
            yield return new WaitForSeconds(0.05f);
        }
    }

    private IEnumerator PlaySound(Audio sound, float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);

        sound.Play();
    }

    public void SetBGMOnOff(bool IsOn)
    {
        BGM.mute = !IsOn;
    }
    public void SetSFXOnOff(bool IsOn)
    {
        for (int i = 0; i < SFXs.Length; i++)
        {
            SFXs[i].GetComponent<AudioSource>().mute = !IsOn;
        }
    }
    public void PlayErrorAudio()
    {
        ErrorAudio.Play();
    }
    public void CallPlayLevelUpSound()
    {
        StartCoroutine(PlayLevelUpSound());
    }
    private IEnumerator PlayLevelUpSound()
    {
        BGM.DOFade(.4f, .2f);
        LevelUpSound.Play();
        yield return new WaitForSeconds(.4f);
        BGM.DOFade(1, .2f);

    }  
    public void PlayBtnClickSound()
    {
        if(BtnClickAudio != null)
        BtnClickAudio.Play();
    }
    public void PlayChatSendSound()
    {
        if (ChatSendAudio != null)
            ChatSendAudio.Play();
    }
}
