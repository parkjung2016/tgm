using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class m_Enum
{
    public enum Pet
    {
        BAT,
        SKULL,
        GOLEM,
        GOLEM_UPGRADE,
        GOLEM_UPGRADE2,

    }
    public enum MAPNAME
    { 
        LOBBY,
        DUNGEON1,
        DUNGEON2,
        DUNGEON3,
        BOSS,
    }
    public  enum STAT
    {
        SPEED,
        HP,
        ATTACKSPEED,
        DEF,
       POWER,
       XP,
       LEVELUPXP,
            MAX
    }
    public enum ACTORTYPE
    {
        SHOP,
        BLACKSMITH,
        QUEST,
        PLAYER,
        MONSTER,
        BOSS,
        MANAGER,
        CITIZEN
    }
    public enum MAP
    { 
        MAP1,
        MAP2,
        MAP3,
        MAP4,
        NULL
    }

    public enum ITEMTYPE
    {
       WEAPON,
       HELMET,
       ARMOR,
       LEGGINGS,
       BOOTS,
       POTION
       
    }
    public enum STAT2
    {
        LEVEL,
        MAX
    }
    public enum MAXSTAT
    {
        MAXHP,
        MAXADDITIONALHP,
        MAXMANA,
        MAX
    }
    public enum AI
    {
        AI_RESET,
        AI_SEARCH,
        AI_MOVE,
        AI_ATTACK,
        AI_DIE,
        AI_END
    }
    public enum ADDITIONALSTAT
    {
        ADDITIONALSPEED,
        ADDITIONALHP,
        ADDITIONALATTACKSPEED,
        ADDITIONALDEF,
        ADDITIONALPOWER,

        MAX
    }
    public enum PortalName
    {
        BACK,
        FRONT,
        BOSS
    }
}
