using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;

public class Cheat : MonoBehaviour
{
    [SerializeField]
    private GameObject Onenable;
    private Transform DungeonPortalTrans;
    private bool OnEnableInventoryCheat;
    private Transform LobbyPortalTrans;
    private void Start()
    {
        //gameObject.SetActive(false);
        PlayFabClientAPI.GetAccountInfo(new GetAccountInfoRequest { PlayFabId = SharedObject.g_playerPrefsdata.playfabid }, (GetAccountInfoResult success) =>
         {
                     gameObject.SetActive(success.AccountInfo.PrivateInfo.Email == "bagj21563@gmail.com");
         }, (PlayFabError error) => { });
        Onenable.SetActive(false);
        LobbyPortalTrans = GameObject.FindGameObjectWithTag("StartPlayerPos").transform;
        DungeonPortalTrans = GameObject.FindGameObjectWithTag("PortalFrontPlayerSpawnPos").transform;
    }
    public void CheatEnableClick()
    {
        Onenable.SetActive(!Onenable.activeSelf);
    }
    public void ClickTeleprotFrontDungeon()
    {
        SharedObject.g_DataManager.m_MyCharacter.transform.position = DungeonPortalTrans.position;
    }
    public void ClickTeleprotFrontLobby()
    {
        SharedObject.g_DataManager.m_MyCharacter.transform.position = LobbyPortalTrans.position;
    }
    public void AddMoney()
    {
        SharedObject.g_playerPrefsdata.playerdataNoSave.EarnedGold += 10000;
        SharedObject.g_playerPrefsdata.playerdataNoSave.Gold += 10000;
        CanvasManager.SetGoldText();
    }
    public void AddLevel()
    {
        SharedObject.g_playerPrefsdata.playerdata.m_fStat[(byte)m_Enum.STAT.XP] = SharedObject.g_playerPrefsdata.playerdata.m_fStat[(byte)m_Enum.STAT.LEVELUPXP];
    }
    public void ShowInventoryCheat()
    {
        OnEnableInventoryCheat = !OnEnableInventoryCheat;
        SharedScript.instance.inventory.OnEnableCheatBtn(OnEnableInventoryCheat);
    }
}
