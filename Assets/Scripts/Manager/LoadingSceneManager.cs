using System.Collections;

using UnityEngine;

using UnityEngine.UI;

using UnityEngine.SceneManagement;



public class LoadingSceneManager : MonoBehaviour

{

    public static string nextScene;
    


    [SerializeField]

    private Image progressBar;
    [SerializeField]
    private Text TipText;
    public string[] Tips;


    private void Start()

    {

        StartCoroutine(LoadScene());
        StartCoroutine(ChangeTextName());
        progressBar.fillAmount = 0;
    }



    public static void LoadScene(string sceneName)
    {
        nextScene = sceneName;

        SceneManager.LoadScene("Loading");

    }



    IEnumerator LoadScene()

    {
        yield return null;



        AsyncOperation op = SceneManager.LoadSceneAsync(nextScene);

        op.allowSceneActivation = false;



        float timer = 0.0f;

        while (!op.isDone)

        {

            yield return null;


          
            timer += Time.deltaTime;
            if (op.progress < 0.9f) 
            { 
                progressBar.fillAmount = Mathf.Lerp(progressBar.fillAmount, op.progress, timer)*.005f; 
                if (progressBar.fillAmount >= op.progress) 
                { 
                    timer = 0f; 
                }
            }
            else
            {
                progressBar.fillAmount = Mathf.Lerp(progressBar.fillAmount, 1f, timer*.1f);
                if (progressBar.fillAmount == 1.0f) 
                {
                    op.allowSceneActivation = true; yield break;
                }
            }
        }

    }
    IEnumerator ChangeTextName()
    {
        TipText.text = Tips[Random.Range(0, Tips.Length)];
        yield return new WaitForSeconds(5f);
        StartCoroutine(ChangeTextName());
    }
}
