using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class AutoHuntManager : MonoBehaviour
{
    private int GetGold;
    [SerializeField]
    private Text Gold;
    [SerializeField]
    private GameObject GetGoldTextPrefab;
    [SerializeField]
    private Transform GetGoldTextTrans;
    [SerializeField]
    private Transform Canvas;
    [SerializeField]
    private Text AutoHuntRewardText;
    [SerializeField]
    private GameObject AutoHuntReward;
    int Reward;
    private void Awake()
    {
        Canvas = FindObjectOfType<Canvas>().gameObject.transform;
    }
    private void Start()
    {
        TimeSpan? timeSpan = TimeServer.Instance.ServerTime- SharedObject.g_playerPrefsdata.playerdataNoSave.ServerTime();
        float Power = SharedObject.g_playerPrefsdata.playerdata.m_fStat[(byte)m_Enum.STAT.POWER] + SharedObject.g_playerPrefsdata.playerdataNoSave.m_fAdditionalStat[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALPOWER];
        int index = UnityEngine.Random.Range(0, 3);
        MonsterBase.MonsterType monsterType = (MonsterBase.MonsterType)index;
        List<int> PlayerLevelMin = new List<int>();
        List<int> PlayerLevelMax = new List<int>();
        string[] CheckLevel = new string[SharedScript.instance.csvdata.Monster.Count];
        string[] MonsterName = new string[SharedScript.instance.csvdata.Monster.Count];
        int[] GoldDrops_ = new int[SharedScript.instance.csvdata.Monster.Count];
        int MonsterGetGold = 0;
        for (int i = 0; i < SharedScript.instance.csvdata.Monster.Count; i++)
        {
            CheckLevel[i] = SharedScript.instance.csvdata.Monster[i]["플레이어 레벨"].ToString();
            MonsterName[i] = SharedScript.instance.csvdata.Monster[i]["몬스터 이름"].ToString();
            GoldDrops_[i] = int.Parse(SharedScript.instance.csvdata.Monster[i]["골드드랍"].ToString());
            string[] level = SharedScript.instance.csvdata.Monster[i]["플레이어 레벨"].ToString().Split('~');
            if (!PlayerLevelMin.Contains(int.Parse(level[0])))
            {

                PlayerLevelMin.Add(int.Parse(level[0]));
                if (level[1] != "")
                {
                    PlayerLevelMax.Add(int.Parse(level[1]));

                }
                else
                {
                    PlayerLevelMax.Add(0);

                }
            }
        }
        int CurLevel = SharedObject.g_playerPrefsdata.playerdata.m_nStat2[(byte)m_Enum.STAT2.LEVEL];
        for (int i = 0; i < PlayerLevelMin.Count; i++)
        {
            if (PlayerLevelMax[i] != 0 && CurLevel >= PlayerLevelMin[i] && CurLevel <= PlayerLevelMax[i] || PlayerLevelMax[i] == 0 && CurLevel >= PlayerLevelMin[i])
            {
                for (int a = 0; a < CheckLevel.Length; a++)
                {
                    if (PlayerLevelMax[i] == 0 && CheckLevel[a].Contains(PlayerLevelMin[i].ToString()) || PlayerLevelMax[i] != 0 && CheckLevel[a].Contains(PlayerLevelMin[i].ToString()) && CheckLevel[a].Contains(PlayerLevelMax[i].ToString()))
                    {
                        if (MonsterName[a] == monsterType.ToString())
                        {
                            MonsterGetGold=GoldDrops_[a];

                        }

                    }

                }
            }
        }

        GetGold = Mathf.Clamp( (int)(MonsterGetGold *30*0.01f) +(int)(Power <= 30 ?  Power * 2: Power * 3),0,(int)(MonsterGetGold * 50 * 0.01f));
        if (timeSpan != null)
            Reward = (int)(GetGold * (timeSpan.Value.TotalSeconds / 2));
        else
            Reward = 0;
        if (!SharedObject.g_playerPrefsdata.playerdataNoSave.IsGottenMoneyAutoHunt && Reward != 0)
        {
            AutoHuntReward.SetActive(true);
            AutoHuntRewardText.text = "+"+ Reward + "골드";
        }
        else
            AutoHuntReward.SetActive(false);
        SharedObject.g_playerPrefsdata.playerdata.serverTime = TimeServer.Instance.ServerTime.ToString();

        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
        InvokeRepeating("_GetGold", 2, 2);
        Gold.text =  SharedObject.g_playerPrefsdata.playerdataNoSave.Gold.ToString();
        SharedObject.g_playerPrefsdata.playerdata.IsAutoHunt = true;
        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
    }
    public void GetAutoHuntReward()
    {
        SharedObject.g_playerPrefsdata.playerdataNoSave.EarnedGold += Reward;
        SharedObject.g_playerPrefsdata.playerdataNoSave.Gold += Reward;

        Gold.text =  SharedObject.g_playerPrefsdata.playerdataNoSave.Gold.ToString();
        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
        SharedObject.g_playerPrefsdata.playerdataNoSave.IsGottenMoneyAutoHunt = true;
        AutoHuntReward.SetActive(false);
    }
    public void ExitAutoHunt()
    {


        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i].DOFade(0, 1f);
        }
        FadeCanvas.instance.Fade.gameObject.SetActive(true);
        NetworkManager.instance.DisConnect();
        FadeCanvas.instance.Fade.DOFade(1,1).OnComplete(()=>
        {
            SharedObject.g_playerPrefsdata.playerdata.IsAutoHunt = false;
            SceneManager.LoadScene("Lobby");
        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
        });
    }
    private void OnApplicationQuit()
    {
        if(SharedObject.g_playerPrefsdata.playfabid != "")
        {
        SharedObject.g_playerPrefsdata.playerdata.serverTime = TimeServer.Instance.ServerTime.ToString();
        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);

        }
    }
    private void _GetGold()
    {
      GameObject obj =  Instantiate(GetGoldTextPrefab, GetGoldTextTrans.position + new Vector3(UnityEngine. Random.Range(-100, 100), UnityEngine.Random.Range(-50, 50)), Quaternion.identity) as GameObject;
        obj.transform.SetParent(Canvas);
        obj.transform.SetAsFirstSibling();
        obj.transform.DOLocalMoveY(20, 1);
        Text text = obj.GetComponent<Text>();
        text.text = "+" + GetGold.ToString();
        text.DOFade(0,1.5f).OnComplete(()=>
        {
            Destroy(obj);
        }
        );
        SharedObject.g_playerPrefsdata.playerdataNoSave.EarnedGold += GetGold;
        SharedObject.g_playerPrefsdata.playerdataNoSave.Gold += GetGold;
        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
        Gold.text =  SharedObject.g_playerPrefsdata.playerdataNoSave.Gold.ToString();
    }
}
