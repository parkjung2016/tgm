using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon;

public class DungeonManager : GameManager
{
    public int remainingenemies;
    protected override void Awake()
    {
        NetworkManager.instance.DisConnect();
        base.Awake();

    }
    protected override void Start()
    {
        SkillBtn.SetActive(true);
        Potions.SetActive(true);
        AttackBtn.SetActive(true);
        base.Start();
        remainingenemies = GameObject.FindObjectsOfType<MonsterBase>().Length;
        if(MyScene.instance.DungeonNum == 3)
        {
            if(QuestManager.instance.questList.ContainsKey(19) && !QuestManager.instance.questList[19].questNames[0].Contains("�Ϸ�"))
            {
                QuestManager.instance.NextMainQuest();
            }
            GameObject.Find("BossDoor").SetActive(MyScene.instance.BossGateVisible);
        }
        Init();
    }
    void Init()
    {
        if (SharedObject.g_DataManager.m_MyCharacter == null)
            Invoke("Init", .2f);
        else
        {
            //SharedObject.g_DataManager.m_MyCharacter.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALHP] = SharedObject.g_playerPrefsdata.playerdata.m_fMaxStat[(byte)m_Enum.MAXSTAT.MAXADDITIONALHP];
            Destroy(SharedObject.g_DataManager.m_MyCharacter.GetComponent<PhotonAnimatorView>());
            Destroy(SharedObject.g_DataManager.m_MyCharacter.GetComponent<PhotonRigidbody2DView>());
            Destroy(SharedObject.g_DataManager.m_MyCharacter.GetComponent<PhotonView>());
        }
    }
}
