using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;
using UnityEngine;
using Photon.Pun;

using UnityEngine.SceneManagement;
using Cinemachine;
using DG.Tweening;


public class LobbyManager : GameManager
{
    [SerializeField]
    private GameObject[] TimeLineobjs;
    [SerializeField]
    UnityEngine.Playables.PlayableDirector playableDirector;
    [SerializeField]
    UnityEngine.Timeline.TimelineAsset[] timelineAsset;
    private Canvas[] canvas;
    protected override void Awake()
    {
    
        for (int i = 0; i < (byte)m_Enum.ADDITIONALSTAT.MAX; i++)
        {
            SharedObject.g_playerPrefsdata.playerdataNoSave.m_fAdditionalStat[i] = 0;
        }
        base.Awake();
        for(int i =0; i <TimeLineobjs.Length;i++)
        {
            TimeLineobjs[i].SetActive(false);
        }
    }
  
    public void CallTimeLineBossFade()
    {
        MyScene.instance.BossGateVisible = true;
           QuestManager.instance.NextMainQuest();
        FadeCanvas.instance.Fade.gameObject.SetActive(true);
        FadeCanvas.instance.Fade.DOFade(1, 2f).OnComplete(() =>
        {
            SharedScript.instance.sceneSequenceManager.StopQuestBossTimeLine(playableDirector, canvas, TimeLineobjs);
        Invoke("CallStopQuestBoss", 2f);
        });
    }
    private void CallStopQuestBoss()
    {
            FadeCanvas.instance.Fade.DOFade(0, 0);
        FadeCanvas.instance.Fade.gameObject.SetActive(false);

    }
    public void CallQuestBossPlay()
    {
        SharedScript.instance.sceneSequenceManager.PlayQuestBossTimeLine(playableDirector,timelineAsset[1],canvas,TimeLineobjs);
    }
    protected override void Start()
    {

        if (SharedObject.g_playerPrefsdata.playerdata.IsAutoHunt)
        {
            ChatTest.instance.DisConnectChannel();
            NetworkManager.instance.DisConnect();
            SceneManager.LoadScene("AutoHunt");

        }
        else
        {
            if(!MyScene.instance.PVPRoom)
        NetworkManager.instance.Connect();
            SharedObject.g_playerPrefsdata.playerdataNoSave.IsGottenMoneyAutoHunt = true;
            if (MyScene.instance.PVPRoom)
            {

                UI_PVPSelect uI_PVPSelect = FindObjectOfType<UI_PVPSelect>();
                Interaction interaction = FindObjectOfType<Interaction>();
                uI_PVPSelect.ActiveType(1, true);
                interaction.PVPSelect.SetActive(true);
                interaction.PVPSelect.GetComponent<CanvasGroup>().alpha = 1;
                uI_PVPSelect.InRoomUpdate(true, 0);
                MapSelect[] mapSelects = FindObjectsOfType<MapSelect>();

                //for (int i = 0; i < mapSelects.Length; i++)
                //{
                //    print(i);
                //    print((int)PhotonNetwork.CurrentRoom.CustomProperties["Map"]);
                //    if ((int)PhotonNetwork.CurrentRoom.CustomProperties["Map"] == i)
                //    {
                //        mapSelects[i].SelectMap();
                //        break;
                //    }
                //}
                if (PhotonNetwork.IsMasterClient)
                {
                    uI_PVPSelect.MapSelect_.SetActive(true);
                    uI_PVPSelect.BtnText.text = "시작하기";
                }
                else
                {

                    uI_PVPSelect.BtnText.text = "준비하기";
                }
                //uI_PVPSelect.currentMap = (m_Enum.MAP)(int)PhotonNetwork.CurrentRoom.CustomProperties["Map"];
                Invoke("Test", 1f);
            }
            canvas = FindObjectsOfType<Canvas>();
        }
        SharedObject.g_playerPrefsdata.playerdata.m_fStat[(byte)m_Enum.STAT.HP] = SharedObject.g_playerPrefsdata.playerdata.m_fMaxStat[(byte)m_Enum.MAXSTAT.MAXHP];
       SharedObject.g_playerPrefsdata.playerdataNoSave.Mana = (int)SharedObject.g_playerPrefsdata.playerdata.m_fMaxStat[(byte)m_Enum.MAXSTAT.MAXMANA];
        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
        Potions.SetActive(false);
        AttackBtn.SetActive(false);
        Application.targetFrameRate = 60;
        base.Start();
        Init();
        MyScene.instance.SpawnPlayerNum = 0;
    }
    void Test()
    {

        //PhotonNetwork.NickName = SharedObject.g_DataManager.m_MyCharacter.name;
        MyScene.instance.PVPRoom = false;
    }
    void Init()
    {
        if (SharedObject.g_DataManager.m_MyCharacter == null)
            Invoke("Init", .2f);
        else
        {
        SkillBtn.SetActive(false);
            SharedObject.g_DataManager.m_MyCharacter.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALHP] = SharedObject.g_playerPrefsdata.playerdata.m_fMaxStat[(byte)m_Enum.MAXSTAT.MAXADDITIONALHP];
            Destroy(SharedObject.g_DataManager.m_MyCharacter.GetComponent<PhotonAnimatorView>());
            Destroy(SharedObject.g_DataManager.m_MyCharacter.GetComponent<PhotonRigidbody2DView>());
            Destroy(SharedObject.g_DataManager.m_MyCharacter.GetComponent<PhotonView>());
        }
    }
}
