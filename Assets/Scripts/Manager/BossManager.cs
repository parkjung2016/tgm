using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine;

public class BossManager : GameManager
{
    [SerializeField]
    private AudioSource BossBGM;
    [SerializeField]
    private GameObject[] TimeLineObj;
    static public BossManager instance;
    public PlayableDirector bossTimeLine;
    [SerializeField]
    private TimelineAsset[] timelineAssets;
    [SerializeField]
    private GameObject XpBar;
    public Boss Boss_;
    [SerializeField]
    private GameObject BossCanvas;
    [SerializeField]
    private Animation Border;
    [SerializeField]
    private AnimationClip[] BorderClip;
    private Canvas[] Canvases;
    [SerializeField]
    private GameObject[] ObjsHideOrVisibleAfterTimeLine;
    protected override void Awake()
    {
        base.Awake();
    Canvases = GameObject.FindObjectsOfType<Canvas>();
        if (instance == null)
            instance = this;
        for (int i = 0; i < TimeLineObj.Length; i++)
            TimeLineObj[i].SetActive(false);
    }
    protected override void Start()
    {
        XpBar.SetActive(false);
        SkillBtn.SetActive(true);
        Potions.SetActive(true);
        BossCanvas.SetActive(false);
        ObjsHideOrVisibleAfterTimeLine[0].SetActive(true);
        ObjsHideOrVisibleAfterTimeLine[1].SetActive(false);
        AttackBtn.SetActive(true);
        Boss_ = FindObjectOfType<Boss>();
        Boss_.gameObject.SetActive(false);
        base.Start();
    }
    static public void  PlayBossTimeLine()
    {
       SharedScript.instance.sceneSequenceManager.PlayBossTimeLine(instance.BossBGM, instance.bossTimeLine, instance.Border, instance.timelineAssets[0], instance.BorderClip[0], instance.ObjsHideOrVisibleAfterTimeLine[0], instance.Canvases, instance.TimeLineObj);
        instance.StartCoroutine(instance.hideborder());
        //instance.BossBGM.Play();
        //instance. bossTimeLine.playableAsset = instance. timelineAssets[0];
        //instance.Border.clip = instance.BorderClip[0];
        //instance.Border.gameObject.SetActive(true);
        //instance.ObjsHideOrVisibleAfterTimeLine[0].SetActive(false);
        //for (int i = 0; i < instance.Canvases.Length; i++)
        //{
        //    if (instance.Canvases[i].CompareTag("BorderCanvas"))
        //       continue;
        //    instance.Canvases[i].gameObject.SetActive(false);

        //}
        //SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().MoveRUp(null,null);
        //SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().MoveLUP(null, null);
        //SharedObject.g_DataManager.m_MyCharacter.gameObject.SetActive(false);
        //for (int i = 0; i < instance.TimeLineObj.Length; i++)
        //    instance.TimeLineObj[i].SetActive(true);
        //    instance.TimeLineObj[0].transform.position = SharedObject.g_DataManager.m_MyCharacter.transform.position;
        //instance.bossTimeLine.Play();
    }
    static public void PlayEndTimeLine()
    {
        SharedScript.instance.sceneSequenceManager.PlayBossEndTimeLine(instance.BossBGM, instance.bossTimeLine, instance.Border, instance.timelineAssets[1], instance.BorderClip[0], instance.ObjsHideOrVisibleAfterTimeLine[0], instance.Canvases);
        //instance.BossBGM.Stop();
        //instance.bossTimeLine.playableAsset = instance.timelineAssets[1];
        //instance.Border.clip = instance.BorderClip[0];
        //instance.Border.Play();
        //instance.ObjsHideOrVisibleAfterTimeLine[0].SetActive(false);
        //for (int i = 0; i < instance.Canvases.Length; i++)
        //{
        //    if (instance.Canvases[i].CompareTag("BorderCanvas"))
        //        continue;
        //    instance.Canvases[i].gameObject.SetActive(false);

        //}
        //instance.bossTimeLine.Play();
    }
    IEnumerator hideborder()
    {
        yield return new WaitForSeconds(6.1f);
        Border.gameObject.SetActive(false);
    }

    public void LoadTitle()
    {
        FadeCanvas.instance.gameObject.SetActive(true);
        FadeCanvas.instance.Fade.DOFade(0,0);

        FadeCanvas.instance.Fade.gameObject.SetActive(true);
            FadeCanvas.instance.Fade.DOFade(1, 2f).OnComplete(() =>
        {
            SceneManager.LoadScene("Title");
        });
    }
    public void StopBossTimeLine()
    {
        SharedScript.instance.sceneSequenceManager.StopBossTimeLine(bossTimeLine, Border, BorderClip[0], ObjsHideOrVisibleAfterTimeLine[0], instance.Canvases, instance.TimeLineObj,Boss_,BossCanvas);
        //instance.ObjsHideOrVisibleAfterTimeLine[1].SetActive(true);
        //instance.Border.clip = instance.BorderClip[1];
        //instance.Border.Play();
        //for (int i = 0; i < instance.Canvases.Length; i++)
        //{
        //    if (instance.Canvases[i].CompareTag("BorderCanvas"))
        //        continue;
        //    instance.Canvases[i].gameObject.SetActive(true);
        //}
        //SharedObject.g_DataManager.m_MyCharacter.gameObject.SetActive(true);
        //SharedObject.g_DataManager.m_MyCharacter.gameObject.transform.position = instance.TimeLineObj[0].transform.position;
        //for (int i = 0; i < instance.TimeLineObj.Length; i++)
        //    instance.TimeLineObj[i].SetActive(false);

        //Boss_.gameObject. SetActive(true);
        //BossCanvas.SetActive(true);
        //instance.bossTimeLine.Stop();
    }
    private void Update()
    {
        if (Boss_.PlayingSkill2)
        {
            if (Boss_.Skill2LiveEnemy <= 0)
            {
                Boss_.Skill2LiveEnemy = 0;
                Boss_.PlayingSkill2 = false;
                Boss_.Hp = Boss_.Hp * (1 + 30 * .01f);
                Boss_.bossHpUI.SetProgressBar(Boss_.Hp, Boss_.MaxHp);
                Boss_.gameObject.SetActive(true);
                Boss_.AttackTrue = true;
            }
        }
    }
}
