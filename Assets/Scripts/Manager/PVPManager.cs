using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using System;
using UnityEngine.Experimental.Rendering.Universal;

public class PVPManager : GameManager
{
   private GameObject Canvas;
    public GameObject player;
    PVPCanvas PVPCanvas_;
    public static PVPManager instance;
    public bool Win;
    public Sprite[] PetImages;
    public RuntimeAnimatorController[] Petanimators;
    public RenderPlayer[] renderPlayers= new RenderPlayer[2];
    public bool playerPetTrue;
    public int PlayerPetNum;
    public PlayerPetInfo playerPetInfo;
    public GameObject[] PetPrefabs;
    public GameObject Playerpointlight;
    public GameObject[] TeleportObjects;
    protected override void Awake()
    {
        if (instance == null)
            instance = this;
        PVPCanvas_ = FindObjectOfType<PVPCanvas>();
        base.Awake();
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
        for(int i =0; i <renderPlayers.Length;i++)
        {
            renderPlayers[i] = GameObject.Find("Render" + i).GetComponentInParent<RenderPlayer>();
        }
        for (int i = 0; i < TeleportObjects.Length; i++)
        {
            TeleportObjects[i] = GameObject.Find("PvPPortal" + i);
        }
    }
    protected override void Start()
    {
        base.Start();
        MyScene.instance.PVPPlayerPos = GameObject.Find("PlayerPos " + PhotonNetwork.LocalPlayer.CustomProperties["Team"].ToString()).transform;
        Canvas = Resources.Load<GameObject>("Prefabs/PlayerHPProgressCanvas");
        CheckSpawn();
        RaiseEvent13();
        //Spawn();
        PVPCanvas_.playercount++;
        //if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("CheckPlayer"))
        //{
        //    int checkplayer = (int)PhotonNetwork.CurrentRoom.CustomProperties["CheckPlayer"];
        //PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable { { "CheckPlayer", checkplayer++} });

        //}
        //else
        //{
        //    PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable { { "CheckPlayer", 1 } });
        //}
        //PhotonNetwork.RaiseEvent(41, null, NetworkManager.instance.raiseEvent, SendOptions.SendReliable);
        SetRenderPlayer();
    }
    void SetRenderPlayer()
    {
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("Team0"))
        {
            if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("Pet0"))
            {
                renderPlayers[0].SetPetImage(PetImages[int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["Pet0"].ToString())], Petanimators[int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["Pet0"].ToString())],
                    int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["Pet0"].ToString()) == 0 ? new Vector3(1.2f, 1.2f) : int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["Pet0"].ToString()) == 1 ? new Vector3(1.5f, 1.5f) : new Vector3(0.9f, 0.9f));

            }
        }
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("Team1"))
        {
            if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("Pet1"))
            {
                renderPlayers[1].SetPetImage(PetImages[int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["Pet1"].ToString())], Petanimators[int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["Pet1"].ToString())],
                         int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["Pet1"].ToString()) == 0 ? new Vector3(1.2f, 1.2f) : int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["Pet1"].ToString()) == 1 ? new Vector3(1.5f, 1.5f) : new Vector3(0.9f, 0.9f));

            }
        }
    }
    void CheckSpawn()
    {
       
        if(PVPCanvas_.playercount != 2)
        {
            Invoke("CheckSpawn", .2f);
        }
        else
        {
        Spawn();

        }
    }
    void Spawn()
    {
        if(MyScene.instance.PVPPlayerPos == null)
        {
            Invoke("Spawn", .2f);
            return;
        }
        player = PhotonNetwork.Instantiate("Player/Player", MyScene.instance.PVPPlayerPos.position
, Quaternion.identity);
        Playerpointlight= Instantiate(Resources.Load<GameObject>("Prefabs/PvPPlayerPointLight"), player.transform);
        Playerpointlight.SetActive(false);
        //PhotonTransformView transformView = player.AddComponent<PhotonTransformView>();
        //transformView.m_SynchronizePosition = true;
        //transformView.m_SynchronizeRotation = true;
        //PhotonView pv = player.AddComponent<PhotonView>();
        //pv.OwnershipTransfer = OwnershipOption.Takeover;
        //pv.ObservedComponents = new List<Component>() { transformView };
        //pv.ViewID = PhotonNetwork.AllocateViewID(0);
        //if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == 2)
        //{
        //    player.GetComponent<Player>().PV.RPC("Flip", RpcTarget.All);
        //}
        Invoke("InitPet", .5f);
        SetMyCharacter();

    }
    void InitPet()
    {
        if (playerPetTrue)
        {
            object[] data = new object[] { PhotonNetwork.NickName, PlayerPetNum, playerPetInfo.AttackDelay, playerPetInfo.Damage, playerPetInfo.Equip, playerPetInfo.PetName, (int)playerPetInfo.PetType, playerPetInfo.Upgrade };
            PhotonNetwork.RaiseEvent(100, data, NetworkManager.instance.otherraiseEvent, SendOptions.SendReliable);
        }

    }
    void RaiseEvent13()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length != 2)
        {
            Invoke("RaiseEvent13", .2f);
        }
        else
        {
            player.name = PhotonNetwork.NickName;
            //GameObject otherplayer;
            //for(int i =0; i <players.Length;i++)
            //{
            //    if(players[i] != player)
            //    {
            //        otherplayer = player;
            //    }
            //}
            GameObject player_ = null;
            player_ = GameObject.Find("Player(Clone)");
            if (player_.GetComponentInChildren<PlayerHPProgressBar>() != null)
            {
                return;
            }
            GameObject gameobj = null;
            gameobj = Instantiate(Canvas, player_.transform);
            gameobj.transform.localScale = new Vector3(0.385f, 0.385f, 0.385f);

            gameobj.GetComponent<PlayerHPProgressBar>().Name.text = PhotonNetwork.IsMasterClient? PhotonNetwork.CurrentRoom.CustomProperties["Team1"].ToString() : PhotonNetwork.CurrentRoom.CustomProperties["Team0"].ToString();
            gameobj.GetComponent<PlayerHPProgressBar>().character = player_.GetComponent<Character>();
            //PhotonNetwork.RaiseEvent(13, null, NetworkManager.instance.otherraiseEvent, SendOptions.SendReliable);
            //GameObject gameobj = null;
            //gameobj = Instantiate(Canvas, otherplayer.transform);
            //gameobj.transform.localScale = new Vector3(0.385f, 0.385f, 0.385f);
            //gameobj.GetComponent<PlayerHPProgressBar>().Name.text = PhotonNetwork.NickName;
            //gameobj.GetComponent<PlayerHPProgressBar>().character = otherplayer.GetComponent<Character>();


            //for (int i =0; i < players.Length;i++)
            //{
            //    if(players[i].GetComponentsInChildren<PlayerHPProgressBar>().Length > 1)
            //    {
            //        Destroy(players[i].GetComponentsInChildren<PlayerHPProgressBar>()[players[i].GetComponentsInChildren<PlayerHPProgressBar>().Length - 1].gameObject);
            //    }
            //}
    }
    UIProgressBar.instance.SetHPProgress();
    }
    public void WinPlayer(int Team)
    {
        if (PhotonNetwork.IsConnected && PhotonNetwork.InRoom)
        {
            PhotonNetwork.RaiseEvent(30, Team-1, NetworkManager.instance.raiseEvent, SendOptions.SendUnreliable);
        }
    }


    private void OnEvent(EventData obj)
    {
        if(obj.Code == 30)
        {
            if (!Win)
            {
                Win = true;
                PVPCanvas_.SetPlayerBGColor();
                    PVPCanvas_.StartCoroutine(PVPCanvas_.SetVisibleGameResult(true, (int)obj.CustomData));  


            }

        }
        if(obj.Code == 41)
        {
            PVPCanvas_.playercount++;
        }
        if (obj.Code == 13)
        {
            GameObject player_ = null;
            player_ = GameObject.Find("Player(Clone)");
            if (player_.GetComponentInChildren<PlayerHPProgressBar>() != null)
            {
                return;
            }
            GameObject gameobj = null;
            gameobj = Instantiate(Canvas, player_.transform);
            gameobj.transform.localScale = new Vector3(0.118f, 0.118f, 0.118f);
            gameobj.GetComponent<PlayerHPProgressBar>().Name.text = (string)obj.CustomData;
            gameobj.GetComponent<PlayerHPProgressBar>().character = player_.GetComponent<Character>();
        }

        if (obj.Code == 100)
        {
       
            object[] data = (object[])obj.CustomData;
            //Player players = FindObjectsOfType<Player>();
            //for(int i =0; i < )
            GameObject otherplayer = GameObject.Find("Player(Clone)");
            Transform transform = otherplayer.transform.GetChild(otherplayer.transform.childCount - 2);
            PlayerPet playerPet = Instantiate(PetPrefabs[(int)data[1]], transform).GetComponent<PlayerPet>();
            playerPet.transform.localPosition = Vector3.zero;
            PlayerPetInfo playerPetInfo = new PlayerPetInfo() { AttackDelay = (int)data[2], Damage = (float)data[3], Equip = (bool)data[4], PetName = (string)data[5], PetType = (m_Enum.Pet)(int)data[6], Upgrade = (int)data[7] };
            playerPet.playerPetInfo = playerPetInfo;
            otherplayer.GetComponent<PlayerMove>().playerpet = playerPet;
            //SharedScript.instance.renderPlayer.SetPetImage(playerPet.GetComponent<SpriteRenderer>().sprite, playerPet.anim.runtimeAnimatorController,
            //    playerPet.playerPetInfo.PetType == m_Enum.Pet.BAT ? new Vector2(1.2f, 1.2f) : playerPet.playerPetInfo.PetType == m_Enum.Pet.SKULL ? new Vector2(1.5f, 1.5f) : new Vector2(0.9f, 0.9f));
        }
    }

}
