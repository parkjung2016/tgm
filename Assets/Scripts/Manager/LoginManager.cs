using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;
using System.IO;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using PlayFab;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using PlayFab.ClientModels;


public class LoginManager : MonoBehaviour
{
    [SerializeField]
    private GameObject[] MakeNameUI;
    public InputField[] inputfield;
    public InputField inputfield2;
    [SerializeField]
    private Text[] ErrorText;
    [SerializeField]
    private Sprite[] pwdvisibleImage;
    bool Google = false;
    private void Awake()
    {
    }
    private void Start()
    {
        for(int i =0; i <MakeNameUI.Length;i++)
        {
        MakeNameUI[i].SetActive(false);
        ErrorText[i].gameObject.SetActive(false);

        }
    }
    public void GoTitleScene()
    {
        SceneManager.LoadScene("Title");
    }
    public void MakeName()
    {
        if (!Google)
        {


            if (inputfield[0].text != "" && inputfield[1].text != "" && inputfield[2].text != "")
            {


                var request = new RegisterPlayFabUserRequest { Email = inputfield[0].text, Password = inputfield[2].text, Username = inputfield[1].text, DisplayName = inputfield[1].text };
                PlayFabClientAPI.RegisterPlayFabUser(request, OnLoginSuccess, OnLoginFail);

            }
            else
            {
                StartCoroutine(VisibleErrorText("입력칸을 채워주세요.", 0));
            }
        }
        else
        {
            if(inputfield2.text != "")
            {
                SharedObject.g_playerPrefsdata.playerdata.m_strPlayerName = inputfield2.text;
                SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
                FadeCanvas.instance.Fade.gameObject.SetActive(true);
                //SharedObject.g_playerPrefsdata.Savelogininfo(inputfield);
                PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest() { DisplayName = inputfield2.text }, (result) =>
                {

                }, error => { });
                PlayFabManager.instance.AddLevelToLeaderBoard(SharedObject.g_playerPrefsdata.playerdata.m_nStat2[(byte)m_Enum.STAT2.LEVEL]);
                FadeCanvas.instance.Fade.DOFade(1, 1f).OnComplete(() =>
                {
                    LoadingSceneManager.LoadScene("Lobby");
                });

            }
            else
            {
                StartCoroutine(VisibleErrorText("입력칸을 채워주세요.", 1));
            }
        }
    }
    public void VisiblePasswordText(Toggle toggle)
    {
        if (toggle.isOn)
        {
            toggle.image.sprite = pwdvisibleImage[0];
            inputfield[2].contentType = InputField.ContentType.Password;
        }
        else
        {
            toggle.image.sprite = pwdvisibleImage[1];
            inputfield[2].contentType = InputField.ContentType.Standard;

        }
         

    }
    IEnumerator VisibleErrorText(string text,int num)
    {
        ErrorText[num].text = text;
        ErrorText[num].gameObject.SetActive(true);
        yield return new WaitForSeconds(1);
        ErrorText[num].gameObject.SetActive(false);
    }
    private void OnLoginFail(PlayFabError error)
    {
        if (ErrorText[0].gameObject.activeSelf)
            return;
        switch (error.Error)
        {
            case PlayFabErrorCode.InvalidParams:
                StartCoroutine(VisibleErrorText("계정을 생성 할 수 없습니다.",0));
                break;
            case PlayFabErrorCode.UnknownError:
                StartCoroutine(VisibleErrorText("이미 이 이름을 사용하고 있습니다.",0));
                break;
            case PlayFabErrorCode.UsernameNotAvailable:
                StartCoroutine(VisibleErrorText("이미 이 이름을 사용하고 있습니다.",0));
                break;
            case PlayFabErrorCode.DuplicateUsername:
                StartCoroutine(VisibleErrorText("이미 이 이름을 사용하고 있습니다.",0));
                break;
            case PlayFabErrorCode.DuplicateEmail:
                StartCoroutine(VisibleErrorText("이미 이 이메일 사용하고 있습니다.",0));
                break;
            case PlayFabErrorCode.InvalidEmailAddress:
                StartCoroutine(VisibleErrorText("이미 이 이메일 사용하고 있습니다.",0));
                break;
            case PlayFabErrorCode.EmailAddressNotAvailable:
                StartCoroutine(VisibleErrorText("이미 이 이메일 사용하고 있습니다.",0));
                break;
        }
    }
    private void OnLoginSuccess(RegisterPlayFabUserResult result)
    {
        SharedObject.g_playerPrefsdata.playerdata.m_strPlayerName = inputfield[1].text;
        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
        FadeCanvas.instance.Fade.gameObject.SetActive(true);
        SharedObject.g_playerPrefsdata.Savelogininfo(inputfield);
        PlayFabManager.instance.AddLevelToLeaderBoard(SharedObject.g_playerPrefsdata.playerdata.m_nStat2[(byte)m_Enum.STAT2.LEVEL]);
        FadeCanvas.instance.Fade.DOFade(1, 1f).OnComplete(() =>
        {
            LoadingSceneManager.LoadScene("Lobby");
        });
    }
    public void googlesignup()
    {
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {

                print("google signed in");

                Google_PlayFabLogin();
            }
            else
            {
                print("google failed to authorize your login");
            }

        });
    }

    public void Google_PlayFabLogin()
    {
       PlayGamesPlatform.Instance.RequestServerSideAccess(true, value=> 
       {
           PlayFabClientAPI.LoginWithGooglePlayGamesServices(new LoginWithGooglePlayGamesServicesRequest()
           {
               TitleId = PlayFabSettings.TitleId,
               ServerAuthCode = value,
              CreateAccount = true,
              
           }, (result) =>
           {
               print("Signed In as " + result.PlayFabId);
               for (byte i = 0; i < (byte)m_Enum.STAT.MAX; i++)
                   SharedObject.g_playerPrefsdata.playerdata.m_fStat[i] = float.Parse(SharedScript.instance.csvdata.PlayerSTAT[i]["fSTAT"].ToString());
               for (byte i = 0; i < (byte)m_Enum.STAT2.MAX; i++)
                   SharedObject.g_playerPrefsdata.playerdata.m_nStat2[i] = (int)SharedScript.instance.csvdata.PlayerSTAT[i]["nSTAT"];
               for (byte i = 0; i < (byte)m_Enum.MAXSTAT.MAX; i++)
                   SharedObject.g_playerPrefsdata.playerdata.m_fMaxStat[i] = float.Parse(SharedScript.instance.csvdata.PlayerSTAT[i]["fMAXSTAT"].ToString());
               SharedObject.g_playerPrefsdata.playerdata.SkillPoint = 0;
               SharedObject.g_playerPrefsdata.playerdataNoSave.Mana = (int)SharedObject.g_playerPrefsdata.playerdata.m_fMaxStat[(byte)m_Enum.MAXSTAT.MAXMANA];
               SharedObject.g_playerPrefsdata.playerdata.m_strPlayerName = "Player";
               SharedObject.g_playerPrefsdata.playerdata.serverTime = TimeServer.Instance.ServerTime.ToString();
               SharedObject.g_playerPrefsdata.playerdata.DungeonOpend = new bool[SharedObject.MapNamestr.Length - 1];
               SharedObject.g_playerPrefsdata.playerdataNoSave.EarnedGold += 1000;
               SharedObject.g_playerPrefsdata.playerdataNoSave.Gold += 1000;
               SharedObject.firstconnect = true;
               for (byte i = 0; i < 3; i++)
               {

                   SharedObject.g_playerPrefsdata.playerdata.EquipSkills.Add(new SkillInfo());
                   SharedObject.g_playerPrefsdata.playerdata.EquipSkills[i].skilltype = PlayerSkills.SkillType.Max;
               }
               Google = true;
               MakeNameUI[1].gameObject.SetActive(true);
           }, (error) =>
           {
               print(error.ErrorMessage);
           });
       });
        //Debug.Log("Server Auth Code: " + serverAuthCode);
        //SharedObject.g_playerPrefsdata.iscurrentTypePlayFab = true;
        //PlayFabClientAPI.GetUserData(new GetUserDataRequest() { PlayFabId = SharedObject.})
    }
    public void GuestLogin()
    {
            //SharedObject.g_playerPrefsdata.iscurrentTypePlayFab =false;
        //if (File.Exists(SharedObject.SavePath))
        //{
        //    SharedObject.g_playerPrefsdata.LoadFile(SharedObject.SavePath);
        //    FadeCanvas.instance.Fade.gameObject.SetActive(true);
        //    FadeCanvas.instance.Fade.DOFade(1, 1f).OnComplete(() =>
        //    {
        //        LoadingSceneManager.LoadScene("Lobby");
        //    });
        //}
        //else
        //{

            for (byte i = 0; i < (byte)m_Enum.STAT.MAX; i++)
                SharedObject.g_playerPrefsdata.playerdata.m_fStat[i] = float.Parse(SharedScript.instance.csvdata.PlayerSTAT[i]["fSTAT"].ToString());
            for (byte i = 0; i < (byte)m_Enum.STAT2.MAX; i++)
                SharedObject.g_playerPrefsdata.playerdata.m_nStat2[i] = (int)SharedScript.instance.csvdata.PlayerSTAT[i]["nSTAT"];
            for (byte i = 0; i < (byte)m_Enum.MAXSTAT.MAX; i++)
                SharedObject.g_playerPrefsdata.playerdata.m_fMaxStat[i] = float.Parse(SharedScript.instance.csvdata.PlayerSTAT[i]["fMAXSTAT"].ToString());
            SharedObject.g_playerPrefsdata.playerdata.SkillPoint = 0;
            SharedObject.g_playerPrefsdata.playerdataNoSave.Mana = (int)SharedObject.g_playerPrefsdata.playerdata.m_fMaxStat[(byte)m_Enum.MAXSTAT.MAXMANA];
            SharedObject.g_playerPrefsdata.playerdata.m_strPlayerName = "Player";
            SharedObject.g_playerPrefsdata.playerdata.serverTime = TimeServer.Instance.ServerTime.ToString();
            SharedObject.g_playerPrefsdata.playerdata.DungeonOpend = new bool[SharedObject.MapNamestr.Length-1];
        SharedObject.g_playerPrefsdata.playerdataNoSave.EarnedGold += 1000;
        SharedObject.g_playerPrefsdata.playerdataNoSave.Gold += 1000;
        SharedObject.firstconnect = true;
            for (byte i = 0; i < 3; i++)
            {

                SharedObject.g_playerPrefsdata.playerdata.EquipSkills.Add(new SkillInfo());
                SharedObject.g_playerPrefsdata.playerdata.EquipSkills[i].skilltype = PlayerSkills.SkillType.Max;
            }
        Google = false;
            MakeNameUI[0].gameObject.SetActive(true);
        //}
    }
}
