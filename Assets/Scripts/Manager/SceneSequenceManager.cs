using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class SceneSequenceManager
{
    public void PlayQuestBossTimeLine(PlayableDirector director,TimelineAsset timelineAsset,Canvas[] canvas,GameObject[] timelineobj)
    {
        director.playableAsset = timelineAsset;
        for (int i = 0; i < canvas.Length; i++)
        {
            if (canvas[i].CompareTag("animCanvas"))
                continue;
            canvas[i].gameObject.SetActive(false);

        }
        SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().MoveRUp(null, null);
        SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().MoveLUP(null, null);
        SharedObject.g_DataManager.m_MyCharacter.gameObject.SetActive(false);
        for (int i = 0; i < timelineobj.Length; i++)
            timelineobj[i].SetActive(true);
        director.Play();
    }
    public void StopQuestBossTimeLine(PlayableDirector director,  Canvas[] canvas, GameObject[] timelineobj)
    {
        for (int i = 0; i < canvas.Length; i++)
        {
            if (canvas[i].CompareTag("animCanvas"))
                continue;
            canvas[i].gameObject.SetActive(true);

        }
        SharedObject.g_DataManager.m_MyCharacter.gameObject.SetActive(true);
        for (int i = 0; i < timelineobj.Length; i++)
            timelineobj[i].SetActive(false);
        director.Stop();
    }
    public void PlayBossTimeLine(AudioSource audio,PlayableDirector director,Animation anim,TimelineAsset timelineAsset,AnimationClip clip,GameObject obj,Canvas[] canvas,GameObject[] timelineobj)
    {
        audio.Play();
        director.playableAsset =timelineAsset;
        anim.clip = clip;
        anim.gameObject.SetActive(true);
        anim.Play();
        obj.SetActive(false);
        for (int i = 0; i < canvas.Length; i++)
        {
            if (canvas[i].CompareTag("animCanvas"))
                continue;
            canvas[i].gameObject.SetActive(false);

        }
        SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().MoveRUp(null, null);
        SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().MoveLUP(null, null);
        SharedObject.g_DataManager.m_MyCharacter.gameObject.SetActive(false);
        for (int i = 0; i < timelineobj.Length; i++)
            timelineobj[i].SetActive(true);
        timelineobj[0].transform.position = SharedObject.g_DataManager.m_MyCharacter.transform.position;
        director.Play();
    }
    public void PlayBossEndTimeLine(AudioSource audio, PlayableDirector director, Animation anim, TimelineAsset timelineAsset, AnimationClip clip, GameObject obj, Canvas[] canvas)
    {
        audio.Stop();
        director.playableAsset = timelineAsset;
        anim.gameObject.SetActive(true);
        anim.clip = clip;
        anim.Play();
        obj.SetActive(false);
        for (int i = 0; i < canvas.Length; i++)
        {
            if (canvas[i].CompareTag("animCanvas"))
                continue;
            canvas[i].gameObject.SetActive(false);

        }
        director.Play();
    }
   public void StopBossTimeLine( PlayableDirector director, Animation anim, AnimationClip clip, GameObject obj, Canvas[] canvas, GameObject[] timelineobj,Boss Boss_,GameObject BossCanvas)
    {
        obj.SetActive(true);
        anim.clip = clip;
        anim.Play();
        for (int i = 0; i < canvas.Length; i++)
        {
            if (canvas[i].CompareTag("animCanvas"))
                continue;
            canvas[i].gameObject.SetActive(true);
        }
        SharedObject.g_DataManager.m_MyCharacter.gameObject.SetActive(true);
        SharedObject.g_DataManager.m_MyCharacter.gameObject.transform.position = timelineobj[0].transform.position;
        for (int i = 0; i < timelineobj.Length; i++)
            timelineobj[i].SetActive(false);

        Boss_.gameObject.SetActive(true);
        BossCanvas.SetActive(true);
        director.Stop();
    }
}
