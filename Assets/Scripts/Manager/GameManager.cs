using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using DG.Tweening;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    ColorAdjustments colorAdjustments;
    AudioSource Audio;
    public Volume volume;
    public GameObject AttackBtn;
    public GameObject Potions;
    public GameObject SkillBtn;
    public ParticleSystem Rain;
    protected virtual void Awake()
    {
        volume = FindObjectOfType<Volume>();
        Potions = GameObject.Find("PotionBtn");
        SkillBtn = GameObject.Find("SkillBtn");
        AttackBtn = GameObject.Find("AttackBtn");
        SharedObject.g_playerPrefsdata.playerdata.m_fMaxStat[1] = 0;

        if (GameObject.FindGameObjectWithTag("Weather") != null)
        {
        Rain = GameObject.FindGameObjectWithTag("Weather").GetComponent<ParticleSystem>();
            Audio = Rain.GetComponent<AudioSource>();
        StartCoroutine(PlayRainEffect());
        }
    }

    IEnumerator PlayRainEffect()
    {
        if (Rain == null)
            yield return null;
        WaitForSeconds wait = new WaitForSeconds(5);
        if (Random.Range(1,100) <= 40)
        {
            if (Rain.isPlaying)
            {
            yield return wait;
            StartCoroutine(PlayRainEffect());

            }
            else
            {
                if (volume.profile.TryGet<ColorAdjustments>(out colorAdjustments))
                {
                    DOTween.To(() => colorAdjustments.colorFilter.value, x => colorAdjustments.colorFilter.value = x, new Color(.5f,.5f,.5f), 1);

                }
                Audio.volume = 0;
                Audio.Play();
                Rain.Play();
                Audio.DOFade(1,.5f);
                yield return new WaitForSeconds(Random.Range(10, 15));
                if (volume.profile.TryGet<ColorAdjustments>(out colorAdjustments))
                {
                    DOTween.To(() => colorAdjustments.colorFilter.value, x => colorAdjustments.colorFilter.value = x, new Color(255 / 255, 255 / 255, 255 / 255), 1);
                }
                Rain.Stop();
                Audio.DOFade(0,.5f);
            }
        }
        else
        {
            yield return wait;
            StartCoroutine(PlayRainEffect());
        }
    }
   public void SetMyCharacter()
    {
        float[] fmax = SharedObject.g_playerPrefsdata.playerdata.m_fMaxStat;
        float[] f = SharedObject.g_playerPrefsdata.playerdata.m_fStat;
        int[] n = SharedObject.g_playerPrefsdata.playerdata.m_nStat2;
        string name = SharedObject.g_playerPrefsdata.playerdata.m_strPlayerName;
        float SkillPoint = SharedObject.g_playerPrefsdata.playerdata.SkillPoint;
        PlayerMove[] players = FindObjectsOfType<PlayerMove>();
        for(int i =0;i< players.Length;i++)
        {
            if(MyScene.instance.PVPRoom  || players[i].PV != null && players[i].PV.IsMine)
            {
        SharedObject.g_DataManager.SetMyCharacter(players[i].character, f, n, fmax, name,SkillPoint);
                break;
            }
        }
        if(SharedObject.g_DataManager.m_MyCharacter == null)
        {
            Invoke("SetMyCharacter", .1f);
        }
    }
    protected virtual void Start()
    {
        SetMyCharacter();
        StartCoroutine(SetSpawnPlayerPos());
    }
    IEnumerator SetSpawnPlayerPos()
    {
        if(!PhotonNetwork.InRoom)
        {
        MyScene.instance.SpawnPlayerPos[0] = GameObject.FindGameObjectWithTag("StartPlayerPos").transform;
        MyScene.instance.SpawnPlayerPos[1] = GameObject.FindGameObjectWithTag("PortalFrontPlayerSpawnPos").transform;

        }
        if (GameObject.FindGameObjectWithTag("Player") == null)
        {
            yield return new WaitForSeconds(.2f);

        StartCoroutine(SetSpawnPlayerPos());

        }
        else
        {
        MyScene.instance.PlayerPos = GameObject.FindGameObjectWithTag("Player").transform;
        }
        yield return null;
    }
    public void SetBGMOnoff(Toggle toggle)
    {
        SharedObject.g_SoundManager.SetBGMOnOff(toggle.isOn);
    }
    public void SetSFXOnoff(Toggle toggle)
    {
        SharedObject.g_SoundManager.SetSFXOnOff(toggle.isOn);
  
    }
}
