using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Photon.Realtime;
using Photon.Pun;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using System;

public class PVPCanvas : MonoBehaviour
{
    [SerializeField]
    private Text[] PlayerNames;
    [SerializeField]
    private CanvasGroup WaitUI;
    [SerializeField]
    private Text Notice;
    public int playercount
    {
        set { PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable { { "CheckPlayer", value } }); }
        get { return (int)PhotonNetwork.CurrentRoom.CustomProperties["CheckPlayer"]; }
    }
    [SerializeField]
    private Image GameResult;
    [SerializeField]
    private Sprite[] GameResultSprites;
    [SerializeField]
    private CanvasGroup GameResultGroup;
    [SerializeField]
    private Text[] NickNames;
    [SerializeField]
    private Image[] GameResults;
    [SerializeField]
    private Image[] PlayerBG;
    [SerializeField]
    private Color[] PlayerBgColors;
    private void Awake()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }
    
    public IEnumerator SetVisibleGameResult(bool visible, int Num)
    {

        GameResult.gameObject.SetActive(visible);
        GameResultGroup.gameObject.SetActive(visible);
        if(visible)
        {
        GameResult.sprite = GameResultSprites[Num+1 == (int)PhotonNetwork.LocalPlayer.CustomProperties["Team"] ? 0 : 1];
            for(int i =0; i < NickNames.Length;i++)
            {
                NickNames[i].text = PhotonNetwork.CurrentRoom.CustomProperties["Team"+i].ToString();
            }

            GameResults[Num].sprite = GameResultSprites[0];
            GameResults[Num == 1 ? 0 : 1].sprite = GameResultSprites[1];
 
        yield return new WaitForSeconds(1);
        GameResult.DOFade(0, .5f);
        GameResultGroup.DOFade(1, .5f);

        }
        else
        {
            GameResult.DOFade(1,0f);
            GameResultGroup.DOFade(0, 0);
        }

    }
    public void SetPlayerBGColor()
    {
        for(int i =0; i <PlayerBG.Length;i++ )
        {
            if(i == (int)PhotonNetwork.LocalPlayer.CustomProperties["Team"]-1)
            {
                PlayerBG[i].color = PlayerBgColors[0];
            }
            else
            {
                PlayerBG[i].color = PlayerBgColors[1];

            }
        }


    }
    public void GoRoom()
    {
        if(PhotonNetwork.IsMasterClient)
        PhotonNetwork.CurrentRoom.IsOpen = true;
            SharedObject.g_SoundManager.PlayBtnClickSound();
            MyScene.instance.PVPRoom = true;
            FadeCanvas.instance.Fade.gameObject.SetActive(true);
            FadeCanvas.instance.Fade.DOFade(1, .5f).OnComplete(() =>
            {
                LoadingSceneManager.LoadScene("Lobby");

            });
        PhotonNetwork.Destroy(PVPManager.instance.player);

    }
    private void OnEvent(EventData obj)
    {
        if(obj.Code == 20)
        {
            if(CheckPlayer() != null)
            StartCoroutine(CheckPlayer());
        }
    }

    private void Start()
    {
       StartCoroutine( SetVisibleGameResult(false, 0));
        WaitUI.alpha = 1;
        WaitUI.gameObject.SetActive(true);
        StartCoroutine(CheckPlayer());
        //if (PhotonNetwork.IsMasterClient)
        //{
        //    PhotonNetwork.RaiseEvent(20,null, NetworkManager.instance.raiseEvent, SendOptions.SendReliable);

        //}

            PlayerNames[0].text = PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("Team0") ?PhotonNetwork.CurrentRoom.CustomProperties["Team0"].ToString() : "";
            PlayerNames[1].text = PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("Team1") ?PhotonNetwork.CurrentRoom.CustomProperties["Team1"].ToString():"";
    }
    //void SetPlayerNameText()
    //{
    //    if()
    //}
    IEnumerator CheckPlayer()
    {

        if(playercount != 2)
        {
            yield return new WaitForSeconds(.2f);
            StartCoroutine(CheckPlayer());
        }
        else
        {
            yield return new WaitForSeconds(2f);
            Notice.text = "3초뒤에 전투가 시작됩니다.";
            WaitForSeconds ws = new WaitForSeconds(1);
            yield return ws;
            Notice.text = "3";
            yield return ws;
            Notice.text = "2";
            yield return ws;
            Notice.text = "1";
            yield return ws;
            Notice.text = "시작";
            WaitUI.DOFade(0, 1).OnComplete(()=>WaitUI.gameObject.SetActive(false));
        }
    }
}
