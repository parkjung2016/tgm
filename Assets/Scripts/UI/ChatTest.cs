﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Chat;
using Photon.Realtime;
using Photon.Pun;
using AuthenticationValues = Photon.Chat.AuthenticationValues;
using ExitGames.Client.Photon;

public class ChatTest : MonoBehaviour, IChatClientListener {

	public static ChatTest instance;
	[SerializeField]
	private GameObject[] ClickOrNotObj;
	[SerializeField]
	private RectTransform[] ClickOrNotTrans;
	[SerializeField]
	private RectTransform ScrollView;
	private ChatClient chatClient;
	private string userName;
	private string currentChannelName = "";
	[SerializeField]
	private GameObject ExitBtn; 

	public InputField inputField;
	public Text outputText;
	UI_PVPSelect uI_PVP;
	private void Awake()
    {
		if (instance == null)
			instance = this;
		uI_PVP = FindObjectOfType<UI_PVPSelect>();

			chatClient = new ChatClient(this);
	}
    public void ConnectCnannel()
    {
		currentChannelName = "Channel " + PhotonNetwork.CurrentRoom.Name;
		if (chatClient.State != ChatState.ConnectedToFrontEnd )
        {
			Invoke("ConnectCnannel", .2f);

        }
		else
        {
		chatClient.Subscribe(new string[] { currentChannelName }, 10);
        }
	}
	public void ReJoinChat()
    {
		currentChannelName = "Channel " + PhotonNetwork.CurrentRoom.Name;
		chatClient.Subscribe(new string[] { currentChannelName },10);
    }
    private void Start()
	{
		if (SharedObject.g_DataManager.m_MyCharacter == null || SharedObject.g_DataManager.m_MyCharacter != null && SharedObject.g_DataManager.m_MyCharacter.name == "")
			Invoke("Start", .2f);
		else
		{
			OnClickedChat(false);
			Application.runInBackground = true;
			userName = SharedObject.g_DataManager.m_MyCharacter.Id;

			if (chatClient != null)
				chatClient.Connect(ChatSettings.Instance.AppId, "1.0", new AuthenticationValues(userName));

		}
	}

    // Use this for initialization
    public void OnClickedChat(bool Enable)
    {
		uI_PVP.BtnText.transform.parent.gameObject.SetActive(!Enable);

			ClickOrNotObj[Convert.ToInt32(!Enable)].SetActive(false);
			ClickOrNotObj[Convert.ToInt32(Enable)].SetActive(true);
		ExitBtn.SetActive(!Enable);
		if(Enable)
        {
			ScrollView.position = ClickOrNotTrans[1].position;
			outputText.fontSize = 200;
			ScrollView.sizeDelta = ClickOrNotTrans[1].sizeDelta;
        }
		else
        {
			outputText.fontSize = 150;
			ScrollView.position = ClickOrNotTrans[0].position;
			ScrollView.sizeDelta = ClickOrNotTrans[0].sizeDelta;

		}
    }
	public void AddLine(string lineString)
	{
		outputText.text += lineString + "\r\n";
	}

	public void OnApplicationQuit ()
	{
		DisConnectChannel();
	}
	public void DisConnectChannel()
    {
		if (chatClient != null)
		{
			chatClient.Disconnect();
		}
	}
	public void UnsubscribedChat()
    {
		if(outputText.text != null)
		outputText.text = "";
    }
	public void DebugReturn (ExitGames.Client.Photon.DebugLevel level, string message)
	{
	}

	public void OnConnected ()
	{
	}

	public void OnDisconnected ()
	{
		AddLine ("서버에 연결이 끊어졌습니다.");
	}

	public void OnChatStateChange (ChatState state)
	{
	}

	public void OnSubscribed(string[] channels, bool[] results)
	{
	}

	public void OnUnsubscribed(string[] channels)
	{
		outputText.text = "";
	}

	public void OnGetMessages(string channelName, string[] senders, object[] messages)
	{
		for (int i = 0; i < messages.Length; i++)
		{
			AddLine (string.Format("{0} : {1}", senders[i], messages[i].ToString()));
		}
	}

	public void OnPrivateMessage(string sender, object message, string channelName)
	{
	}

	public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
	{
	}

	void Update ()
	{
		if(chatClient != null)
		chatClient.Service();
	}

	public void Input_OnEndEdit (string text)
	{
		if (chatClient.State == ChatState.ConnectedToFrontEnd && inputField.text != "")
		{
			SharedObject.g_SoundManager.PlayChatSendSound();
			//chatClient.PublishMessage(currentChannelName, text);
			chatClient.PublishMessage(currentChannelName, inputField.text);

			inputField.text = "";
		}
	}

    public void OnUserSubscribed(string channel, string user)
    {

	}

    public void OnUserUnsubscribed(string channel, string user)
    {

    }
}
