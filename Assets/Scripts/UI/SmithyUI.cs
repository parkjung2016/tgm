using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;

public class SmithyUI : MonoBehaviour
{
    CanvasGroup canvasGroup;
    [SerializeField]
    private Scrollbar[] scrollbar_;
    [SerializeField]
    private Transform[] ItemPrefabTrans;
    [SerializeField]
    private GameObject[] ItemGroups;
    [SerializeField]
    private Text ItemType;
    [SerializeField]
    private GameObject NoItem;
    [SerializeField]
    private GameObject ItemInfo;
    [SerializeField]
    private GameObject CheckUpgrade;
    Coroutine savecoroutine;
    [SerializeField]
    private List<GameObject> m_WeaponList = new List<GameObject>();
    private List<GameObject> m_HelmetList = new List<GameObject>();
    private List<GameObject> m_ArmorList = new List<GameObject>();
    private List<GameObject> m_LeggingsList = new List<GameObject>();
    private List<GameObject> m_BootsList = new List<GameObject>();
    private List<GameObject> m_WeaponList_Inventory = new List<GameObject>();
    private List<GameObject> m_HelmetList_Inventory = new List<GameObject>();
    private List<GameObject> m_ArmorList_Inventory = new List<GameObject>();
    private List<GameObject> m_LeggingsList_Inventory = new List<GameObject>();
    private List<GameObject> m_BootsList_Inventory = new List<GameObject>();
    [SerializeField]
    private GameObject LackOfMoneyText;
    [SerializeField]
    private ItemInfo iteminfo;
    private ItemInfo iteminfo_Inventory;
    [SerializeField]
    private Text ReinforcedPrice;
    [SerializeField]
    private Text[] itemData;
    [SerializeField]
    private Text[] itemValues;
    [SerializeField]
    private Text[] CheckitemUpgradeValues;
    [SerializeField]
    private Text[] CheckitemUpgradeData;
    [SerializeField]
    private Text GoldText;
    private int CurScrollNum;
    [SerializeField]
    private GameObject  UpgradeBtn;
    [SerializeField]
    private Text SelectItemInfoName;
    [SerializeField]
    private Text SelectItemInfoDescription;
    bool _scrollbarreset;
    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        CancelUpgrade();
        ItemInfo.SetActive(false);
    }
    void Start()
    {
        ResetAnim();
    }
    private void OnEnable()
    {
        if(SharedObject.g_DataManager.m_MyCharacter != null)
        GoldText.text = SharedObject.g_playerPrefsdata.playerdataNoSave.Gold.ToString();
    }
    public void HideUI()
    {
        canvasGroup.DOFade(0, .5f).OnComplete(() =>
        {
            ItemInfo.SetActive(false);
            gameObject.SetActive(false);
        });
    }
    public void ResetAnim()
    {
        VisibleItemInfo(false);
        canvasGroup.alpha = 0;
        LackOfMoneyText.SetActive(false);
        gameObject.SetActive(false);
    }
    public void ShowItemGroups(int CurrentScrollNum)
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        for (int i = 0; i < ItemGroups.Length; i++)
            ItemGroups[i].SetActive(false);
        CurScrollNum = CurrentScrollNum;
        VisibleItemInfo(false);
        ItemGroups[CurrentScrollNum].SetActive(true);
        ItemType.text = SharedScript.instance.StringBuilder(SharedScript.instance.ItemType[CurrentScrollNum]);
        switch (CurrentScrollNum)
        {
            case 0:
        ResetItem(m_WeaponList,m_WeaponList_Inventory);
                    NoItem.SetActive(SharedScript.instance.inventory.m_WeaponList.Count == 0);
                for (int i = 0; i < SharedScript.instance.inventory.m_WeaponList.Count; i++)
                {
                            ItemAdd(m_WeaponList_Inventory, SharedScript.instance.inventory.m_WeaponList[i].GetComponent<Item>().itemInfo.itemdata, CurrentScrollNum, m_WeaponList, SharedScript.instance.inventory.m_WeaponList, i, _scrollbarreset);
                }
                    break;
            case 1:
                ResetItem(m_HelmetList,m_HelmetList_Inventory);
                NoItem.SetActive(SharedScript.instance.inventory.m_HelmetList.Count == 0);
                for (int i = 0; i < SharedScript.instance.inventory.m_HelmetList.Count; i++)
                {
                    ItemAdd(m_HelmetList_Inventory,SharedScript.instance.inventory.m_HelmetList[i].GetComponent<Item>().itemInfo.itemdata, CurrentScrollNum, m_HelmetList, SharedScript.instance.inventory.m_HelmetList, i, _scrollbarreset);
                }
                break;
            case 2:
                ResetItem(m_ArmorList,m_ArmorList_Inventory);
                NoItem.SetActive(SharedScript.instance.inventory.m_ArmorList.Count == 0);
                for (int i = 0; i < SharedScript.instance.inventory.m_ArmorList.Count; i++)
                {
                    ItemAdd(m_ArmorList_Inventory,SharedScript.instance.inventory.m_ArmorList[i].GetComponent<Item>().itemInfo.itemdata, CurrentScrollNum, m_ArmorList, SharedScript.instance.inventory.m_ArmorList,i,_scrollbarreset);
                }
                break;
            case 3:
                ResetItem(m_LeggingsList,m_LeggingsList_Inventory);
                NoItem.SetActive(SharedScript.instance.inventory.m_LeggingsList.Count == 0);
                for (int i = 0; i < SharedScript.instance.inventory.m_LeggingsList.Count; i++)
                {
                    ItemAdd(m_LeggingsList_Inventory,SharedScript.instance.inventory.m_LeggingsList[i].GetComponent<Item>().itemInfo.itemdata, CurrentScrollNum, m_LeggingsList, SharedScript.instance.inventory.m_LeggingsList,i,_scrollbarreset);
                }
                break;
            case 4:
                ResetItem(m_BootsList,m_BootsList_Inventory);
                NoItem.SetActive(SharedScript.instance.inventory.m_BootsList.Count == 0);
                for (int i = 0; i < SharedScript.instance.inventory.m_BootsList.Count; i++)
                {
                    ItemAdd(m_BootsList_Inventory ,SharedScript.instance.inventory.m_BootsList[i].GetComponent<Item>().itemInfo.itemdata, CurrentScrollNum,m_BootsList, SharedScript.instance.inventory.m_BootsList,i,_scrollbarreset);
                }
                break;
                
        }
        _scrollbarreset = false;
    }
    public void VisibleItemInfo(bool visible)
    {

        ItemInfo.SetActive(visible);
    }
    public void ShowItemInfo(ItemInfo _iteminfo,Sprite sprite)
    {
        VisibleItemInfo(true);
        switch (CurScrollNum)
        {
            case 0:
        iteminfo.itemdata = m_WeaponList_Inventory[ m_WeaponList.IndexOf( _iteminfo.gameObject)].GetComponent<ItemInfo>().itemdata;
                iteminfo_Inventory = m_WeaponList_Inventory[m_WeaponList.IndexOf(_iteminfo.gameObject)].GetComponent<ItemInfo>();
                break;
            case 1:
                iteminfo.itemdata = m_HelmetList_Inventory[m_HelmetList.IndexOf(_iteminfo.gameObject)].GetComponent<ItemInfo>().itemdata;
                iteminfo_Inventory = m_HelmetList_Inventory[m_HelmetList.IndexOf(_iteminfo.gameObject)].GetComponent<ItemInfo>();
                break;
            case 2:
                iteminfo.itemdata = m_ArmorList_Inventory[m_ArmorList.IndexOf(_iteminfo.gameObject)].GetComponent<ItemInfo>().itemdata;
                iteminfo_Inventory = m_ArmorList_Inventory[m_ArmorList.IndexOf(_iteminfo.gameObject)].GetComponent<ItemInfo>();
                break;
            case 3:
                iteminfo.itemdata = m_LeggingsList_Inventory[m_LeggingsList.IndexOf(_iteminfo.gameObject)].GetComponent<ItemInfo>().itemdata;
                iteminfo_Inventory = m_LeggingsList_Inventory[m_LeggingsList.IndexOf(_iteminfo.gameObject)].GetComponent<ItemInfo>();
                break;
            case 4:
                iteminfo.itemdata = m_BootsList_Inventory[m_BootsList.IndexOf(_iteminfo.gameObject)].GetComponent<ItemInfo>().itemdata;
                iteminfo_Inventory = m_BootsList_Inventory[m_BootsList.IndexOf(_iteminfo.gameObject)].GetComponent<ItemInfo>();
                break;

        }
                iteminfo.weaponImg.sprite = sprite;
        iteminfo.ResetInfo();
        ResetCheckBuyitemInfoText();
    }
    private void ResetCheckBuyitemInfoText()
    {
        if (iteminfo == null)
            return;
        ReinforcedPrice.text = iteminfo.itemdata.ReinforcedPrice.ToString();
        iteminfo.ResetInfo();
        GoldText.text = SharedObject.g_playerPrefsdata.playerdataNoSave.Gold.ToString();
        for (byte i = 0; i < itemData.Length; i++)
        {
            itemData[i].text = iteminfo.itemdata.Data[i] + ":";
            itemValues[i].text = iteminfo.itemdata.Values[i].ToString();
        }
        SelectItemInfoDescription.text = iteminfo.itemdata.Description;
        SelectItemInfoName.text = iteminfo.itemdata.Name;
    }
    public void UpgradeItem()
    {
        if (iteminfo.itemdata.Level > 4)
        {
            iteminfo.itemdata.Level = 5;
            return;
        }
        int RemoveMoeny = int.Parse(SharedScript.instance.csvdata.ItemUpgradeValue[iteminfo.itemdata.Level + SharedScript.instance.csvdata.ItemTypeNum[iteminfo.itemdata.rating]]["Gold"].ToString());
            if (SharedObject.g_playerPrefsdata.playerdataNoSave.Gold < RemoveMoeny)
        {
           StartCoroutine( lackofmoney());
        }
        else
        {
            CancelUpgrade();
            for (int i = 0; i < iteminfo.itemdata.Values.Length; i++)
            {
                iteminfo.itemdata.Values[i] += float.Parse(SharedScript.instance.csvdata.ItemUpgradeValue[iteminfo.itemdata.Level +SharedScript.instance.csvdata.ItemTypeNum[iteminfo.itemdata.rating]][i == 0 ? SharedScript.instance.csvdata.ItemUpgradeType[(byte)iteminfo.itemdata.m_ITEMTYPE] : SharedScript.instance.csvdata.ItemUpgradeType[(byte)iteminfo.itemdata.m_ITEMTYPE] + "2"].ToString());

            }
            PlayFabClientAPI.SubtractUserVirtualCurrency(new SubtractUserVirtualCurrencyRequest() { VirtualCurrency = "GD", Amount = RemoveMoeny }, result => {  }, error => { });
            SharedObject.g_playerPrefsdata.playerdataNoSave.Gold -= RemoveMoeny;
            iteminfo.itemdata.Level++;
            _scrollbarreset = true;
            ShowItemGroups(CurScrollNum);
            iteminfo_Inventory.ResetInfo();
            iteminfo.ResetInfo();
            if(savecoroutine == null)
           savecoroutine = StartCoroutine(save()); 
        }
        ResetCheckBuyitemInfoText();
    }
    IEnumerator save()
    {
        yield return new WaitForSeconds(3);
        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
        savecoroutine = null;
    }
    IEnumerator lackofmoney()
    {
        SharedObject.g_SoundManager.PlayErrorAudio();
        LackOfMoneyText.SetActive(true);
        yield return new WaitForSeconds(.5f);
        LackOfMoneyText.SetActive(false);
    }
    public void CheckUpgrade_()
    {

        CheckUpgrade.SetActive(true);
        if (iteminfo.itemdata.Level > 4)
        {
            for (int i = 0; i < CheckitemUpgradeValues.Length; i++)
            {
                CheckitemUpgradeData[i].text = "";
                CheckitemUpgradeValues[i].text = "";
                UpgradeBtn.SetActive(false);
            }
            return;
        }
                UpgradeBtn.SetActive(true);
        for (int i = 0; i < CheckitemUpgradeValues.Length; i++)
        {
            CheckitemUpgradeData[i].text = iteminfo.itemdata.Data[i].ToString();
            CheckitemUpgradeValues[i].text = (iteminfo.itemdata.Values[i] + float.Parse(SharedScript.instance.csvdata.ItemUpgradeValue[iteminfo.itemdata.Level + SharedScript.instance.csvdata.ItemTypeNum[iteminfo.itemdata.rating]][i == 0 ? SharedScript.instance.csvdata.ItemUpgradeType[(byte)iteminfo.itemdata.m_ITEMTYPE] : SharedScript.instance.csvdata.ItemUpgradeType[(byte)iteminfo.itemdata.m_ITEMTYPE] + "2"].ToString())).ToString();
        }
    }
    public void CancelUpgrade()
    {
        CheckUpgrade.SetActive(false);
    }
    public void ItemRemove(List<GameObject> list, int ListNum)
    {

        if (PlayerHPManager.instance.character.Death)
            return;
        Destroy(list[ListNum]);
        list.Remove(list[ListNum]);
        return;

    }
    public void ResetItem(List<GameObject> list,List<GameObject> list_Inventory)
    {

        if (PlayerHPManager.instance.character.Death)
            return;
        for(int i =0; i < list.Count;i++)
        {

        Destroy(list[i].gameObject);
            
        }
        list.Clear();
        list_Inventory.Clear();

        return;

    }
    public void ItemAdd(List<GameObject> list_Inventory, PlayerPrefsData.ItemData itemInfo_,int CurrentScrollNum,List<GameObject> list,List<GameObject> CheckList,int ListNum,bool scrollbarreset)
    {
            if (PlayerHPManager.instance.character.Death || itemInfo_.Equip)
            return;
        GameObject obj = Instantiate(SharedScript.instance.inventory.ItemPrefab[CurrentScrollNum], Vector2.zero, Quaternion.identity, ItemPrefabTrans[CurrentScrollNum]) as GameObject;
        list.Add(obj);
        list_Inventory.Add(CheckList[ListNum]);
        ItemInfo itemInfo = obj.GetComponent<ItemInfo>();
        if(scrollbarreset)
        scrollbar_[CurrentScrollNum].value = 1;
        itemInfo.itemdata = itemInfo_;
        obj.AddComponent<Item_Smithy>();
        Destroy(obj.GetComponent<Item>());
        itemInfo.ResetInfo();
        obj.transform.localScale = Vector2.one;
        return;

    }
}
