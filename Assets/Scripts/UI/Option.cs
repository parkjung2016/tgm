using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using DG.Tweening;
using UnityEngine.Experimental.Rendering.Universal;

public class Option : MonoBehaviour
{
    [SerializeField]
    private Text UserIdText;
    [SerializeField]
    private Text GameVersionText;
    [SerializeField]
    private Toggle ShadowBtn;
    [SerializeField]
    private GameObject[] GraphicQuality;
    [SerializeField]
    private Image[] GraphicQualityBtnBg;
    [SerializeField]
    private Image[] GraphicQualityBtnOutLine;
    [SerializeField]
    private Color[] GraphicQualityBtnColors;
    [SerializeField]
    private Color[] GraphicQualityBtnSelectColors;
    [SerializeField]
    private GameObject ChangedText;

    private bool Init;
    int GraphicNum 
        {
        set { SharedObject.g_playerPrefsdata.playerdata.GrahicNum = value; }
        get { return SharedObject.g_playerPrefsdata.playerdata.GrahicNum; }
    }
    private void Start()
    {
        Init = true;
        SetGraphicquality(GraphicNum);
        gameObject.SetActive(false);
        ChangedText.SetActive(false);
    }
    private IEnumerator ChangedTextVisible()
    {
        if (ChangedText.activeSelf )
            yield return null;
        ChangedText.SetActive(true);
        yield return new WaitForSeconds(.5f);
        ChangedText.SetActive(false);

    }
    public void SetGraphicquality(int Num)
    {
        if (!Init)
            SharedObject.g_SoundManager.PlayBtnClickSound();
        else
            Init = false;
        if (GraphicQuality.Length == 0)
            return;
        SharedObject.g_DataManager.Graphicquality = Num;
        for(int i=0; i < GraphicQualityBtnBg.Length; i++)
        {
        Image[] imgs = { GraphicQualityBtnBg[i], GraphicQualityBtnOutLine[i] };

        Color[] UnSelectcolors = GraphicQualityBtnColors;
        PublicFunction.SetImageColor(imgs, UnSelectcolors);
        }
        for (int i =0; i < GraphicQuality.Length;i++)
        {
            GraphicQuality[i].SetActive(false);
        }
        for (int i = 0; i <= Num; i++)
        {
            if (i.Equals(0))
                continue;
            GraphicQuality[i].SetActive(true);
        }
        Image[] imgs2 = { GraphicQualityBtnBg[Num], GraphicQualityBtnOutLine[Num] };
        Color[] SelectColors = GraphicQualityBtnSelectColors;
        PublicFunction.SetImageColor(imgs2, SelectColors);
        GraphicQuality[0].SetActive(Num.Equals(0));
        GraphicNum = Num;
        if(gameObject.activeSelf)
        StartCoroutine( ChangedTextVisible());
    }
    public void LogOut()
    {
        PlayerPrefs.DeleteAll();
        PlayFabClientAPI.ForgetAllCredentials();
        TitleScene();
    }
    public void VisibleOptionUI()
    {
        gameObject.SetActive(true);
        UserIdText.text = SharedObject.g_playerPrefsdata.playerdata.m_strPlayerName;
        GameVersionText.text = "1.0";

    }
    public void hideOptionUI()
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        gameObject.SetActive(false);
    }
    public void TitleScene()
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i].DOFade(0, .5f);
        }
        FadeCanvas.instance.Fade.gameObject.SetActive(true);
        NetworkManager.instance.DisConnect();
        
        FadeCanvas.instance.Fade.DOFade(1, .5f).OnComplete(() =>
        {

            SceneManager.LoadSceneAsync("Title");

        });
    }
}
