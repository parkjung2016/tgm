using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine;

public class UIProgressBar : MonoBehaviour
{
    [SerializeField]
    private Image HPProgresBar;
    [SerializeField]
    private Text HPText;
    [SerializeField]
    private Image ManaProgresBar;
    [SerializeField]
    private float ProgressBarLerpSpeed;
    [SerializeField]
    private Image XPProgressBar;
    [SerializeField]
    private Text XPText;
    [SerializeField]
    private Text LevelText;
    Character character;
   public static UIProgressBar instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    private void Start()
    {
        SetCharacter();
    }
    void SetCharacter()
    {
        character = SharedObject.g_DataManager.m_MyCharacter;
        if (character == null)
            Invoke("SetCharacter", .2f);
        else
        {
            SetLevelText();
            SetXPProgress();
            SetHPProgress();
            SetManaProgress();
        }
    }
    public void SetLevelText()
    {
        if(character != null)
        LevelText.text = character.N[(int)m_Enum.STAT2.LEVEL].ToString() ;

    }
    public void SetXPProgress()
    {
        if (character != null)
        {

            XPProgressBar.DOFillAmount( character.F[(int)m_Enum.STAT.XP] /character.F[(int)m_Enum.STAT.LEVELUPXP],ProgressBarLerpSpeed);
        XPText.text = character.F[(int)m_Enum.STAT.XP] + "/" + character.F[(int)m_Enum.STAT.LEVELUPXP]+ "XP";
        }

    }
    public void SetHPProgress()
    {
        if (character != null)
        {
            HPProgresBar.DOFillAmount((character.F[(int)m_Enum.STAT.HP] + character.Additional[(int)m_Enum.ADDITIONALSTAT.ADDITIONALHP]) / (character.Max[(int)m_Enum.MAXSTAT.MAXHP] + character.Max[(int)m_Enum.MAXSTAT.MAXADDITIONALHP]), ProgressBarLerpSpeed);
        HPText.text = (int)(character.F[(int)m_Enum.STAT.HP] + character.Additional[(int)m_Enum.ADDITIONALSTAT.ADDITIONALHP]) + "/" + (character.Max[(int)m_Enum.MAXSTAT.MAXHP] + character.Max[(int)m_Enum.MAXSTAT.MAXADDITIONALHP]);

        }
    }
    public void SetManaProgress()
    {
        if (character != null)
        {
            ManaProgresBar.DOFillAmount(character.Mana / character.Max[(int)m_Enum.MAXSTAT.MAXMANA], ProgressBarLerpSpeed);

        }
    }
}
