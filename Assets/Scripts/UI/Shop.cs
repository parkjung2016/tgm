using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    [SerializeField]
    private GameObject[] ItemScrolls;
    [SerializeField]
    private Text ItemTypeText;
    [SerializeField]
   private GameObject CheckBuy;
    [SerializeField]
    private GameObject ItemInfoGroup;
    [SerializeField]
    private ItemInfo iteminfo;
    [SerializeField]
    private Text[] itemValues;
    [SerializeField]
    private Text[] itemData;
    [SerializeField]
    private Text GoldText;
    [SerializeField]
    private Text PriceText;
    [SerializeField]
    private GameObject LackOfMoneyObj;
    private int CurrentItemTypeNum;
    [SerializeField]
    private GameObject NoItem;
    private CanvasGroup canvasGroup;
    [SerializeField]
    private GameObject[] ItemPrefab;
    [SerializeField]
    private GameObject ItemBG;
    [SerializeField]
    private Transform[] ItemPrefabTrans;
    [SerializeField]
    private Text[] PetPrcies;
    [SerializeField]
    private GameObject[] PetBuyBtns;
    [SerializeField]
    private Text[] PetEquipBtnTexts;
    [SerializeField]
    private GameObject[] PetUpgradBtns;
    [SerializeField]
    private GameObject[] PetEquipBtns;
    [SerializeField]
    private Text[] PetNames;
    [SerializeField]
    private Text Pet_ErrorText;
    [SerializeField]
    private Image[] PetImages;
    [SerializeField]
    public Sprite[] Pet1UpgradeImages;
    [SerializeField]
    private Sprite[] Pet2UpgradeImages;
    [SerializeField]
    private Sprite[] Pet3UpgradeImages;
    [SerializeField]
    private Animator[] PetImageAnimator;
    private List<Sprite[]> PetUpgradeImages = new List<Sprite[]>();
    private int Pet_ShopIndex;
    private int Pet_ShopAnimatorParm;
    [SerializeField]
    private Text[] Pet1InfoTexts;
    [SerializeField]
    private Text[] Pet2InfoTexts;
    [SerializeField]
    private Text[] Pet3InfoTexts;
    private List<Text[]> PetInfoTexts = new List<Text[]>();
    private int CheckBuyPetNum;
    [SerializeField]
    private GameObject[] CheckbuyBtns;
    [SerializeField]
    private Text BuyItemInfoName;
    [SerializeField]
    private Text BuyItemInfoDescription;
    bool TrueBuy;
    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        PetUpgradeImages.Add(Pet1UpgradeImages);
        PetUpgradeImages.Add(Pet2UpgradeImages);
        PetUpgradeImages.Add(Pet3UpgradeImages);
        PetInfoTexts.Add(Pet1InfoTexts);
        PetInfoTexts.Add(Pet2InfoTexts);
        PetInfoTexts.Add(Pet3InfoTexts);
    }
    private void Start()
    {
        TrueBuy = true;
        ShowCheckbuybtn_1(false);
        NoItem.SetActive(false);
        LackOfMoneyObj.SetActive(false);
        ResetAnim();
        ItemTypeText.text = "";
        for (byte i = 0; i < ItemScrolls.Length; i++)
        {
            ItemScrolls[i].SetActive(false);
            ItemScrolls[i].GetComponentInChildren<Scrollbar>().value = 0;
        }
            for(int i =0; i < ItemPrefab.Length;i++)
            {
                for(int a =0;  a < Random.Range(3,6);a++)
                addShopItem(i);
            }
        for (int i = 0; i < PetPrcies.Length; i++)
        {
            for (int a = 0; a < SharedScript.instance.csvdata.Pet.Count; a++)
            {
                if (SharedScript.instance.csvdata.Pet[a]["PetNum"].ToString() == i.ToString())
                {

                    PetPrcies[i].text = SharedScript.instance.csvdata.Pet[a]["Price"].ToString();
                    PetNames[i].text = SharedScript.instance.csvdata.Pet[a]["PetName"].ToString();
                    PetInfoTexts[i][0].text = "���ݷ�:" + SharedScript.instance.csvdata.Pet[a]["Damage"].ToString();
                    PetInfoTexts[i][1].text = "���ݵ�����:" + SharedScript.instance.csvdata.Pet[a]["AttackDelay"].ToString();
                }
            }
        }
        CheckCanBuyPet();
        Pet_ErrorText.gameObject.SetActive(false);
    }

    public void DestroyItemGroupWithoutItem()
    {
        GameObject[] groups = GameObject.FindGameObjectsWithTag("ShopItemGroup");
        for(byte i =0; i < groups.Length;i++)
        {
            if(groups[i].GetComponentInChildren<ItemInfo>() == null)
            {
                Destroy(groups[i]);
            }
        }
    }
    
    public void HideObj()
    {
        canvasGroup.DOFade(0, .5f).OnComplete(() =>
         {
             gameObject.SetActive(false);
         });
    }
    public IEnumerator VisiblePetErrorText(string Text)
    {
        Pet_ErrorText.gameObject.SetActive(true);
        Pet_ErrorText.text = Text;
        yield return new WaitForSeconds(1);
        Pet_ErrorText.gameObject.SetActive(false);
    }
    public void ShowCheckBuyPet(int num)
    {
        CheckBuy.SetActive(true);
        CheckBuyPetNum = num;
        ShowCheckbuybtn_1(true);

    }
    private void ShowCheckbuybtn_1(bool enable)
    {
        CheckbuyBtns[1].SetActive(enable);
        CheckbuyBtns[0].SetActive(!enable);
    }
    public void BuyPet()
    {
        PlayerPetInfo playerPetInfo = new PlayerPetInfo();
        for(int i =0; i < SharedScript.instance.csvdata.Pet.Count;i++)
        {
            if(SharedScript.instance.csvdata.Pet[i]["PetNum"].ToString() == CheckBuyPetNum.ToString())
            {
               
                if ( SharedObject.g_playerPrefsdata.playerdataNoSave.Gold >= int.Parse(SharedScript.instance.csvdata.Pet[i]["Price"].ToString()))
                {

                    playerPetInfo.Damage = float.Parse(SharedScript.instance.csvdata.Pet[i]["Damage"].ToString());
                    playerPetInfo.PetName = SharedScript.instance.csvdata.Pet[i]["PetName"].ToString();
                    playerPetInfo.PetType = (m_Enum.Pet)i;
                    playerPetInfo.AttackDelay =int.Parse( SharedScript.instance.csvdata.Pet[i]["AttackDelay"].ToString());
                    PlayFabClientAPI.SubtractUserVirtualCurrency(new SubtractUserVirtualCurrencyRequest() { VirtualCurrency = "GD", Amount = int.Parse(SharedScript.instance.csvdata.Pet[i]["Price"].ToString()) }, result => {  }, error => { });
                    SharedObject.g_playerPrefsdata.playerdataNoSave.Gold -= int.Parse(SharedScript.instance.csvdata.Pet[i]["Price"].ToString());
                    SharedObject.g_playerPrefsdata.playerdata.Pets.Add(playerPetInfo);
        CheckBuy.gameObject.SetActive(false);
        ShowCheckbuybtn_1(false);
                    break;
                }
                else
                {
                    StartCoroutine(VisiblePetErrorText("���� �����մϴ�."));
                }
            }
        }
        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
        CheckCanBuyPet();
        GoldText.text = SharedObject.g_playerPrefsdata.playerdataNoSave.Gold.ToString();
        CanvasManager.SetGoldText();
    }

    public void CheckCanBuyPet()
    {
        for(int i =0; i < PetEquipBtnTexts.Length;i++)
        {
            PetBuyBtns[i].SetActive(true);
            PetEquipBtns[i].SetActive(false);
            PetUpgradBtns[i].SetActive(false);
            PetEquipBtnTexts[i].text = "����";
        }
        for(int i =0; i < SharedObject.g_playerPrefsdata.playerdata.Pets.Count;i++)
        {
            for(int a= 0; a< SharedScript.instance.csvdata.Pet.Count;a++)
            {
                int petnum = 0;
            if (int.TryParse(SharedScript.instance.csvdata.Pet[a]["PetNum"].ToString(), out petnum) &&SharedObject.g_playerPrefsdata.playerdata.Pets[i].PetType == (m_Enum.Pet)petnum)
                {
                    if (PetEquipBtnTexts.Length >= a+1)
                    {

                        if (SharedObject.g_playerPrefsdata.playerdata.Pets[i].Equip)
                        {
                            PetEquipBtnTexts[a].text = "��������";
                        }
                        else
                        {
                            PetEquipBtnTexts[a].text = "����";
                        }
                        int num;
                        if (SharedObject.g_playerPrefsdata.playerdata.Pets[i].Upgrade < int.Parse(SharedScript.instance.csvdata.Pet[a]["MaxUpgrade"].ToString()))
                        {
                            PetUpgradBtns[a].SetActive(true);
                             num = a + SharedObject.g_playerPrefsdata.playerdata.Pets[i].Upgrade + 1;

                            Pet_ShopAnimatorParm = SharedObject.g_playerPrefsdata.playerdata.Pets[i].Upgrade + 1;
                        }
                        else
                        {
                             num = a + SharedObject.g_playerPrefsdata.playerdata.Pets[i].Upgrade;
                        Pet_ShopAnimatorParm = SharedObject.g_playerPrefsdata.playerdata.Pets[i].Upgrade ;

                        }
                        Pet_ShopIndex = a;
                        PetImages[a].sprite = PetUpgradeImages[a][Pet_ShopAnimatorParm];
                        CallSetPet_ShopAnimatorParm();
                        PetPrcies[a].text = SharedScript.instance.csvdata.Pet[num]["Price"].ToString();
                        PetInfoTexts[a][0].text = "���ݷ�:" + SharedScript.instance.csvdata.Pet[num]["Damage"].ToString();
                        PetInfoTexts[a][1].text = "���ݵ�����:" + SharedScript.instance.csvdata.Pet[num]["AttackDelay"].ToString();
                        PetNames[a].text = SharedScript.instance.csvdata.Pet[num]["PetName"].ToString();
                        PetBuyBtns[a].SetActive(false);
                        PetEquipBtns[a].SetActive(true);
                    }
                }
            }
        }
    }
    private void CallSetPet_ShopAnimatorParm()
    {
        if(!gameObject.activeSelf)
        {
            Invoke("CallSetPet_ShopAnimatorParm", .2f);
        }
        else
        {
                            StartCoroutine(SetPet_ShopAnimatorParm(Pet_ShopIndex, Pet_ShopAnimatorParm));
        }
    }
    IEnumerator SetPet_ShopAnimatorParm(int Index, int num)
    {
        if (ItemScrolls[ItemScrolls.Length-1].activeSelf)
        {
        PetImageAnimator[Index].SetInteger("Upgrade", num);
        yield return null;
        }
        else
        {
            yield return new WaitForSeconds(.2f);
            StartCoroutine(SetPet_ShopAnimatorParm(Index, num));
        }
    }
    public void UpgradePet(int num)
    {
        for (int a = 0; a < SharedObject.g_playerPrefsdata.playerdata.Pets.Count; a++)
        {
            if (SharedObject.g_playerPrefsdata.playerdata.Pets[a].PetType == (m_Enum.Pet)num)
            {

                int index = ((int)SharedObject.g_playerPrefsdata.playerdata.Pets[a].PetType) + SharedObject.g_playerPrefsdata.playerdata.Pets[a].Upgrade + 1;
                if ( SharedObject.g_playerPrefsdata.playerdataNoSave.Gold >= int.Parse(SharedScript.instance.csvdata.Pet[index]["Price"].ToString()))
                {
                    SharedObject.g_playerPrefsdata.playerdata.Pets[a].Damage = float.Parse(SharedScript.instance.csvdata.Pet[index]["Damage"].ToString());
                    SharedObject.g_playerPrefsdata.playerdata.Pets[a].PetName = SharedScript.instance.csvdata.Pet[index]["PetName"].ToString();
                    SharedObject.g_playerPrefsdata.playerdata.Pets[a].AttackDelay = int.Parse(SharedScript.instance.csvdata.Pet[index]["AttackDelay"].ToString());
                    SharedObject.g_playerPrefsdata.playerdata.Pets[a].Upgrade++;
                    PlayFabClientAPI.SubtractUserVirtualCurrency(new SubtractUserVirtualCurrencyRequest() { VirtualCurrency = "GD", Amount = int.Parse(SharedScript.instance.csvdata.Pet[index]["Price"].ToString()) }, result => { }, error => { });
                    SharedObject.g_playerPrefsdata.playerdataNoSave.Gold -= int.Parse(SharedScript.instance.csvdata.Pet[index]["Price"].ToString());
                    if (SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet != null && SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet.playerPetInfo.PetType == SharedObject.g_playerPrefsdata.playerdata.Pets[a].PetType)
                    {
                        SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet.playerPetInfo.Equip = false;
                        Destroy(SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet.gameObject);
                        SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet = null;
                        SharedScript.instance.inventory.AddPet(SharedObject.g_playerPrefsdata.playerdata.Pets[a], a + SharedObject.g_playerPrefsdata.playerdata.Pets[a].Upgrade, false);
                    }
                    break;
                }
                else
                {
                    StartCoroutine(VisiblePetErrorText("���� �����մϴ�."));
                }
            }
                
            }
        CheckCanBuyPet();
        GoldText.text = SharedObject.g_playerPrefsdata.playerdataNoSave.Gold.ToString();
    }
    public void ResetAnim()
    {
        VisibleItemInfoGroup(false);
        for (byte i = 0; i < ItemScrolls.Length; i++)
        {
            ItemScrolls[i].SetActive(false);
        }
        CancelBuy();
        if(SharedObject.g_DataManager.m_MyCharacter != null)
        GoldText.text = SharedObject.g_playerPrefsdata.playerdataNoSave.Gold.ToString();
        ItemTypeText.text = "";
        NoItem.SetActive(false);
        canvasGroup.alpha = 0;
        gameObject.SetActive(false);
    }
    public void ShowItem(int Num)
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        for (byte i = 0; i < ItemScrolls.Length; i++)
        {
            ItemScrolls[i].SetActive(false);
        }
        VisibleItemInfoGroup(false);
        CurrentItemTypeNum = Num;
        ItemScrolls[Num].SetActive(true);
        ItemScrolls[Num].GetComponent<ScrollRect>().horizontalScrollbar.value = 0;
        if (ItemScrolls[Num].GetComponentInChildren<ShopItemButton>() == null && Num != 6)
        {
            NoItem.SetActive(true);
        }
        else
        {
            NoItem.SetActive(false);
        }
        ItemTypeText.text = SharedScript.instance.StringBuilder(SharedScript.instance.ItemType[Num]);
        DestroyItemGroupWithoutItem();
    }
    public void addShopItem(int Type)
    {
        GameObject BG = Instantiate(ItemBG, Vector2.zero, Quaternion.identity,ItemPrefabTrans[Type]) as GameObject;
        GameObject obj = Instantiate(ItemPrefab[Type], Vector2.zero, Quaternion.identity,BG.transform) as GameObject;
        ItemInfo itemInfo = obj.GetComponent<ItemInfo>();
        Destroy(obj.GetComponent<Item>());
        byte random = (byte)Random.Range(0, obj.GetComponent<ItemInfo>().itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.POTION ? 4 : 6);
        itemInfo.itemdata.rating = random;
        for (int i = 0; i < itemInfo.itemdata.Values.Length; i++)
        {
            int min;
            int max;
            if (int.TryParse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating][i == 0 ? "MinValue" : "MinValue2"].ToString(), out min))
            {
                if (int.TryParse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating][i == 0 ? "MaxValue" : "MaxValue2"].ToString(), out max))
                    itemInfo.itemdata.Values[i] = Random.Range(min, max + 1);

            }

        }
        if (itemInfo.itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.POTION)
        {
            itemInfo.itemdata.Data[0] = ItemRating.instance.PotionRatingName[itemInfo.itemdata.rating];
            itemInfo.itemdata.Data[1] = itemInfo.itemdata.Values[1] != 0 ? SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["property2"].ToString() : "";
        }
        itemInfo.itemdata.Name = SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["Name"].ToString();
        itemInfo.itemdata.Description = SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["Description"].ToString();
        if (itemInfo.itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.WEAPON)
            itemInfo.itemdata.Values[1] = float.Parse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["MinValue2"].ToString());
        itemInfo.ResetInfo();
        BG.GetComponent<ShopItemButton>().shop = this;
        BG.GetComponent<ShopItemButton>().ValueInit();
    }
  
    public void CheckBuyItem()
    {
        CheckBuy.SetActive(true);
    }
    public void Buy()
    {
        string[] itemids = new string[] { "��", "���", "����", "����", "�Ź�", "����" };
        string[] itemclasses = new string[] { "����", "��", "����" };
        if (SharedObject.g_playerPrefsdata.playerdataNoSave.Gold < iteminfo.itemdata.Prices[0])
        {
            StartCoroutine(lackofmoney());
            return;
        }
        else if(!TrueBuy)
        {
            return;
        }
        else
        {
            string id = "";
            if(iteminfo.itemdata.rating == 0)
            {
                id = itemids[(byte)iteminfo.itemdata.m_ITEMTYPE];
            }
            else
            {
                id = itemids[(byte)iteminfo.itemdata.m_ITEMTYPE] +"_"+ iteminfo.itemdata.rating.ToString();

            }
            buyitem(id);
            TrueBuy = true;
        }
    }
    public void buyitem(string id)
    {

        PlayFabClientAPI.GetCatalogItems(new GetCatalogItemsRequest() { CatalogVersion = "Main" }, success =>
           {
               for(int i =0; i < success.Catalog.Count;i++ )
               {
                   if(success.Catalog[i].ItemId == id)
                   {

                       PlayFabClientAPI.PurchaseItem(new PurchaseItemRequest() { CatalogVersion = "Main", VirtualCurrency = "GD", Price =(int)success.Catalog[i].VirtualCurrencyPrices["GD"], ItemId = id }, success =>
                       {

                           //PlayFabClientAPI.SubtractUserVirtualCurrency(new SubtractUserVirtualCurrencyRequest() { VirtualCurrency = "GD", Amount = iteminfo.itemdata.Prices[0] }, result => { Debug.Log("����"); }, error => { });
                           SharedObject.g_playerPrefsdata.playerdataNoSave.Gold -= iteminfo.itemdata.Prices[0];
                           GoldText.text = SharedObject.g_playerPrefsdata.playerdataNoSave.Gold.ToString();
                           iteminfo.itemdata.ItemId = success.Items[0].ItemInstanceId;
                           CancelBuy();
                           SharedScript.instance.inventory.CurrentScrollNum = CurrentItemTypeNum;
                           SharedScript.instance.inventory.ItemAdd(iteminfo.itemdata);
                           SharedScript.instance.AddItemListData(iteminfo.itemdata,CurrentItemTypeNum);
                           //UpdateInventory(iteminfo, iteminfo.itemdata.rating, 0);
                           CanvasManager.SetGoldText();

                           SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
                           TrueBuy = false;
                       }, error => { });
                   }
               }
           }, error => { });
    }
    public void UpdateInventory(ItemInfo itemInfo, byte rating, int num)
    {
        GetUserInventoryRequest request = new GetUserInventoryRequest();
        PlayFabClientAPI.GetUserInventory(request, result =>
        {
            List<ItemInstance> ii = result.Inventory;
            itemInfo.itemdata.rating = rating;
            itemInfo.itemdata.ReinforcedPrice = int.Parse(SharedScript.instance.csvdata.ItemUpgradeValue[itemInfo.itemdata.Level]["Gold"].ToString());
            for (int a = 0; a < itemInfo.itemdata.Values.Length; a++)
            {
                //print( ii[a].CustomData.ToString());
                int min;
                int max;
                if (int.TryParse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating][a == 0 ? "MinValue" : "MinValue" + (a + 1)].ToString(), out min))
                {
                    if (int.TryParse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating][a == 0 ? "MaxValue" : "MaxValue" + (a + 1)].ToString(), out max))
                        itemInfo.itemdata.Values[a] = UnityEngine.Random.Range(min, max + 1);
                }
                else
                    itemInfo.itemdata.Values[a] = float.Parse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating][a == 0 ? "MinValue" : "MinValue" + (a + 1)].ToString());
            }
            if (itemInfo.itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.POTION)
            {
                itemInfo.itemdata.Data[0] = ItemRating.instance.PotionRatingName[itemInfo.itemdata.rating];
                itemInfo.itemdata.Data[1] = itemInfo.itemdata.Values[1] != 0 ? SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["property2"].ToString() : "";
            }
            else
            {
                itemInfo.itemdata.Data[0] = SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["property1"].ToString();
                itemInfo.itemdata.Data[1] = SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["property2"].ToString();
            }
            int Price = int.Parse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["Price"].ToString());
            itemInfo.itemdata.Prices[0] = Price;
            itemInfo.itemdata.Prices[1] = (int)(Price * 80 * 0.01f);
            itemInfo.itemdata.Name = ii[num].DisplayName;
            itemInfo.itemdata.ItemId = ii[num].ItemInstanceId;
            itemInfo.itemdata.Description = SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["Description"].ToString();
            if (itemInfo.itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.WEAPON)
                itemInfo.itemdata.Values[1] = float.Parse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["MinValue2"].ToString());
            itemInfo.ResetInfo();
        }, error => { });
    }
    IEnumerator lackofmoney()
    {
        SharedObject.g_SoundManager.PlayErrorAudio();
        LackOfMoneyObj.SetActive(true);
        yield return new WaitForSeconds(.5f);
        LackOfMoneyObj.SetActive(false);
    }
    public void CancelBuy()
    {
        CheckBuy.SetActive(false);
        ShowCheckbuybtn_1(false);
    }
    public void VisibleItemInfoGroup(bool visible)
    {

        ItemInfoGroup.SetActive(visible);
    }
    public void ShowItemInfo(ItemInfo _iteminfo)
    {
        VisibleItemInfoGroup(true);
        string[] data = iteminfo.itemdata.Data;
        iteminfo.itemdata = _iteminfo.itemdata;
        iteminfo.itemdata.Equip = false;
        iteminfo.sprite = _iteminfo.sprite;
        BuyItemInfoName.text = _iteminfo.itemdata.Name;
        BuyItemInfoDescription.text = _iteminfo.itemdata.Description;
        PriceText.text = iteminfo.itemdata.Prices[0].ToString();
        iteminfo.ResetInfo();
        for (byte i =0; i < itemData.Length;i++)
        {
            itemData[i].text = iteminfo.itemdata.Data[i] + ":";
            itemValues[i].text = iteminfo.itemdata.Values[i].ToString();
        }
        if(_iteminfo.itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.POTION)
        {
            itemData[1].text = "";
            itemValues[1].text = "";
        }
    }
   
    private void OnEnable()
    {
        if(GoldText !=null && SharedObject.g_DataManager.m_MyCharacter != null)
        GoldText.text = SharedObject.g_playerPrefsdata.playerdataNoSave.Gold.ToString();
    }
}
