using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon;
using Photon.Realtime;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ExitGames.Client.Photon;

public class roominfo : MonoBehaviour
{
    [SerializeField]
    private Text PlayerCount; 
    [SerializeField]
    private Text RoomNameText;
    [SerializeField]
    private Image ViewMap;
    public RoomInfo RoomInfo_;
    public Sprite MapSprite;
    UI_PVPSelect uI_PVPSelect;
    private void Awake()
    {
        uI_PVPSelect = FindObjectOfType<UI_PVPSelect>();
    }
    private void Start()
    {
        Init();
    }
    public void Init()
    {
        RoomNameText.text = RoomInfo_.Name;
        PlayerCount.text = RoomInfo_.PlayerCount.ToString() + "/" + RoomInfo_.MaxPlayers.ToString() ;
        if(MapSprite == null)
        {
            ViewMap.color = new Color(ViewMap.color.r, ViewMap.color.g, ViewMap.color.b, 0);
        }
        else
            ViewMap.color = new Color(ViewMap.color.r, ViewMap.color.g, ViewMap.color.b, 1);

        ViewMap.sprite = MapSprite;
    }

    public void JoinRoom()
    {
        if (RoomInfo_.IsOpen)
        {
        if((m_Enum.MAP)(int)RoomInfo_.CustomProperties["Map"] != m_Enum.MAP.NULL)
        uI_PVPSelect.currentMap = (m_Enum.MAP)(int)RoomInfo_.CustomProperties["Map"];
        PhotonNetwork.JoinRoom(RoomInfo_.Name);
        uI_PVPSelect.ActiveType(1,true);

        }

    }
}
