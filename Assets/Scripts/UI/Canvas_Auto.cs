using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;

public class Canvas_Auto : MonoBehaviour
{
    [SerializeField]
   private GameObject CheckAutoHunt;
    private void Start()
    {
        CheckAutoHunt.SetActive(false);
    }
    public void ClickAutoHunt()
    {
        CheckAutoHunt.SetActive(true);
    }
    public void CancelAutoHunt()
    {
        CheckAutoHunt.SetActive(false);
    }
    public void StartAutoHunt()
    {
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i].DOFade(0, 1f);
        }
        FadeCanvas.instance.Fade.gameObject.SetActive(true);
        FadeCanvas.instance.Fade.DOFade(1, 1f).OnComplete(() =>
        {
            ChatTest.instance.DisConnectChannel();
            NetworkManager.instance.DisConnect();
            SceneManager.LoadScene("AutoHunt");

        });
    }
}
