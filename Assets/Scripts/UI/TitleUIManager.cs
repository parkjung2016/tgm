using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine;
using GooglePlayGames;

using PlayFab;
using PlayFab.ClientModels;

using UnityEngine.UI;
using DG.Tweening;

public class TitleUIManager : MonoBehaviour
{
    [SerializeField]
    private Sprite[] pwdvisibleImage;
    [SerializeField]
    private AudioSource BtnClickSound;
   public  Animation GameNameAnim;
   public  AnimationClip GameNameOutLineAnim;
   public  AnimationClip GameNameDissolveAnim;
    public GameObject TouchScreen;
    bool IsClicked;
    [SerializeField]
    private GameObject LoginGroup;
    [SerializeField]
    private InputField[] inputFields;
    [SerializeField]
    private Text ErrorLoginText;
    [SerializeField]
    private GameObject LogOutBtn;
    bool googlelogin= false;
    private void Awake()
    {
        Application.runInBackground = true;
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;

        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();
    }
    private void Start()
    {
        PlayGamesPlatform.Instance.RequestServerSideAccess(true, value =>
        {
            PlayFabClientAPI.LoginWithGooglePlayGamesServices(new LoginWithGooglePlayGamesServicesRequest()
            {
                TitleId = PlayFabSettings.TitleId,
                ServerAuthCode = value,
                CreateAccount = false,
            },result=> 
            {
                googlelogin = true;
                LogOutBtn.SetActive(true);
            }, error => { googlelogin = false; LogOutBtn.SetActive(PlayerPrefs.HasKey("logininfo")); });
            });
        ErrorLoginText.gameObject.SetActive(false);
        LoginGroup.SetActive(false);
            IsClicked = false;
        Application.targetFrameRate = 30;
        GameNameAnim.clip = GameNameOutLineAnim;
        GameNameAnim.Play();
    }
    public void LogOut()
    {
        PlayerPrefs.DeleteAll();
        PlayFabClientAPI.ForgetAllCredentials();
        LogOutBtn. gameObject.SetActive(false);
    }
    public void VisiblePasswordText(Toggle toggle)
    {
        if (toggle.isOn)
        {
            toggle.image.sprite = pwdvisibleImage[0];
            inputFields[1].contentType = InputField.ContentType.Password;
        }
        else
        {
            toggle.image.sprite = pwdvisibleImage[1];
            inputFields[1].contentType = InputField.ContentType.Standard;

        }


    }
    public void JoinLobby()
    {
        if (IsClicked)
            return;
        BtnClickSound.Play();
        IsClicked = true;
        TouchScreen.SetActive(false);
        if(googlelogin)
        {
            SharedObject.g_playerPrefsdata.LoadFile(SharedObject.SavePath);
            StartCoroutine(FadeAnim());
        }
        else if (PlayerPrefs.HasKey("logininfo"))
        {
            string[] infos = PlayerPrefs.GetString("logininfo").Split(',');
            var request = new LoginWithEmailAddressRequest { Email = infos[0], Password = infos[2] };
            PlayFabClientAPI.LoginWithEmailAddress(request, (LoginResult success) =>
            {
                SharedObject.g_playerPrefsdata.playfabid = success.PlayFabId;
                SharedObject.g_playerPrefsdata.LoadFile(SharedObject.SavePath);
                    StartCoroutine(FadeAnim());
            }, (PlayFabError error) => {
                LoginGroup.SetActive(true);
            });
        }
        else
        {
            LoginGroup.SetActive(true);
        }
    }
    public void DataRemove()
    {
        if (File.Exists(SharedObject.SavePath))
        {
            File.Delete(SharedObject.SavePath);
        }
    }
    IEnumerator FadeAnim()
    {
        yield return new WaitForSeconds(.15f);
        GameNameAnim.clip = GameNameDissolveAnim;
        GameNameAnim.Play();
        yield return new WaitForSeconds(4f);
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i].DOFade(0, 1f);
        }
        FadeCanvas.instance.Fade.gameObject.SetActive(true);
        FadeCanvas.instance.Fade.DOFade(1, 1f).OnComplete(() =>
        {
            LoadingSceneManager.LoadScene("Lobby");
        });
    }
   public void Register()
    {
        SceneManager.LoadScene("Login");
    }
    public void Login()
    {
        var request = new LoginWithEmailAddressRequest { Email = inputFields[0].text, Password = inputFields[1].text };
        PlayFabClientAPI.LoginWithEmailAddress(request, (LoginResult success) =>
        {
            SharedObject.g_playerPrefsdata.playfabid = success.PlayFabId;
            SharedObject.g_playerPrefsdata.LoadFile(SharedObject.SavePath);
            SharedObject.g_playerPrefsdata.Savelogininfo(inputFields);
            LoginGroup.SetActive(false);
            StartCoroutine(FadeAnim());
        }, (PlayFabError error) => { if(!ErrorLoginText.gameObject.activeSelf) StartCoroutine(ErrorLogin("이메일 또는 비밀번호가 일치하지 않습니다.")); });
    }
    IEnumerator ErrorLogin(string text)
    {
        ErrorLoginText.text = text;
        ErrorLoginText.gameObject.SetActive(true);
        yield return new WaitForSeconds(1);
        ErrorLoginText.gameObject.SetActive(false);
    }
}
