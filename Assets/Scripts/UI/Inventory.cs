using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;
using Newtonsoft.Json;
using Photon.Pun;
using Photon;
using ExitGames.Client.Photon;

public class Inventory : MonoBehaviour
{
    #region 변수
    public GameObject[] ItemPrefab;
    public Transform[] ItemPrefabTrans;
    [SerializeField]
    private Scrollbar[] scrollbar_;

    public List<GameObject> m_WeaponList = new List<GameObject>();

    public List<GameObject> m_HelmetList = new List<GameObject>();

    public List<GameObject> m_ArmorList = new List<GameObject>();

    public List<GameObject> m_LeggingsList = new List<GameObject>();
    public List<GameObject> m_BootsList = new List<GameObject>();
    public List<GameObject> m_PotionList = new List<GameObject>();
    [SerializeField]
    private Text[] Values;
    private Character character;
    [SerializeField]
    private GameObject[] Scrolls;
    public int CurrentScrollNum;
    [SerializeField]
    private GameObject _ItemInfo;
    [SerializeField]
    private Text[] ItemInfoValueTexts;
    [SerializeField]
    private Text[] ItemInfoValues;
    [SerializeField]
    private Text[] StatUpgradeValueTexts;
    [SerializeField]
    private Text[] StatUpgradeValues;
    [SerializeField]
    private Text Equip;
    [SerializeField]
    private Image ItemImg;
    public Item[] Currentitem;
    public Item[] item;
    public Item ChooseItem;
    public bool ItemClickPossible;
    [SerializeField]
    private GameObject EquipInfo;
    [SerializeField]
    private Transform[] EquipInfoPoses;
    [SerializeField]
    private Image[] ItemEquipImg;
    [SerializeField]
    private Sprite[] ItemUnEquipImg;
    private Color[] ItemUnEquipColors = new Color[2];
    [SerializeField]
    private Image[] ItemEquipBg;
    [SerializeField]
    private Image[] ItemEquipOutLine;
    private float ItemUnEquipColora;
    [SerializeField]
    private int MaxNumPotionsPossessed;
    [SerializeField]
    private GameObject PotionEquipInfo;
    [SerializeField]
    private Image[] PotionEquipImgs;
    [SerializeField]
    private GameObject StatUpgrade;
    public List<Item> EquipPotions = new List<Item>();
    [SerializeField]
    private GameObject[] CheatBtns;
    [SerializeField]
    private Text StatPointText;
    [SerializeField]
    private Sprite[] PotionCount;
    [SerializeField]
    private GameObject[] PetPrefabs;
    private Transform PetTrans;
    [SerializeField]
    private GameObject[] ItemInfoBtns;
    [SerializeField]
    private Transform[] ItemEquipTrans;
    [SerializeField]
    private Text ItemInfoErrorText;
    [SerializeField]
    private Text ItemDescriptionText;
    [SerializeField]
    private Text ItemNameText;
    [SerializeField]
    private Image[] EquipItemOutLine;
    [SerializeField]
    private Material[] LevelOutLineMat;
    [SerializeField]
    private Text[] EquipItemUpgradeTexts;
    [SerializeField]
    private GameObject[] PlayerStatAddBtns;
    #endregion
    private void Awake()
    {
        ItemUnEquipImg = new Sprite[ItemEquipImg.Length];
        for (int i = 0; i < ItemEquipImg.Length; i++)
        {
            ItemUnEquipImg[i] = ItemEquipImg[i].sprite;
        }
        ItemUnEquipColors[0] = ItemEquipBg[0].color;
        ItemUnEquipColors[1] = ItemEquipOutLine[0].color;
        ItemUnEquipColora = ItemEquipImg[0].color.a;
        OnEnableCheatBtn(false);
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnEvent(EventData obj)
    {
        if(obj.Code == 99)
        {
            GameObject otehrplayer = GameObject.Find((string)obj.CustomData);
            Destroy(otehrplayer.GetComponent<PlayerMove>().playerpet.gameObject);
            otehrplayer.GetComponent<PlayerMove>().playerpet = null;
        }
    }

    public void OnEnableCheatBtn(bool show)
    {
        for (int i = 0; i < CheatBtns.Length; i++)
        {
            CheatBtns[i].gameObject.SetActive(show);
        }
    }
    public void PetEquip(int type)
    {
        for (int i = 0; i < SharedObject.g_playerPrefsdata.playerdata.Pets.Count; i++)
        {
            if (SharedObject.g_playerPrefsdata.playerdata.Pets[i].PetType == (m_Enum.Pet)type)
            {
                if (SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet != null && SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet.playerPetInfo != SharedObject.g_playerPrefsdata.playerdata.Pets[i])
                {

                    SharedScript.instance.shop.StartCoroutine(SharedScript.instance.shop.VisiblePetErrorText("이미 다른펫을 장착 중입니다."));
                }
                else
                {

                    AddPet(SharedObject.g_playerPrefsdata.playerdata.Pets[i], type+ SharedObject.g_playerPrefsdata.playerdata.Pets[i].Upgrade, false);
                }
            }
        }
    }
    public void AddPet(PlayerPetInfo playerPetInfo, int num, bool Init)
    {
        if (!Init)
            playerPetInfo.Equip = !playerPetInfo.Equip;
        if (!playerPetInfo.Equip)
        {
            if (SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet != null)
            {
                PhotonNetwork.RaiseEvent(99, PhotonNetwork.NickName,NetworkManager.instance.otherraiseEvent, SendOptions.SendReliable);
                Destroy(SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet.gameObject);
                SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet = null;
                SharedScript.instance.renderPlayer.SetPetImage(null,null,Vector2.zero);
            }
        }
        else
        {
            if (SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet == null)
            {
                if(PVPManager.instance != null)
                {

                    PVPManager.instance.playerPetTrue = true;
                    PVPManager.instance.playerPetInfo = playerPetInfo;
                    PVPManager.instance.PlayerPetNum = num;
                    PVPManager.instance.PetPrefabs = PetPrefabs;
                    
                }
                PlayerPet playerPet = Instantiate(PetPrefabs[num], PetTrans).GetComponent<PlayerPet>();
                playerPet.transform.localPosition = Vector3.zero;
                playerPet.playerPetInfo = playerPetInfo;
                SharedObject.g_DataManager.m_MyCharacter.GetComponent<PlayerMove>().playerpet = playerPet;
                SharedScript.instance.renderPlayer.SetPetImage(playerPet.GetComponent<SpriteRenderer>().sprite, playerPet.anim.runtimeAnimatorController,
                    playerPet.playerPetInfo.PetType == m_Enum.Pet.BAT ? new Vector2(1.2f, 1.2f) : playerPet.playerPetInfo.PetType == m_Enum.Pet.SKULL  ? new Vector2(1.5f, 1.5f) : new Vector2(0.9f,0.9f));
            }

        }
        SharedScript.instance.shop.CheckCanBuyPet();
    }
    void InitListItem(int Num, List<GameObject> list, List<PlayerPrefsData.ItemData> itemList)
    {
        CurrentScrollNum = Num;

        for (byte a = 0; a < itemList.Count; a++)
        {

            ItemAdd(itemList[a]);
        }
        for(byte a  =0; a<itemList.Count;a++)
        {
            if (itemList[a].Equip)
            {

                SetInitItem(list[a].GetComponent<Item>());
                break;
            }
        }
        ItemSort();
    }
    void SetCharacter()
    {
        character = SharedObject.g_DataManager.m_MyCharacter;
        if (character == null)
        {
            Invoke("SetCharacter", .2f);
        }
        else
        {
            for (byte i = 0; i < (byte)m_Enum.ADDITIONALSTAT.MAX; i++)
                character.Additional[i] = 0;
            if(PhotonNetwork.InRoom&& PVPManager.instance != null)
            {
            PetTrans = character.transform.GetChild(character.transform.childCount - 2);

            }
            else
            {
            PetTrans = character.transform.GetChild(character.transform.childCount - 1);

            }
            InitListItem(0, m_WeaponList, SharedObject.g_playerPrefsdata.playerdataNoSave.WeaponItem);
            InitListItem(1, m_HelmetList, SharedObject.g_playerPrefsdata.playerdataNoSave.HelmetItem);
            InitListItem(2, m_ArmorList, SharedObject.g_playerPrefsdata.playerdataNoSave.ArmorItem);
            InitListItem(3, m_LeggingsList, SharedObject.g_playerPrefsdata.playerdataNoSave.LeggingsItem);
            InitListItem(4, m_BootsList, SharedObject.g_playerPrefsdata.playerdataNoSave.BootsItem);
            InitListItem(5, m_PotionList, SharedObject.g_playerPrefsdata.playerdataNoSave.PotionsItem);
            for (int i = 0; i < SharedObject.g_playerPrefsdata.playerdata.Pets.Count; i++)
            {
                if (SharedObject.g_playerPrefsdata.playerdata.Pets[i].Equip)
                {
                 
                    AddPet(SharedObject.g_playerPrefsdata.playerdata.Pets[i],(int)SharedObject.g_playerPrefsdata.playerdata.Pets[i].PetType + SharedObject.g_playerPrefsdata.playerdata.Pets[i].Upgrade, true);


                }
            }
        }
    }
    private void Start()
    {
        for (int i = 0; i < EquipItemUpgradeTexts.Length; i++)
        {
            if (EquipItemUpgradeTexts[i] != null)
                EquipItemUpgradeTexts[i].gameObject.SetActive(false);
        }
        ItemInfoErrorText.gameObject.SetActive(false);
        StatUpgrade.SetActive(true);
        for (int i = 0; i < MaxNumPotionsPossessed; i++)
        {

            EquipPotions.Add(null);
        }
        EquipInfo.transform.position = EquipInfoPoses[0].position;
        ItemClickPossible = true;
        _ItemInfo.SetActive(false);
        InventoryExitBtnClick();
        PotionEquipInfo.SetActive(false);
        SetCharacter();
        SharedScript.instance.renderPlayer.SetPetImage(null, null, Vector2.zero);
    }
    void SetInitItem(Item item_)
    {
        ChooseItem = item_;
        ChooseItem.itemInfo.itemdata.Equip = false;
        item[CurrentScrollNum] = ChooseItem;
        ItemEquip();
    }
    #region 인벤토리 표시
    public void InventoryBtnClick()
    {
        if (PlayerHPManager.instance.character.Death)
            return;
        HideItemInfo();
        ResetValues();
        gameObject.SetActive(true);
    }
    #endregion
    #region 플레이어스탯 새로고침
    public void ResetValues()
    {
        for (int i = 0; i < Values.Length; i++)
        {
                PlayerStatAddBtns[i].gameObject.SetActive(!(character.AddStatValues[i] >= float.Parse(SharedScript.instance.csvdata.PlayerSTAT[i]["MaxAddSTAT"].ToString())));

            StatUpgradeValues[i].text = character.F[i].ToString();
            if(i == 2)
            {

                Values[i].text = character.Additional[i].Equals(0) ? character.F[i].ToString() : character.F[i].ToString() + "<size=100><color=#0D6800>(" +( character.Additional[i]+character.AddStatValues[i]) + ")</color></size>";
            }
            else
            {
            if (i != 5)
                Values[i].text = character.Additional[i].Equals(0) ? character.F[i].ToString() : character.F[i].ToString() + "<size=100><color=#0D6800>(+" + character.Additional[i] + ")</color></size>";

            }
        }
        Values[5].text = character.N[(byte)m_Enum.STAT2.LEVEL].ToString();
        StatUpgradeValues[5].text = character.Max[(byte)m_Enum.MAXSTAT.MAXMANA].ToString();
        StatPointText.text = SharedObject.g_playerPrefsdata.playerdata.StatPoint.ToString();
        Invoke("SetHPProgress", .1f);
        Invoke("SetManaProgress", .1f);
    }
    public void SetHPProgress()
    {
        UIProgressBar.instance.SetHPProgress();
    }
    public void SetManaProgress()
    {
        UIProgressBar.instance.SetManaProgress();
    }
    #endregion
    #region 아이템정보표시확인
    public void VisibleItemInfoEquip(int Num)
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();

        if (Num == 5)
        {
            PotionEquipInfo.SetActive(true);
            for (int i = 0; i < EquipPotions.Count; i++)
            {
                if (EquipPotions[i] != null)
                {
                    PotionEquipImgs[i].sprite = EquipPotions[i].itemInfo.sprite;
                }
            }
            return;
        }
        else
            PotionEquipInfo.SetActive(false);
        if (item[Num] != null && item[Num].itemInfo.itemdata.Equip)
        {

            VisibleItemInfo(item[Num]);
            return;
        }

    }
    #endregion
    private void Update()
    {
        if (!gameObject.activeSelf)
            return;
        if (_ItemInfo.activeSelf || PotionEquipInfo.activeSelf)
        {
            if (Input.touchCount > 0)
            {
                if (EventSystem.current.IsPointerOverGameObject(0).Equals(false))
                {
                    if (Input.GetTouch(1).phase.Equals(TouchPhase.Began))
                    {
                        HideItemInfo();
                    }
                }
            }
            else if (Input.GetMouseButtonDown(0))
            {
                if (EventSystem.current.IsPointerOverGameObject().Equals(false))
                {
                    HideItemInfo();
                }
            }
        }
    }
    #region 인벤토리닫기
    public void InventoryExitBtnClick()
    {
        gameObject.SetActive(false);
        VisibleScroll(0);
    }
    #endregion
    #region 아이템추가치트
    public void ItemAddCheat()
    {
   
        //int[] itemprices = new int[] { 40000, 25000, 10000, 8000, 5000, 3000 };
        //int[] itemprices2 = new int[] { 30000, 18000, 10000, 8000, 5000, 2000 };
        //int[] itemprices3 = new int[] { 35000, 20000, 10000, 8500, 5000, 3500 };
        //int[] itemprices4 = new int[] { 35000, 25000, 15000, 8000, 5500, 4000 };
        //int[] itemprices5 = new int[] { 30000, 20000, 13000, 8000, 5000, 3200 };
        //int[] itemprices6 = new int[] { 10000, 10000, 10000, 10000};
        //List<int[]> itempricelist = new List<int[]>() { itemprices, itemprices2, itemprices3, itemprices4, itemprices5, itemprices6 };
        if (PlayerHPManager.instance.character.Death)
            return;
        List<GameObject> list = new List<GameObject>();
        m_Enum.ITEMTYPE iTEMTYPE = m_Enum.ITEMTYPE.WEAPON;
        iTEMTYPE = (m_Enum.ITEMTYPE)CurrentScrollNum;
        switch (CurrentScrollNum)
        {
            case 0:
                list = m_WeaponList;
                break;
            case 1:
                list = m_HelmetList;
                break;
            case 2:
                list = m_ArmorList;
                break;
            case 3:
                list = m_LeggingsList;
                break;
            case 4:
                list = m_BootsList;
                break;
            case 5:
                list = m_PotionList;
                break;
        }
        scrollbar_[CurrentScrollNum].value = 1;
        byte random = (byte)UnityEngine.Random.Range(0, iTEMTYPE == m_Enum.ITEMTYPE.POTION ? 5 : 7);
        string id = "";
        if(random == 0)
        {
         id = SharedScript.instance.itemids[CurrentScrollNum];

        }
        else
        {
         id = SharedScript.instance.itemids[CurrentScrollNum] + "_" + random.ToString();

        }
 
        List<string> itemidlist = new List<string>();
        itemidlist.Add(id);
        PlayFabServerAPI.GrantItemsToUser(new PlayFab.ServerModels.GrantItemsToUserRequest() { ItemIds = itemidlist, PlayFabId = SharedObject.g_playerPrefsdata.playfabid,CatalogVersion = "Main" }, success =>
         {
             GetCatalogItemsRequest requestCatalog = new GetCatalogItemsRequest() { CatalogVersion = "Main" };
             PlayFabClientAPI.GetCatalogItems(requestCatalog, result =>
             {
                 List<CatalogItem> items = result.Catalog;
                 foreach (CatalogItem i in items)
                 {
                     if (i.ItemId == id)
                     {
                         for(int a= 0; a< success.ItemGrantResults .Count;a++)
                         {
                             if (success.ItemGrantResults[a].ItemId == i.ItemId)
                             {
                                 success.ItemGrantResults[a].CustomData = JsonConvert.DeserializeObject<Dictionary<string, string>>(i.CustomData);
                                 //ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest()
                                 //{
                                 //    FunctionName = "UpdateUserInventoryItemCustomData",
                                 //    GeneratePlayStreamEvent = true,
                                 //    FunctionParameter = new
                                 //    {

                                 //        ItemInstanceId = result.Items[a].ItemInstanceId,
                                 //        data = result.Items[a].CustomData
                                 //    }

                                 //};
                                 //PlayFabClientAPI.ExecuteCloudScript(request, run =>
                                 // {

                                 //     print("성공");

                                 // }, error => { });

                                 //PlayFabServerAPI.UpdateUserInventoryItemCustomData(new PlayFab.ServerModels.UpdateUserInventoryItemDataRequest()
                                 //{
                                 //    PlayFabId = SharedObject.g_playerPrefsdata.playfabid,
                                 //    ItemInstanceId = result.Items[a].ItemInstanceId,
                                 //    Data = JsonConvert.DeserializeObject<Dictionary<string, string>>(i.CustomData)
                                 //},
                                 //    success =>
                                 //    {
                                 //        print("성공");
                                 //    },
                                 //    error =>
                                 //    {
                                 //        print(error.ErrorMessage);
                                 //        print(error.ErrorDetails);
                                 //    }); ;
                             }
                         }
                                SharedScript.instance.UpdateInventory(list, random, success.ItemGrantResults.Count - 1,CurrentScrollNum);
                         break;
                     }
                 }
             }, error => { });

         }, error => {  });
        //GetCatalogItemsRequest requestCatalog = new GetCatalogItemsRequest() { CatalogVersion = "Main" };
        //PlayFabClientAPI.GetCatalogItems(requestCatalog, result =>
        //{
        //    List<CatalogItem> items = result.Catalog;
        //    foreach (CatalogItem i in items)
        //    {
        //        if (i.ItemId == id)
        //        {

        //            int price = (int)i.VirtualCurrencyPrices["GD"];

        //            var requestADD = new AddUserVirtualCurrencyRequest() { VirtualCurrency = "GD", Amount = price };
        //            var request = new PurchaseItemRequest() { CatalogVersion = "Main", ItemId = id, VirtualCurrency = "GD", Price = price };
        //            print(price);
        //            //playfabser.grant
        //            PlayFabClientAPI.AddUserVirtualCurrency(requestADD, result =>
        //            {

        //                PlayFabClientAPI.PurchaseItem(request, result =>
        //                {
        //                    for (int a = 0; a < result.Items.Count; a++)
        //                    {
        //                        if (result.Items[a].ItemId == i.ItemId)
        //                        {
        //                            result.Items[a].CustomData = JsonConvert.DeserializeObject<Dictionary<string, string>>(i.CustomData);
        //                            //ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest()
        //                            //{
        //                            //    FunctionName = "UpdateUserInventoryItemCustomData",
        //                            //    GeneratePlayStreamEvent = true,
        //                            //    FunctionParameter = new
        //                            //    {

        //                            //        ItemInstanceId = result.Items[a].ItemInstanceId,
        //                            //        data = result.Items[a].CustomData
        //                            //    }
                                        
        //                            //};
        //                            //PlayFabClientAPI.ExecuteCloudScript(request, run =>
        //                            // {

        //                            //     print("성공");

        //                            // }, error => { });
        //                            print(SharedObject.g_playerPrefsdata.playfabid);
        //                            print(result.Items[a].ItemInstanceId);
        //                            print((i.CustomData));
        //                            //PlayFabServerAPI.UpdateUserInventoryItemCustomData(new PlayFab.ServerModels.UpdateUserInventoryItemDataRequest()
        //                            //{
        //                            //    PlayFabId = SharedObject.g_playerPrefsdata.playfabid,
        //                            //    ItemInstanceId = result.Items[a].ItemInstanceId,
        //                            //    Data = JsonConvert.DeserializeObject<Dictionary<string, string>>(i.CustomData)
        //                            //},
        //                            //    success =>
        //                            //    {
        //                            //        print("성공");
        //                            //    },
        //                            //    error =>
        //                            //    {
        //                            //        print(error.ErrorMessage);
        //                            //        print(error.ErrorDetails);
        //                            //    }); ;
        //                        }
        //                    }
        //                    UpdateInventory(list, random, result.Items.Count - 1);
        //                }, error => { print(error.ErrorMessage); });
        //            }, error => { });
        //            break;
        //        }
        //    }
        //}, error => { });
        //GameObject obj = Instantiate(ItemPrefab[CurrentScrollNum], Vector2.zero, Quaternion.identity, ItemPrefabTrans[CurrentScrollNum]) as GameObject;
        //ItemInfo itemInfo = obj.GetComponent<ItemInfo>();
        //scrollbar_[CurrentScrollNum].value = 1;
        //itemInfo.itemdata.rating = random;
        //itemInfo.itemdata.ReinforcedPrice = int.Parse(SharedScript.instance.csvdata.ItemUpgradeValue[itemInfo.itemdata.Level]["Gold"].ToString());
        //obj.transform.localScale = Vector2.one;
        //for (int i = 0; i < itemInfo.itemdata.Values.Length; i++)
        //{
        //    int min;
        //    int max;
        //    if (int.TryParse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating][i == 0 ? "MinValue" : "MinValue" + (i + 1)].ToString(), out min))
        //    {
        //        if (int.TryParse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating][i == 0 ? "MaxValue" : "MaxValue" + (i + 1)].ToString(), out max))
        //            itemInfo.itemdata.Values[i] = UnityEngine.Random.Range(min, max + 1);
        //    }
        //    else
        //        itemInfo.itemdata.Values[i] = float.Parse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating][i == 0 ? "MinValue" : "MinValue" + (i + 1)].ToString());
        //}
        //if (itemInfo.itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.POTION)
        //{
        //    itemInfo.itemdata.Data[0] = ItemRating.instance.PotionRatingName[itemInfo.itemdata.rating];
        //    itemInfo.itemdata.Data[1] = itemInfo.itemdata.Values[1] != 0 ? SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["property2"].ToString() : "";
        //}
        //itemInfo.itemdata.Name = SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["Name"].ToString();
        //itemInfo.itemdata.Description = SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["Description"].ToString();
        //if (itemInfo.itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.WEAPON)
        //    itemInfo.itemdata.Values[1] = float.Parse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["MinValue2"].ToString());

        //list.Add(obj);
        //AddItemListData(obj.GetComponent<ItemInfo>().itemdata);
        //itemInfo.ResetInfo();
        //SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);

    }
    public void SetInteInstaceValue()
    {
        List<PlayerPrefsData.ItemData> list = new List<PlayerPrefsData.ItemData>();
        GetUserInventoryRequest request = new GetUserInventoryRequest();
        PlayFabClientAPI.GetUserInventory(request, result =>
        {
            List<ItemInstance> ii = result.Inventory;
            for (int i = 0; i < ii.Count; i++)
            {
                switch ((m_Enum.ITEMTYPE)int.Parse(ii[i].CustomData["m_ITEMTYPE"]))
                {
                    case m_Enum.ITEMTYPE.WEAPON:
                        list = SharedObject.g_playerPrefsdata.playerdataNoSave.WeaponItem;
                        break;
                    case m_Enum.ITEMTYPE.HELMET:
                        list = SharedObject.g_playerPrefsdata.playerdataNoSave.HelmetItem;
                        break;
                    case m_Enum.ITEMTYPE.ARMOR:
                        list = SharedObject.g_playerPrefsdata.playerdataNoSave.ArmorItem;
                        break;
                    case m_Enum.ITEMTYPE.LEGGINGS:
                        list = SharedObject.g_playerPrefsdata.playerdataNoSave.LeggingsItem;
                        break;
                    case m_Enum.ITEMTYPE.BOOTS:
                        list = SharedObject.g_playerPrefsdata.playerdataNoSave.BootsItem;
                        break;
                    case m_Enum.ITEMTYPE.POTION:
                        list = SharedObject.g_playerPrefsdata.playerdataNoSave.PotionsItem;
                        break;
                }
                ii[i].CustomData["Data"] = JsonUtility.ToJson(list[i]);

                //ii[i].CustomData["Level"] = list[i].Level.ToString();
                //ii[i].CustomData["colorStore"] = list[i].colorStore.ToString();
                //ii[i].CustomData["colorStore2"] = list[i].colorStore2.ToString();
                //ii[i].CustomData["rating"] = list[i].rating.ToString();
                //ii[i].CustomData["ratingName"] = list[i].ratingName.ToString();
                //ii[i].CustomData["Equip"] = list[i].Equip.ToString();
                //ii[i].CustomData["Data"] = list[i].Data[0].ToString();
                //ii[i].CustomData["Data2"] = list[i].Data[1].ToString();
                //ii[i].CustomData["Value"] = list[i].Values[0].ToString();
                //ii[i].CustomData["Value2"] = list[i].Values[1].ToString();
                //ii[i].CustomData["BuyPrice"] = list[i].Prices[0].ToString();
                //ii[i].CustomData["SellPrice"] = list[i].Prices[1].ToString();
                //ii[i].CustomData["ReinforcedPrice"] = list[i].ReinforcedPrice.ToString();
            }
        }, error => { });
    }
    public int GetItemPrices(string id, string itemclass)
    {
        CatalogItem item = null;
        GetCatalogItemsRequest request = new GetCatalogItemsRequest() { CatalogVersion = "Main" };
        PlayFabClientAPI.GetCatalogItems(request, result =>
        {
            List<CatalogItem> items = result.Catalog;
            foreach (CatalogItem i in items)
            {
                if (i.ItemId == id)
                {
                    item = i;

                    break;
                }
            }
        }, error => { });
        return Convert.ToInt32(item.VirtualCurrencyPrices["GD"]);
    }
    #endregion
    void RemoveItemListData(Item item)
    {
        switch (CurrentScrollNum)
        {
            case 0:
                SharedObject.g_playerPrefsdata.playerdataNoSave.WeaponItem.Remove(item.itemInfo.itemdata);
                break;
            case 1:
                SharedObject.g_playerPrefsdata.playerdataNoSave.HelmetItem.Remove(item.itemInfo.itemdata);
                break;
            case 2:
                SharedObject.g_playerPrefsdata.playerdataNoSave.ArmorItem.Remove(item.itemInfo.itemdata);
                break;
            case 3:
                SharedObject.g_playerPrefsdata.playerdataNoSave.LeggingsItem.Remove(item.itemInfo.itemdata);
                break;
            case 4:
                SharedObject.g_playerPrefsdata.playerdataNoSave.BootsItem.Remove(item.itemInfo.itemdata);
                break;
            case 5:
                SharedObject.g_playerPrefsdata.playerdataNoSave.PotionsItem.Remove(item.itemInfo.itemdata);
                break;

        }
        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
    }
    #region 아이템추가
    public void ItemAdd(PlayerPrefsData.ItemData itemInfo_)
    {

        if (PlayerHPManager.instance.character.Death)
            return;
        List<GameObject> list = new List<GameObject>();
        switch (CurrentScrollNum)
        {
            case 0:
                list = m_WeaponList;
                break;
            case 1:
                list = m_HelmetList;
                break;
            case 2:
                list = m_ArmorList;

                break;
            case 3:
                list = m_LeggingsList;
                break;
            case 4:
                list = m_BootsList;
                break;
            case 5:
                list = m_PotionList;
                break;

        }
        GameObject obj = Instantiate(ItemPrefab[CurrentScrollNum], Vector2.zero, Quaternion.identity, ItemPrefabTrans[CurrentScrollNum]) as GameObject;
        ItemInfo itemInfo = obj.GetComponent<ItemInfo>();
        obj.GetComponent<Item>().itemInfo = itemInfo;
        list.Add(obj);
        scrollbar_[CurrentScrollNum].value = 1;
        itemInfo.itemdata = itemInfo_;
        obj.transform.localScale = Vector2.one;
        itemInfo.ResetInfo();

    }
    public void SellItem()
    {
        SharedObject.g_playerPrefsdata.playerdataNoSave.EarnedGold += ChooseItem.itemInfo.itemdata.Prices[1];
        SharedObject.g_playerPrefsdata.playerdataNoSave.Gold += ChooseItem.itemInfo.itemdata.Prices[1];
        int index = 0;
        switch (CurrentScrollNum)
        {
            case 0:
                index = m_WeaponList.IndexOf(ChooseItem.gameObject);
                m_WeaponList.RemoveAt(index);
                SharedObject.g_playerPrefsdata.playerdataNoSave.WeaponItem.RemoveAt(index);
                break;
            case 1:
                index = m_HelmetList.IndexOf(ChooseItem.gameObject);
                m_HelmetList.RemoveAt(index);
                SharedObject.g_playerPrefsdata.playerdataNoSave.HelmetItem.RemoveAt(index);
                break;
            case 2:
                index = m_ArmorList.IndexOf(ChooseItem.gameObject);
                m_ArmorList.RemoveAt(index);
                SharedObject.g_playerPrefsdata.playerdataNoSave.ArmorItem.RemoveAt(index);
                break;
            case 3:
                index = m_LeggingsList.IndexOf(ChooseItem.gameObject);
                m_LeggingsList.RemoveAt(index);
                SharedObject.g_playerPrefsdata.playerdataNoSave.LeggingsItem.RemoveAt(index);
                break;
            case 4:
                index = m_BootsList.IndexOf(ChooseItem.gameObject);
                m_BootsList.RemoveAt(index);
                SharedObject.g_playerPrefsdata.playerdataNoSave.BootsItem.RemoveAt(index);
                break;
        }
        Destroy(ChooseItem.gameObject);
        CanvasManager.SetGoldText();
        HideItemInfo();
    }

    #endregion
    #region 아이템 제거
    public void ItemRemove()
    {
        if (PlayerHPManager.instance.character.Death)
            return;
        switch (CurrentScrollNum)
        {
            case 0:
                ItemRemoveType(m_WeaponList);
                break;
            case 1:
                ItemRemoveType(m_HelmetList);
                break;
            case 2:
                ItemRemoveType(m_ArmorList);
                break;
            case 3:
                ItemRemoveType(m_LeggingsList);
                break;
            case 4:
                ItemRemoveType(m_BootsList);
                break;
            case 5:
                ItemRemoveType(m_PotionList);
                break;
        }
        if (_ItemInfo.activeSelf)
            HideItemInfo();
        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
    }
    #endregion
    private void ItemRemoveType(List<GameObject> list)
    {
        if (list.Count.Equals(0))
            return;
        for (int i = list.Count - 1; i >= 0; i--)
        {
            if (list[i].activeSelf)
            {

                DestroyItem(list[i], list);
                break;
            }

        }
        return;
    }
    public void DestroyItem(GameObject item, List<GameObject> list)
    {
        int num = list.IndexOf(item);
        PlayFabServerAPI.RevokeInventoryItem(new PlayFab.ServerModels.RevokeInventoryItemRequest() { ItemInstanceId = item.GetComponent<Item>().itemInfo.itemdata.ItemId, PlayFabId = SharedObject.g_playerPrefsdata.playfabid }, success =>
        {

        }, error => {  });
        Destroy(list[num]);
        RemoveItemListData(item.GetComponent<Item>());
        list.RemoveAt(num);
    }
    #region 아이템 정렬
    public void ItemSort()
    {
        switch (CurrentScrollNum)
        {
            case 0:
                var result = (from n in m_WeaponList orderby n.GetComponent<ItemInfo>().itemdata.rating select n).ToList();
                m_WeaponList = result;
                for (int i = 0; i < m_WeaponList.Count; i++)
                {
                    m_WeaponList[i].transform.SetSiblingIndex(i);
                }
                break;
            case 1:
                var result2 = (from n in m_HelmetList orderby n.GetComponent<ItemInfo>().itemdata.rating select n).ToList();
                m_HelmetList = result2;
                for (int i = 0; i < m_HelmetList.Count; i++)
                {
                    m_HelmetList[i].transform.SetSiblingIndex(i);
                }
                break;
            case 2:
                var result3 = (from n in m_ArmorList orderby n.GetComponent<ItemInfo>().itemdata.rating select n).ToList();
                m_ArmorList = result3;
                for (int i = 0; i < m_ArmorList.Count; i++)
                {
                    m_ArmorList[i].transform.SetSiblingIndex(i);
                }
                break;
            case 3:
                var result4 = (from n in m_LeggingsList orderby n.GetComponent<ItemInfo>().itemdata.rating select n).ToList();
                m_LeggingsList = result4;
                for (int i = 0; i < m_LeggingsList.Count; i++)
                {
                    m_LeggingsList[i].transform.SetSiblingIndex(i);
                }
                break;
            case 4:
                var result5 = (from n in m_BootsList orderby n.GetComponent<ItemInfo>().itemdata.rating select n).ToList();
                m_BootsList = result5;
                for (int i = 0; i < m_BootsList.Count; i++)
                {
                    m_BootsList[i].transform.SetSiblingIndex(i);
                }
                break;
            case 5:
                var result6 = (from n in m_PotionList orderby n.GetComponent<ItemInfo>().itemdata.rating select n).ToList();
                m_PotionList = result6;
                for (int i = 0; i < m_PotionList.Count; i++)
                {
                    m_PotionList[i].transform.SetSiblingIndex(i);
                }
                break;

        }

    }
    #endregion
    #region 아이템 스크롤 변경
    public void VisibleScroll(int Num)
    {
        switch (Num)
        {
            case 0:
                for (int i = 0; i < m_WeaponList.Count; i++)
                {
                    if (m_WeaponList[i].GetComponent<ItemInfo>().itemdata.Level > 0)
                        m_WeaponList[i].GetComponent<ItemInfo>().ResetInfo();
                }
                break;
            case 1:
                for (int i = 0; i < m_HelmetList.Count; i++)
                {
                    if (m_HelmetList[i].GetComponent<ItemInfo>().itemdata.Level > 0)
                        m_HelmetList[i].GetComponent<ItemInfo>().ResetInfo();
                }
                break;
            case 2:
                for (int i = 0; i < m_ArmorList.Count; i++)
                {
                    if (m_ArmorList[i].GetComponent<ItemInfo>().itemdata.Level > 0)
                        m_ArmorList[i].GetComponent<ItemInfo>().ResetInfo();
                }
                break;
            case 3:
                for (int i = 0; i < m_LeggingsList.Count; i++)
                {
                    if (m_LeggingsList[i].GetComponent<ItemInfo>().itemdata.Level > 0)
                        m_LeggingsList[i].GetComponent<ItemInfo>().ResetInfo();
                }
                break;

        }
        for (int i = 0; i < Scrolls.Length; i++)
        {
            Scrolls[i].SetActive(false);
        }
        Scrolls[Num].SetActive(true);
        CurrentScrollNum = Num;
    }
    #endregion
    #region 아이템정보 표시
    public void VisibleItemInfo(Item _item)
    {
        bool active = false;
        if (_item.itemInfo.itemdata.m_ITEMTYPE != m_Enum.ITEMTYPE.POTION)
        {
            active = _item.itemInfo.itemdata.Equip;

        }
        else
        {
            active = true;
        }
        ItemInfoBtns[1].SetActive(!active);
        ItemInfoBtns[0].gameObject.GetComponent<RectTransform>().anchoredPosition = ItemEquipTrans[Convert.ToInt32(active)].GetComponent<RectTransform>().anchoredPosition;
        ItemInfoBtns[0].gameObject.transform.localScale = ItemEquipTrans[Convert.ToInt32(active)].localScale;
        ItemDescriptionText.text = _item.itemInfo.itemdata.Description;
        ItemNameText.text = _item.itemInfo.itemdata.Name;
        StatUpgrade.SetActive(false);
        EquipInfo.transform.position = EquipInfoPoses[1].position;
        _ItemInfo.SetActive(true);
        ChooseItem = _item;
        SetItemNum((byte)_item.itemInfo.itemdata.m_ITEMTYPE, _item);
        for (int i = 0; i < ItemInfoValueTexts.Length; i++)
        {
            ItemInfoValueTexts[i].text = _item.itemInfo.itemdata.Data[i] == "" ? "" : _item.itemInfo.itemdata.Data[i] + ":";
            ItemInfoValues[i].text = _item.itemInfo.itemdata.Values[i] == 0 ? "" : _item.itemInfo.itemdata.Values[i].ToString();

        }
        ItemImg.sprite = _item.itemInfo.sprite;
        Equip.text = _item.itemInfo.itemdata.Equip ? "장착해제" : "장착";
    }
    #endregion
    #region 아이템 정보 숨기기
    public void HideItemInfo()
    {
        StatUpgrade.SetActive(true);
        _ItemInfo.SetActive(false);
        EquipInfo.transform.position = EquipInfoPoses[0].position;
        ItemClickPossible = true;
        PotionEquipInfo.SetActive(false);
    }
    #endregion
    #region 아이템 설정
    private void SetItemNum(int Num, Item _item)
    {

        Currentitem[Num] = _item;
        if (item[Num] == null)
            item[Num] = _item;
    }
    #endregion
    #region 아이템 장착
    public void ItemEquip()
    {
        if (character.GetComponent<Player>().IsFighting)
        {
            StartCoroutine(SetItemInfoErrorText("전투 중에는 이 행동을 하실 수 없습니다."));
            return;
        }
        ItemEquipNum((byte)ChooseItem.itemInfo.itemdata.m_ITEMTYPE);
        if (PotionEquipInfo.activeSelf)
            VisibleItemInfoEquip(5);
        bool active = false;
        if (ChooseItem.itemInfo.itemdata.m_ITEMTYPE != m_Enum.ITEMTYPE.POTION)
        {
            active = ChooseItem.itemInfo.itemdata.Equip;

        }
        else
        {
            active = true;
        }
        if (ChooseItem.itemInfo.itemdata.Equip)
        {
            HideItemInfo();
        }
        ItemInfoBtns[1].SetActive(!active);
        ItemInfoBtns[0].gameObject.GetComponent<RectTransform>().anchoredPosition = ItemEquipTrans[Convert.ToInt32(active)].GetComponent<RectTransform>().anchoredPosition;
        ItemInfoBtns[0].gameObject.transform.localScale = ItemEquipTrans[Convert.ToInt32(active)].localScale;
        ResetValues();

        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
    }
    IEnumerator SetItemInfoErrorText(string text)
    {
        ItemInfoErrorText.gameObject.SetActive(true);
        ItemInfoErrorText.text = text;
        yield return new WaitForSeconds(1);
        ItemInfoErrorText.gameObject.SetActive(false);

    }
    #endregion
    public void VisibleItemInfoEquip_Potion(int Num)
    {
        if (EquipPotions[Num] != null)
        {
            VisibleItemInfo(EquipPotions[Num]);
            item[5] = EquipPotions[Num];

        }
    }
    #region 아이템 장착확인
    private void ItemEquipNum(int Num)
    {
        if (!ItemClickPossible)
            return;
        Image[] imgs = { ItemEquipBg[Num], ItemEquipOutLine[Num] };
        List<GameObject> list = new List<GameObject>();
        List<PlayerPrefsData.ItemData> itemlist = new List<PlayerPrefsData.ItemData>();
        switch (Num)
        {
            case 0:
                list = m_WeaponList;
                itemlist = SharedObject.g_playerPrefsdata.playerdataNoSave.WeaponItem;
                break;
            case 1:
                list = m_HelmetList;
                itemlist = SharedObject.g_playerPrefsdata.playerdataNoSave.HelmetItem;
                break;
            case 2:
                list = m_ArmorList;
                itemlist = SharedObject.g_playerPrefsdata.playerdataNoSave.ArmorItem;
                break;
            case 3:
                list = m_LeggingsList;
                itemlist = SharedObject.g_playerPrefsdata.playerdataNoSave.LeggingsItem;
                break;
            case 4:
                list = m_BootsList;
                itemlist = SharedObject.g_playerPrefsdata.playerdataNoSave.BootsItem;
                break;
            case 5:
                list = m_PotionList;
                itemlist = SharedObject.g_playerPrefsdata.playerdataNoSave.PotionsItem;
                break;

        }
        for (int i = 0; i < list.Count; i++)
        {
            if (!list[i].activeSelf)
                list[i].SetActive(true);
        }
        if (item[Num] != null)
        {

            if (!item[Num].Equals(ChooseItem))
            {
                if (!item[Num].itemInfo.itemdata.m_ITEMTYPE.Equals(m_Enum.ITEMTYPE.POTION))
                {
                    if (item[Num].itemInfo.itemdata.Equip)
                    {
                        item[Num].itemInfo.itemdata.Equip = false;
                        switch (Num)
                        {
                            case 0:
                                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALPOWER] -= item[Num].itemInfo.itemdata.Values[0];
                                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALATTACKSPEED] -= item[Num].itemInfo.itemdata.Values[1];


                                break;
                            case 1:
                                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALDEF] -= item[Num].itemInfo.itemdata.Values[0];
                                character.Max[(byte)m_Enum.MAXSTAT.MAXADDITIONALHP] -= item[Num].itemInfo.itemdata.Values[1];
                                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALHP] -= item[Num].itemInfo.itemdata.Values[1];
                                break;
                            case 2:
                                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALDEF] -= item[Num].itemInfo.itemdata.Values[0];
                                character.Max[(byte)m_Enum.MAXSTAT.MAXADDITIONALHP] -= item[Num].itemInfo.itemdata.Values[1];
                                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALHP] -= item[Num].itemInfo.itemdata.Values[1];

                                break;
                            case 3:
                                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALDEF] -= item[Num].itemInfo.itemdata.Values[0];
                                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALSPEED] -= item[Num].itemInfo.itemdata.Values[1];
                                break;
                            case 4:
                                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALDEF] -= item[Num].itemInfo.itemdata.Values[0];
                                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALSPEED] -= item[Num].itemInfo.itemdata.Values[1];
                                break;
                        }
                    }
                }
                else
                {
                    if (EquipPotions[MaxNumPotionsPossessed - 1] != null)
                    {
                        if (item[Num].itemInfo.itemdata.Equip)
                        {
                            item[Num].itemInfo.itemdata.Equip = false;
                        }
                            item[Num].gameObject.SetActive(true);
                        EquipPotions[EquipPotions.IndexOf(item[Num])] = null;
                    }
                }

            }

            item[Num] = ChooseItem;
            ChooseItem.itemInfo.itemdata.Equip = !item[Num].itemInfo.itemdata.Equip;
            if (ChooseItem.itemInfo.itemdata.Equip)
            {
                item[Num].gameObject.SetActive(false);
                if (item[Num].itemInfo.itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.POTION)
                {
                    int count = 0;
                    for (int i = 0; i < EquipPotions.Count;)
                    {
                        if (EquipPotions[i] == null)
                        {

                            EquipPotions[i] = item[Num];
                            Color colorP = PotionEquipImgs[i].color;
                            SharedScript.instance.itemUI.EquipItem(EquipPotions[i].itemInfo.sprite, i);
                            colorP.a = 1;
                            PotionEquipImgs[i].color = colorP;
                            i = EquipPotions.Count;

                            continue;
                        }
                        else
                        {
                            count++;
                            ItemEquipImg[Num].sprite = item[Num].itemInfo.sprite;
                            Color color = ItemEquipImg[Num].color;
                            color.a = 1;
                            ItemEquipImg[Num].color = color;
                            i++;
                        }

                    }
                    ItemEquipImg[Num].sprite = PotionCount[count];
                }
                else
                {
                    Color[] colors = { new Color(item[Num].itemInfo.itemdata.colorStore[0], item[Num].itemInfo.itemdata.colorStore[1], item[Num].itemInfo.itemdata.colorStore[2]), new Color(item[Num].itemInfo.itemdata.colorStore2[0], item[Num].itemInfo.itemdata.colorStore2[1], item[Num].itemInfo.itemdata.colorStore2[2]) };
                    PublicFunction.SetImageColor(imgs, colors);
                    ItemEquipImg[Num].sprite = item[Num].itemInfo.sprite;
                    Color color = ItemEquipImg[Num].color;
                    color.a = 1;
                    ItemEquipImg[Num].color = color;
                    ItemEquip(Num, 1);
                    item[Num].gameObject.SetActive(false);
                }
            }
            else
            {
                item[Num].gameObject.SetActive(true);
                if (item[Num].itemInfo.itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.POTION)
                {
                    EquipPotions[EquipPotions.IndexOf(item[Num])] = null;
                    int count = 0;
                    for (int i = 0; i < EquipPotions.Count; i++)
                    {
                        if (EquipPotions[i] == null)
                        {


                            PotionEquipImgs[i].sprite = null;
                            SharedScript.instance.itemUI.EquipItem(null, i);
                        }
                        else
                            count++;
                    }
                    if (count == 0)
                    {
                        Color[] colors = { ItemUnEquipColors[0], ItemUnEquipColors[1] };
                        ItemEquipImg[Num].sprite = ItemUnEquipImg[Num];
                        PublicFunction.SetImageColor(imgs, colors);
                        Color color = ItemEquipImg[Num].color;
                        color.a = ItemUnEquipColora;
                    }
                    else
                        ItemEquipImg[Num].sprite = PotionCount[count - 1];
                    for (int i = 0; i < PotionEquipImgs.Length; i++)
                    {
                        if (PotionEquipImgs[i].sprite != null)
                            continue;
                        Color colorP = PotionEquipImgs[i].color;
                        colorP.a = 0;
                        PotionEquipImgs[i].color = colorP;

                    }
                }
                else
                {
                    item[Num] = null;
                    Color[] colors = { ItemUnEquipColors[0], ItemUnEquipColors[1] };
                    ItemEquipImg[Num].sprite = ItemUnEquipImg[Num];
                    PublicFunction.SetImageColor(imgs, colors);
                    Color color = ItemEquipImg[Num].color;
                    ItemEquip(Num, -1);
                    color.a = ItemUnEquipColora;
                    ItemEquipImg[Num].color = color;
                }
            }
            Equip.text = ChooseItem.itemInfo.itemdata.Equip ? "장착해제" : "장착";
            for (int i = 0; i < list.Count; i++)
            {

                if (list[i].GetComponent<ItemInfo>().itemdata == ChooseItem.itemInfo.itemdata)
                {
                    itemlist[i].Equip = ChooseItem.itemInfo.itemdata.Equip;
                    break;
                }
            }
            if (ChooseItem.itemInfo.itemdata.m_ITEMTYPE != m_Enum.ITEMTYPE.POTION && ChooseItem.itemInfo.itemdata.Level > 0 && ChooseItem.itemInfo.itemdata.Equip)
                EquipItemOutLine[(byte)ChooseItem.itemInfo.itemdata.m_ITEMTYPE].material = LevelOutLineMat[Convert.ToInt32(ChooseItem.itemInfo.itemdata.Level > 4)];
            else
                EquipItemOutLine[(byte)ChooseItem.itemInfo.itemdata.m_ITEMTYPE].material = null;
            if (EquipItemUpgradeTexts[(byte)ChooseItem.itemInfo.itemdata.m_ITEMTYPE] != null)
            {

                EquipItemUpgradeTexts[(byte)ChooseItem.itemInfo.itemdata.m_ITEMTYPE].gameObject.SetActive(ChooseItem.itemInfo.itemdata.Level != 0 && ChooseItem.itemInfo.itemdata.Equip);
                EquipItemUpgradeTexts[(byte)ChooseItem.itemInfo.itemdata.m_ITEMTYPE].text = "+" + ChooseItem.itemInfo.itemdata.Level.ToString();
            }

            if (character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALHP] > character.Max[(byte)m_Enum.MAXSTAT.MAXADDITIONALHP] || SceneManager.GetActiveScene().name == "Lobby")
            {
                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALHP] = character.Max[(byte)m_Enum.MAXSTAT.MAXADDITIONALHP];
            }
            if (Currentitem[Num] != null && !Currentitem[Num].itemInfo.itemdata.Equip)
            {
                Currentitem[Num] = null;
            }

            if (gameObject.activeSelf)
                StartCoroutine(ItemClickDelay());
        }
    }
    #endregion
    #region 플레이어 스탯 추가,제거
    private void ItemEquip(int Num, int increase)
    {
        switch (Num)
        {
            case 0:
                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALPOWER] = character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALPOWER] + increase * ChooseItem.itemInfo.itemdata.Values[0];
                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALATTACKSPEED] = character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALATTACKSPEED] + increase * ChooseItem.itemInfo.itemdata.Values[1];

                break;
            case 1:
                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALDEF] = character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALDEF] + increase * ChooseItem.itemInfo.itemdata.Values[0];
                character.Max[(byte)m_Enum.MAXSTAT.MAXADDITIONALHP] = character.Max[(byte)m_Enum.MAXSTAT.MAXADDITIONALHP] + increase * ChooseItem.itemInfo.itemdata.Values[1];
                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALHP] = character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALHP] + increase * ChooseItem.itemInfo.itemdata.Values[1];
                break;
            case 2:
                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALDEF] = character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALDEF] + increase * ChooseItem.itemInfo.itemdata.Values[0];
                character.Max[(byte)m_Enum.MAXSTAT.MAXADDITIONALHP] = character.Max[(byte)m_Enum.MAXSTAT.MAXADDITIONALHP] + increase * ChooseItem.itemInfo.itemdata.Values[1];
                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALHP] = character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALHP] + increase * ChooseItem.itemInfo.itemdata.Values[1];
                break;
            case 3:
                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALDEF] = character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALDEF] + increase * ChooseItem.itemInfo.itemdata.Values[0];
                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALSPEED] = character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALSPEED] + increase * ChooseItem.itemInfo.itemdata.Values[1];
                break;
            case 4:
                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALDEF] = character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALDEF] + increase * ChooseItem.itemInfo.itemdata.Values[0];
                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALSPEED] = character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALSPEED] + increase * ChooseItem.itemInfo.itemdata.Values[1];
                break;
        }
    }
    #endregion
    IEnumerator ItemClickDelay()
    {
        ItemClickPossible = false;
        yield return new WaitForSeconds(0.5f);
        ItemClickPossible = true;
    }

    public void AddPlayerStat(int Num)
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        if (character.StatPoint > 0 && character.AddStatValues[Num] < float.Parse(SharedScript.instance.csvdata.PlayerSTAT[Num]["MaxAddSTAT"].ToString()))
        {
            character.StatPoint--;
            if (Num == 5)
            {
                character.Mana += int.Parse(SharedScript.instance.csvdata.PlayerSTAT[Num]["addSTAT"].ToString());
                character.Max[(byte)m_Enum.MAXSTAT.MAXMANA] += float.Parse(SharedScript.instance.csvdata.PlayerSTAT[Num]["addSTAT"].ToString());
            }
            else
            {
                character.F[Num] += float.Parse(SharedScript.instance.csvdata.PlayerSTAT[Num]["addSTAT"].ToString());
                if (Num == (byte)m_Enum.STAT.HP)
                    character.Max[Num] += float.Parse(SharedScript.instance.csvdata.PlayerSTAT[Num]["addSTAT"].ToString());

            }
            character.AddStatValues[Num] += float.Parse(SharedScript.instance.csvdata.PlayerSTAT[Num]["addSTAT"].ToString());
            ResetValues();

        }
        else
        {
            SharedObject.g_SoundManager.PlayErrorAudio();
        }
    }
    
}
