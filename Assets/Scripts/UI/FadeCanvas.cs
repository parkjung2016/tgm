using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class FadeCanvas : MonoBehaviour
{
    public Image Fade;
    static public FadeCanvas instance;

    private void Awake()
    {
        if(instance == null)
        {
        instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    private void Start()
    {
        Fade.gameObject.SetActive(false);
        Fade.DOFade(0, 0);
    }
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Fade.DOFade(0, 0);
        Fade.gameObject.SetActive(false);
    }
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
