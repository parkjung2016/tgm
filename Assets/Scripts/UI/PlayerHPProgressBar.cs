using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Photon.Pun;
using Photon;
using Photon.Realtime;
using ExitGames.Client.Photon;
using System;

public class PlayerHPProgressBar : MonoBehaviour
{
    public Character character;
    public Image Progress;
    public Text Name;
    private Vector3 vector;
    public RectTransform recttransform;
    private void Awake()
    {
        recttransform = GetComponent<RectTransform>();
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }
    private void Start()
    {
        PlayerHPProgressBar[] playerHPProgressBar = GetComponentsInChildren<PlayerHPProgressBar>();
        if (playerHPProgressBar.Length > 1)
        {
            Destroy(gameObject);
        }
        vector = new Vector3(0.118f, 0.118f, 0.118f);
        StartCoroutine(Flip());
        SetCharacter();
    }
    private void OnEnable()
    {
        
        StartCoroutine(Flip());
    }
    IEnumerator Flip()
    {
  
        if (character != null)
        {
            if (character.transform.localScale.x  < 0)
            {

                transform.localScale = new Vector2(-vector.x, vector.y);

            }
            else
            {
                transform.localScale = vector;

            }
        }
        yield return new WaitForSeconds(.2f);
        StartCoroutine(Flip());
    }
    public void SetCharacter()
    {
        character = GetComponentInParent<Character>();
        if (character == null)
        {
            Invoke("SetCharacter", .02f);
            return;
        }
        recttransform.anchoredPosition = Vector2.zero;
        SetHP();
    }
    private void OnEvent(EventData obj)
    {

        //if(obj.Code == 19)
        //{
        //    StartCoroutine(Flip());
        //}
    }
    public void SetHP()
    {
        Progress.DOFillAmount((SharedObject.g_DataManager.m_MyCharacter.F[(int)m_Enum.STAT.HP] + SharedObject.g_DataManager.m_MyCharacter.Additional[(int)m_Enum.ADDITIONALSTAT.ADDITIONALHP]) / (SharedObject.g_DataManager.m_MyCharacter.Max[(int)m_Enum.MAXSTAT.MAXHP] + SharedObject.g_DataManager.m_MyCharacter.Max[(int)m_Enum.MAXSTAT.MAXADDITIONALHP]), 1);
    }
}
