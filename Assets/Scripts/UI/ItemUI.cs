using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Photon.Pun;
using DG.Tweening;
using ExitGames.Client.Photon;
using System;

public class ItemUI : MonoBehaviour
{
    static public ItemUI instance;
    public Image[] Potions;
    public Image[] Skills;
    [SerializeField]
    private GameObject PotionColl;
    public CoolDown coolDown_;
    [SerializeField]
    private GameObject HealthUpEffect;
    [SerializeField]
    private GameObject ManaUpEffect;
    
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        for (int i = 0; i < Potions.Length; i++)
        {
            Color color = Potions[i].color;
            color.a = 0;
            Potions[i].color = color;
        }
        coolDown_ = FindObjectOfType<CoolDown>();
    }

    public void EquipItem(Sprite sprite,int num)
    {
        Potions[num].sprite = sprite;
        if(Potions[num].sprite == null)
        {
  
            Potions[num].color = new Color(Potions[num].color .r, Potions[num].color .g, Potions[num].color .b,0);
        }
        else
        {
            Potions[num].color = new Color(Potions[num].color.r, Potions[num].color.g, Potions[num].color.b, 1);
        }
    }
    public void EquipSkill(Sprite sprite, int num)
    {
        Skills[num].sprite = sprite;
        if (Skills[num].sprite == null)
        {

            Skills[num].color = new Color(Potions[num].color.r, Potions[num].color.g, Potions[num].color.b, 0);
        }
        else
        {
            Skills[num].color = new Color(Potions[num].color.r, Potions[num].color.g, Potions[num].color.b, 1);
        }
    }
    public void UsePotion(int num)
    {
        if (Potions[num].color.a  != 0)
        {
            bool colora0 = true;
            if(SharedScript.instance.inventory.EquipPotions[num] !=null)
            {
                switch(SharedScript.instance.inventory.EquipPotions[num].itemInfo.itemdata.ratingName)
                {
                    case "체력회복":
                        if (SharedObject.g_DataManager.m_MyCharacter.Additional[(int)m_Enum.ADDITIONALSTAT.ADDITIONALHP] > 0)
                        {
                            if (SharedObject.g_DataManager.m_MyCharacter.Additional[(int)m_Enum.ADDITIONALSTAT.ADDITIONALHP] < SharedObject.g_DataManager.m_MyCharacter.Max[(int)m_Enum.MAXSTAT.MAXADDITIONALHP])
                            {
                                object data = Mathf.Clamp(SharedObject.g_DataManager.m_MyCharacter.Additional[(int)m_Enum.ADDITIONALSTAT.ADDITIONALHP] + SharedScript.instance.inventory.EquipPotions[num].itemInfo.itemdata.Values[0], 0, SharedObject.g_DataManager.m_MyCharacter.Max[(int)m_Enum.MAXSTAT.MAXADDITIONALHP]);
                                if (PhotonNetwork.InRoom)
                                {

                                    PlayerHPManager.instance.PV.RPC("PunRPC_AddAdditionalHP", RpcTarget.All, data);
                                }
                                else
                                {

                                    SharedObject.g_DataManager.m_MyCharacter.Additional[(int)m_Enum.ADDITIONALSTAT.ADDITIONALHP] = (float)data;
                                    SummonHealthUpeffect(SharedObject.g_DataManager.m_MyCharacter.transform);
                                }
                            }
                            else
                            {
                                SharedObject.g_SoundManager.PlayErrorAudio();
                                colora0 = false;
                                return;
                            }
                        }
                        else
                        {
                            if (SharedObject.g_DataManager.m_MyCharacter.F[(int)m_Enum.STAT.HP] < SharedObject.g_DataManager.m_MyCharacter.Max[(int)m_Enum.MAXSTAT.MAXADDITIONALHP] + SharedObject.g_DataManager.m_MyCharacter.Max[(int)m_Enum.MAXSTAT.MAXHP])
                            {
                                    object data = Mathf.Clamp(SharedObject.g_DataManager.m_MyCharacter.F[(int)m_Enum.STAT.HP] + SharedScript.instance.inventory.EquipPotions[num].itemInfo.itemdata.Values[0], 0, SharedObject.g_DataManager.m_MyCharacter.Max[(int)m_Enum.MAXSTAT.MAXHP]);
                                if (PhotonNetwork.InRoom)
                                {
                                    PlayerHPManager.instance.PV.RPC("PunRPC_AddHP", RpcTarget.All, data);
                                }
                                else
                                {
                                    SharedObject.g_DataManager.m_MyCharacter.F[(int)m_Enum.STAT.HP] = (float)data;
                                    SummonHealthUpeffect(SharedObject.g_DataManager.m_MyCharacter.transform);
                                }
                               
                            }
                            else
                            {
                                SharedObject.g_SoundManager.PlayErrorAudio();
                                colora0 = false;
                                return;
                            }
                        }
                        break;
                    case "힘":
                        PotionDelay((byte)m_Enum.STAT.POWER, (byte)SharedScript.instance.inventory.EquipPotions[num].itemInfo.itemdata.Values[1], SharedScript.instance.inventory.EquipPotions[num].itemInfo.itemdata.Values[0]);
                        break;
                    case "이속":
                        PotionDelay((byte)m_Enum.STAT.SPEED, (byte)SharedScript.instance.inventory.EquipPotions[num].itemInfo.itemdata.Values[1], SharedScript.instance.inventory.EquipPotions[num].itemInfo.itemdata.Values[0]);
                        break;
                    case "마나":
                        if (SharedObject.g_DataManager.m_MyCharacter.Mana < SharedObject.g_DataManager.m_MyCharacter.Max[(int)m_Enum.MAXSTAT.MAXMANA])
                        {
                            object data = Mathf.Clamp(SharedObject.g_DataManager.m_MyCharacter.Mana + SharedScript.instance.inventory.EquipPotions[num].itemInfo.itemdata.Values[0], 0, SharedObject.g_DataManager.m_MyCharacter.Max[(int)m_Enum.MAXSTAT.MAXMANA]);
                                SharedObject.g_DataManager.m_MyCharacter.Mana = (int)(float)data;
                                SummonManaUpeffect(SharedObject.g_DataManager.m_MyCharacter.transform);
                        }
                        else
                        {
                            SharedObject.g_SoundManager.PlayErrorAudio();
                            colora0 = false;
                            return;
                        }
                        break;
                }
            SharedScript.instance.inventory.ChooseItem = SharedScript.instance.inventory.EquipPotions[num];
            SharedScript.instance.inventory.CurrentScrollNum = 5;
            SharedScript.instance.inventory.item[5] = SharedScript.instance.inventory.ChooseItem;
            SharedScript.instance.inventory.ItemEquip();
            SharedScript.instance.inventory.DestroyItem(SharedScript.instance.inventory.m_PotionList[SharedScript.instance.inventory.m_PotionList.IndexOf(SharedScript.instance.inventory.ChooseItem.gameObject)], SharedScript.instance.inventory.m_PotionList);
                SharedScript.instance.inventory.EquipPotions[num] = null;
                if (colora0)
                {

                    Color color = Potions[num].color;
                    color.a = 0;
                    Potions[num].color = color;
                }
            }


        }
    }
    private void PotionDelay(byte num,byte Delay,float Value)
    {

        SharedObject.g_DataManager.m_MyCharacter.F[num] += Value;
        GameObject obj =  coolDown_.AddGrid(PotionColl.gameObject) ;
        obj.transform.GetChild(1).GetComponent<Image>().DOFillAmount(1, 0);
        obj.transform.GetChild(1).GetComponent<Image>().DOFillAmount(0, Delay).OnComplete(() =>
        {
            SharedObject.g_DataManager.m_MyCharacter.F[num] -= Value;
            Destroy(obj);

        });
    }
    public void UseSkill(int Num)
    {
        if (SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num] != null)
        {
            if (Photon.Pun.PhotonNetwork.IsConnected)
                SharedObject.g_DataManager.m_MyCharacter.GetComponent<Photon.Pun.PhotonView>().RPC("UseSkill", Photon.Pun.RpcTarget.All, SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].skilltype, Num, SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].SkillCool);
            else
              PlayerSkillManager.instance.UseSkill(SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].skilltype, Num, SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].SkillCool);
        }
    }
    public void SummonHealthUpeffect(Transform _transform)
    {
        GameObject obj = Instantiate(HealthUpEffect, _transform.position, Quaternion.Euler(-90, 0, 0));
        Destroy(obj, 4);
    }
    public void SummonManaUpeffect(Transform _transform)
    {
        GameObject obj = Instantiate(ManaUpEffect, _transform.position, Quaternion.Euler(-90, 0, 0));
        Destroy(obj, 4);
    }
}
