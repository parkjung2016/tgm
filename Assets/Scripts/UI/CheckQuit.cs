using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckQuit : MonoBehaviour
{
    public GameObject Group;
    private void Start()
    {

        Group.SetActive(false);
    }
    private void Update()
    {

        //if (Application.platform == RuntimePlatform.Android)
        //{
            if (Input.GetKey(KeyCode.Escape))
            {

                if (!Group.activeSelf)
                Group.SetActive(true);

            }

        //}

    }
    public void Quit()
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit(); // 어플리케이션 종료
#endif
    }
    public void Cancel()
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        Group.SetActive(false);
    }

}
