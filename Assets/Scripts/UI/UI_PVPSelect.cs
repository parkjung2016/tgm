using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Realtime;
using Photon.Pun;
using DG.Tweening;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using System;

public class UI_PVPSelect : MonoBehaviour
{
    CanvasGroup canvasGroup;
    [SerializeField]
    private GameObject[] Selects;
    [SerializeField]
    private GameObject RoomBtn;
    [SerializeField]
    private Transform Content;
    [SerializeField]
    private GameObject[] Type;
    [SerializeField]
    private GameObject[] TeamImg;
    [SerializeField]
    private Text[] PlayerName;
    public GameObject MapSelect_;
    public List<roominfo> RoomList;
    public Text BtnText;
    public bool CheckExit;
    public m_Enum.MAP currentMap;
    [SerializeField]
    private Text ErrorText;
    private bool Ready = false;
    public Sprite[] MapImgs;
    public List<int>  Map = new List<int>();
    public bool trueleftroom;
    [SerializeField]
    private GameObject ErrorRoom;
    [SerializeField]
    private Text ErrorRoomText;
    MapSelect[] mapSelect;
    [SerializeField]
    private Image[] PlayerPetImages;
    [SerializeField]
    private Sprite[] PlayerPetSprites;
    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        PhotonNetwork.NetworkingClient.EventReceived +=OnEvent;
        mapSelect = FindObjectsOfType<MapSelect>();
    }
    void Start()
    {

        currentMap = m_Enum.MAP.NULL;
        Ready = false;
        ErrorText.gameObject.SetActive(false);
        if(!MyScene.instance.PVPRoom)
        {

        gameObject.SetActive(false);
        canvasGroup.alpha = 0;
        ActiveType(0,false);
            for (int i = 0; i < TeamImg.Length; i++)
            {
                TeamImg[i].SetActive(false);
                PlayerName[i].gameObject.SetActive(false);
            }
        }
        if (!PhotonNetwork.IsMasterClient)
        {
            for (int i = 0; i < NetworkManager.instance.myroomlist.Count; i++)
                Map.Add(100);

        }
        HideErrorRoom(); 
    }
    private void Update()
    {

        for (int i =0; i < NetworkManager.instance.myroomlist.Count;i++)
        {
         if(   NetworkManager.instance.myroomlist[i].CustomProperties.ContainsKey("Map") &&(int)NetworkManager.instance.myroomlist[i].CustomProperties["Map"] != Map[i])
            {
                Map[i] = (int)NetworkManager.instance.myroomlist[i].CustomProperties["Map"];
                if((int)NetworkManager.instance.myroomlist[i].CustomProperties["Map"] < (int)m_Enum.MAP.NULL)
                {

                RoomList[i].MapSprite = MapImgs[(int)NetworkManager.instance.myroomlist[i].CustomProperties["Map"]];
                RoomList[i].Init();
                }
            }
        }
    }
    public void Exit_UI_PVPSelect()
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        canvasGroup.DOFade(0, .5f).OnComplete(() =>
        {
            gameObject.SetActive(false); ActiveType(0,true); MapSelect_.SetActive(false);
        });

    }
    public void ActiveType(byte num, bool SFXOn)
    {
        if (SFXOn)
        {
            SharedObject.g_SoundManager.PlayBtnClickSound();
        }
            if (Type[num == 0 ? num + 1 : num - 1] == null || Type[num] == null)
            return;
        Type[num == 0 ? num + 1 : num - 1].SetActive(false);
        Type[num].SetActive(true);
    }
    public void Exit_Ready()
    {

        if(PhotonNetwork.IsMasterClient)
        {
            trueleftroom = true;
            object[] data = new object[]
            {
                (int)PhotonNetwork.LocalPlayer.CustomProperties["Team"],
                1
            };
        PhotonNetwork.RaiseEvent(40, null, NetworkManager.instance.otherraiseEvent, SendOptions.SendReliable);
        PhotonNetwork.RaiseEvent(3, null, NetworkManager.instance.raiseEvent, SendOptions.SendReliable);
        PhotonNetwork.RaiseEvent(7, null, NetworkManager.instance.raiseEvent, SendOptions.SendReliable);
            PhotonNetwork.RaiseEvent(0, data, NetworkManager.instance.raiseEvent, SendOptions.SendReliable);

        }
  else
        {
            PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable { { "Team1", "Remove" } });
            PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable { { "Pet1", "Remove" } });
            trueleftroom = false;

            object[] data = new object[]
          {
                (int)PhotonNetwork.LocalPlayer.CustomProperties["Team"],
                0
          };
            PhotonNetwork.RaiseEvent(0, data, NetworkManager.instance.raiseEvent, SendOptions.SendReliable);
            Ready = false;
            currentMap = m_Enum.MAP.NULL;
            HideMapSelect();
            PhotonNetwork.LeaveRoom();
        ActiveType(0, true);
        }
    }
    public void HideMapSelect()
    {
        if(mapSelect.Length > 0)
        for (int i = 0; i < mapSelect.Length; i++)
        {
            if (mapSelect[i] != null)
                mapSelect[i].Select.SetActive(false);
        }
    }
    IEnumerator Error(string Text )
    {
        ErrorText.gameObject.SetActive(true);
        SharedObject.g_SoundManager.PlayErrorAudio();
        ErrorText.text = Text;
        yield return new WaitForSeconds(1);
        ErrorText.gameObject.SetActive(false);
    }
    public void SelectType(int Num)
    {
        
        for (int i = 0; i < Selects.Length; i++)
            Selects[i].SetActive(false);
        Selects[Num].SetActive(true);
    }
    public void ClickStartOrReadyBtn()
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        if (PhotonNetwork.IsMasterClient)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount ==1)
            {
                StartCoroutine(Error("방 인원이 2명이어야 합니다."));
                return;
            }
            else if (currentMap == m_Enum.MAP.NULL)
            {
                StartCoroutine(Error("맵을 선택해야합니다."));

                return;
            }
            else
            {

                if (Ready)
                {

                    PhotonNetwork.RaiseEvent(8, null, NetworkManager.instance.raiseEventNoCache, SendOptions.SendUnreliable);
                    PhotonNetwork.CurrentRoom.IsOpen = false;

                }
                else
                {
                    StartCoroutine(Error("참가자가 준비를 해야합니다."));
                    return;
                }
            }
            //PhotonNetwork.RaiseEvent(8, null, NetworkManager.instance.raiseEventNoCache, SendOptions.SendUnreliable);
            //PhotonNetwork.CurrentRoom.IsOpen = false;
        }
        else
        {
            PhotonNetwork.RaiseEvent(6, null, NetworkManager.instance.raiseEventNoCache, SendOptions.SendReliable);
            BtnText.text = Ready ? "준비하기" : "준비해제";
        }   
    }
    public void HideErrorRoom()
    {
        ErrorRoom.SetActive(false);
    }
    public void EnableErrorRoom(string text)
    {

        ErrorRoom.SetActive(true);
        ErrorRoomText.text = text; 
    }
    public void InRoomUpdate(bool IsActive,int TeamNum)
    {
        if (PhotonNetwork.LocalPlayer.CustomProperties.ContainsKey("Team"))
        {
            if (!IsActive)
            {
                if (TeamNum - 1 == 0)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        if (TeamImg[i] != null)
                            TeamImg[i].SetActive(IsActive);
                        if (PlayerName[i] != null)
                            PlayerName[i].gameObject.SetActive(IsActive);
                    }
                }
                else
                {
                    if (TeamImg[TeamNum - 1] != null)
                        TeamImg[TeamNum - 1].SetActive(IsActive);
                    if (PlayerName[TeamNum - 1] != null)
                        PlayerName[TeamNum - 1].gameObject.SetActive(IsActive);

                }
                if (PhotonNetwork.NetworkingClient.State != ClientState.Leaving && trueleftroom)
                {
                    PhotonNetwork.LeaveRoom();
                }
            }
            else
            {
                TeamImg[0].SetActive(PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("Team0") && PhotonNetwork.CurrentRoom.CustomProperties["Team0"].ToString() != "Remove");
                PlayerName[0].gameObject.SetActive(PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("Team0") && PhotonNetwork.CurrentRoom.CustomProperties["Team0"].ToString() != "Remove");
                if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("Team0"))
                {
                    PlayerName[0].text = PhotonNetwork.CurrentRoom.CustomProperties["Team0"].ToString();
                    if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("Pet0"))
                    {

                PlayerPetImages[0].gameObject.SetActive(PhotonNetwork.CurrentRoom.CustomProperties["Pet0"].ToString() != "Remove");
                        if(PhotonNetwork.CurrentRoom.CustomProperties["Pet0"].ToString() != "Remove")
                        {
                        PlayerPetImages[0].sprite = PlayerPetSprites[int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["Pet0"].ToString())];
                        PlayerPetImages[0].transform.localScale = int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["Pet0"].ToString()) == 0 ? new Vector3(0.7f, 0.7f) : int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["Pet0"].ToString()) == 1 ? new Vector3(0.824f, 0.824f) : new Vector3(0.511f, 0.511f);

                        }

                    }
                }
                TeamImg[1].SetActive(PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("Team1") && PhotonNetwork.CurrentRoom.CustomProperties["Team1"].ToString() != "Remove");
                PlayerName[1].gameObject.SetActive(PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("Team1"));
                if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("Team1") && PhotonNetwork.CurrentRoom.CustomProperties["Team1"].ToString() != "Remove")
                {
                    PlayerName[1].text = PhotonNetwork.CurrentRoom.CustomProperties["Team1"].ToString();
                    if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("Pet1"))
                    {

                        PlayerPetImages[1].gameObject.SetActive(PhotonNetwork.CurrentRoom.CustomProperties["Pet1"].ToString() != "Remove");
                        if(PhotonNetwork.CurrentRoom.CustomProperties["Pet1"].ToString() != "Remove")
                        {
                        PlayerPetImages[1].sprite = PlayerPetSprites[int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["Pet1"].ToString())];
                        PlayerPetImages[1].transform.localScale = int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["Pet1"].ToString()) == 0 ? new Vector3(0.7f, 0.7f) : int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["Pet1"].ToString()) == 1 ? new Vector3(0.824f, 0.824f) : new Vector3(0.511f, 0.511f);

                        }

                    }
                }
            }
        }
    }
    public void JoinRoom(string name)
    {

        PhotonNetwork.JoinRoom(name);
        ActiveType(1, true);
    }
    public void CreateRoomBtn(RoomInfo roominfo)
    {
      roominfo roominfo_ =  Instantiate(RoomBtn,Content).GetComponent<roominfo>();
        RoomList.Add(roominfo_);
        roominfo_.RoomInfo_ = roominfo;
    }
    public void RemoveRoomBtn(int num)
    {
        Destroy( RoomList[num].gameObject);
        RoomList.RemoveAt(num);
    }
    public void CreateRoom(InputField input)
    {
        PhotonNetwork.CreateRoom(input.text, new RoomOptions { MaxPlayers = 2,CustomRoomProperties = new ExitGames.Client.Photon.Hashtable { { "Map", 90} },  CustomRoomPropertiesForLobby = new string[] { "Map" },IsOpen = true });
        ActiveType(1, true);
    }

    public void OnEvent(EventData photonEvent)
    {
        if(photonEvent.Code == 2)
        {
            object datas = photonEvent.CustomData;
            GameObject.Find("Map (" + ((byte)datas + 1) + ")").GetComponent<MapSelect>().SelectMap();
        }
        if(photonEvent.Code == 40)
        {
            EnableErrorRoom("방장이 나갔습니다.");
        }
        if (photonEvent.Code == 0)
        {

            object[] data = (object[])photonEvent.CustomData;

            if ((int)data[1] == 1)
                trueleftroom = true;
            else
            {
                    trueleftroom = false;
            }
            CheckExit = true;
            InRoomUpdate(false, (int)data[0]);

        }
        else if (photonEvent.Code == 1)
        {
            CheckExit = false;
        }
        else if (photonEvent.Code == 6)
        {
            Ready = !Ready;
        }
        else if (photonEvent.Code == 7)
        {
            Ready = false;
            ActiveType(0, true);
            currentMap = m_Enum.MAP.NULL;
        }
        else if (photonEvent.Code == 8)
        {
            AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
            for (int i = 0; i < audioSources.Length; i++)
            {
                audioSources[i].DOFade(0, .5f);
            }
            FadeCanvas.instance.Fade.gameObject.SetActive(true);
            FadeCanvas.instance.Fade.DOFade(1, 1f).OnComplete(() =>
            {
                LoadingSceneManager.LoadScene("PVPMap " + (byte)currentMap);
            });
        }

    }
}
