using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class CanvasManager : MonoBehaviour
{
    public static CanvasManager instance;
    [SerializeField]
    private GameObject Fade;
    [SerializeField]
    private GameObject DeathBG;
    [SerializeField]
    private Text GoldText;
    [SerializeField]
    private AnimationClip[] FadeClip;
    [SerializeField]
    private GameObject LevelUpImage;
    [SerializeField]
    private Text LevelUpText;
    public event EventHandler PlayerMoveLDown;
    public event EventHandler PlayerMoveLUp;
    public event EventHandler PlayerMoveRDown;
    public event EventHandler PlayerMoveRUp;
    public event EventHandler PlayerJump;
    public event EventHandler PlayerAttack;
    [SerializeField]
    private GameObject MapName;
    [SerializeField]
    private Text MapNameText;
    public bool NotLoad;
    private void Awake()
    {
        if (instance == null)
            instance = this;

    }
    private void Start()
    {
        MapName.SetActive(!Photon.Pun.PhotonNetwork.InRoom);
        if (!Photon.Pun.PhotonNetwork.InRoom)
        {
        MapNameText.text = SharedObject.MapNamestr[MyScene.instance.DungeonNum];

        }


        if (Fade.activeSelf)
            OutFade();
        if (DeathBG.activeSelf)
            FadeDeathBGHide();
        if(PVPManager.instance ==null)
        {
        FadeCanvas.instance.Fade.DOFade(1, 0);
        FadeCanvas.instance.Fade.gameObject.SetActive(true);

        }
        Init();
        LevelUpImage.transform.DOScale(0, 0f);
        LevelUpImage.GetComponent<Image>().DOFade(0, 0f);
        Invoke("MapNameHide", 3);
    }
    private void MapNameHide()
    {
        MapName.SetActive(false);
    }
    void Init()
    {

        if(SharedObject.g_DataManager.m_MyCharacter == null)
        {
            Invoke("Init", .2f);
        }
        else
        {
            if (!NotLoad)
            {


                FadeCanvas.instance.Fade.DOFade(0, .5f).OnComplete(() =>
                {
                    FadeCanvas.instance.Fade.gameObject.SetActive(false);
                });
            }
            SetGoldText();
        }
    }
    public static void InFade()
    {
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i].DOFade(0, 1f);
        }
        instance.FadeVisible();
        instance.Fade.GetComponentInChildren<Animation>().clip = instance.FadeClip[0];
        instance.Fade.GetComponentInChildren<Animation>().Play();
    }
    public void OutFade()
    {
        Fade.GetComponentInChildren<Animation>().clip = FadeClip[1];
        Fade.GetComponentInChildren<Animation>().Play();
        Invoke("FadeHide", 1);

    }
    public void FadeDeathUI()
    {
        FadeDeathBGVisible();
        DeathBG.GetComponentInChildren<Animation>().Play();

    }
    public IEnumerator VisibleLevelUp()
    {
        WaitForSeconds waitSec = new WaitForSeconds(2);
        LevelUpImage.transform.DOScale(0f, 0f);
        LevelUpImage.GetComponent<Image>().DOFade(0, 0f);
        LevelUpText.text = SharedObject.g_DataManager.m_MyCharacter.N[(byte)m_Enum.STAT2.LEVEL].ToString();
        LevelUpImage.transform.DOScale(.65f, .5f);
        LevelUpImage.GetComponent<Image>().DOFade(1, .5f);
        yield return waitSec;
        LevelUpImage.transform.DOScale(0, .5f);
        LevelUpImage.GetComponent<Image>().DOFade(0, .5f);
    }
    private void FadeVisible()
    {
        Fade.SetActive(true);
    }
    public void FadeHide()
    {
        Fade.SetActive(false);
    }
    public void FadeDeathBGHide()
    {
        DeathBG.SetActive(false);
    }
    public void FadeDeathBGVisible()
    {
        DeathBG.SetActive(true);
    }
    public static void SetGoldText()
    {

        if(SharedObject.g_DataManager.m_MyCharacter !=null)
        instance.GoldText.text = SharedObject.g_playerPrefsdata.playerdataNoSave.Gold.ToString();
    }
    public void CallPlayerMoveLDown()
    {
        if(PlayerMoveLDown != null)
            PlayerMoveLDown(this, EventArgs.Empty);
    }
    public void CallPlayerMoveRDown()
    {
        if (PlayerMoveRDown != null)
            PlayerMoveRDown(this, EventArgs.Empty);
    }
    public void CallPlayerMoveLUp()
    {
        if (PlayerMoveLUp != null)
            PlayerMoveLUp(this, EventArgs.Empty);
    }
    public void CallPlayerMoveRUp()
    {
        if (PlayerMoveRUp != null)
            PlayerMoveRUp(this, EventArgs.Empty);
    }
    public void CallPlayerJump()
    {
        if (PlayerJump != null)
            PlayerJump(this, EventArgs.Empty);
    }
    public void CallPlayerAttack()
    {
        if (PlayerAttack != null)
            PlayerAttack(this, EventArgs.Empty);
    }
    public void CallRespawnPlayer()
    {
        FindObjectOfType<ReSpawnPlayer>().ReSpawn();
    }
}
