using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine;

public class BossHpUI : MonoBehaviour
{
    private Image ProgressBar;
    private void Awake()
    {   
        ProgressBar = GetComponent<Image>();
    }
    public void SetProgressBar(float Hp, float MaxHp)
    {
        ProgressBar.DOFillAmount(Hp / MaxHp, 1f);
    }
}
