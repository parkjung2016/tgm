using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using DG.Tweening;

public class Interaction : MonoBehaviour
{
    private GameObject Player;
    public GameObject InteractionBtn;
    [SerializeField]
    private GameObject Portal;
    [SerializeField]
    private float InteractionDis;
    [SerializeField]
    private float DialogDis;
    [SerializeField]
    private NpcInfo NPC;
    public GameObject DialogueBtn;
    DialogueTrigger dialogueTrigger;
    public List<GameObject> FoundPortals;
    public List<GameObject> FoundNPCs;
    public string TagName;
    [SerializeField]
    private GameObject[] DialogBtns;
    public GameObject PVPSelect;
    private Text ErrorDungeon;
    private Coroutine runningCoroutine;

    private void Awake()
    {
        DialogBtns = new GameObject[GameObject.FindGameObjectsWithTag("DialogBtns").Length];
        ErrorDungeon = GameObject.Find("ErrorDungeonEnter").GetComponent<Text>();
        for(int i =0; i <DialogBtns.Length;i++)
        {
        DialogBtns[i] = GameObject.Find("DialogBtn"+i);
        }
        dialogueTrigger = GameObject.FindObjectOfType<DialogueTrigger>();
        SetPlayer();
    }
    private void Start()
    {
        ErrorDungeon.gameObject.SetActive(false);
        ErrorDungeon.DOFade(0, 0);
        dialogueTrigger.dialogueManager.gameObject.SetActive(false);
        DialogueBtn.SetActive(false);

        FoundPortals = new List<GameObject>(GameObject.FindGameObjectsWithTag(TagName));
        FoundNPCs = new List<GameObject>(GameObject.FindGameObjectsWithTag("NPC"));
        if(FoundPortals.Count != 0)
        Portal = FoundPortals[0];
        if(FoundNPCs.Count != 0)
        NPC = FoundNPCs[0].GetComponent<NpcInfo>();
        StartCoroutine(CheckNearbyPortal());
        StartCoroutine(CheckNearbyNPC());
    }
    void SetPlayer()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        if (Player == null)
            Invoke("SetPlayer",.2f);
    }
    IEnumerator CheckNearbyNPC()
    {
        yield return new WaitForSeconds(.1f);
        if(Player !=null)
        for (int i = 0; i < FoundNPCs.Count; i++)
        {
            float Dis = Vector2.Distance(Player.transform.position, FoundNPCs[i].transform.position);

            if (Dis < InteractionDis) // 위에서 잡은 기준으로 거리 재기
            {
                NPC = FoundNPCs[i].GetComponent<NpcInfo>();
            }
        }
        StartCoroutine(CheckNearbyNPC());
    }
    IEnumerator CheckNearbyPortal()
    {
        yield return new WaitForSeconds(.1f);
        if (Player != null)
            for (int i = 0; i < FoundPortals.Count; i++)
        {
            float Dis = Vector2.Distance(Player.transform.position, FoundPortals[i].transform.position);

            if (Dis < InteractionDis) // 위에서 잡은 기준으로 거리 재기
            {
                    if (FoundPortals[i].gameObject.activeSelf)
                        Portal = FoundPortals[i];
            }
        }
        StartCoroutine(CheckNearbyPortal());
    }
    private void Update()
    {
        if(Player != null)
        {
        if(Portal!=null && Portal.gameObject.activeSelf )
            InteractionBtn.SetActive(Vector2.Distance(Player.transform.position, Portal.transform.position) <= InteractionDis);
        else
        {
            InteractionBtn.SetActive(false);
        }
        if(NPC != null)
        {
            DialogueBtn.SetActive(Vector2.Distance(Player.transform.position, NPC.transform.position) <= DialogDis);
        }
        else
        {
            DialogueBtn.SetActive(false);
        }

        }
    }
    public void ClickInteractionBtn()
    {
        if (PlayerHPManager.instance.character.Death)
            return;
        switch (Portal.GetComponent<PortalInfo>().PortalName)
        {
            case m_Enum.PortalName.FRONT:
                if (SharedScript.instance.manager != null && !SharedObject.g_playerPrefsdata.playerdata.DungeonOpend[MyScene.instance.DungeonNum-1])
                    if ( SharedScript.instance.manager.remainingenemies > 0)
                    {
                        if (runningCoroutine != null)
                        {
                            SharedObject.g_SoundManager.PlayErrorAudio();
                            ErrorDungeon.DOFade(0, 0);
                            StopCoroutine(runningCoroutine);
                        }
                        runningCoroutine = StartCoroutine(ErrorDungeonEnter());
                        return;
                    }
                NetworkManager.instance.DisConnect  ();
                if (SceneManager.GetSceneByName(MyScene.instance.DungeonNum == 1 ? "Dungeon" : "Dungeon" + MyScene.instance.DungeonNum) == null)
                    return;
                MyScene.instance.SpawnPlayerNum = 0;
                MyScene.instance.DungeonNum++;
                StartCoroutine(LoadDungeon());
                break;
            case m_Enum.PortalName.BACK:
                //NetworkManager.instance.DisConnect  ();
                if (MyScene.instance.DungeonNum == 0)
                {
                    StartCoroutine(LoadLobby());
                }
                else
                {

                    MyScene.instance.DungeonNum--;
                    StartCoroutine(LoadDungeon());
                }
                    MyScene.instance.SpawnPlayerNum = 1;
                break;
            case m_Enum.PortalName.BOSS:
                StartCoroutine(LoadBoss());
                //NetworkManager.instance.DisConnect  ();
                break;
        }
            if (Portal.GetComponent<PortalInfo>().PortalName != m_Enum.PortalName.BOSS)
            CanvasManager.InFade();
    }
    private IEnumerator ErrorDungeonEnter()
    {
        WaitForSeconds ws = new WaitForSeconds(1.5f);
        ErrorDungeon.gameObject.SetActive(true);
        ErrorDungeon.DOFade(1, .5f);
        yield return ws;
        ErrorDungeon.DOFade(0, .5f).OnComplete(() => ErrorDungeon.gameObject.SetActive(false));
    
    }
    private IEnumerator LoadBoss()
    {

        
        FadeCanvas.instance.Fade.gameObject.SetActive(true);
        FadeCanvas.instance.Fade.DOFade(1, 1f);
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i].DOFade(0, 1f);
        }
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Boss");

    }
    private IEnumerator LoadDungeon()
    {
        ChatTest.instance.DisConnectChannel();
        yield return new WaitForSeconds(1f);
        LoadingSceneManager.LoadScene(MyScene.instance.DungeonNum == 0 ? "Lobby" :MyScene.instance.DungeonNum == 1 ? "Dungeon" : "Dungeon" + MyScene.instance.DungeonNum);
        
    }
    private IEnumerator LoadLobby()
    {
        yield return new WaitForSeconds(1f);
        LoadingSceneManager.LoadScene("Lobby");
        MyScene.instance.SpawnPlayerNum = 1;
    }
    public void DialogueBtnClick()
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        dialogueTrigger.dialogueManager.gameObject.SetActive(true);
        if (NPC == null)
            return;
        dialogueTrigger.dialogue.name = NPC.NpcName;
        bool IsQuest = false;
        for(int i=0; i < SharedScript.instance.csvdata.Quest.Count;i++)
        {
            if (QuestManager.instance.questList.ContainsKey(10 + i) && QuestManager.instance.questList[10 + i].questNames[0] != SharedScript.instance.csvdata.Quest[i]["메인퀘스트내용"].ToString() + "(완료)")
            {
                for (int a = 0; a < QuestManager.instance.questList[10 + i].npcId.Length; a++)
                {
                    if (NPC.id == QuestManager.instance.questList[10 + i].npcId[a])
                    {
                        IsQuest = true;
                        DialogBtns[1].SetActive(false);
                        DialogBtns[0].SetActive(false);
                        string[] Texts = SharedScript.instance.csvdata.Quest[i]["메인퀘스트대화"].ToString().Split('>');
                        dialogueTrigger.Auto = false;
                        DialogBtns[2].SetActive(false);
                        DialogBtns[3].SetActive(true);
                        DialogBtns[4].SetActive(false);
                        dialogueTrigger.dialogue.sentences = Texts;
                        dialogueTrigger.dialogueManager.QuestNum = i;
                        dialogueTrigger.dialogueManager.QuestKey = 10+i;
                    }
                }
            }
        }
       if(!IsQuest)
        {
            dialogueTrigger.Auto = true;
            dialogueTrigger.dialogue.sentences = NPC.sentences;
                        DialogBtns[0].SetActive(true);
            DialogBtns[1].SetActive(true);
            DialogBtns[2].SetActive(false);
            DialogBtns[3].SetActive(false);
            DialogBtns[4].SetActive(false);

            switch (NPC.ActorType)
            {
                case m_Enum.ACTORTYPE.BLACKSMITH:
                    dialogueTrigger.dialogueManager.BtnText.text = "강화";
                    break;
                case m_Enum.ACTORTYPE.SHOP:
                    dialogueTrigger.dialogueManager.BtnText.text = "구매/판매";
                    break;
                case m_Enum.ACTORTYPE.MANAGER:
                    DialogBtns[2].SetActive(true);
                    DialogBtns[4].SetActive(true);
                    dialogueTrigger.dialogueManager.BtnText.text = "자동사냥";
                    break;
                case m_Enum.ACTORTYPE.CITIZEN:
                    DialogBtns[0].SetActive(false);
                    break;
            }
        }

        dialogueTrigger.TriggerDialogue();
    }
    public void DialogueExitBtnClick()
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        dialogueTrigger.dialogueManager.gameObject.SetActive(false);

    }
    public void InDialogueBtnClick()
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        if (NPC.ActorType == m_Enum.ACTORTYPE.MANAGER)
        {
            FindObjectOfType<Canvas_Auto>().ClickAutoHunt();
            return;
        }
        dialogueTrigger.dialogueManager.ActiveInDialogueActionObj(dialogueTrigger.dialogueManager.InDialogueActionObj[(byte)NPC.ActorType]);
    }
    public void ClickPVPBtn()
    {
        dialogueTrigger.dialogueManager.ActiveInDialogueActionObj(PVPSelect);
    }
}
