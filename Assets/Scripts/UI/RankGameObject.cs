using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankGameObject : MonoBehaviour
{
    [SerializeField]
    private Text displaynameText;
    [SerializeField]
    private Text levelText;
    [SerializeField]
    private Text rankingText;
    [SerializeField]
    private Image Effect;
    [SerializeField]
    private Color[] leveltextcolor;
    public void SetInfo(string _displayname,int _level,int ranking)
    {
      
            levelText.color = leveltextcolor[Convert.ToInt32(ranking > 2)];
            Effect.gameObject.SetActive(ranking == 0 || ranking == 1 || ranking == 2);
        displaynameText.text = "<"+_displayname+">";
        levelText.text = _level.ToString();
        rankingText.text = (ranking +1).ToString();
    }
}
