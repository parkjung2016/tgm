using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestUI : MonoBehaviour
{
    public static QuestUI instance;
    [SerializeField]
    private GameObject QuestPrefab;
    [SerializeField]
    private Transform[] SpawnQuestPrefabTrans;
    public List<Text> QuestTextList = new List<Text>();
   public AudioSource audioSource;
    private void Awake()
    {
        if(instance == null)
        instance = this;
        audioSource = GetComponent<AudioSource>();
    }
    private void Start()
    {
    }
    public void AddQuest(string text,string text2,byte Type)
    {
        Text QuestText = Instantiate(QuestPrefab, SpawnQuestPrefabTrans[Type]).GetComponent<Text>();
        if(text2 != "없음")
        {
        QuestText.text = "<size=100><color=black>◆</color></size> " + text + System.Environment.NewLine +text2;

        }
        else
        {

        QuestText.text = "<size=100><color=black>◆</color></size> " + text;
        }
        QuestTextList.Add(QuestText);
    }
    public void RemoveQuest(int Num, byte Type)
    {
        QuestTextList.RemoveAt(Num);
        Destroy(SpawnQuestPrefabTrans[Type].transform.GetChild(Num).gameObject);
    }
    public void RefreshQuestValue()
    {
        for(int i =0; i <SharedObject.g_playerPrefsdata.playerdata.questListQUESTDATA.Count;i++)
        {
            if(SharedScript.instance.csvdata.Quest[i].ContainsKey("메인퀘스트목표") && SharedScript.instance.csvdata.Quest[i]["메인퀘스트목표"].ToString().Contains("0/"))
            {
            string text = SharedScript.instance.csvdata.Quest[i]["메인퀘스트내용"].ToString() + SharedScript.instance.csvdata.Quest[i]["메인퀘스트목표"].ToString().Replace("0/",SharedObject.g_playerPrefsdata.playerdata.QuestTargetNum[i].ToString()+"/");
            string text2 = SharedScript.instance.csvdata.Quest[i]["메인퀘스트내용2"].ToString() + SharedScript.instance.csvdata.Quest[i]["메인퀘스트목표2"].ToString().Replace("0/",SharedObject.g_playerPrefsdata.playerdata.QuestTargetNum2[i].ToString()+"/");
                for(int a=0; a < QuestTextList.Count;a++)
                {
                    if ( QuestTextList[a].text.Contains(SharedScript.instance.csvdata.Quest[i]["메인퀘스트내용"].ToString()))
                    {
                        if (SharedScript.instance.csvdata.Quest[i]["메인퀘스트내용2"].ToString() != "없음")
                        {

                        QuestTextList[a].text = "<size=100><color=black>◆</color></size> " + text+ System.Environment.NewLine+"    "+text2;
                        }
                        else
                        {
                            QuestTextList[a].text = "<size=100><color=black>◆</color></size> " + text;
                        }
                    }
                }
        


            }
        }
    }
        public void EnableGameObj(bool enable)
    {
        if(instance != null && gameObject != null)
        gameObject.SetActive(enable);
    }
}
