using System;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class SkillTree : MonoBehaviour
{
    [SerializeField]
    private Text SkillPoint;
    [SerializeField]
    private Color[] SkillColor;
    [SerializeField]
    private Color[] SkillBGColor;
    [SerializeField]
    private Color[] SkillOutLineColor;
    [SerializeField]
    public GameObject[] SkillBGs;
    [SerializeField]
    private GameObject[] SKillOutLines;
    [SerializeField]
    private GameObject[] SKills;
    private PlayerSkills playerSkills;

    [SerializeField]
    private Text Notice;
    [SerializeField]
    private GameObject SkillInfo;
    private SkillInfo skillInfoScript;
    [SerializeField]
    private Image SkillImg;
    [SerializeField]
    private Text[] SkillInfoValue;
    [SerializeField]
    private Text[] SkillInfoText;
    [SerializeField]
    private Text SkillDescription;
    [SerializeField]
    private Image[] EquipSkillBtnImg;
    private Sprite SkillSprite;
    [SerializeField]
    private Text EquipText;
    [SerializeField]
    private Color[] SkillEquipColors;
    [SerializeField]
    private AudioSource Audio;
    [SerializeField]
    private Text LevelLimitText;
    [SerializeField]
    private Color[] LevelLimitColor;
    private void Awake()
    {
        Audio = GetComponent<AudioSource>();
        SKills =new GameObject[ GameObject.FindGameObjectsWithTag("Skill").Length];
        SkillBGs = new GameObject[GameObject.FindGameObjectsWithTag("SkillBG").Length];
        SKillOutLines = new GameObject[GameObject.FindGameObjectsWithTag("SkillOutLine").Length];
        for (int i = 0; i < SKills.Length; i++)
        {
            SkillBGs[i] = GameObject.Find(((PlayerSkills.SkillType)i).ToString() +"Btn");
            SKills[i] = SkillBGs[i].transform.GetChild(1).GetChild(0).gameObject;
            SKillOutLines[i] = SkillBGs[i].transform.GetChild(0).gameObject;
        }

    }
    private void Start()
    {
        HideSkillInfo();
        Notice.text = "";
        gameObject.SetActive(false);
        SetCharacter();
        for (int i = 0; i < EquipSkillBtnImg.Length; i++)
        {
            if (SharedObject.g_playerPrefsdata.playerdata.EquipSkills[i].skilltype == PlayerSkills.SkillType.Max)
            {

                Color color = EquipSkillBtnImg[i].color;
                color.a = 0;
                EquipSkillBtnImg[i].color = color;
            }
        }

    }
    void SetCharacter()
    {
        if (SharedObject.g_DataManager.m_MyCharacter == null)
            Invoke("SetCharacter", .2f);
        else
        {
            SetPlayerSkills(SharedObject.g_DataManager.m_MyCharacter.GetComponent<Player>().GetPlayerSkills());
            for (byte i = 0; i < SharedObject.g_playerPrefsdata.playerdata.UnLockSkill.Count; i++)
            {
                byte index = (byte)SharedObject.g_playerPrefsdata.playerdata.UnLockSkill[i];
                playerSkills.TryUnlockSkill(SharedObject.g_playerPrefsdata.playerdata.UnLockSkill[i], true);
                if(SharedObject.g_playerPrefsdata.playerdata.StatUpSkillLevel[i] >= (int)SharedScript.instance.csvdata.SkillInfo[index]["Level"]+10)
                {

                    for (int a = 0; a < (SharedObject.g_playerPrefsdata.playerdata.StatUpSkillLevel[i] / ((int)SharedScript.instance.csvdata.SkillInfo[index]["Level"]+10)); a++)
                    {
                        int skillvalue = int.Parse(SkillBGs[index].GetComponent<Skill>().skillInfo.SkillValue[0]);
                        int skillvalue2 = int.Parse(SkillBGs[index].GetComponent<Skill>().skillInfo.SkillValue[1]);
                        SkillBGs[index].GetComponent<Skill>().skillInfo.SkillValue[0] = (skillvalue + int.Parse(SharedScript.instance.csvdata.SkillInfo[index]["StatUpgrade"].ToString())).ToString();
                        SkillBGs[index].GetComponent<Skill>().skillInfo.SkillValue[1] = (skillvalue2 + int.Parse(SharedScript.instance.csvdata.SkillInfo[index]["StatUpgrade2"].ToString())).ToString();

                    }
                }
            }
            for (int i = 0; i < SharedObject.g_playerPrefsdata.playerdata.EquipSkills.Count; i++)
            {
                if (SharedObject.g_playerPrefsdata.playerdata.EquipSkills[i].skilltype != PlayerSkills.SkillType.Max && SharedObject.g_playerPrefsdata.playerdata.EquipSkills[i].Equip)
                {
                    SkillBGs[(byte)SharedObject.g_playerPrefsdata.playerdata.EquipSkills[i].skilltype].GetComponent<Skill>().skillInfo.Equip = false;
                    SkillSprite = SKills[(byte)SharedObject.g_playerPrefsdata.playerdata.EquipSkills[i].skilltype].GetComponent<Image>().sprite;
                    skillInfoScript = SkillBGs[(byte)SharedObject.g_playerPrefsdata.playerdata.EquipSkills[i].skilltype].GetComponent<Skill>().skillInfo;
                    SkillEquip(true);
                }
            }
        }
    }
    private void SetSkillColor()
    {
        for (int i = 0; i < SkillBGs.Length; i++)
        {
            if (SkillBGs[i].GetComponent<Skill>().skillInfo.Equip)
            {

                SKillOutLines[i].GetComponent<Image>().color = SkillEquipColors[0];
            }
            else
            {
                if (SharedObject.g_playerPrefsdata.playerdata.UnLockSkill.Contains(SkillBGs[i].GetComponent<Skill>().skillInfo.skilltype))
                    SKillOutLines[i].GetComponent<Image>().color = SkillEquipColors[1];
            }
        }
    }
    private void Update()
    {
        if (SkillInfo.activeSelf)
        {
            if (Input.touchCount > 0)
            {

                if (EventSystem.current.IsPointerOverGameObject(0).Equals(false))
                {
                    if (Input.GetTouch(1).phase.Equals(TouchPhase.Began))
                    {
                        HideSkillInfo();
                    }
                }
            }
            else if (Input.GetMouseButtonDown(0))
            {
                if (EventSystem.current.IsPointerOverGameObject().Equals(false))
                {
                    HideSkillInfo();
                }
            }
        }
    }
    public void ClickSkillInfoBtn()
    {
        if (!playerSkills.TryUnlockSkill(skillInfoScript.skilltype, false))
        {
            SkillEquip(false);
        }
        else
        {
            VisibleSkillInfo();
        }
    }
    public void ClickSkillBtn(GameObject obj)
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        SkillSprite = obj.GetComponent<Skill>().SkillImg.sprite;
        skillInfoScript = obj.GetComponent<Skill>().skillInfo;
        VisibleSkillInfo();
        SkillPoint.text = SharedObject.g_DataManager.m_MyCharacter.SkillPoint.ToString();
    }
    public void SkillTreeBtnClick()
    {
        if (PlayerHPManager.instance.character.Death)
            return;
        gameObject.SetActive(true);
        SkillPoint.text = SharedObject.g_DataManager.m_MyCharacter.SkillPoint.ToString();
    }
    public void SkillTreeExitBtnClick()
    {
        Notice.text = "";
        SharedObject.g_SoundManager.PlayBtnClickSound();
        HideSkillInfo();
        gameObject.SetActive(false);
    }
    public void HideSkillInfo()
    {
        SkillInfo.SetActive(false);
    }
    public void VisibleSkillInfo()
    {
        if (skillInfoScript != null)
        {
            SkillInfo.SetActive(true);
            SkillDescription.text = skillInfoScript.SkillDescription;
            SkillImg.sprite = SkillSprite;
            LevelLimitText.transform.GetChild(0).GetComponent<Text>().text = skillInfoScript.LevelLimit.ToString();
            LevelLimitText.transform.GetChild(0).gameObject.SetActive(!skillInfoScript.Equip);
            LevelLimitText.gameObject.SetActive(!SharedObject.g_playerPrefsdata.playerdata.UnLockSkill.Contains(skillInfoScript.skilltype));
            LevelLimitText.color = LevelLimitColor[Convert.ToInt32(SharedObject.g_DataManager.m_MyCharacter.N[(byte)m_Enum.STAT2.LEVEL] >= skillInfoScript.LevelLimit)];
            LevelLimitText.transform.GetChild(0).GetComponent<Text>().color = LevelLimitColor[Convert.ToInt32(SharedObject.g_DataManager.m_MyCharacter.N[(byte)m_Enum.STAT2.LEVEL] >= skillInfoScript.LevelLimit)];
            for (byte i = 0; i < SkillInfoValue.Length; i++)
            {
                SkillInfoValue[i].text = skillInfoScript.SkillValue[i];
            }
            EquipText.text = SharedObject.g_playerPrefsdata.playerdata.UnLockSkill.Contains(skillInfoScript.skilltype) ? skillInfoScript.Equip ? "장착해제" : "장착" : "배우기";
        }
    }
    public void SkillEquip(bool Init)
    {
        if (skillInfoScript != null)
        {
            if (!SharedObject.g_DataManager.m_MyCharacter.GetComponent<Player>().IsFighting)
            {


                if (!skillInfoScript.Equip)
                {
                    if (playerSkills.IsSkillUnLocked(skillInfoScript.skilltype))
                    {
                        if (Init)
                        {
                            int num = SharedObject.g_playerPrefsdata.playerdata.EquipSkills.IndexOf(SharedObject.g_playerPrefsdata.playerdata.EquipSkills.Find(x => x.skilltype == skillInfoScript.skilltype));

                            SkillEquip_(num, true, skillInfoScript);
                            return;
                        }
                        else
                        {
                            int Num = 0;
                            for (int i = 0; i < SharedObject.g_playerPrefsdata.playerdata.EquipSkills.Count; i++)
                            {
                                if (SharedObject.g_playerPrefsdata.playerdata.EquipSkills[i].skilltype != PlayerSkills.SkillType.Max)
                                    Num++;
                                if (!SharedObject.g_playerPrefsdata.playerdata.EquipSkills.Contains(skillInfoScript) && SharedObject.g_playerPrefsdata.playerdata.EquipSkills[i].skilltype == PlayerSkills.SkillType.Max)
                                {
                                    SkillEquip_(i, true, skillInfoScript);
                                    return;
                                }
                                if (Num > 2)
                                    SharedObject.g_SoundManager.PlayErrorAudio();


                            }
                        }
                    }
                }
                else
                {
                    if (SharedObject.g_playerPrefsdata.playerdata.EquipSkills.Contains(skillInfoScript))
                    {
                        SkillEquip_(SharedObject.g_playerPrefsdata.playerdata.EquipSkills.IndexOf(skillInfoScript), false, null);
                        return;
                    }
                }
            }
            else
            {

                StartCoroutine(ShowNotice("전투 중에는 이 행동을 하실 수 없습니다."));
            }

        }
    }
    private void SkillEquip_(int Num, bool Equip, SkillInfo skillInfo)
    {

        if (skillInfoScript == null)
            return;
        skillInfoScript.Equip = Equip;
        if (skillInfo == null)
        {
            SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num] = SharedScript.instance.NullSkillInfo;
            SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].skilltype = PlayerSkills.SkillType.Max;
        }
        else
        {
            SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num] = skillInfo;
        }
        SetSkillEquipImg();
        VisibleSkillInfo();
        SetSkillColor();
        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
    }
    private void SetSkillEquipImg()
    {
        for (int i = 0; i < SharedObject.g_playerPrefsdata.playerdata.EquipSkills.Count; i++)
        {
            if (SharedObject.g_playerPrefsdata.playerdata.EquipSkills[i].skilltype == skillInfoScript.skilltype)
            {
                EquipSkillBtnImg[i].sprite = SkillSprite;
            }

            Color color = EquipSkillBtnImg[i].color;
            color.a = SharedObject.g_playerPrefsdata.playerdata.EquipSkills[i].skilltype != PlayerSkills.SkillType.Max ? 1 : 0;
            EquipSkillBtnImg[i].color = color;
        }
    }
    public void SetPlayerSkills(PlayerSkills playerSkills)
    {
        this.playerSkills = playerSkills;
        playerSkills.OnSkillUnlocked += PlayerSkills_OnSkillUnlocked;
        playerSkills.skillTree = this;
        UpdateSkillVisuals();
    }

    private void PlayerSkills_OnSkillUnlocked(object sender, PlayerSkills.OnSkillUnlockedEventArgs e)
    {
        SkillPoint.text = SharedObject.g_DataManager.m_MyCharacter.SkillPoint.ToString();
        Audio.Play();
        UpdateSkillVisuals();
    }
    public IEnumerator ShowNotice(string str)
    {
        if (Notice.text == "")
        {
            Notice.text = str;
            yield return new WaitForSeconds(1);
            Notice.text = "";

        }
        yield return null;
    }
    private void UpdateSkillVisuals()
    {
        for (byte i = 0; i < (byte)PlayerSkills.SkillType.Max; i++)
        {
            SKills[i].GetComponent<Image>().color = SkillColor[Convert.ToInt16(playerSkills.IsSkillUnLocked((PlayerSkills.SkillType)i))];
            SkillBGs[i].GetComponent<Image>().color = SkillBGColor[Convert.ToInt16(playerSkills.IsSkillUnLocked((PlayerSkills.SkillType)i))];
            SKillOutLines[i].GetComponent<Image>().color = SkillOutLineColor[Convert.ToInt16(playerSkills.IsSkillUnLocked((PlayerSkills.SkillType)i))];
        }
        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
    }
}
