using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using PlayFab;

public class RankUI : MonoBehaviour
{
    public GameObject PlayerInfoPrefab;
    public Transform PlayerInfoPrefabtrans;
    public CanvasGroup canvasgroup;
    public List<GameObject> rankgameobjectlist= new List<GameObject>();
    private void Awake()
    {
      canvasgroup =  GetComponent<CanvasGroup>();
    }
    public void VisibleGameObject(bool visible)
    {
        if(visible)
        {
        gameObject.SetActive(true);
            canvasgroup.DOFade(1, .5f).OnComplete(() =>
            {
                CheckRank();
            });
        }
        else
        {
            canvasgroup.DOFade(0, .5f).OnComplete(() =>
            {
        gameObject.SetActive(false);
            });
        }

    }
    private void Start()
    {
        gameObject.SetActive(false);
        canvasgroup.alpha = 0;
        CheckRank();
    }
    public void CheckRank()
    {
        if (rankgameobjectlist.Count > 0)
        {
            for (int i = 0; i < rankgameobjectlist.Count; i++)
            {
                Destroy(rankgameobjectlist[i].gameObject);
            }
                rankgameobjectlist.Clear();
            UpdateRankUI();
        }
        else
        {
            UpdateRankUI();
        }
    }

    public void UpdateRankUI()
    {

        PlayFabClientAPI.GetLeaderboard(new PlayFab.ClientModels.GetLeaderboardRequest() { StatisticName = "LevelRank", StartPosition = 0, MaxResultsCount = 10 }, result =>
                 {
                     print(result.NextReset);
                     for(int i =0; i < result.Leaderboard.Count;i++)
                     {
                         AddPlayerInfoPrefab(result.Leaderboard[i].DisplayName, result.Leaderboard[i].StatValue,result.Leaderboard[i].Position);
                     }

                 }, error => { });
    }
    public void AddPlayerInfoPrefab(string displayname,int level,int ranking)
    {
       RankGameObject rankGameObject = Instantiate(PlayerInfoPrefab, PlayerInfoPrefabtrans).GetComponent<RankGameObject>();
        rankGameObject.SetInfo(displayname, level,ranking);
        rankgameobjectlist.Add(rankGameObject.gameObject);
    }
}
