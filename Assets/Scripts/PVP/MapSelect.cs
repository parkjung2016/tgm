using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon;
using ExitGames.Client.Photon;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class MapSelect : MonoBehaviour,IPointerDownHandler
{
    public m_Enum.MAP Map;
    UI_PVPSelect uI_PVPSelect;
    public GameObject Select;
    [SerializeField]
    private Image MapSprite;
    private void Awake()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
        uI_PVPSelect = FindObjectOfType<UI_PVPSelect>();
        Select.SetActive(false);
    }
    private void OnEvent(EventData obj)
    {
        if (obj.Code == 3)
        {
           uI_PVPSelect. HideMapSelect();
        }
    }
    public void SelectMap()
    {
          
       uI_PVPSelect.HideMapSelect();
        uI_PVPSelect.currentMap = Map;
        Select.SetActive(true);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable { { "Map", (int)Map } });
    PhotonNetwork.RaiseEvent(2, (byte)Map, NetworkManager.instance.raiseEvent, SendOptions.SendUnreliable);
        }
    }
}
