using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveMat : MonoBehaviour
{
    public float OffsetSpeed = .5f;
    private float Offset;
    new MeshRenderer renderer;
    private void Awake()
    {
        renderer = GetComponent<MeshRenderer>();
    }
    private void Update()
    {
        Offset += Time.deltaTime * OffsetSpeed;
        renderer.material.mainTextureOffset = new Vector2(Offset, 0);
    }
}
