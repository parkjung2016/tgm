using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GoldMove : MonoBehaviour
{
    public GameObject obj;
    public int GoldValue= 500;
    public float speed;
    private Rigidbody2D rig2d;

    private void Start()
    {
        obj = GameObject.FindGameObjectWithTag("Player");
        float x = Random.Range(0, 2) == 0 ? -1 : 1;
        rig2d = GetComponent<Rigidbody2D>();
       rig2d.AddForce(new Vector2(Random.Range(50.0f,100.0f)*x , Random.Range(150.0f,300.0f)));
        InvokeRepeating("move", 1, .2f);
    }
    private void move()
    {
        if (rig2d != null)
            Destroy(rig2d);
        transform.DOMove(obj.transform.position - new Vector3(0, 1), speed).SetEase(Ease.Linear);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
                 SharedObject.g_playerPrefsdata.playerdataNoSave.Gold += GoldValue;
            CanvasManager.SetGoldText();
            SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
            Destroy(gameObject);
        }
    }
}
