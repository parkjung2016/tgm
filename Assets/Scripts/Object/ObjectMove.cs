using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMove : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer[] Clouds;
    [SerializeField]
    private GameObject endPoint;
    [SerializeField]
    private GameObject startPos;
    [SerializeField]
    private float Speed;
    [SerializeField]
    private float Delay = 0.5f;
    void Start()
    {
        Clouds = GetComponentsInChildren<SpriteRenderer>();
        StartCoroutine(Move());
    }
    IEnumerator Move()
    {
        yield return new WaitForSeconds(Delay);
        for (int i = 0; i < Clouds.Length; i++)
        {
            Clouds[i].transform.Translate(Vector2.right * Speed * Time.deltaTime);
            if (Mathf.Abs(endPoint.transform.position.x) / endPoint.transform.position.x == 1 && Clouds[i].transform.position.x >= endPoint.transform.position.x || Mathf.Abs(endPoint.transform.position.x) / endPoint.transform.position.x == -1 && Clouds[i].transform.position.x <= endPoint.transform.position.x)
            {
                Clouds[i].transform.position = new Vector3(startPos.transform.position.x, Clouds[i].transform.position.y, Clouds[i].transform.position.z);
            }
        }
        StartCoroutine(Move());
    }
}
