using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Duck : MonoBehaviour
{
    private void Start()
    {
        InvokePlayAnim();
    }
    public void InvokePlayAnim()
    {
        Invoke("Playanim", Random.Range(0, 5));
    }
 private void Playanim()
    {
        GetComponent<Animator>().Play("Idle");
    }
}
