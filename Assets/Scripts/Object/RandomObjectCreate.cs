using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomObjectCreate : MonoBehaviour
{
    [SerializeField]
    private GameObject[] Prefabs;
    private Transform CreateTrans;
    [SerializeField]
    private Transform FirstCreateTrans;
    [SerializeField]
    private Transform RepeatCreateTrans;
    [SerializeField]
    private float FirstCreateTime;
    [SerializeField]
    private float RepeatCreateTime;
    private Transform LastObj;
    public int CurCreateNum;
    [SerializeField]
    private int MaxCreateNum = 4;
    [SerializeField]
    private int MinCreateNum = 2;
    [SerializeField]
    private bool Loop;
    [SerializeField]
    private GameObject[] RandomCreate;
    private void Awake()
    {
        RandomCreate = GameObject.FindGameObjectsWithTag("EnemySpawnPos");
        
    }
    private void Start()
    {
        CreateTrans = FirstCreateTrans;
        if (Loop)
            InvokeRepeating("Create", FirstCreateTime, RepeatCreateTime);
        else
        {
            Create();
        }
    }
    void Create()
    {
        if (CurCreateNum >= MaxCreateNum)
            return;
        GameObject obj;
        if (!Loop)
        {

            for (int i = 0; i < Random.Range(MinCreateNum, RandomCreate.Length);i++)
            {
                obj = Instantiate(Prefabs[Random.Range(0, Prefabs.Length)], RandomCreate[Random.Range(0, RandomCreate.Length)].transform.position + new Vector3(Random.Range(-3.0f,3.0f),0), Quaternion.identity);
            }
        }
        else
        {
           obj =  Instantiate(Prefabs[Random.Range(0, Prefabs.Length)], LastObj == null ? CreateTrans.position : LastObj.position + new Vector3(26, 0), Quaternion.identity);
        ObjectMove_Destroy objectMove_Destroy_ = obj.GetComponent<ObjectMove_Destroy>();
            if (objectMove_Destroy_ != null)
            {

                objectMove_Destroy_.speed = -1;
            }
        LastObj = obj.transform;
        }
        if (CreateTrans == FirstCreateTrans)
        {
            CreateTrans = RepeatCreateTrans;
        }
        CurCreateNum++;
    }
}
