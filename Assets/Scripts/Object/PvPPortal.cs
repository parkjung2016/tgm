using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon;
using DG.Tweening;

public class PvPPortal : MonoBehaviour
{
    public Transform TeleportTransform;
    bool Teleporting;
    Coroutine coroutine;
    public Tween CheckPlayerTeleportT;
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(Teleporting)
        {
            Teleporting = false;
            StopCoroutine(coroutine);
        collision.GetComponent<PhotonView>().RPC("TeleportMatOrigin", RpcTarget.All, null);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Teleporting = true;
        coroutine= StartCoroutine(Teleport(collision.gameObject));
    }
    
    IEnumerator Teleport(GameObject player)
    {
        yield return new WaitForSeconds(1);
        if (!Teleporting)
            yield return null;
        object[] data= null;
        for(int i =0; i <PVPManager.instance.TeleportObjects.Length;i++)
        {
            if (gameObject == PVPManager.instance.TeleportObjects[i].gameObject)
            {
                data = new object[] { i };
                break;
            }
        }
        player.GetComponent<PhotonView>().RPC("TeleportMat", RpcTarget.All, data);
        CheckPlayerTeleportT.OnComplete(() =>
        {
            bool active =  TeleportTransform.gameObject.name.Contains("Under");
            if (player.GetComponent<PlayerHPManager>()._PlayerHPProgressBar !=null)
            {
          player.GetComponent<PlayerHPManager>(). _PlayerHPProgressBar.gameObject.SetActive(!active);

            }
            if(player ==SharedObject.g_DataManager.m_MyCharacter.gameObject)
            PVPManager.instance.Playerpointlight.gameObject.SetActive(active);
        });
        yield return null;
    }

}
