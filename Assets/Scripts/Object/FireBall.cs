using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    [SerializeField]
    private float Speed;
    [SerializeField]
    private int Damage;
    private void FixedUpdate()
    {
        transform.Translate(transform.right * Speed * Time.fixedDeltaTime);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
            if (collision.CompareTag("Player"))
            {
            print("23");
                PlayerHPManager.instance.StartCoroutine(PlayerHPManager.instance.ApplyDamage(Damage));
            }

            Destroy(gameObject);
    }
}
