using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMove_Destroy : MonoBehaviour
{
    public float speed;
    private RandomObjectCreate RandomObjectCreate_;
    private SpriteRenderer[] Decorations;
    private void Start()
    {
        Decorations = GetComponentsInChildren<SpriteRenderer>();
        RandomObjectCreate_ = FindObjectOfType<RandomObjectCreate>();
        int EnableCount = Random.Range(0, Decorations.Length+1);
        for(int i =0; i < Decorations.Length;i++)
        {
            if (Decorations[0])
                continue;
            Decorations[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < EnableCount; i++)
        {
            Decorations[Random.Range(0, Decorations.Length)].gameObject.SetActive(true);
        }
    }
    private void Update()
    {
        
        transform.Translate(Vector2.right * speed * Time.deltaTime);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("ObjectDestroy"))
        {
            RandomObjectCreate_.CurCreateNum--;
            Destroy(gameObject);
        }
    }
}
