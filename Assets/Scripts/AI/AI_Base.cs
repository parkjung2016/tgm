using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Base
{
    public m_Enum.AI m_AI = m_Enum.AI.AI_SEARCH;
    public MonsterBase m_Character = null;

    public m_Enum.AI AI
    {
        set { m_AI = value; }
        get { return m_AI; }
    }
    public virtual void InitAI(MonsterBase _character, float _fCreateTime)
    {
        m_Character = _character;
    }
    public void UpdateState()
    {
        if (m_Character == null || m_Character.Death)
            return;

        switch (m_AI)
        {
            case m_Enum.AI.AI_RESET:
                SetReset();
                break;
            case m_Enum.AI.AI_SEARCH:
                SetSearch();
                break;
            case m_Enum.AI.AI_MOVE:
                SetMove();
                break;
            case m_Enum.AI.AI_ATTACK:
                SetAttack();
                break;
            case m_Enum.AI.AI_DIE:
                SetDie();
                break;
        }
    }
    
    public void SetReset()
    {
        m_Character.gameObject.SetActive(true);
    }
    public virtual void SetSearch()
    {
        m_AI = m_Enum.AI.AI_MOVE;
    }
    public virtual void SetMove()
    {
        m_AI = m_Enum.AI.AI_ATTACK;
        m_Character.AttackTrue = true;
    }
    public virtual void SetAttack()
    {
        m_AI = m_Enum.AI.AI_SEARCH;
        m_Character.AttackTrue = false;
    }
    public virtual void SetDie()
    {
        m_AI = m_Enum.AI.AI_DIE;

    }
}
