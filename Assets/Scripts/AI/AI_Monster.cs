using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Monster : AI_Base
{

    public GameObject Player_;
    public float FollowDis;

    public LayerMask la;
    public float AttackDis;
    public Transform castPoint;
    public SpriteRenderer spriteRenderer_;
    public float Radi;
    public Vector2 BoxSize;
    public bool isFacingLeft;
    public bool isFollow = false;
    public bool isSearching = false;
    public override void SetSearch()
    {
        StopChasingPlayer();
        if (CanSeePlayer(FollowDis))
        {
            base.SetSearch();
        }
    }
    public override void SetMove()
    {
        if (m_Character.Attacking || SharedObject.g_DataManager.m_MyCharacter.Death)
            return;
        if (!CanSeePlayer(FollowDis))
        {
            base.SetAttack();
        }
        if (Vector2.Distance(m_Character.transform.position, Player_.transform.position) <= AttackDis)
        {

            base.SetMove();
        return;
        }
        if (m_Character.transform.position.x < Player_.transform.position.x)
        {
            m_Character.rig2d.velocity = new Vector2(m_Character.moveSpeed, 0);
            m_Character.transform.localScale = new Vector2(m_Character.Scale, m_Character.Scale);
            isFacingLeft = false;
        }
        else
        {
            m_Character.rig2d.velocity = new Vector2(-m_Character.moveSpeed, 0);
            m_Character.transform.localScale = new Vector2(-m_Character.Scale, m_Character.Scale);
            isFacingLeft = true;
        }

        m_Character.anim.SetBool("Move", true);
    }
    public bool CanSeePlayer(float Dis)
    {
        bool val = false;
        float castDis = Dis;
        if (isFacingLeft)
        {
            castDis = -Dis;
        }
        RaycastHit2D[] hit = Physics2D.BoxCastAll(castPoint.position,BoxSize,m_Character.transform.rotation.z,m_Character.transform.forward,0,la);
        Vector2 dirtoTarget = m_Character.transform.position - Player_.transform.position;
        float Angle = Vector2.Angle(m_Character.transform.position, dirtoTarget);

        for (int i = 0; i < hit.Length; i++)
        {
            if (hit[i].collider != null)
            {
                if (hit[i].collider.CompareTag("Player") && Mathf.Abs( Player_.transform.position.y - m_Character.transform.position.y) <= 3)
                {
                    if (-m_Character.transform.localScale.x  == m_Character.Scale && Mathf.Abs(Angle) < 90 || -m_Character.transform.localScale.x != m_Character.Scale && Mathf.Abs(Angle) > 90)
                    val = true;
                    else
                    {
                        val = false;
                        
                    }
                }
                else
                {
                    val = false;
                }
            }

        }
        return val;
    }
    public override void SetAttack()
    {
        if (SharedObject.g_DataManager.m_MyCharacter.Death)
            return;
        StopChasingPlayer();
        m_Character.Attacking = true;
        if (Vector2.Distance(Player_.transform.position,m_Character.transform.position) > AttackDis)
            base.SetAttack();
        else
        {

        if (m_Character.ComboStep.Equals(0))
        {
            m_Character.anim.Play("AttackA");
            m_Character.ComboStep = 1;
            return;
        }
        else
        {
            if (m_Character.ComboPossible)
            {
               m_Character. ComboPossible = false;
                m_Character.ComboStep++;
            }
        }
        }
    }
    protected void StopChasingPlayer()
    {
        if (m_AI == m_Enum.AI.AI_DIE)
            return;
        isFollow = false;
        m_Character.rig2d.velocity = new Vector2(0, 0);
        m_Character.anim.SetBool("Move", false);
    }
}
