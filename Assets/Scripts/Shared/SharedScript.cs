using System.Collections;
using System.Collections.Generic;
using System.Text;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

public class SharedScript : MonoBehaviour
{
    public static SharedScript instance;
    public Inventory inventory;
    public CSVData csvdata = new CSVData();
    public ItemUI itemUI;
    public DungeonManager manager;
    public readonly string[] ItemType = { "����", "���", "����", "����", "�Ź�", "����","��" };
    public SkillInfo NullSkillInfo = new SkillInfo();
    public SceneSequenceManager sceneSequenceManager = new SceneSequenceManager();
    public  Shop shop;
    public RenderPlayer renderPlayer;
  public readonly  string[] itemids = new string[] { "��", "���", "����", "����", "�Ź�", "����" };
    public readonly string[] itemclasses = new string[] { "����", "��", "����" };
    private void Awake()
    {
        if (instance == null)
            instance = this;
        renderPlayer = FindObjectOfType<RenderPlayer>();
        manager = FindObjectOfType<DungeonManager>();
        csvdata.PlayerSTAT = CSVReader.Read("PlayerStat");
        csvdata.Itemdata = CSVReader.Read("Item");
        csvdata.ItemUpgradeValue = CSVReader.Read("ItemUpgradeValue");
        csvdata.SkillInfo = CSVReader.Read("SkillInfo");
        csvdata.Quest = CSVReader.Read("Quest");
        csvdata.Monster = CSVReader.Read("Monster");
        csvdata.Pet = CSVReader.Read("Pet");
        itemUI = FindObjectOfType<ItemUI>();
        inventory = GameObject.FindObjectOfType<Inventory>();
        shop = GameObject.FindObjectOfType<Shop>();
    }
    public void UpdateInventory(List<GameObject> list, byte rating, int num,int scrollnum)
    {
        GetUserInventoryRequest request = new GetUserInventoryRequest();
        PlayFabClientAPI.GetUserInventory(request, result =>
        {
            List<ItemInstance> ii = result.Inventory;
            GameObject obj = Instantiate(inventory.ItemPrefab[scrollnum], Vector2.zero, Quaternion.identity, inventory.ItemPrefabTrans[scrollnum]) as GameObject;
            ItemInfo itemInfo = obj.GetComponent<ItemInfo>();
            itemInfo.itemdata.rating = rating;
            itemInfo.itemdata.ReinforcedPrice = int.Parse(SharedScript.instance.csvdata.ItemUpgradeValue[itemInfo.itemdata.Level]["Gold"].ToString());
            obj.transform.localScale = Vector2.one;
            for (int a = 0; a < itemInfo.itemdata.Values.Length; a++)
            {
                //print( ii[a].CustomData.ToString());
                int min;
                int max;
                if (int.TryParse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating][a == 0 ? "MinValue" : "MinValue" + (a + 1)].ToString(), out min))
                {
                    if (int.TryParse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating][a == 0 ? "MaxValue" : "MaxValue" + (a + 1)].ToString(), out max))
                        itemInfo.itemdata.Values[a] = UnityEngine.Random.Range(min, max + 1);
                }
                else
                    itemInfo.itemdata.Values[a] = float.Parse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating][a == 0 ? "MinValue" : "MinValue" + (a + 1)].ToString());
            }
            if (itemInfo.itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.POTION)
            {
                itemInfo.itemdata.Data[0] = ItemRating.instance.PotionRatingName[itemInfo.itemdata.rating];
                itemInfo.itemdata.Data[1] = itemInfo.itemdata.Values[1] != 0 ? SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["property2"].ToString() : "";
            }
            else
            {
                itemInfo.itemdata.Data[0] = SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["property1"].ToString();
                itemInfo.itemdata.Data[1] = SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["property2"].ToString();
            }
            int Price = int.Parse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo. itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["Price"].ToString());

            itemInfo.itemdata.Prices[0] = Price;
            itemInfo.itemdata.Prices[1] = (int)(Price*80*0.01f);
            itemInfo.itemdata.Name = ii[num].DisplayName;
            itemInfo.itemdata.ItemId = ii[num].ItemInstanceId;
            itemInfo.itemdata.Description = SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["Description"].ToString();
            if (itemInfo.itemdata.m_ITEMTYPE == m_Enum.ITEMTYPE.WEAPON)
                itemInfo.itemdata.Values[1] = float.Parse(SharedScript.instance.csvdata.Itemdata[SharedScript.instance.csvdata.ItemTypeNum[(byte)itemInfo.itemdata.m_ITEMTYPE] + itemInfo.itemdata.rating]["MinValue2"].ToString());

            list.Add(obj);
            AddItemListData(obj.GetComponent<ItemInfo>().itemdata, scrollnum);
            itemInfo.ResetInfo();
            SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
        }, error => { });
    }
    public void AddItemListData(PlayerPrefsData.ItemData item,int scrollnum)
    {

        switch (scrollnum)
        {
            case 0:
                SharedObject.g_playerPrefsdata.playerdataNoSave.WeaponItem.Add(item);
             
                break;
            case 1:
                SharedObject.g_playerPrefsdata.playerdataNoSave.HelmetItem.Add(item);
                break;
            case 2:
                SharedObject.g_playerPrefsdata.playerdataNoSave.ArmorItem.Add(item);
                break;
            case 3:
                SharedObject.g_playerPrefsdata.playerdataNoSave.LeggingsItem.Add(item);
                break;
            case 4:
                SharedObject.g_playerPrefsdata.playerdataNoSave.BootsItem.Add(item);
                break;
            case 5:
                SharedObject.g_playerPrefsdata.playerdataNoSave.PotionsItem.Add(item);
                break;

        }
    }
    public static Texture2D textureFromSprite(Sprite sprite)
    {
        if (sprite.rect.width != sprite.texture.width)
        {
            Texture2D newText = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
            Color[] newColors = sprite.texture.GetPixels((int)sprite.textureRect.x,
                                                         (int)sprite.textureRect.y,
                                                         (int)sprite.textureRect.width,
                                                         (int)sprite.textureRect.height);
            newText.SetPixels(newColors);
            newText.Apply();
            return newText;
        }
        else
            return sprite.texture;
    }
    public string StringBuilder(string Type)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<");
        sb.Append(Type);
        sb.Append(">");
        return sb.ToString();
    }
    private void OnApplicationQuit()
    {
        if(SharedObject.g_playerPrefsdata.playfabid != null && SharedObject.g_playerPrefsdata.playfabid != "")
            SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
    }
}
