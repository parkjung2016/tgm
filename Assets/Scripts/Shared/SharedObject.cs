using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SharedObject
{
    static public DataManager g_DataManager;
   static  public bool firstconnect;
    static public SoundManager g_SoundManager;
    static public PlayerPrefsData g_playerPrefsdata = new PlayerPrefsData();
    static public readonly string SavePath = Application.persistentDataPath + "/SaveData.json";
    static public string[] MapNamestr = new string[] { "은신처", "고블린 초원", "서식지", "통로", "세탄의 지역" };
}
