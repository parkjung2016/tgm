using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Hellmade.Sound;

public class SummonWeapon : MonoBehaviour
{
    ParticleSystem ps;
    [SerializeField]
    private AudioClip[] Sounds;
    private int curNumofPs = 0;
    [SerializeField]
    private float[] SoundVolumes;
    public int damage;
    [SerializeField]
    private bool NotParticle = false;
    private void Awake()
    {
        ps = GetComponent<ParticleSystem>();
        if (ps != null && ps.trigger.enabled)
        {
            Monster[] monster = FindObjectsOfType<Monster>();
            Boss boss = FindObjectOfType<Boss>();
            for (int i = 0; i < monster.Length; i++)
            {

                ps.trigger.AddCollider(monster[i].transform);
            }
            if (boss != null)
            {
                ps.trigger.AddCollider(boss.transform);

            }

        }
        if ( NotParticle )
        {
            gameObject.SetActive(false);
            ps = GetComponentInParent<ParticleSystem>();
            ActiveGameobj();
        }
        //ps.collision.AddPlane(SharedObject.g_DataManager.m_MyCharacter.transform);
    }
    private void ActiveGameobj()
    {
            gameObject.SetActive(ps.isPlaying);
        Invoke("ActiveGameobj", .2f);
    }
    // Update is called once per frame
    void Update()
    {
        if (ps == null)
            return;
        var amount = Mathf.Abs(curNumofPs - ps.particleCount);

        if (ps.particleCount > curNumofPs)
        {
            SharedObject.g_SoundManager.CallPlaySound(Sounds[0], amount, SoundVolumes[0],transform);
        }

        curNumofPs = ps.particleCount;
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!NotParticle)
            return;
        if (other.gameObject.layer == LayerMask.NameToLayer("Enemy") &&!other.CompareTag("Boss"))
        {
            Monster mon = other.GetComponent<Monster>();
            mon.StartCoroutine(mon.ApplyDamage(damage));
        }
        else if (other.CompareTag("Boss"))
        {
            Boss bos = other.GetComponent<Boss>();
            bos.StartCoroutine(bos.ApplyDamage(damage));
        }
        else if (other.CompareTag("Player") && GameObject.FindGameObjectsWithTag("Player").Length != 1)
        {
            GameObject[] Players = GameObject.FindGameObjectsWithTag("Player");
            for(int i = 0; i<Players.Length;i++)
            {
                if(Players[i].name == "Player(Clone)")
                {
            PlayerHPManager player = Players[i].GetComponent<PlayerHPManager>();

            player.Pun_ApplyDamage(damage);

                }
            }
        }
    }
    private void OnParticleCollision(GameObject other)
    {
        if (NotParticle)
            return;
        if(!other.CompareTag("Boss"))
        {
            Monster mon = other.GetComponent<Monster>();
            mon.StartCoroutine(mon.ApplyDamage(damage));
        }
        else if(other.CompareTag("Boss"))
        {
            Boss bos = other.GetComponent<Boss>();
            bos.StartCoroutine(bos.ApplyDamage(damage));
        }   
        else if (other.CompareTag("Player"))
        {
            PlayerHPManager player = other.GetComponent<PlayerHPManager>();

            player.Pun_ApplyDamage(damage);
        }
    }
  
}

