using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Skill : MonoBehaviour
{
    public SkillInfo skillInfo;
    public Image SkillImg;
    private void Start()
    {
        skillInfo.SkillDescription = SharedScript.instance.csvdata.SkillInfo[(byte)skillInfo.skilltype]["SkillDescription"].ToString();
        skillInfo.LevelLimit = int.Parse(SharedScript.instance.csvdata.SkillInfo[(byte)skillInfo.skilltype]["Level"].ToString());
        for (int i = 0; i < skillInfo.SkillValue.Length; i++)
        {

            skillInfo.SkillValue[i] = SharedScript.instance.csvdata.SkillInfo[(byte)skillInfo.skilltype][i==0 ? "SkillValue" : "SkillValue"+i.ToString() ] .ToString();
        }
            skillInfo.SkillCool = float.Parse( SharedScript.instance.csvdata.SkillInfo[(byte)skillInfo.skilltype]["SkillCool"].ToString());
        skillInfo.ManaCons = int.Parse(SharedScript.instance.csvdata.SkillInfo[(byte)skillInfo.skilltype]["Mana"].ToString());
    }
}
