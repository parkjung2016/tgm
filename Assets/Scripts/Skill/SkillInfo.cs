using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

[Serializable]
public class SkillInfo
{
    public string SkillDescription;
    public string[] SkillValue;
    public PlayerSkills.SkillType skilltype;
    public bool Equip;
    public int ManaCons;
    public float SkillCool;
    public int LevelLimit;
}
