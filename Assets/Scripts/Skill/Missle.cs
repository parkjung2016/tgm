using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Missle : MonoBehaviour
{
   public GameObject Target;
    float Damage;
    private AudioSource audioSource;
    [SerializeField]
    private AudioClip[] Sounds;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    private void Start()
    {
        audioSource.Play();
        Damage = PlayerSkillManager.instance.MissleDamage;
        if (Target == null)
            Destroy(gameObject);
        transform.DOLocalMove(Target.transform.position, 1).SetLoops(-1);
        transform.DOLookAt(Target.transform.position, .2f).SetLoops(-1);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == Target)
        {
            Monster monster = collision.GetComponent<Monster>();
            if (monster != null)
            {
                monster.StartCoroutine(monster.ApplyDamage(Damage));
            }
            Boss boss
                 = collision.GetComponent<Boss>();
            if (boss != null)
            {
                boss.StartCoroutine(boss.ApplyDamage(Damage));
            }
            Player player = collision.GetComponent<Player>();
            if (player != null)
            {
                PlayerHPManager.instance.StartCoroutine(PlayerHPManager.instance.ApplyDamage((int)Damage));
            }
            SharedObject.g_SoundManager.CallPlaySound(Sounds[1], 1, 1f, collision.gameObject.transform);
            Destroy(gameObject);
        }
    }
}
