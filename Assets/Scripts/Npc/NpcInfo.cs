using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcInfo : Actor
{
    public string NpcName;
    public int id;
    public string[] sentences;
    public AudioSource auidosource;
        private void Awake()
    {
        auidosource = GetComponentInChildren<AudioSource>();
    }
    public void PlaySound()
    {
        auidosource.Play();
    }
}
