using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RenderPlayer : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer petimage;
    public void SetPetImage(Sprite sprite,RuntimeAnimatorController anim,Vector2 scale)
    {
        if(sprite == null)
        {
            petimage.gameObject.SetActive(false);
        }
        else
        {
            petimage.gameObject.SetActive(true);
            petimage.sprite = sprite;
            petimage.transform.localScale = scale;
            petimage.GetComponent<Animator>().runtimeAnimatorController = anim;

        }
    }
}
