using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class MoveSound : MonoBehaviour
{
    public static MoveSound instance;
    [SerializeField]
    private GameObject[] tilemaps;

    [SerializeField]
    private List<TileData> tileDatas;
    [SerializeField]
    private LayerMask la;
    [SerializeField]
    private AudioClip[] MaterialClips;
    private Dictionary<TileBase, TileData> dataFromTiles;
    private void Awake()
    {
        var obj = FindObjectsOfType<MoveSound>();
        if (obj.Length == 1)
        {
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        if (instance == null)
        {
            instance = this;
        }   

        
        dataFromTiles = new Dictionary<TileBase, TileData>();
        foreach (var tiledata in tileDatas)
        {
            foreach (var tile in tiledata.tiles)
            {
                if (!dataFromTiles.ContainsKey(tile))
                    dataFromTiles.Add(tile, tiledata);
            }
        }
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        tilemaps = GameObject.FindGameObjectsWithTag("Ground");
    }

    public void PlayRunSFX(Transform CheckTrans,AudioSource audioSource)
    {
        RaycastHit2D hit2D = Physics2D.Raycast(CheckTrans.position, -CheckTrans.up, 50, la);
        if (hit2D.collider != null)
        {
            for (int i = 0; i < tilemaps.Length; i++)
            {
                if (hit2D.collider.gameObject == tilemaps[i])
                {
                    //if (tilemaps[i].GetComponent<Tilemap>() == null)
                    //    return;
                    Vector3Int gridPos = tilemaps[i].GetComponent<Tilemap>().WorldToCell(hit2D.point);
                    TileBase CurGround = tilemaps[i].GetComponent<Tilemap>().GetTile(gridPos);
                    if (CurGround != null && dataFromTiles.ContainsKey(CurGround) && dataFromTiles[CurGround] != null)
                    {
                        switch (dataFromTiles[CurGround].type)
                        {
                            case TileData.Type.Grass:
                                PlayMaterialSound(0,audioSource);
                                break;
                            case TileData.Type.Rock:
                                PlayMaterialSound(3, audioSource);
                                break;
                        }

                    }
                }
            }
        }
    }
    public void PlayMaterialSound(int Num,AudioSource audioSource)
    {
        audioSource.clip = MaterialClips[Random.Range(Num, Num + 3)];
        audioSource.Play();
    }
}
