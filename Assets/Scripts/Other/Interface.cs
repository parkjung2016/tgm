using System;
public interface IAttack
{
    void PunAttack();
    void ComboPossible_();
    void Combo();
    void ComboReset();
}
public interface IWeaponCol
{
    void WeaponColVisible();
    void WeaponColHide();
}

