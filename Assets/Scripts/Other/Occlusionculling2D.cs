using UnityEngine;
using System.Collections;

public class Occlusionculling2D : MonoBehaviour
{
    public BoxCollider2D col2d;
    Camera cam;
    private void Awake()
    {
        cam = Camera.main;
        col2d = GetComponent<BoxCollider2D>();

    }
    private void Start()
    {
        Renderer[] allobjs = GameObject.FindObjectsOfType<Renderer>();
        for(int i =0; i <allobjs.Length;i++)
        {
            if (allobjs[i].gameObject.CompareTag("Player")|| allobjs[i].gameObject.layer.Equals(LayerMask.NameToLayer("Ground")) || allobjs[i].gameObject.layer.Equals(LayerMask.NameToLayer("PvPPortal")) || allobjs[i].gameObject.CompareTag("ReSpawn") || allobjs[i].gameObject.layer.Equals(LayerMask.NameToLayer("NotOclusiionculling")) || allobjs[i].gameObject.layer.Equals(LayerMask.NameToLayer("Enemy")) || allobjs[i].gameObject.CompareTag("Booty"))
                continue;
            allobjs[i].enabled = false;
        }
        float height = 2.1f * cam.orthographicSize;
        float width = (height * cam.aspect)+8;
        col2d.size = new Vector2(width, height);
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<Renderer>() != null)
        collision.gameObject.GetComponent<Renderer>().enabled = false;
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        Renderer renderer = collision.gameObject.GetComponent<Renderer>();
        if (renderer == null || renderer.enabled)
            return;
       renderer.enabled = true;
    }

}
