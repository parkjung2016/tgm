using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Photon.Pun;
using ExitGames.Client.Photon;

public class WeaponCollider : MonoBehaviour
{
    protected Player player;
    protected Monster monster;
    protected Boss boss;
    [SerializeField]
    private Transform CheckSphere;
    [SerializeField]
    private LayerMask CheckLayer;
    
    private void Awake()
        
    {
    
        boss = GetComponentInParent<Boss>();
        player = GetComponentInParent<Player>();
        monster = GetComponentInParent<Monster>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        Monster monster_ = collision.GetComponent<Monster>();
        if (monster_ != null)
        {
            gameObject.SetActive(false);
            if (monster_.Death)
                return;
 
            if (PlayerSkillManager.instance.IsWidAreaAttack)
            {
                if (!IsSkeleton(monster_))
                {
                    monster_.StartCoroutine(monster_.ApplyDamage(PlayerSkillManager.instance.WideAreaPower[0]));
                    if (PlayerSkillManager.instance.IsBite)
                    {

                        Bite(monster_.gameObject);
                    }
                }
                Collider2D[] colls = Physics2D.OverlapCircleAll(CheckSphere.position, 10, CheckLayer);

                for (int i = 0; i < colls.Length; i++)
                {
                    if (!IsSkeleton(colls[i].GetComponent<Monster>()))
                    {
                        if (monster_.gameObject != colls[i].gameObject)
                        {

                            Monster mon = colls[i].GetComponent<Monster>();
                            if (PlayerSkillManager.instance.IsVampire)
                                Vampire(monster_.Hp,10);
                            if (PlayerSkillManager.instance.IsBite)
                            {

                                Bite(mon.gameObject);
                            }
                            else
                                mon.StartCoroutine(mon.ApplyDamage(PlayerSkillManager.instance.WideAreaPower[1]));
                        }
                    }
                    else if(colls[i].GetComponent<Boss>())
                    {
                        Boss bos = colls[i].GetComponent<Boss>();
                        if (PlayerSkillManager.instance.IsVampire && player.character.F[(byte)m_Enum.STAT.HP] < player.character.Max[(byte)m_Enum.MAXSTAT.MAXHP])
                        {
                            player.character.F[(byte)m_Enum.STAT.HP] = monster_.Hp * 30 * .01f;
                            PlayerSkillManager.instance.IsVampire = false;
                            UIProgressBar.instance.SetHPProgress();
                        }
                        if (PlayerSkillManager.instance.IsBite)
                        {

                            Bite(bos.gameObject);
                        }
                        else
                        bos.StartCoroutine(bos.ApplyDamage(PlayerSkillManager.instance.WideAreaPower[1]));

                    }
                }
                PlayerSkillManager.instance.IsVampire = false;
                PlayerSkillManager.instance.SummonWideAreaEffect();
            }
            else
            {

                if (PlayerSkillManager.instance.IsVampire)
                    Vampire(monster_.Hp,30);
                if(PlayerSkillManager.instance.IsBite)
                {

                    Bite(monster_.gameObject);
                }
                else
                {
                if (!IsSkeleton(monster_))
                {
                    monster_.StartCoroutine(monster_.ApplyDamage(SharedObject.g_DataManager.m_MyCharacter.F[(byte)m_Enum.STAT.POWER] + SharedObject.g_DataManager.m_MyCharacter.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALPOWER]));
                }

                }
                PlayerSkillManager.instance.IsVampire = false;
            }
        }
        Player player_ = collision.GetComponent<Player>(); 
        if (player != null && player_ != null&& player_ != player || player_ != null && player == null)
        {
            //if (player != null && player_ != null && player_ != player && !player.PV.IsMine)
            //    return;
                gameObject.SetActive(false);

                if (player_.Death)
                    return;
            if (PhotonNetwork.InRoom)
            {
          
                player_.GetComponent<PlayerHPManager>().PV.RPC("Pun_ApplyDamage", RpcTarget.All, (int)(player.F[(byte)m_Enum.STAT.POWER] + player.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALPOWER]));
            }
            else
            {

                player_.GetComponent<PlayerHPManager>().StartCoroutine(player_.GetComponent<PlayerHPManager>().ApplyDamage(monster != null ? monster.Damage : boss.Damage));
            }
        }
        Boss boss_ = collision.GetComponent<Boss>();
        if (boss_ != null)
        {
                gameObject.SetActive(false);
            if (boss_.Death)
                return;
            if (PlayerSkillManager.instance.IsWidAreaAttack)
            {
                boss_.StartCoroutine(boss_.ApplyDamage(SharedObject.g_DataManager.m_MyCharacter.F[(byte)m_Enum.STAT.POWER] + SharedObject.g_DataManager.m_MyCharacter.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALPOWER]));
                Collider2D[] colls = Physics2D.OverlapCircleAll(CheckSphere.position, 10, CheckLayer);
                for (int i = 0; i < colls.Length; i++)
                {
                    if (!IsSkeleton(colls[i].GetComponent<Monster>()))
                    {
                        if (monster_.gameObject != colls[i].gameObject)
                        {
                            Monster mon = colls[i].GetComponent<Monster>();
                            if (PlayerSkillManager.instance.IsVampire)
                                Vampire(monster_.Hp,10);
                            if (PlayerSkillManager.instance.IsBite)
                            {
                                Bite(mon.gameObject);
          
                            }
                            else
                                mon.StartCoroutine(mon.ApplyDamage(PlayerSkillManager.instance.WideAreaPower[1]));

                        }
                    }
                }
                PlayerSkillManager.instance.IsVampire = false;
                PlayerSkillManager.instance.SummonWideAreaEffect();
            }
            else
            {
                if (PlayerSkillManager.instance.IsVampire)
                    Vampire(monster_.Hp, 30);

                    PlayerSkillManager.instance.IsVampire = false;
                if (PlayerSkillManager.instance.IsBite)
                {
                    Bite(boss_.gameObject);
                }
                else
                    boss_.StartCoroutine(boss_.ApplyDamage(SharedObject.g_DataManager.m_MyCharacter.F[(byte)m_Enum.STAT.POWER] + SharedObject.g_DataManager.m_MyCharacter.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALPOWER]));

            }

        }
    }

    private void Bite(GameObject obj)
    {
            Monster mon = obj.GetComponent<Monster>();
        if (mon != null)
        {
            mon.StartCoroutine(mon.ApplyDamage(PlayerSkillManager.instance.BitePower));
            if (mon.Hp <= mon.Hp * 9 * .01f)
            {
                mon.StartCoroutine(mon.ApplyDamage(99999));
            }
        }
            PlayerSkillManager.instance.IsBite = false;
        CallSkillSFX((byte)PlayerSkills.SkillType.Bite,obj.transform);
        PlayerSkillManager.instance.SkillCol(PlayerSkillManager.instance.BiteSkillCool, PlayerSkillManager.instance.BiteSkillNum);

    }
    void CallSkillSFX(int Num,Transform objtransform)
    {
        PlayerSkillManager.instance.SkillSFX(Num, objtransform);
    }
    void Vampire(float EnemyHP,int Value)
    {
            
            player.character.F[(byte)m_Enum.STAT.HP] = Mathf.Clamp(player.character.F[(byte)m_Enum.STAT.HP] + EnemyHP * Value * .2f,0, player.character.Max[(byte)m_Enum.MAXSTAT.MAXHP]);
            UIProgressBar.instance.SetHPProgress();
        CallSkillSFX((byte)PlayerSkills.SkillType.VampireSword,transform);
        PlayerSkillManager.instance.SkillCol(PlayerSkillManager.instance.VampireSkillCool, PlayerSkillManager.instance.VampireSkillNum);
            PlayerSkillManager.instance.SummonVampireEffect();
    }
    public bool IsSkeleton(Monster obj)
    {
        Vector2 dirtoTarget = obj.transform.position - SharedObject.g_DataManager.m_MyCharacter.transform.position;
        float Angle = Vector2.Angle(obj.transform.position, dirtoTarget);
        if (obj is Skeleton && -obj.transform.localScale.x == obj.Scale && Mathf.Abs(Angle) < 90 || -obj.transform.localScale.x != obj.Scale && Mathf.Abs(Angle) > 90)
        {
            Skeleton skeleton = obj as Skeleton;
            if (skeleton != null)
            {
                if (UnityEngine.Random.Range(1, 100) <= skeleton.DefencePercent)
                {
                    skeleton.anim.SetTrigger("Blocked");
                    skeleton.ComboReset();
                    skeleton.AttackTrue = true;
                    skeleton.MoveTrue = true;
                }
                else
                {
                    return false;
                }
                return true;
            }
            else
                return false;
        }
        else
        {
            return false;
        }
    }
}
