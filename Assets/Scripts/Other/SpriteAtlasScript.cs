using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class SpriteAtlasScript : MonoBehaviour
{
    [SerializeField]
    private SpriteAtlas atlas;
    [SerializeField]
    private string sprite_name;
    private void Start()
    {
        SpriteRenderer sprite;
        Image image;
        if(TryGetComponent<Image>(out image))
            {
            image.sprite = atlas.GetSprite(sprite_name);
        }
        else if(TryGetComponent<SpriteRenderer>(out sprite))
        {
            sprite.sprite = atlas.GetSprite(sprite_name);
        }
    }
}
