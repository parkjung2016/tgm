using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PublicFunction 
{
    public static void SetImageColor(Image[] Imgs,Color[] color)
    {
        for (int i = 0; i < Imgs.Length; i++)
            {
            Imgs[i].color = color[i];
        }
    }
}