using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;

public class PlayFabManager : MonoBehaviour
{
   public static PlayFabManager instance;
    private void Awake()
    {
        PlayFabManager[] objs = FindObjectsOfType<PlayFabManager>();
        if(objs.Length == 1)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void AddLevelToLeaderBoard(int level)
    {
        var request = new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate> {  new StatisticUpdate
            {
                StatisticName = "LevelRank",
                Value = level

            } }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, success =>
        {
         
        }, error => {  });
    }
}
