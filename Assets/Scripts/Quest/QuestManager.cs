using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class QuestManager : MonoBehaviour
{
    public static QuestManager instance;
    public int questId;
    public Dictionary<int, QuestData> questList = new Dictionary<int, QuestData>();
    LobbyManager lobbyManager;
    private void Awake()
    {
        
      var obj = FindObjectsOfType<QuestManager>();
        if (obj.Length == 1)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        for (int i = 0; i < SharedObject.g_playerPrefsdata.playerdata.questListID.Count; i++)
        {
           if (!questList.ContainsKey(SharedObject.g_playerPrefsdata.playerdata.questListID[i]))
            {

                questList.Add(SharedObject.g_playerPrefsdata.playerdata.questListID[i], SharedObject.g_playerPrefsdata.playerdata.questListQUESTDATA[i]);
            }
        }
        SceneManager.sceneLoaded += OnSceneLoad;
        RefreshQuestList();
        CheckQuestComplete();
    }
    public void NextMainQuest()
    {
        int FinalNum = questList.Count - 1;
        QuestUI.instance.audioSource.Play();
        QuestUI.instance.RemoveQuest(0, 0);
        questList[10+FinalNum].questNames[0] = questList[10+FinalNum].questNames[0] + "(완료)";
        SharedObject.g_playerPrefsdata.playerdata.questListQUESTDATA[FinalNum].questNames[0] = SharedScript.instance.csvdata.Quest[FinalNum]["메인퀘스트내용"].ToString() + "(완료)";
        GenerateData(10+FinalNum+1, SharedScript.instance.csvdata.Quest[FinalNum + 1]["메인퀘스트내용"].ToString(), SharedScript.instance.csvdata.Quest[FinalNum + 1]["메인퀘스트내용2"].ToString(), SharedScript.instance.csvdata.Quest[FinalNum + 1]["메인퀘스트목표"].ToString().Contains("(0/") ? true : false, FinalNum+1, SharedScript.instance.csvdata.Quest[FinalNum+1]["NPC번호"].ToString().Split(';'));
        CheckQuestComplete();
    }
    public void CheckQuestComplete()
    {
        for (int i = 0; i < questList.Count; i++)
        {

            if (SharedScript.instance.csvdata.Quest[i].ContainsKey("메인퀘스트목표")  && questList[10 + i].questNames[0] != SharedScript.instance.csvdata.Quest[i]["메인퀘스트내용"].ToString() + "(완료)")
            {
                if (SharedScript.instance.csvdata.Quest[i]["메인퀘스트목표"].ToString().Contains("0/"))
                {


                    int[] value = new int[2];
                    int[] value2 = new int[2];
                    string[] Value = SharedScript.instance.csvdata.Quest[i]["메인퀘스트목표"].ToString().Replace('(', ' ').Replace(')', ' ').Split('/');
                    string[] Value2 = SharedScript.instance.csvdata.Quest[i]["메인퀘스트목표2"].ToString().Replace('(', ' ').Replace(')', ' ').Split('/');
                    value[0] = SharedObject.g_playerPrefsdata.playerdata.QuestTargetNum[i];
                    value2[0] = SharedObject.g_playerPrefsdata.playerdata.QuestTargetNum2[i];
                    int.TryParse(Value[1], out value[1]);
                    int.TryParse(Value2[1], out value2[1]);
                    if (value2[1] != 0)
                    {
                        if (value[0] >= value[1] && value2[0] >= value2[1])
                        {

                            NextMainQuest();
                        }
                    }
                    else
                    {
                        if (value[0] >= value[1])
                        {
                            NextMainQuest();
                        }

                    }
                }
                else if (SharedScript.instance.csvdata.Quest[i]["메인퀘스트목표"].ToString().Contains("레벨 달성하기"))
                {
                    int TargetLevel = int.Parse( SharedScript.instance.csvdata.Quest[i]["메인퀘스트목표"].ToString().Replace("레벨 달성하기", ""));
                    if(SharedObject.g_playerPrefsdata.playerdata.m_nStat2[(byte)m_Enum.STAT2.LEVEL]>=TargetLevel)
                    {
                        if(int.Parse(SharedScript.instance.csvdata.Quest[i]["퀘스트 보상"].ToString()) == 4000)
                        {
                            if(lobbyManager != null)
                            {

        CanvasManager.instance.NotLoad = true;
        FadeCanvas.instance.Fade.gameObject.SetActive(true);
        FadeCanvas.instance.Fade.DOFade(1, 0);
                            CallBossSequence();
                            }
                        }
                    }
                }    
            }
        }
    }
    private void CallBossSequence()
    {
        if(SharedObject.g_DataManager.m_MyCharacter == null)
        {
            Invoke("CallBossSequence", .5f);
        }
        else
        {
       lobbyManager.CallQuestBossPlay();
        FadeCanvas.instance.Fade.DOFade(0, 2f).OnComplete(() =>
        {
            CanvasManager.instance.NotLoad = false;
            FadeCanvas.instance.Fade.gameObject.SetActive(false);
        });

        }
    }
    public void RefreshQuestList()
    {
        //if (QuestUI.instance == null)
        //    return;
        if (!questList.ContainsKey(10))
        {
            GenerateData(10, SharedScript.instance.csvdata.Quest[0]["메인퀘스트내용"].ToString(), "없음", false, 0, SharedScript.instance.csvdata.Quest[0]["NPC번호"].ToString().Split(';'));
        }
        else
        {

            for (int i = 0; i < SharedScript.instance.csvdata.Quest.Count; i++)
            {
                if (questList.ContainsKey(10 + i) && SharedScript.instance.csvdata.Quest[i].ContainsKey("메인퀘스트내용"))
                {
                    if (questList[10 + i].questNames[0] != SharedScript.instance.csvdata.Quest[i]["메인퀘스트내용"].ToString() + "(완료)")
                    {
                        QuestUI.instance.EnableGameObj(true);
                            if(questList[10 + i].questNames[1] != "없음")
                            {
                        QuestUI.instance.AddQuest(questList[10 + i].questNames[0] + SharedScript.instance.csvdata.Quest[i]["메인퀘스트목표"].ToString(), questList[10 + i].questNames[1] +SharedScript.instance.csvdata.Quest[i]["메인퀘스트목표2"].ToString(), 0);
                            }
                            else
                            {
                        QuestUI.instance.AddQuest(SharedScript.instance.csvdata.Quest[i]["메인퀘스트내용"].ToString(),"없음", 0);

                            }
                    }

                }
            }
        }
        QuestUI.instance.RefreshQuestValue();
    }
    private void OnSceneLoad(Scene arg0, LoadSceneMode arg1)
    {
        CheckQuestComplete();
        RefreshQuestList();
        lobbyManager = FindObjectOfType<LobbyManager>();
    }
   public void GenerateData(int Key,string msg,string msg2,bool IsTargetOn,int ArrayNum,string[] NPCNum)
    {
        QuestUI.instance.EnableGameObj(true);
        int[] NPCNum_ = new int[NPCNum.Length];
        for(int i=0; i <NPCNum.Length;i++)
        {
            NPCNum_[i] = int.Parse(NPCNum[i]);
        }
       questList.Add(Key, new QuestData(new string[2] { msg, msg2 }, NPCNum_, new string[2] { SharedScript.instance.csvdata.Quest[ArrayNum]["메인퀘스트타입"].ToString(), SharedScript.instance.csvdata.Quest[ArrayNum]["메인퀘스트타입2"].ToString() }));
        SharedObject.g_playerPrefsdata.playerdata.questListQUESTDATA.Add(questList[Key]);
        SharedObject.g_playerPrefsdata.playerdata.questListID.Add(Key);
        SharedObject.g_playerPrefsdata.playerdata.QuestTargetNum.Add(0);
        SharedObject.g_playerPrefsdata.playerdata.QuestTargetNum2.Add(0);
        
        if (IsTargetOn)
        {
            if (msg2 != "없음")
            {
                QuestUI.instance.AddQuest( msg + SharedScript.instance.csvdata.Quest[ArrayNum]["메인퀘스트목표"].ToString(),msg2 + SharedScript.instance.csvdata.Quest[ArrayNum]["메인퀘스트목표2"].ToString(), 0);
            }
            else
            {
                QuestUI.instance.AddQuest( msg + SharedScript.instance.csvdata.Quest[ArrayNum]["메인퀘스트목표"].ToString(), "없음", 0);

            }
            
        }
        else
        { 
        QuestUI.instance.AddQuest( msg, "없음", 0);
        }

        SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
    }
    public int GetQuestTalkIndex(int id) // Npc Id를 받아 퀘스트 번호를 반환하는 함수 
    {
        return questId;
    }
}
