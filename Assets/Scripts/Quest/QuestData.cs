using System;
[Serializable]
public class QuestData 
{
    public string[] questNames= new string[2];
    public int[] npcId;
    public string[] type;
    public  QuestData(string[] name,int[] npc, string[] type_)
    {
        questNames = name;
        npcId = npc;
        type = type_;
    }

}
