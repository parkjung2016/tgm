using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerSound : Player
{
    [SerializeField]
    private Transform CheckGround;
    [SerializeField]
    private AudioSource BehaviourAudios;
    [SerializeField]
    private AudioClip[] BehaviourClips;
    [SerializeField]
    private AudioSource SlashAudio;
    [SerializeField]
    private AudioClip[] SlashClips;

    public void PlayRunSFX()
    {
        if (PV != null &&!PV.IsMine)
            return;
        MoveSound.instance.PlayRunSFX(CheckGround, BehaviourAudios);
    }
    public void PlayDashSFX()
    {
        BehaviourAudios.clip = BehaviourClips[0];
        BehaviourAudios.Play();
    }
    public void PlayHitSFX()
    {
        BehaviourAudios.clip = BehaviourClips[1];
        BehaviourAudios.Play();
    }
    public void PlaySlashSFX(int Num)
    {
        SlashAudio.clip = SlashClips[Num];
        SlashAudio.Play();
    }
}
