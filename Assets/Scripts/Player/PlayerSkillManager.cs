using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using DG.Tweening;

public class PlayerSkillManager : Player
{
    Coroutine runningCoroutine = null;
    public static PlayerSkillManager instance;
    [SerializeField]
    private float[] SkilltriggerDis;

    public GameObject[] SkillEffcts;
    [SerializeField]
    List<GameObject> FindEnemies = new List<GameObject>();
    [SerializeField]
    Monster[] Enemies;
    bool[] SkillPlaying = new bool[3];
    [SerializeField]
    private Image[] SkillCoolProgressbar= new Image[3];
    public bool IsShield;
    public bool IsWidAreaAttack;
    public float[] WideAreaPower;
    public float BitePower;
    private int ShieldSkillNum;
    [SerializeField]
    private Transform ManySwordTrans;
    [SerializeField]
    private Transform WideAreaTrans;
    private Image LackOfMana;
    public bool IsVampire;
    public int VampireSkillNum;
    public float VampireSkillCool;
    public float MissleDamage;
    public int BiteSkillNum;
    public float BiteSkillCool;
    [SerializeField]
    private Transform[] MissleTranses;
    [SerializeField]
    private Transform VampireTrans;
    public bool IsBite;
    private Boss boss;
    public AudioClip[] SkillSFXs;
    private void Awake()
    {
        LackOfMana = GameObject.Find("LackOfMana").GetComponent<Image>();
        LackOfMana.DOFade(0, 0);
        awake();
        if (instance == null)
            instance = this;
    }
    private void Start()
    {
        if (PVPManager.instance != null)
        {
            enabled = false;
            return;
        }
        else
        {

        for (int i =0; i <SkillCoolProgressbar.Length;i++)
        {
            SkillCoolProgressbar[i] = MyScene.instance.SkillCollProgressBarObjs[i].GetComponent<Image>();
            SkillCoolProgressbar[i].fillAmount = 0;
            SkillCoolProgressbar[i].gameObject.SetActive(false);
        }
        start();
        Invoke("SetEnemies", .5f);
        boss = FindObjectOfType<Boss>();
        }
    }

    void SetEnemies()
    {
        Enemies = FindObjectsOfType<Monster>();
    }
    private void Update()
    {
        if (boss != null)
        {
            FindEnemy(boss.gameObject);
        }
        for (int i =0; i < Enemies.Length;i++)
        {
            if (Enemies[i] != null)
            {
                FindEnemy(Enemies[i].gameObject);
            }

        }
    }
    private void FindEnemy(GameObject obj)
    {
        if (Vector2.Distance(obj.transform.position, transform.position) <= SkilltriggerDis[0])
        {
            if (FindEnemies.Count < 3 && !FindEnemies.Contains(obj.gameObject))
            {
                FindEnemies.Add(obj.gameObject);
            }
        }
        else
        {
            for (int a = 0; a < FindEnemies.Count; a++)
            {
                if (FindEnemies[a] != null)
                {

                    if (FindEnemies.Contains(obj.gameObject))
                    {
                        FindEnemies.Remove(obj.gameObject);
                    }
                }

            }

        }
        for(int i =0; i < FindEnemies.Count;i++)
        {
            if(FindEnemies[i] == null)
            {
                FindEnemies.RemoveAt(i);
            }
        }
    }
    [PunRPC]
    public void UseSkill(PlayerSkills.SkillType skillType,int Num,float SkillCool)
    {
        if (character.Mana < SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].ManaCons)
        {
            if (runningCoroutine != null)
            {
                StopCoroutine(runningCoroutine);
            }
            runningCoroutine = StartCoroutine(LackOfManaVisible());
            return;
        }
        if (SkillPlaying[Num])
            return;
        bool T =true;
        switch (skillType)
        {
            case PlayerSkills.SkillType.NearbyEnemies:
                if (FindEnemies.Count != 0)
                {
                    SkillCol(SkillCool, Num);
                    T = true;
                }
                else
                    T = false;

                for (int i = 0; i < FindEnemies.Count; i++)
                {
                    SummonEffect(SkillEffcts[(byte)skillType], FindEnemies[i].transform.position,0,1);
                    float[] damage = new float[SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].SkillValue.Length];
                    for (int a = 0; a < damage.Length; a++)
                    {
                        damage[a] = float.Parse(SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].SkillValue[a]);
                    }
                    SkillSFX((byte)PlayerSkills.SkillType.NearbyEnemies,FindEnemies[i].transform);
                    FindEnemies[i].GetComponent<Monster>().StartCoroutine(FindEnemies[i].GetComponent<Monster>().ApplyDamage(damage[0] + damage[1]));
                }
                break;
            case PlayerSkills.SkillType.GuidedMissle:
                MissleDamage =float.Parse( SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].SkillValue[0]);
                T = false;
      
                if (FindEnemies.Count == 0)
                {

                    break;
                }
                PlayerManaManager.instance.StopCoroutine(PlayerManaManager.instance.regenerationMana());
                SkillPlaying[Num] = true;
                SkillCoolProgressbar[Num].gameObject.SetActive(true);
                SkillCoolProgressbar[Num].fillAmount = 1;
                character.Mana -= SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].ManaCons;
                PlayerManaManager.instance.StartCoroutine(PlayerManaManager.instance.regenerationMana());
                SkillCol(SkillCool, Num);
                for (int i = 0; i < MissleTranses.Length; i++)
                {
                    if (FindEnemies[i] == null)
                        continue;
                    Missle missle = Instantiate(SkillEffcts[(byte)skillType], MissleTranses[i].position, Quaternion.identity).GetComponent<Missle>();
                    missle.Target = FindEnemies[i];
                }
             
                break;
            case PlayerSkills.SkillType.Shield:
                SkillEffcts[(byte)skillType].GetComponent<ParticleSystem>().Play();
                SkillEffcts[(byte)skillType].GetComponent<AudioSource>().Play();
                ShieldSkillNum = (byte)skillType;
                SkillSFX((byte)PlayerSkills.SkillType.Shield, transform);
                StartCoroutine(WaitShield(Num, SkillCool));
                IsShield = true;
                T = true;
                break;
            case PlayerSkills.SkillType.Lightning:
                GameObject obj = SummonEffect(SkillEffcts[(byte)skillType], transform.position, -90, 3);
                SummonWeapon[] summonWeapons = obj.GetComponentsInChildren<SummonWeapon>();
                for (int i = 0; i < summonWeapons.Length; i++)
                {
                summonWeapons[i].damage = int.Parse(SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].SkillValue[0]);

                }

                T = true;
                SkillCol(SkillCool, Num);
                break;
            case PlayerSkills.SkillType.Bite:
                IsBite = true;
                BitePower = float.Parse( SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].SkillValue[0]);
                BiteSkillCool = SkillCool;
                BiteSkillNum = Num;
                T = true;
                StartCoroutine(FalseBite());
                break;
            case PlayerSkills.SkillType.WideAreaAttack:
                StartCoroutine(WaitWideArea(Num, SkillCool));
 
                T = true;
                break;
            case PlayerSkills.SkillType.VampireSword:
                if (character.F[(byte)m_Enum.STAT.HP] >= character.Max[(byte)m_Enum.MAXSTAT.MAXHP])
                {
                    SharedObject.g_SoundManager.PlayErrorAudio();
                    T = false;
                    break;
                }
                IsVampire = true;
                VampireSkillCool = SkillCool;
                VampireSkillNum = Num;
                StartCoroutine(FalseVampire());
                break;
            case PlayerSkills.SkillType.ManySword:
                GameObject obj2 = SummonEffect(SkillEffcts[(byte)skillType], ManySwordTrans.position, -90, 5);
                obj2.GetComponent<SummonWeapon>().damage  = int.Parse(SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].SkillValue[0]);
                T = true;
                SkillCol(SkillCool, Num);
                break;
            case PlayerSkills.SkillType.ManaRecovery:
                    T = false;
                if (character.Mana >= character.Max[(byte)m_Enum.MAXSTAT.MAXMANA] || character.F[(byte)m_Enum.STAT.HP] <= 20)
                {
                    SharedObject.g_SoundManager.PlayErrorAudio();
                    break;
                }
                character.F[(byte)m_Enum.STAT.HP] = (int)(character.F[(byte)m_Enum.STAT.HP] * (1 - 10 * .01f));
                character.Mana = (int)(character.Mana * (1 + 15 * .01f));
                SummonManaRecoveryEffect();
                SkillSFX((byte)PlayerSkills.SkillType.ManaRecovery, transform);
                UIProgressBar.instance.SetHPProgress();
                break;
        }
        if(T)
        {
            PlayerManaManager.instance. StopCoroutine(PlayerManaManager.instance.regenerationMana());
        SkillPlaying[Num] = true;
        SkillCoolProgressbar[Num].gameObject.SetActive(true);
        SkillCoolProgressbar[Num].fillAmount = 1;
        character.Mana -= SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].ManaCons;
            PlayerManaManager.instance.StartCoroutine(PlayerManaManager.instance.regenerationMana());
        }
        UIProgressBar.instance.SetManaProgress();
    }
      public void SkillSFX(int num,Transform objtransform)
    {
        SharedObject.g_SoundManager.CallPlaySound(SkillSFXs[num], 1, .7f, objtransform);
    }
    IEnumerator FalseVampire()
    {
        yield return new WaitForSeconds(8);
        IsVampire = false;
    }
    IEnumerator FalseBite()
    {
        yield return new WaitForSeconds(6);
        IsBite = false;
    }
    IEnumerator LackOfManaVisible()
    {
        WaitForSeconds ws = new WaitForSeconds(.3f);
        LackOfMana.DOFade(0.6f, 0.2f);
        yield return ws;
            LackOfMana.DOFade(0, .2f);
        yield return null;
    }
    IEnumerator WaitShield(int Num, float SkillCool)
    {
            if (!SkillEffcts[ShieldSkillNum].GetComponent<ParticleSystem>().isPlaying)
            {
                IsShield = false;
                SkillCol(SkillCool, Num);
                yield return null;
            }
        else
        {
            yield return new WaitForSeconds(.1f);
            StartCoroutine(WaitShield(Num,SkillCool));
        }
    }
    public void SummonWideAreaEffect()
    {

        SummonEffect(SkillEffcts[(byte)PlayerSkills.SkillType.WideAreaAttack],WideAreaTrans.position,-90,4);
    }
    private void SummonManaRecoveryEffect()
    {
        SummonEffect(SkillEffcts[(byte)PlayerSkills.SkillType.ManaRecovery],transform.position,-90,4);

    }
    public void SummonVampireEffect()
    {

        SummonEffect(SkillEffcts[(byte)PlayerSkills.SkillType.VampireSword], VampireTrans.position, 90,4);
    }
    private GameObject SummonEffect(GameObject obj,Vector3 pos,float xRot,float time)
    {
        GameObject obj_ = Instantiate(obj, pos, Quaternion.Euler(xRot, 0,0));
        Destroy(obj_, time);
        return obj;
    }

    IEnumerator WaitWideArea(int Num, float SkillCool)
    {
        WideAreaPower = new float[SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].SkillValue.Length];
        for (int a = 0; a < WideAreaPower.Length; a++)
        {
            WideAreaPower[a] = float.Parse(SharedObject.g_playerPrefsdata.playerdata.EquipSkills[Num].SkillValue[a]);
        }
        IsWidAreaAttack = true;
        yield return new WaitForSeconds(SkillCool);
        IsWidAreaAttack = false;
            SkillCol(SkillCool, Num);
    }
    public void SkillCol(float time,int num)
    {
        SkillCoolProgressbar[num].DOFillAmount(0, time).OnComplete(() =>
        {
            SkillPlaying[num] = false;
        SkillCoolProgressbar[num].gameObject.SetActive(false);
        });
    }
}