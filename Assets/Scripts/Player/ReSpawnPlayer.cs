using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class ReSpawnPlayer : MonoBehaviour
{
    public static ReSpawnPlayer reSpawnPlayer_;
    private void Awake()
    {
        reSpawnPlayer_ = this;
    }
    public void ReSpawn()
    {
        SharedObject.g_SoundManager.PlayBtnClickSound();
        MyScene.instance.DungeonNum = 0;
        MyScene.instance.SpawnPlayerNum = 0;
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i].DOFade(0, .5f);
        }
        LoadingSceneManager.LoadScene("Lobby");
    }

}
