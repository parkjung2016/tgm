using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Photon;
using Photon.Pun;

public class PlayerPhotonSerialize : Player
{
    public PlayerHPManager playerHPmanager;
    private void Awake()
    {
        playerHPmanager = GetComponent<PlayerHPManager>();
        base.awake();
    }
    public override void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
     
            stream.SendNext( transform.position);
            //if (playerHPmanager._PlayerHPProgressBar != null)
            //{
            //    stream.SendNext(playerHPmanager._PlayerHPProgressBar.Progress.fillAmount);
            //    stream.SendNext(playerHPmanager._PlayerHPProgressBar.transform.localScale);

            //}
        }
        //클론이 통신을 받는 
        else
        {
        
            playerMove.curPos = (Vector3)stream.ReceiveNext();
            //if (playerHPmanager._PlayerHPProgressBar != null)
            //{
            //    playerHPmanager._PlayerHPProgressBar.Progress.DOFillAmount((float)stream.ReceiveNext(), 1);
            //    playerHPmanager._PlayerHPProgressBar.
            //        transform.localScale = (Vector3)stream.ReceiveNext();
            //}
        }
        //base.OnPhotonSerializeView(stream,info);
    }
}
