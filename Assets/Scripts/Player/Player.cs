using Cinemachine;
using DG.Tweening;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine.UI;


public  class Player : Character,IPunObservable
{
   
    [HideInInspector]
    public PlayerAnim playerAnim;
    [HideInInspector]
    public PlayerMove playerMove;
    [HideInInspector]
    public PlayerAttack playerAttack;
    [HideInInspector]
    public CharacterController2D Controller;
    public Character character;
    private CinemachineImpulseSource impulse;
    private GameObject HpBar;
    private Image OnHitColor;
    private PlayerSkills playerSkills;
    public PlayerSkills.SkillType[] CurrentSkillTypes;
    private CinemachineVirtualCamera cinemachineVirtual;
    public PhotonView PV;
    public bool IsFighting;
    public Coroutine runningcoroutine = null;
    public virtual void awake()
    {
        PV = GetComponent<PhotonView>();
        playerSkills = new PlayerSkills();
        playerSkills.OnSkillUnlocked += PlayerSkils_OnSkillUnlocked;
        HpBar = GameObject.FindGameObjectWithTag("HPBar");
        OnHitColor = GameObject.FindGameObjectWithTag("OnHitColor").GetComponent<Image>();
        impulse = FindObjectOfType<CinemachineImpulseSource>();
        character = GetComponent<Character>();
        WeaponCol = GetComponentInChildren<WeaponCollider>().gameObject;
        ActorType = m_Enum.ACTORTYPE.PLAYER;
        anim = GetComponent<Animator>();
        Controller = GetComponent<CharacterController2D>();
        playerMove = GetComponent<PlayerMove>();
        playerAnim = GetComponent<PlayerAnim>();
        spriteRenderer_ = GetComponent<SpriteRenderer>();
        playerAttack = GetComponent<PlayerAttack>();
        rig2d = GetComponent<Rigidbody2D>();
        cinemachineVirtual = FindObjectOfType<CinemachineVirtualCamera>();
    }
    IEnumerator checkImpossibleBehaviour()
    {
            if(!anim.GetCurrentAnimatorStateInfo(0).IsName("PlayerHit"))
            {

            if(!anim.GetCurrentAnimatorStateInfo(0).IsTag("Behaviour"))
            {
                if(!anim.GetCurrentAnimatorStateInfo(0).IsName("PlayerDash"))
                {
                    playerMove.isDashing = false;
                    //playerMove.DashTimerTrue = false;

                }
                if (!anim.GetCurrentAnimatorStateInfo(0).IsName("PlayerRoll"))
                {
                playerMove.Rolling = false;
                //playerMove.RollTimerTrue = false;
                //playerMove.RollCurrentTimer = 0;

                }
            }
            if(!anim.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
            {

            playerAttack.PunComboReset();
            AttackTrue = true;
            MoveTrue = true;
            }
            }
        yield return new WaitForSeconds(.2f);
        StartCoroutine(checkImpossibleBehaviour());
    }
    private void PlayerSkils_OnSkillUnlocked(object sender, PlayerSkills.OnSkillUnlockedEventArgs e)
    {
       switch(e.skilltype)
        {
            case PlayerSkills.SkillType.GuidedMissle:

                break;
            case PlayerSkills.SkillType.Shield:

                break;
            case PlayerSkills.SkillType.ManySword:

                break;
            case PlayerSkills.SkillType.Bite:

                break;
            case PlayerSkills.SkillType.ManaRecovery:

                break;
            case PlayerSkills.SkillType.WideAreaAttack:

                break;
            case PlayerSkills.SkillType.VampireSword:


                break;
            case PlayerSkills.SkillType.Lightning:

                break;
        }
    }
    public IEnumerator SetIsFightingTrue()
    {
        IsFighting = true;
        yield return new WaitForSeconds(6);
        IsFighting = false;
    }
    public virtual void start()
    {
        //StartCoroutine(checkImpossibleBehaviour());
        OnHitColor.color = new Color(OnHitColor.color.r, OnHitColor.color.g, OnHitColor.color.b, 0);
        Invoke("SetFollow", .1f);
    }
    void SetFollow()
    {
        if(PV !=null)
        {
        if (PV.IsMine || PVPManager.instance == null)
        cinemachineVirtual.Follow = transform;

        }
        else
        {
        cinemachineVirtual.Follow = transform;
        }
    }
    public bool CanUseGuidedMissle()
    {
        return playerSkills.IsSkillUnLocked(PlayerSkills.SkillType.GuidedMissle);
    }

    public PlayerSkills GetPlayerSkills()
    {
        return playerSkills;
    }
    public void ShakeCam()
    {
        impulse.GenerateImpulse(0.7f);
        HpBar.transform.DOScale(new Vector2(1.5f,1.5f),.2f).OnComplete( () => {HpBar.transform.DOScale(new Vector2(1.3f, 1.3f), .2f);  });
        OnHitColor.DOColor(new Color(OnHitColor.color.r, OnHitColor.color.g, OnHitColor.color.b, .5f),.1f).OnComplete(()=> { OnHitColor.DOColor(new Color(OnHitColor.color.r, OnHitColor.color.g, OnHitColor.color.b, 0), .2f); });
 
    }
    public virtual void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //print("446");
    }
}
