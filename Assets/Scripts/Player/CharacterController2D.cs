using UnityEngine;
using Photon.Pun;
using UnityEngine.Events;

public class CharacterController2D : MonoBehaviour
{
	[SerializeField] private float m_JumpForce = 400f;							// Amount of force added when the player jumps.
	[Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;	// How much to smooth out the movement
	[SerializeField] private bool m_AirControl = false;							// Whether or not a player can steer while jumping;
	[SerializeField] private LayerMask m_WhatIsGround;							// A mask determining what is ground to the character
	[SerializeField] private Transform m_GroundCheck;							// A position marking where to check if the player is grounded.
	[SerializeField] private Transform m_CeilingCheck;							// A position marking where to check for ceilings
	[SerializeField] private Collider2D m_CrouchDisableCollider;				// A collider that will be disabled when crouching

	const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
	public bool m_Grounded;            // Whether or not the player is grounded.
	private Rigidbody2D m_Rigidbody2D;
	public bool m_FacingRight = true;  // For determining which way the player is currently facing.
	private Vector3 m_Velocity = Vector3.zero;

	[Header("Events")]
	[Space]

	public UnityEvent OnLandEvent;
	public PhotonView PV;
	[System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }
	private void Awake()
	{
		m_Rigidbody2D = GetComponent<Rigidbody2D>();

		if (OnLandEvent == null)
			OnLandEvent = new UnityEvent();
	}

	private void FixedUpdate()
	{
		bool wasGrounded = m_Grounded;
		m_Grounded = false;

		// The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
		// This can be done using layers instead but Sample Assets will not overwrite your project settings.
		Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
		for (int i = 0; i < colliders.Length; i++)
		{
			if (colliders[i].gameObject != gameObject)
			{
				m_Grounded = true;
				if (!wasGrounded)
					OnLandEvent.Invoke();
			}
		}
	}


	public void Move(float move,bool jump) // 움직이는 함수
	{
		//only control the player if grounded or airControl is turned on
		if (m_Grounded || m_AirControl) // 땅에 있거나 공중에서 움직일 수 있는지 체크
		{
			Vector3 targetVelocity = new Vector3(move * 10, m_Rigidbody2D.velocity.y); //목표 속도를 설정
			m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing); //rigidbody2d의 velocity를 targetVelocity를 이용하여 설정
			if (move > 0 && !m_FacingRight) // 움직이는 방향이 오른쪽이고 오른쪽을 보고 있지 않는지 체크
			{
				if (PhotonNetwork.IsConnected && PhotonNetwork.InRoom)

					PV.RPC("Flip", RpcTarget.All);
				else
					Flip(); //플레이어를 회전시키는 함수
			}
			else if (move < 0 && m_FacingRight) // 움직이는 방향이 왼쪽이고 오른쪽을 보고 있는지(왼쪽을 보고있지 않는지) 체크
			{
				if (PhotonNetwork.IsConnected && PhotonNetwork.InRoom)

					PV.RPC("Flip", RpcTarget.All);
				else
					Flip(); //플레이어를 회전시키는 함수
			}
		}
		if (m_Grounded && jump) //땅에 있고 점프할수 있는지 체크
		{
			m_Grounded = false; //땅에 있는지 체크하는 변수를 false로 설정
			m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce)); // rigidbody2d의 있는 addforce를 활용하여 플레이어를 점프시킴.
		}
	}

	[PunRPC]
	public void Flip() // 회전시키는 함수
	{
		m_FacingRight = !m_FacingRight; // 오른쪽을 보고있는지 확인하는 변수를 반대로 설정
		Vector3 theScale = transform.localScale; // 플레이어를 회전시킬때 필요한 변수
		theScale.x *= -1; // 변수를 x크기를 -1로 하여 방향을 보고있는 방향과 반대방향으로 설정
		transform.localScale = theScale;  // 플레이어의 크기를 변수와 같은 크기로 설정
	}
}
