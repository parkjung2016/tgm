using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerSkills
{
    public event EventHandler<OnSkillUnlockedEventArgs> OnSkillUnlocked;
    public class OnSkillUnlockedEventArgs: EventArgs
    {
        public SkillType skilltype;
    }
    public enum SkillType
    {
        NearbyEnemies,
        GuidedMissle,
        Shield,
        Lightning,
        Bite,
        WideAreaAttack,
        VampireSword,
        ManySword,
        ManaRecovery,
        Max
    }
    public List<SkillType> unlockedSkillType;
    public SkillTree skillTree;
    public PlayerSkills()
    {
        unlockedSkillType = new List<SkillType>();
    }

    private void UnLockSkill(SkillType skillType)
    {
        if(!IsSkillUnLocked(skillType))
        {
        unlockedSkillType.Add(skillType);
            if (!SharedObject.g_playerPrefsdata.playerdata.UnLockSkill.Contains(skillType))
            {
            SharedObject.g_playerPrefsdata.playerdata.UnLockSkill.Add(skillType);
            SharedObject.g_playerPrefsdata.playerdata.StatUpSkillLevel.Add((int)SharedScript.instance.csvdata.SkillInfo[(byte)skillType]["Level"]);

            }
        }
        OnSkillUnlocked?.Invoke(this, new OnSkillUnlockedEventArgs { skilltype = skillType });
    }
    public bool IsSkillUnLocked(SkillType skillType)
    {
        return unlockedSkillType.Contains(skillType);
    }
    public bool CanUnlock(SkillType skillType)
    {
            if (!IsSkillUnLocked(skillType))
            {
              
                return true;
            }
            else
            {
                return false;
            }
            
    }
    public bool TryUnlockSkill(SkillType skillType,bool Init)
    {
        if (CanUnlock(skillType))
        {
            if(Init)
            {
            UnLockSkill(skillType);
                return true;
            }
            if(SharedObject.g_DataManager.m_MyCharacter.N[(byte)m_Enum.STAT2.LEVEL] >= int.Parse(SharedScript.instance.csvdata.SkillInfo[(byte)skillType]["Level"].ToString()) )
            {

            if (SharedObject.g_DataManager.m_MyCharacter.SkillPoint >= int.Parse(SharedScript.instance.csvdata.SkillInfo[(byte)skillType]["PointConsumption"].ToString()))
            {
                SharedObject.g_DataManager.m_MyCharacter.SkillPoint -= int.Parse(SharedScript.instance.csvdata.SkillInfo[(byte)skillType]["PointConsumption"].ToString());
            UnLockSkill(skillType);
                skillTree.StartCoroutine(skillTree.ShowNotice("스킬을 잠금해제하였습니다."));
            return true;
            }
            else
            {
                SharedObject.g_SoundManager.PlayErrorAudio();
                skillTree.StartCoroutine(skillTree. ShowNotice("스킬포인트가 부족합니다."));
                return true;
            }
            }
            else
            {
                SharedObject.g_SoundManager.PlayErrorAudio();
                skillTree.StartCoroutine(skillTree.ShowNotice("레벨이 부족합니다."));
                return true;
            }

        }
        else
        {
            return false;
        }
    }
}
