using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class DashCoolDownProgress : MonoBehaviour
{
    private PlayerMove playerMove_;
    private Image img;
    private void Start()
    {
        img = GetComponent<Image>();
        playerMove_ = GameObject.FindObjectOfType<PlayerMove>();
    }
    void Update()
    {
        img.fillAmount = playerMove_.DashCurrentCooldown / playerMove_.DashCooldown;
        if(img.fillAmount <= 0)
        {
            Destroy(transform.parent.gameObject);
        }
    }
}
