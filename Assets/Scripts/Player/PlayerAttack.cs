using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Photon.Pun;
using Photon.Realtime;

public class PlayerAttack : Player,IAttack,IWeaponCol
{
    private void Awake()
    {
        awake();
    }
    private void Start()
    {
       
        start();
        if (PhotonNetwork.InRoom )
        {
        if( GameObject.Find("PlayerPos 2") != null && Vector2.Distance(transform.position, GameObject.Find("PlayerPos 2").transform.position) < .1f)
            Controller.Flip();
        }
        AttackTrue = true;
        WeaponColHide();
        CheckWeaponColliderDestroy();
            CanvasManager.instance.PlayerAttack += Attack;
    }
    public void CheckWeaponColliderDestroy()
    {
        if ( PV != null && !PV.IsMine)
        {
            Destroy(WeaponCol.GetComponent<WeaponCollider>());

        }
    }
    void Attack(object sender, EventArgs e)
    {
        if (PV != null && PV.IsMine)
        {
            if(PhotonNetwork.InRoom)
            {

        PV.RPC("PunAttack", RpcTarget.All);
            }
            else
            PunAttack();

        }
        else if( PV ==null)
        {
            PunAttack();

        }

    }
    [PunRPC]
    public void PunAttack() // 공격하는 함수
    {

        if (!AttackTrue || anim.GetCurrentAnimatorStateInfo(0).IsName("PlayerRoll")) // 공격을 할수 없거나 플레이어가 구르는 중인지 체크
            return;
       Attacking = true; // 공격중인지 확이하는 변수를 true로 설정
        rig2d.velocity = Vector2.zero; // rigidbody의 velocity를 0으로 설정
        if ( ComboStep == 0) // 콤보 단계가 0인지 체크
        {
            anim.Play("AttackA"); // AttackA라는 애니메이션 실행
             ComboStep = 1; // 콤보 단계를 0으로 설정
            return;
        }
        else
        {
            if ( ComboPossible) // 콤보 공격이 가능한지 체크
            {
                 ComboPossible = false; // 콤보 공격이 가능한지 확인하는 변수를 false로 설정
                 ComboStep++; //콤보 단계를 1단계 올림
            }
        }
        playerMove.RollingFalse(); // 플레이어가 구르는것을 취소시키는 함수
    }
    public void ComboPossible_()
    {
        if (PV != null && PV.IsMine)
        {
            if (PhotonNetwork.InRoom)
            {

                PV.RPC("PunComboPossible_", RpcTarget.All);
            }
            else
                PunComboPossible_();
        }
        else
                PunComboPossible_();
    }
    [PunRPC]
    public void PunComboPossible_() // 콤보가 가능하게 하는 함수
    {

        ComboPossible = true; // 콤보 공격이 가능한지 확인하는 변수를 true로 설정
    }
    public void Combo()
    {
        if ( PV != null && PV.IsMine)
        {
            if (PhotonNetwork.InRoom)
            {

                PV.RPC("PunCombo", RpcTarget.All);
            }
            else
                PunCombo();
        }
        else if(PV == null)
                PunCombo();
    }
    [PunRPC]
    public void PunCombo() // 콤보 공격
    {
        switch (ComboStep) // 콤보 단계마다 실행하는 애니메이션이 달라짐
        {
            case 2:
                anim.Play("AttackB"); // 콤보 단계가 2일때 AttackB라는 애니메이션 실행
                break;
            case 3:
                anim.Play("AttackC"); // 콤보 단계가 3일때 AttackC라는 애니메이션 실행
                break;
        }
    }
    public void WeaponColVisible()
    {
        if ( PV != null && PV.IsMine)
        {
            if (PhotonNetwork.InRoom)
            {

                PV.RPC("WeaponColActive", RpcTarget.All,true);
            }
            else
                WeaponColActive(true);


        }
            else if(PV== null)
                WeaponColActive(true);
    }
    [PunRPC]
    public void WeaponColActive(bool Active)
    {
        WeaponCol.SetActive(Active);
    }
    public void WeaponColHide()
    {
        if (PV != null && PV.IsMine)
        {
            if (PhotonNetwork.InRoom)
            {

                PV.RPC("WeaponColActive", RpcTarget.All, false);
            }
            else
                WeaponColActive(false);
        }
            else if(PV==null)
                WeaponColActive(false);

    }
    public void ComboReset()
    {
        if ( PV != null && PV.IsMine)
        {
            if (PhotonNetwork.InRoom)
            {

                PV.RPC("PunComboReset", RpcTarget.All);
            }
                else

                PunComboReset();
        }
            else if(PV == null)
                PunComboReset();
    }
    [PunRPC]
    public void PunComboReset()
    {
        Attacking = false;
        ComboPossible = false;
        ComboStep = 0;
    }
}
