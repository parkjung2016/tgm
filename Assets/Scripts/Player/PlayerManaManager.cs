using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManaManager : Player
{
     public static PlayerManaManager instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    private void Start()
    {
        StartCoroutine(regenerationMana());
    }
    public void PunRPC_AddMana(int add)
    {
        AddMana(add);

    }
    void AddMana(int Add)
    {
        Mana = Add;
        ItemUI.instance.SummonManaUpeffect(transform);
    }
    public IEnumerator regenerationMana()
    {
        WaitForSeconds ws = new WaitForSeconds(.8f);
        if(SharedObject.g_DataManager.m_MyCharacter == null)
        {
            yield return ws;
            StartCoroutine(regenerationMana());
        }
        else
        {
        yield return new WaitForSeconds(4);
        while (SharedObject.g_DataManager.m_MyCharacter.Mana < SharedObject.g_DataManager.m_MyCharacter.Max[(byte)m_Enum.MAXSTAT.MAXMANA])
        {
                for(int i=0; i < SharedScript.instance.csvdata.PlayerSTAT.Count;i++)
                {
                    if(SharedObject.g_playerPrefsdata.playerdata.m_nStat2[(byte)m_Enum.STAT2.LEVEL] >= int.Parse(SharedScript.instance.csvdata.PlayerSTAT[i]["Level"].ToString()) )
                    {
            SharedObject.g_DataManager.m_MyCharacter.Mana += int.Parse(SharedScript.instance.csvdata.PlayerSTAT[i]["AddManaRegeneration"].ToString());

                    }

                }
                UIProgressBar.instance.SetManaProgress();
            yield return ws;
        }

        }

    }
}
