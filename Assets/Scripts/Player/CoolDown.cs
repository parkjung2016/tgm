using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CoolDown : MonoBehaviour
{
    [SerializeField]
    private Image DashCollDownProgressBar;
    [SerializeField]
    private GameObject Grid;
    public void DashCollDown()
    {
        AddGrid(DashCollDownProgressBar.gameObject);
    }
    public GameObject AddGrid(GameObject Obj)
    {
        GameObject Img =Instantiate(Obj, Grid.transform);
        return Img;

    }
}
