using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab.ClientModels;
using PlayFab;


public class PlayerXPManager : Player
{
    public static PlayerXPManager instance;
    Coroutine runningCoroutine = null;
    SkillTree skilltree;
    private void Awake()
    {
        awake();
        if (instance == null)
            instance = this;
        skilltree = FindObjectOfType<SkillTree>();
    }
    private void Start()
    {
        start();
    }
    private void Update()
    {
        if (PV != null &&!PV.IsMine)
            return;
        if (character.F[(byte)m_Enum.STAT.XP] >= character.F[(byte)m_Enum.STAT.LEVELUPXP])
        {
            SharedObject.g_SoundManager.CallPlayLevelUpSound();
            for(int i =0; i < SharedScript.instance.csvdata.PlayerSTAT.Count;i++)
            {

            if(character.N[(byte)m_Enum.STAT2.LEVEL] <int.Parse( SharedScript.instance.csvdata.PlayerSTAT[i]["AddXP"].ToString()))
                {

            character.F[(byte)m_Enum.STAT.LEVELUPXP] +=float.Parse(SharedScript.instance.csvdata.PlayerSTAT[i]["AddXP"].ToString());
                    SharedObject.g_playerPrefsdata.playerdataNoSave.EarnedGold += int.Parse(SharedScript.instance.csvdata.PlayerSTAT[i]["AddMoney"].ToString());
                    SharedObject.g_playerPrefsdata.playerdataNoSave.Gold += int.Parse(SharedScript.instance.csvdata.PlayerSTAT[i]["AddMoney"].ToString());
                    break;
                }
            }
            character.F[(byte)m_Enum.STAT.XP] = 0;
            character.SkillPoint += 2;
            character.StatPoint += 2;
            character.N[(byte)m_Enum.STAT2.LEVEL]++;
            PlayFabManager.instance.AddLevelToLeaderBoard(character.N[(byte)m_Enum.STAT2.LEVEL]);
                    QuestManager.instance.CheckQuestComplete();
            if (runningCoroutine != null)
            {
                StopCoroutine(runningCoroutine);
            }
            runningCoroutine= CanvasManager.instance.StartCoroutine(CanvasManager.instance.VisibleLevelUp());
            UIProgressBar.instance.SetXPProgress();
            UIProgressBar.instance.SetLevelText();
            CanvasManager.SetGoldText();
            for(int i =0; i <SharedObject.g_playerPrefsdata.playerdata.UnLockSkill.Count;i++)
            {
                if (character.N[(byte)m_Enum.STAT2.LEVEL] >= SharedObject.g_playerPrefsdata.playerdata.StatUpSkillLevel[i] + 10)
                {
                    SharedObject.g_playerPrefsdata.playerdata.StatUpSkillLevel[i] = character.N[(byte)m_Enum.STAT2.LEVEL];
                    //��ų
                    int skillvalue = int.Parse( skilltree.SkillBGs[(byte)SharedObject.g_playerPrefsdata.playerdata.UnLockSkill[i]].GetComponent<Skill>().skillInfo.SkillValue[0]);
                    int skillvalue2 = int.Parse( skilltree.SkillBGs[(byte)SharedObject.g_playerPrefsdata.playerdata.UnLockSkill[i]].GetComponent<Skill>().skillInfo.SkillValue[1]);
                    skilltree.SkillBGs[(byte)SharedObject.g_playerPrefsdata.playerdata.UnLockSkill[i]].GetComponent<Skill>().skillInfo.SkillValue[0] = (skillvalue+int.Parse( SharedScript.instance.csvdata.SkillInfo[i]["StatUpgrade"].ToString())).ToString();
                    skilltree.SkillBGs[(byte)SharedObject.g_playerPrefsdata.playerdata.UnLockSkill[i]].GetComponent<Skill>().skillInfo.SkillValue[1] = (skillvalue2 + int.Parse(SharedScript.instance.csvdata.SkillInfo[i]["StatUpgrade2"].ToString())).ToString();

                }

            }
            //SharedObject.g_playerPrefsdata.SaveFile(SharedObject.SavePath);
        }
    }
}
