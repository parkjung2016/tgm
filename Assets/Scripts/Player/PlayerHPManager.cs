using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Photon.Pun;
using Photon;
using ExitGames.Client.Photon;

public class PlayerHPManager : Player
{
    public static PlayerHPManager instance;
    public PlayerHPProgressBar _PlayerHPProgressBar;
    private void Awake()
    {
        awake();
        if (instance == null)
            instance = this;

    }
    private void Start()
    {
        start();
        SetPlayerHPProgressBar();
    }
  void  SetPlayerHPProgressBar()
    {
        if (!PhotonNetwork.InRoom)
            return;
        if (GetComponentInChildren<PlayerHPProgressBar>() == null)
            Invoke("SetPlayerHPProgressBar", .2f);
        else
            _PlayerHPProgressBar = GetComponentInChildren<PlayerHPProgressBar>();
    }
    private void Update()
    {
        if ( PV != null &&!PV.IsMine)
            return;
        if (character.F[(byte)m_Enum.STAT.HP] + character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALHP] <= 0)
        {
            character.F[(byte)m_Enum.STAT.HP] = 0;
            if(BossManager.instance != null)
            {
                if(!character.Death && !BossManager.instance.bossTimeLine.playableGraph.IsValid())
                {
                    DeathFunction();
                }
            }
            else
            {
                if (!character.Death)
                {
                    if (PhotonNetwork.InRoom)

                        PV.RPC("Pun_DeathFunction", RpcTarget.All) ;

                    else
                        DeathFunction();

                }
            }
            if(PVPManager.instance != null &&!PVPManager.instance.Win)
            {
                PVPManager.instance.WinPlayer((int)PhotonNetwork.LocalPlayer.CustomProperties["Team"]);
            }
        }
    }
    [PunRPC]
    public void Pun_DeathFunction()
    {
        DeathFunction();
    }
    [PunRPC]
    public void Pun_ApplyDamage(int Damage)
    {
        StartCoroutine(ApplyDamage(Damage));
    }
    private void DeathFunction()
    {
        character.rig2d.velocity = Vector2.zero;
        playerAnim._Death();
        character.Death = true;
        if (PVPManager.instance == null)
            StartCoroutine(DeathUIVisible());
    }
    private IEnumerator DeathUIVisible()
    {
        yield return new WaitForSeconds(1f);
        CanvasManager.instance.FadeDeathUI();
    }
    public IEnumerator ApplyDamage(int Damage)
    {
        if (PV == null || PV != null&& PV.IsMine)
        {
            bool shake = true;
            if (character.Death)
                yield return null;
            if (runningcoroutine != null)
            {
                StopCoroutine(runningcoroutine);
            }
           runningcoroutine = StartCoroutine(SetIsFightingTrue());
            PlayerManaManager.instance.StopCoroutine(PlayerManaManager.instance.regenerationMana());
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("PlayerRoll") || PlayerSkillManager.instance.IsShield)
            {
                if(PlayerSkillManager.instance.IsShield) SharedObject.g_SoundManager.CallPlaySound(PlayerSkillManager.instance.SkillSFXs[(byte)PlayerSkills.SkillType.Shield],1,1,transform);
                Damage = 0;
                shake = false;
            }
            if (shake)
                ShakeCam();
            if (character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALHP] > 0)
            {

                character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALHP] = Mathf.Clamp(character.Additional[(byte)m_Enum.ADDITIONALSTAT.ADDITIONALHP] - (Damage * (1 - (character.F[(byte)m_Enum.STAT.DEF] / (100 + character.F[(byte)m_Enum.STAT.DEF])))), 0, character.Max[(byte)m_Enum.MAXSTAT.MAXADDITIONALHP]);
            }
            else if (character.F[(byte)m_Enum.STAT.HP] > 0)
            {
                character.F[(byte)m_Enum.STAT.HP] = Mathf.Clamp(character.F[(byte)m_Enum.STAT.HP] - (Damage * (1 - (character.F[(byte)m_Enum.STAT.DEF] / (100 + character.F[(byte)m_Enum.STAT.DEF])))), 0, character.Max[(byte)m_Enum.MAXSTAT.MAXHP]);

            }
            if (!anim.GetCurrentAnimatorStateInfo(0).IsTag("Attack") && !anim.GetCurrentAnimatorStateInfo(0).IsName("PlayerRoll")  && !anim.GetCurrentAnimatorStateInfo(0).IsName("PlayerDash") && !PlayerSkillManager.instance.IsShield)
            {
                playerAnim.Hit();
                if(playerMove.playerpet != null)
                {
                    playerMove.playerpet.Hit();
                    playerMove.playerpet.Idle();

                }
                AttackTrue = false;
                yield return new WaitForSeconds(0.5f);
                playerMove.Rolling = false;
                playerAttack.ComboReset();
                AttackTrue = true;
            }
            UIProgressBar.instance.SetHPProgress();
            if(_PlayerHPProgressBar != null)
            _PlayerHPProgressBar.SetHP();
            PlayerManaManager.instance.StartCoroutine(PlayerManaManager.instance.regenerationMana());
        }
    }
    public void PunRPC_AddAdditionalHP(float add)
    {
        AddAdditionalHP(add); 

    }
    void AddAdditionalHP(float Add)
    {
        Additional[(int)m_Enum.ADDITIONALSTAT.ADDITIONALHP] = Add;
        ItemUI.instance.SummonHealthUpeffect(transform);
    }
    public void PunRPC_AddHP(float add)
    {
        AddHP(add);

    }
    void AddHP(float Add)
    {
       F[(int)m_Enum.STAT.HP] = Add;
        ItemUI.instance.SummonHealthUpeffect(transform);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("ReSpawn"))
        {
            if (PlayerSkillManager.instance.IsShield)
                PlayerSkillManager.instance.IsShield = false;
            StartCoroutine(ApplyDamage(9999999));
        }
    }
    //public override void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    //{
    //    //통신을 보내는 
    //    if (stream.IsWriting)
    //    {
    //        if(_PlayerHPProgressBar != null)
    //        {
    //        stream.SendNext(_PlayerHPProgressBar.Progress.fillAmount);
    //        stream.SendNext(_PlayerHPProgressBar.
    //            transform.localScale);

    //        }
    //    }

    //    //클론이 통신을 받는 
    //    else
    //    {
    //        if (_PlayerHPProgressBar != null)
    //        {
    //            print(stream.ReceiveNext());
    //            _PlayerHPProgressBar.Progress.DOFillAmount((float)stream.ReceiveNext(), 1);
    //            _PlayerHPProgressBar.transform.localScale = (Vector3)stream.ReceiveNext();
    //        }
    //    }
    //}

}
