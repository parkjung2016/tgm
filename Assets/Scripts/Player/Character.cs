using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBase : Actor
{
}
public partial class Character : CharacterBase
{
    [HideInInspector]
    public bool Death;
    public bool Attacking;
    public byte ComboStep;
    public bool ComboPossible;
    public Animator anim;
    public Rigidbody2D rig2d;
    public bool AttackTrue;
    public bool MoveTrue;
    public GameObject WeaponCol;
    public float Scale;
    public SpriteRenderer spriteRenderer_;
    public float[] Additional
    {
        set { SharedObject.g_playerPrefsdata.playerdataNoSave.m_fAdditionalStat = value; }
        get { return SharedObject.g_playerPrefsdata.playerdataNoSave.m_fAdditionalStat; }
    }
    public int[] N
    {
        set { SharedObject.g_playerPrefsdata.playerdata.m_nStat2 = value; }
        get { return SharedObject.g_playerPrefsdata.playerdata.m_nStat2; }

    }
    public int Mana
    {
        set { SharedObject.g_playerPrefsdata.playerdataNoSave.Mana = value; }
        get { return SharedObject.g_playerPrefsdata.playerdataNoSave.Mana; }

    }
    public float[]F
    {
        set { SharedObject.g_playerPrefsdata.playerdata.m_fStat = value; SharedObject.g_playerPrefsdata.playerdata.m_fStat[(byte)m_Enum.STAT.HP] = SharedObject.g_playerPrefsdata.playerdata.m_fMaxStat[(byte)m_Enum.MAXSTAT.MAXHP];
        }
        get { return SharedObject.g_playerPrefsdata.playerdata.m_fStat; }
    }
    public float SkillPoint
    {
        set { SharedObject.g_playerPrefsdata.playerdata.SkillPoint = value; }
        get { return SharedObject.g_playerPrefsdata.playerdata.SkillPoint; }
    }
    public float StatPoint
    {
        set { SharedObject.g_playerPrefsdata.playerdata.StatPoint = value; }
        get { return SharedObject.g_playerPrefsdata.playerdata.StatPoint; }
    }
    public float[] Max
    {
        set { SharedObject.g_playerPrefsdata.playerdata.m_fMaxStat = Max;}
        get { return SharedObject.g_playerPrefsdata.playerdata.m_fMaxStat; }
    }
    public string Id
    {
        set { SharedObject.g_playerPrefsdata.playerdata.m_strPlayerName = value; }
        get { return SharedObject.g_playerPrefsdata.playerdata.m_strPlayerName; }
    }
    public float[] AddStatValues
    {
        set { SharedObject.g_playerPrefsdata.playerdata.AddStatValues = value; }
        get { return SharedObject.g_playerPrefsdata.playerdata.AddStatValues; }
    }
}
