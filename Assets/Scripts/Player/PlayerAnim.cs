using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Photon.Pun;
public class PlayerAnim : Player
{
    private float AttackSpeed;
    [SerializeField]
    private Material Teleportmat;
    private Material OriginMat;

    private void Awake()
    {
        awake();
    }
    private void Start()
    {
        start();
        OriginMat = spriteRenderer_.material;
    }
    void Update()
    {
        if (PV != null && !PV.IsMine)
            return;


        AttackSpeed = (character.Additional[(int)m_Enum.ADDITIONALSTAT.ADDITIONALATTACKSPEED] == 0 ? character.F[(int)m_Enum.STAT.ATTACKSPEED]: character.Additional[(int)m_Enum.ADDITIONALSTAT.ADDITIONALATTACKSPEED])+ character.AddStatValues[(int)m_Enum.ADDITIONALSTAT.ADDITIONALATTACKSPEED];
        character.anim.SetBool("IsGround", Controller.m_Grounded);
        character. anim.SetBool("Fall", character.rig2d.velocity.y <0);
        character. anim.SetBool("Move",!playerMove.MoveValue.Equals(0));
        character. anim.SetFloat("AttackSpeed", AttackSpeed);
    }
    [PunRPC]
    public void Pun_PetMove()
    {
        if (playerMove.playerpet != null)
      playerMove.playerpet.anim.SetBool("Run", true);
 
    }
    [PunRPC]
    public void Pun_PetIdle( )
    {
        if (playerMove.playerpet != null)
        playerMove.playerpet.anim.SetBool("Run", false);
    }
    [PunRPC]
    public void Pun_PetDeath()
    {
        if (playerMove.playerpet != null)
        playerMove.playerpet.anim.SetTrigger("Death");
    }
    [PunRPC]
    public void Pun_PetHit()
    {
        if (playerMove.playerpet != null)
        playerMove.playerpet.anim.SetTrigger("Hit");
    }
    [PunRPC]
    public void Pun_PetAttack( )
    {
        if (playerMove.playerpet != null)
        {
        playerMove.playerpet.AttackAudio.Play();
        playerMove.playerpet.anim.SetTrigger("Attack");

        }
    }
    [PunRPC]
    public void TeleportMat(int num)
    {
        PvPPortal pvPPortal = PVPManager.instance.TeleportObjects[num].GetComponent<PvPPortal>();
        spriteRenderer_.material = Teleportmat;
       pvPPortal.CheckPlayerTeleportT = spriteRenderer_.material.DOFloat(0, "_Progress", 1f).OnComplete(() =>
          {
               transform.position = pvPPortal.TeleportTransform.position;
               spriteRenderer_.material = OriginMat;
               spriteRenderer_.material.SetFloat("_Progress", 2);
           });
    }
    [PunRPC]
    public void TeleportMatOrigin()
    {
        DOTween.Kill(spriteRenderer_.material);
        spriteRenderer_.material = OriginMat;
        spriteRenderer_.material.SetFloat("_Progress", 2);
    }
    public void Roll()
    {
        if (PhotonNetwork.IsConnected && PhotonNetwork.InRoom)
            PV.RPC("Roll_PunRPC", RpcTarget.All);
        else
            Roll_PunRPC();
    }
    public void Hit()
    {
        if (PhotonNetwork.IsConnected && PhotonNetwork.InRoom)
            PV.RPC("Hit_PunRPC", RpcTarget.All);
        else
            Hit_PunRPC();
    }
    [PunRPC]
    void Hit_PunRPC()
    {
        if (PV == null)
            character.anim.SetTrigger("Hit");
        else if (PV.IsMine)
            character.anim.SetTrigger("Hit");
        return;

    }
    [PunRPC]
    void Roll_PunRPC()
    {
        if (PV == null)
        character. anim.SetTrigger("Roll");
        else if (PV.IsMine)
        character. anim.SetTrigger("Roll");
                    return;

    }
    public void Dash()
    {
        if (PhotonNetwork.IsConnected && PhotonNetwork.InRoom)
            PV.RPC("Dash__PunRPC", RpcTarget.All);
        else
            Dash__PunRPC();
    }
    [PunRPC]
    void Dash__PunRPC()
    {
        if (PV == null)
        character. anim.SetTrigger("Dash");
        else if (PV.IsMine)
        character. anim.SetTrigger("Dash");
            return;

    }
    public void _Death()
    {
        if (PhotonNetwork.IsConnected && PhotonNetwork.InRoom)
            PV.RPC("Death_PunRPC", RpcTarget.All);
        else
            Death_PunRPC();
    }
    [PunRPC]
    public void Death_PunRPC()
    {
        if (PV == null)
        character.anim.Play("PlayerDeath");
        else if (PV.IsMine)
        character.anim.Play("PlayerDeath");
            return;
        
    }
}
