using System.Collections;
using System;
using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : Player
{
    private bool Jump;
    [HideInInspector]
    public  float MoveValue;
    public bool DashTimerTrue;
     public float DashCurrentTimer;
    public float DashTimer;
    public Vector3 curPos;
    public bool RollTimerTrue;
    public float RollCurrentTimer;
    [SerializeField]
    private float RollTimer;
    [SerializeField]
    private float DashPower;
    [SerializeField]
    private float RollPower;
    [SerializeField]
    private GameObject DashEffect;
    [SerializeField]
    private ParticleSystem DustEffect;
    [SerializeField]
    private Transform DashEffectTrans;
    public bool Rolling;
    public bool isDashing;
    [HideInInspector]
    public bool dashCollDownTiming;
    [HideInInspector]
    public float DashCurrentCooldown;
    public float DashTrueTimer;
    public float DashCooldown;
    private CoolDown coolDown_;
  private  bool JumpTrue;
    public Transform tr;
    public PlayerPet playerpet;
    private void Awake()
    {
        tr = GetComponent<Transform>();
        awake();
        coolDown_ = FindObjectOfType<CoolDown>();
        StartCoroutine(ResetAnimValue());
    }
   
    IEnumerator ResetAnimValue()
    {
        if(!anim.GetCurrentAnimatorStateInfo(0).IsTag("Behaviour") )
        {
            if (isDashing)
                isDashing = false;
            if (Rolling)
                Rolling = false;
        }
        yield return new WaitForSeconds(.5f);
        StartCoroutine(ResetAnimValue());
    }
    void Start()
    {
        start();
        JumpTrue = true;
        Controller.PV = PV;
        MoveTrue = true;
        StartCoroutine(FirstSetPosition());
        CanvasManager.instance.PlayerMoveLDown += MoveLDown;
        CanvasManager.instance.PlayerMoveLUp += MoveLUP;
        CanvasManager.instance.PlayerMoveRDown += MoveRDown;
        CanvasManager.instance.PlayerMoveRUp += MoveRUp;
        CanvasManager.instance.PlayerJump += Jump_;
        PV.ViewID
               =MyScene.instance.PVPRoom  && PhotonNetwork.IsMasterClient?  PhotonNetwork.AllocateViewID(1) : PhotonNetwork.AllocateViewID(0);
    }
 
    IEnumerator FirstSetPosition()
    {
        yield return new WaitForSeconds(0.2f);
        if(!PhotonNetwork.InRoom)
        {
        transform.position = MyScene.instance.SpawnPlayerPos[MyScene.instance.SpawnPlayerNum].position;
            if(SharedObject.firstconnect)
            {
                transform.position = new Vector3(transform.position.x,50);
                SharedObject.firstconnect = false;
            }

        }

    }
    private void Update()
    {
        if (DashTimerTrue)
        {
            DashTrueTimer -= Time.deltaTime;
            if (DashTrueTimer <= 0)
            {
                DashTimerTrue = false;
            }
        }
        if (dashCollDownTiming)
        {
            DashCurrentCooldown -= Time.deltaTime;
            if (DashCurrentCooldown <= 0)
            {
                dashCollDownTiming = false;

            }
        }
        if (RollTimerTrue)
        {
            RollCurrentTimer += Time.deltaTime;
            if (RollCurrentTimer >= RollTimer)
            {
                RollCurrentTimer = 0;
                RollTimerTrue = false;
            }
        }
        if (PV != null && !PV.IsMine)
        {

            //끊어진 시간이 너무 길 경우(텔레포트)
            if ((tr.position - curPos).sqrMagnitude >= 10.0f * 10.0f)
            {
                tr.position = curPos;
            }
            //끊어진 시간이 짧을 경우(자연스럽게 연결 - 데드레커닝)
            else
            {
                tr.position = Vector3.Lerp(tr.position, curPos, Time.deltaTime * 10.0f);
            }

        }

    }
    private void FixedUpdate()
    {
        if (PV != null && PV.IsMine || PV == null)
        {
            if (Death) // 플레이어가 죽었는지 확인
                return;
            if (!Rolling && !playerAttack.Attacking && MoveTrue) // 플레이어가 움직일 수 있고 다른 행동을 하고 있는지 체크
            {
                Controller.Move(MoveValue * (F[(int)m_Enum.STAT.SPEED] + Additional[(int)m_Enum.ADDITIONALSTAT.ADDITIONALSPEED]) * Time.deltaTime, Jump); // 움직이는 함수
            }
            Jump = false; // 점프를 할수 없게 점프 변수를 false로 설정
            if (isDashing) //대쉬 중인지 체크
            {
                rig2d.velocity = transform.right * transform.localScale.x * DashPower; // rigidbody2d의 velocity를 플레이어의 방향과 DashPower를 이용하여 설정
                DashCurrentTimer -= Time.deltaTime; // 대쉬를 할 수 있는 시간을 설정
                if (DashCurrentTimer <= 0) // 대쉬를 할수 있는 현재시간이 0보다 작거나 같은지 체크
                {
                    isDashing = false; // 대쉬중인지를 확인하는 변수를 false로 설정
                    if(playerpet != null)
                    {
                        if(MoveValue == 0)
                        {
                            playerpet.Idle();
                        }
                    }
                }
            }
        }

    }
    public void MoveLDown(object sender,EventArgs e) // 왼쪽 방향키를 눌렀을 때
    {
        if (Death)  // 플레이어가 죽었는지 확인
            return;
        MoveValue = -1; // 움직이는 방향을 -1로 설정하여 왼쪽으로 움직임
        DashCheck(); //같은 방향키를 2번 연속으로 누르면 대쉬를 할수 있게 대쉬 체크
        if (playerpet != null)
        {
            playerpet.Move();
        }
    }
    public void MoveLUP(object sender, EventArgs e) // 왼쪽 방향키를 땟을 때
    {
        MoveValue = 0; // 움직이는 방향을 0으로 설정하여 움직임을 멈춤
        if (playerpet != null)
        {
            if(!isDashing)
            {
                playerpet.Idle();
            }
        }
    }
    public void MoveRUp(object sender, EventArgs e)
    {
        MoveValue = 0; // 움직이는 방향을 0으로 설정하여 움직임을 멈춤
        if (playerpet != null)
        {
            if (!isDashing)
            {
                playerpet.Idle();
            }
        }
    }
    public void MoveRDown(object sender, EventArgs e) // 오른쪽 방향키를 눌렀을 때
    {
        if (Death) // 플레이어가 죽었는지 확인
            return; 

        MoveValue = 1; // 움직이는 방향을 1로 설정하여 오른쪽으로 움직임
        DashCheck(); //같은 방향키를 2번 연속으로 누르면 대쉬를 할수 있게 대쉬 체크
        if (playerpet != null)
        {
            playerpet.Move();
        }

    }
    private void DashCheck()
    {
        if (PV.IsMine|| PV == null)
        {


            if (!MoveTrue)
                return;
            if (!Controller.m_Grounded)
            {
                if (dashCollDownTiming)
                    return;
                if (!DashTimerTrue)
                {
                    DashTrueTimer = 0.5f;
                    DashTimerTrue = true;
                    return;
                }
                if (DashTimerTrue)
                {

                    if (PhotonNetwork.IsConnected && PhotonNetwork.InRoom)
                        PV.RPC("DashEffectPlay", RpcTarget.All);
                    else
                        DashEffectPlay();
                    playerAnim.Dash();
                    DashCurrentTimer = DashTimer;
                    coolDown_.DashCollDown();
                    dashCollDownTiming = true;
                    DashCurrentCooldown = DashCooldown;
                    isDashing = true;
                    DashTimerTrue = false;
                }
            }
            else
            {
                if (Rolling || playerAttack.Attacking)
                    return;
                if (!RollTimerTrue)
                {

                    RollTimerTrue = true;
                    return;
                }
                if (RollTimerTrue)
                {
                    rig2d.AddForce(new Vector2(transform.localScale.x * RollPower, 0));
                    playerAnim.Roll();
                    if (PhotonNetwork.IsConnected && PhotonNetwork.InRoom)
                        PV.RPC("DustEffectPlay", RpcTarget.All);
                    else
                    DustEffectPlay();
                    Rolling = true;
                    RollCurrentTimer = 0;
                    RollTimerTrue = false;
                }
            }
        }
    }
    [PunRPC]
    void DustEffectPlay()
    {
        DustEffect.Play();
    }
    [PunRPC]
    void DashEffectPlay()
    {
        GameObject obj = Instantiate(DashEffect, DashEffectTrans.position, Quaternion.identity);
        Destroy(obj, 2);
    }
    public void RollingFalse()
    {
        Rolling = false;
    }

  
    //public override void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    //{
    //    //통신을 보내는 
    //    if (stream.IsWriting)
    //    {
    //        print("쓰다");
    //        stream.SendNext(tr.position);
    //    }

    //    //클론이 통신을 받는 
    //    else
    //    {
    //        print("받다");
    //        curPos = (Vector3)stream.ReceiveNext();
    //    }
    //}
    public void Jump_(object sender, EventArgs e)
    {
        if (PV.IsMine || PV == null)
        {

        if (JumpTrue)
        {
            if(anim.GetBool("IsGround"))
            {
        Jump = true;
                    if (PhotonNetwork.IsConnected && PhotonNetwork.InRoom)

                        PV.RPC("DustEffectPlay", RpcTarget.All);
                    else
                        DustEffectPlay();

                }
        }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("BossTimeLine"))
        {
            BossManager.PlayBossTimeLine();
            collision.gameObject.SetActive(false);
        }
        if (collision.CompareTag("Don'tJump"))
        {
            JumpTrue = false;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Don'tJump"))
        {
            JumpTrue = true;
        }
    }
}
